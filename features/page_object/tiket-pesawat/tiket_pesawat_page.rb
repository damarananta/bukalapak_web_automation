# @confirmation_page
class FlightBookPage < SitePrism::Page
  path = "/tiket-pesawat"
  set_url(path)

  element  :login_button, ".c-btn.c-btn--block.c-btn--naked.c-btn--small.js-dropdown-toggle"

  element  :radio_oneway,    :xpath, "//*[@id='reskinned_page']/div[2]/div[2]/div/div/div/div[1]/label[2]/input"
  element  :radio_return,    ".c-inp-wrap.u-pad-top--0.qa-flight-2-trip"
  
  element  :field_departure,         ".c-inp.c-inp--small.js-airport-input.qa-fr-input-airport"
  elements :departure_list,          ".c-list-ui__item"

  element  :switch_place,    :xpath, "#phone_number"
  
  element  :field_arrived,           ".c-inp.c-inp--small.js-airport-input.qa-to-input-airport"
  elements :arrive_list,             ".c-list-ui__item"

  element  :departure_date,          ".c-inp.c-inp--small.qa-dep-input-date"
  element  :return_date,             ".c-inp.c-inp--small.qa-ret-input-date"

  element :date_now, ".c-calendar__day.c-calendar__day--highlighted-range.is-start.is-end.qa-dep-choose-date"

  elements :weekday, ".c-calendar__day.qa-choose-date.qa-dep-choose-date"

  element  :passenger,             ".c-inp.c-inp--small.js-passenger-input.qa-input-passenger"
  element  :plus_dewasa,           ".c-btn.c-btn--small.qa-adult-add"
  element  :plus_anak,             ".c-btn.c-btn--small.qa-child-add"
  element  :plus_bayi,             ".c-btn.c-btn--small.qa-infant-add"

  element  :search_button,  ".c-btn.c-btn--small.c-btn--green.c-btn--block.qa-flight-search-btn"
  
  def departure(dep)
    self.field_departure.set dep
    sleep 5
    self.departure_list.first.click
  end

  def arrived(arr)
    self.field_arrived.set arr
    self.arrive_list.first.click
  end

  def dep_date
    self.departure_date.click
    #self.departure_date.set "24-02-2017"
    #puts DateTime.now 
  end

  def back_date
    #self.departure_date.set "28-02-2017"
    self.return_date.click
    sleep 10
    #page.all(".c-calendar__day.qa-choose-date.qa-dep-choose-date").each do |dd|
    #  puts dd.text
    #end

    #within('.c-calendar.u-float-left') do 
    #  find("span",:text => "3").click
    #end


  end

  def add_passenger(dewasa, anak, bayi)
    self.passenger.click
    sleep 3

    i =1
    while i <= dewasa
      dewasa = dewasa - 1
      self.plus_dewasa.click
      i +=1 
    end

    i =1
    while i <= anak
      self.plus_anak.click
      i +=1 
    end

    i =1
    while i <= bayi
      self.plus_bayi.click
      i +=1 
    end
  end

end
