class SearchResultFlightPage < SitePrism::Page
  
  element  :passenger,             ".c-inp.c-inp--small.js-passenger-input.qa-input-passenger"
  element  :plus_dewasa,           ".c-btn.c-btn--small.qa-adult-add"
  element  :plus_anak,             ".c-btn.c-btn--small.qa-child-add"
  element  :plus_bayi,             ".c-btn.c-btn--small.qa-infant-add"

  #ganti pencarian
  element :research_button, ".c-btn.c-btn--small.qa-toggle-search"

  #progress
  element :progress,:xpath, "//*[@id='reskinned_page']/div[3]/div[3]/div/div/div[2]/div[2]/div/div/div[2]/div[2]"

  #flight list 
  elements :list_flight,           ".c-panel__body.u-pad-top--3.u-pad-bottom--3"
  elements :list_maskapai, :xpath, "//*[@id='reskinned_page']/div[2]/div[4]/div/div/div[2]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/p[2]"
  elements :list_departure_price,  ".u-txt--medium.u-align-right.u-mrgn-bottom--1"
  elements :select_pergi_button,   ".c-btn.c-btn--block.c-btn--green.qa-dep-flight-choose"
  
  #return list
  elements :list_return_price,      ".u-txt--medium.u-align-right.u-mrgn-bottom--1"
  elements :select_pulang_button,  ".c-btn.c-btn--block.c-btn--green.qa-ret-flight-choose"
  #card
  

  # return or oneway
  element :radio_return,   ".c-inp-wrap.u-pad-top--0.qa-flight-2-trip"
  element :radio_oneway,   ".c-inp-wrap.u-pad-top--0.qa-flight-1-trip"

  #Maskapai
  element :checkbox_AirAsia,:xpath,       "//*[@id='reskinned_page']/div[3]/div[4]/div/div/div[1]/div/div[1]/label[1]/span"
  element :checkbox_AirAsiaX,             "input[value='AirAsia X']"      
  element :checkbox_BatikAir,             "input[value='Batik Air']"
  element :checkbox_Citilink,:xpath,      "//*[@id='reskinned_page']/div[3]/div[4]/div/div/div[1]/div/div[1]/label[4]/span"
  element :checkbox_GarudaIndonesia,      "input[value='Garuda Indonesia']"
  element :checkbox_Jetstar,              "input[value='Jetstar']"
  element :checkbox_Kalstar,              "input[value='Kalstar']"
  element :checkbox_LionAir,              "input[value='Lion Air']"
  element :checkbox_MalindoAir,           "input[value='Malindo Air']"
  element :checkbox_NamAir,               "input[value='Nam Air']"
  element :checkbox_Sriwijaya,            "input[value='Sriwijaya']"
  element :checkbox_ThaiLionAir,          "input[value='Thai Lion Air']"
  element :checkbox_TigerAir,             "input[value='Tiger Air']"
  element :checkbox_WingsAir,             "input[value='Wings Air']"
  
  #rentang harga
  element :field_min_price,".c-inp.c-inp--small.qa-filter-min-price"

  element :field_max_price,".c-inp.c-inp--small.u-mrgn-bottom--2.qa-filter-max-price"

  element :display_button,".c-btn.c-btn--small.c-btn--block.qa-filter-price-btn"

  elements :price_list, ".u-txt--medium.u-align-right.u-mrgn-bottom--1"


  #transit
  element :checkbox_langsung,    ".c-inp.c-inp--checkbox.qa-filter-0-transit"
  element :checkbox_one_transit, ".c-inp.c-inp--checkbox.qa-filter-1-transit"
  element :checkbox_two_transit, ".c-inp.c-inp--checkbox.qa-filter-2-transit"

  #waktu berangkat
  element :checkbox_0000_0700_dp, :xpath, "//*[@id='reskinned_page']/div[3]/div[4]/div/div/div[1]/div/div[4]/label[1]"
  element :checkbox_0700_1200_dp, :xpath, "//*[@id='reskinned_page']/div[3]/div[4]/div/div/div[1]/div/div[4]/label[2]"
  element :checkbox_1200_1800_dp,         ".c-inp.c-inp--checkbox.qa-filter-2-departure"
  element :checkbox_1800_0000_dp,         ".c-inp.c-inp--checkbox.qa-filter-3-departure"

  #waktu tiba
  element :checkbox_0000_0700_ar,".c-inp.c-inp--checkbox.qa-filter-0-arrival"
  element :checkbox_0700_1200_ar,".c-inp.c-inp--checkbox.qa-filter-1-arrival"
  element :checkbox_1200_1800_ar,".c-inp.c-inp--checkbox.qa-filter-2-arrival"
  element :checkbox_1800_0000_ar,".c-inp.c-inp--checkbox.qa-filter-3-arrival"
  
end
