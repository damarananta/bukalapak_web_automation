# @confirmation_page
class FlightCheckoutPage < SitePrism::Page
  
  element  :title_dropdown, :xpath, "//*[@id='reskinned_page']/div[2]/div[2]/div/div/div/div[1]/label[2]/input"
  element  :name_field,     :xpath, "//*[@id='reskinned_page']/div[2]/div[2]/div/div/div/div[1]/label[2]/input"
  element  :email_field,    :xpath, "//*[@id='reskinned_page']/div[2]/div[2]/div/div/div/div[1]/label[2]/input"
  element  :no_hp_field,    :xpath, "//*[@id='reskinned_page']/div[2]/div[2]/div/div/div/div[1]/label[2]/input"

  element :total_belanja,   ".u-txt-price.u-fg--red-brand"

  element :departure,       :xpath,"//*[@id='reskinned_page']/div[3]/div[4]/section/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[3]/p[2]"
  element :departure_time,  :xpath,"//*[@id='reskinned_page']/div[3]/div[4]/section/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/p[1]"

  element :arrive,          :xpath,"//*[@id='reskinned_page']/div[3]/div[4]/section/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[3]/p[5]"
  element :arrive_time,     :xpath,"//*[@id='reskinned_page']/div[3]/div[4]/section/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/p[2]"

  element :lanjut_pembayaran_button, ".js-button-book-flight.c-btn.c-btn--block.c-btn--red"

  

end
