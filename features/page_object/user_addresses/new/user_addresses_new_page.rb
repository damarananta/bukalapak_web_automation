class UserAddressesNewPage < SitePrism::Page
  path = "/user_addresses/new"
  set_url(ENV['BASE_URL'] + path)

  element :address_name_field,         "input[name='user_address[title]']"
  element :on_behalf_field,            "input[name='user_address[name]']"
  element :phone_number_field,         "input[name='user_address[phone]']"
  element :province_dropdown,          "#js_user_address_province_chzn"
  element :province_select_jakarta,    "#js_user_address_province_chzn_o_4"
  element :city_dropdown,              "#js_user_address_city_chzn"
  element :city_select_jaksel,         "#js_user_address_city_chzn_o_3"
  element :area_dropdown,              "#js_user_address_area_chzn"
  element :area_select_pasming,        "#js_user_address_area_chzn_o_7"
  element :alamat_text_area,           "textarea[name='user_address[address_attributes][address]']"
  element :zipcode_dropdown,           "#js_user_address_post_code_chzn"
  element :zipcode_select_12510,       "#js_user_address_post_code_chzn_o_1"
  element :save_button, :xpath,        "//input[@value='Simpan']"

  def set_address(fullname, phone_number, address)
    self.address_name_field.set("Rumah")
    self.on_behalf_field.set(fullname)
    self.phone_number_field.set(phone_number)
    scroll_down(200)
    self.province_dropdown.click
    self.province_select_jakarta.click
    self.city_dropdown.click
    self.city_select_jaksel.click
    self.area_dropdown.click
    self.area_select_pasming.click
    self.alamat_text_area.set(address)
    self.zipcode_dropdown.click
    self.zipcode_select_12510.click
    self.save_button.click
  end
end
