class MobileCartsPage < SitePrism::Page
  path = "/cart/carts"
  set_url('https://m.bukalapak.com'+ path)

  ICON_DELETE='.cart-item-remove__link.js-cart-item-remove.ui-link'

  #button
  element :buy_button,                 ".btn.btn--block.btn--custom-radius.btn--green.btn--large.js-express-checkout-trigger"
  element :add_to_cart_button,         ".btn.btn--block.btn--gray.btn--large.btn--custom-radius.js-adskom-addtocart"
  element :icon_delete_button,         ".ion-trash-b"

  element :product_name_cart,          ".product__name.ui-link"
  element :empty_cart_text,            "#page > div.blank-slate.align-center > a"

  element :ui_state_dialog_menu,:xpath, "//div[1]/header/div/a"
  element :hapus_semua_link,:xpath,     '//*[@id="js-cart-utilities"]/ul/li/a/span'
  element :icon_mini_cart,              '.m-icon.m-icon--cart'

  element :pc_love_icon,                '.m-icon.ion-ios-heart'
  element :pc_delete_icon,              '.m-icon.m-icon--remove'
  element :pc_title,:xpath,             '//*[@id="wrapper"]/div[2]/div[1]/div[1]'
  element :pc_product_name,:xpath,      '//*[@id="wrapper"]/div[2]/div[2]/div/div/form/div[1]/a/div[2]/p[1]'
  element :pc_seller_name,:xpath,       '//*[@id="wrapper"]/div[2]/div[2]/div/div/form/div[1]/a/div[2]/p[2]'
  element :pc_product_price,:xpath,     '//*[@id="wrapper"]/div[2]/div[2]/div/div/form/div[1]/a/div[2]/p[4]/span[2]'
  element :pc_biaya_kirim,:xpath,       '//*[@id="wrapper"]/div[2]/div[2]/div/div/form/div[1]/a/div[2]/p[5]/em'
  element :pc_total_product,:xpath,     '//*[@id="wrapper"]/div[2]/div[2]/div/div/form/div/div/div/div/div[1]/div[1]'
  element :pc_total_price,:xpath,       '//*[@id="wrapper"]/div[2]/div[2]/div/div/form/div/div/div/div/div[1]/div[2]'
  element :pc_empty_notice,             '.sneakpeek-cart__notice__text'
  element :pc_empty_icon,               '.m-icon.m-icon--cart-empty'

  def verify_product_exist_on_basket_mweb
    visit_split_true '/cart/carts'
    self.wait_for_product_name_cart(3)
    expect(self).to have_product_name_cart
    sleep 1
  end

  def add_to_cart
    sleep 1
    click_button('Tambah')
    sleep 3
  end

  def buy
    sleep 1
    self.buy_button.click
    sleep 3
  end

  def delete_cart_mobile
    first(ICON_DELETE).click
    click_button('Hapus')
    sleep 1
  end

  def delete_cart_and_favorites
    self.ui_state_dialog_menu.click
    self.hapus_semua_link.click
    sleep 1
    click_button('Hapus dan Simpan ke Daftar Barang Favorit')
  end

  def delete_one_cart_mobile
    if(page.has_css?('.ion-trash-b'))
      @cartCount = page.all('.ion-trash-b').count
      while @cartCount > 0
        first('.ion-trash-b').click
        click_button('Hapus')
        sleep 1
        @cartCount -= 1
      end
    end
  end

  def verify_cart_should_empty
    sleep 1
    visit '/cart/carts'
    empty_text=empty_cart_text.text
    empty_text.should=='Ayo Mulai Belanja!'
  end

  def verify_poppup_cart_content
    expect(self.pc_title).to have_text('Keranjang Belanja')
    expect(self.pc_product_name).to have_text(pc_product_name.text)
    expect(self.pc_seller_name).to have_text(pc_seller_name.text)
    expect(self.pc_product_price).to have_text(pc_product_price.text)
    expect(self.pc_biaya_kirim).to have_text(pc_biaya_kirim.text)
    expect(self.pc_total_product).to have_text(pc_total_product.text)
    expect(self.pc_total_price).to have_text(pc_total_price.text)
    expect(page).to have_css('.m-icon.ion-ios-heart')
  end

  def verify_pc_should_empty
    expect(self.pc_total_product).to have_text('Total 0 barang')
    expect(self.pc_total_price).to have_text('Rp0')
  end

end
