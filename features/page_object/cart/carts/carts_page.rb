class CartsPage < SitePrism::Page
  path = "/cart/carts"

  element :button_add_to_cart, :xpath,       "//*[@id='mod-product-detail-1']/div[1]/div[1]/div/div[2]/div[5]/form[1]/div[3]/div[1]/div[2]/button"
  element :buy_button,                       ".qa-main-buy-button"
  element :add_to_cart_button,               ".btn.btn--gray.btn--large.product-buy-actions__btn-cart.js-adskom-addtocart"
  element :icon_delete_button,               ".btn.btn--gray.btn--small.js-remove-carts-title"
  element :popup_ok_accept_delete_button,    ".btn.btn--gray.btn--large.btn--block.js-cart-seller-remove__link"
  element :hupus_save_to_favorite_button,    "#cart-seller-remove-popup > div > button.btn.btn--red.btn--large.btn--block.js-cart-seller-remove__link"
  element :empty_cart_text,                  '.btn.btn--green.btn--large'
  element :product_name,                     '.product__name'
  element :cart_title_icon,                  '.ion-android-cart'

  element :pc_title,                         '.c-popup-cart__title'
  element :pc_sub_title,:xpath               ,'//*[@id="cboxLoadedContent"]/div/div[1]/div/div/a'
  element :pc_message_success,:xpath         ,'//*[@id="cboxLoadedContent"]/div/div[2]/div[1]/div/div/div[2]'
  element :pc_product_name,:xpath            ,'//*[@id="cboxLoadedContent"]/div/div[2]/div[3]/div[1]/div/div[1]/div[2]/h3/a'
  element :pc_price,:xpath                   ,'//*//*[@id="cboxLoadedContent"]/div/div[2]/div[3]/div[1]/div/div[1]/div[2]/div/div/span[1]/span[2]'
  element :pc_delete_cart_icon,:xpath        ,'//*[@id="cboxLoadedContent"]/div/div[2]/div[3]/div[1]/div/div[3]/a'
  element :pc_total_price,:xpath             ,'//*[@id="js-carts-cart--1"]/dl/dt[1]'
  element :pc_biaya_kirim,:xpath             ,'//*[@id="js-carts-cart--1"]/dl/dt[2]'
  element :pc_sub_total,:xpath               ,'//*[@id="js-carts-cart--1"]/dl/dt[3]'
  element :pc_total_price_value,:xpath       ,'//*[@id="js-carts-cart--1"]/dl/dd[1]/div/span/span[1]'
  element :pc_biaya_kirim_value,:xpath       ,'//*[@id="js-carts-cart--1"]/dl/dd[1]/div/span/span[2]'
  element :pc_sub_total_value,:xpath         ,'//*[@id="js-carts-cart--1"]/dl/dd[3]/strong/span/span[2]'
  element :pc_isntallment_message,:xpath     ,'//*[@id="js-carts-cart--1"]/div[1]/div/div[2]'
  element :pc_lihat_keranjang,:xpath         ,'//*[@id="js-carts-cart--1"]/div[2]/div'
  element :pc_lanjutkan_pembayaran,:xpath    ,'//*[@id="js-carts-cart--1"]/div[2]/div/a'
  element :pc_cart_empty,:xpath,             '//*[@id="cboxLoadedContent"]/div/div[2]/div[3]/div[1]'

  # Please don't delete, still not found appropriate element
  def add_to_cart_btp(product)
    visit_split_true product
    sleep 1
    click_button('Tambahkan ke Keranjang')
  end

  def add_to_cart
    sleep 1
    click_button('Tambahkan ke Keranjang')
    sleep 3
  end

  def buy
    sleep 3
    self.buy_button.click
    sleep 5
  end

  def verify_product_exist_on_basket
    sleep 2
    visit_split_true '/cart/carts'
    self.wait_for_cart_title_icon(3)
    expect(self).to have_product_name
    assert_count_qty_cart
  end

  def delete_cart_desktop
    if(page.has_css?('.btn.btn--gray.btn--small.js-remove-carts-title'))
      self.icon_delete_button.click
      self.popup_ok_accept_delete_button.click
      self.wait_for_empty_cart_text(5)
    end
  end

  def delete_cart_and_favorites
    self.icon_delete_button.click
    click_button('Hapus dan Simpan ke Daftar Barang Favorit')
  end

  def delete_one_cart_desktop
    visit '/cart/carts'
    @cartCount = page.all('.ion-trash-b').count
    while @cartCount > 0
      first('.ion-trash-b').click
      find('.btn.btn--gray.btn--large.btn--block.js-cart-item-remove__link').click
      sleep 1
      @cartCount -= 1
    end
  end

  def verify_cart_should_empty
    visit '/cart/carts'
    sleep 1
    empty_text=empty_cart_text.text
    empty_text.should=='Ayo Mulai Belanja!'
  end

  def verify_poppup_cart_content
    expect(self.pc_title).to have_text(pc_title.text)
    expect(self.pc_sub_title).to have_text(pc_sub_title.text)
    expect(self.pc_message_success).to have_text(pc_message_success.text)
    expect(self.pc_product_name).to have_text(pc_product_name.text)
    expect(self.pc_price).to have_text(pc_price.text)
    expect(self.pc_delete_cart_icon).to have_text(pc_delete_cart_icon.text)
    expect(self.pc_total_price).to have_text(pc_total_price.text)
    expect(self.pc_biaya_kirim).to have_text(pc_biaya_kirim.text)
    expect(self.pc_sub_total).to have_text(pc_sub_total.text)
    expect(self.pc_biaya_kirim_value).to have_text(pc_biaya_kirim_value.text)
    expect(self.pc_sub_total_value).to have_text(pc_sub_total_value.text)
    expect(self.pc_isntallment_message).to have_text(pc_isntallment_message.text)
    expect(self.pc_lihat_keranjang).to have_text(pc_lihat_keranjang.text)
    expect(self.pc_lanjutkan_pembayaran).to have_text(pc_lanjutkan_pembayaran.text)
  end

  def click_delete_icon_popup
    self.pc_delete_cart_icon.click
  end

  def verify_popup_cart_should_empty
    expect(self.pc_message_success).to have_text(pc_message_success.text)
    expect(self.pc_cart_empty).to have_text('Keranjang Belanja Anda kosong.')
  end
end
