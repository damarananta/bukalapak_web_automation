class PopularProductPage < SitePrism::Page
  path = "/popular"
  set_url(ENV['BASE_URL'] + path)

  #constant
  POPULAR_PRODUCT_SAMPLE='/p/hobi-hiburan/buku/14-jual-buku-testing-30'
  POPULAR_PRODUCT_COUNT="//*[@id='manage-popular']/section[1]/div/div[1]/div[2]/div"
  POPULAR_PRODUCT_NAME_DASHBOARD= "//*[@id='manage-popular']/section[1]/div/div[1]/div[2]/div[#{@a}]/div[3]/a"
  POPULAR_PRODUCT_NAME_HOME= "//*[@id='popular-1-1']/ul/li[#{@i}]/div/article/div[3]/h3/a"

  #field
  element :popular_product_url,"#popular_product_url"
  element :popular_section_name,"#popular_sections_name"
  element :campaign_terkait_drop_down,:xpath,'//*[@id="popular_sections_campaign_id_chzn"]/a'
  element :select_campaign_terkait,:xpath,"//*[@id='popular_sections_campaign_id_chzn_o_1']"
  element :popular_tab1, :xpath,'//*[@id="main"]/section[2]/div[1]/div/div[1]/ul/li[1]/a'
  element :popular_tab2, :xpath,'//*[@id="main"]/section[2]/div[1]/div/div[1]/ul/li[2]/a'
  element :popular_tab, :xpath,'//*[@id="main"]/section/div[1]/h2/span[2]'
  element :popular_section_name_label,:xpath,'//*[@id="manage-popular"]/section[1]/h6'

  #button
  element :tambahkan_popular_product_button, :xpath,".//*[@id='manage-popular']/section[1]/a[1]"
  element :simpan_button,'.green'
  element :delete_popular_product_button,:xpath,".//*[@id='manage-popular']/section[1]/div/div[1]/div[2]/div[1]/div[4]/a"

  #link
  element :link_pengaturan_popular_section, :xpath ,"//*[@id='manage-popular']/section[1]/a[2]"

  def add_popular_product(product_link)
    visit_url '/popular'
    self.tambahkan_popular_product_button.click
    self.popular_product_url.set(product_link)
    self.simpan_button.click
  end

  def delete_popular_product
    self.delete_popular_product_button.click
    sleep 1
    page.driver.browser.switch_to.alert.accept
    sleep 1
  end

  def edit_popular_product_section(section,name_section)
    self.link_pengaturan_popular_section.click
    self.popular_section_name.set(name_section)
    self.campaign_terkait_drop_down.click
    sleep 1
    self.select_campaign_terkait.click
    self.simpan_button.click
  end
end
