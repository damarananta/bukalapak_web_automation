class MobilePulsaPage < SitePrism::Page
  path = "/pulsa"
  set_url(ENV['MOBILE_URL'] + path)

  element :phone_number_field, :xpath, "//*[@id='js-vp-phone']"
  element :beli_button, :xpath, "//*[@id='main']/div[1]/div[2]/form/div/div/input"
end
