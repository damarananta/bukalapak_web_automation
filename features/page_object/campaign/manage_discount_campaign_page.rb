class ManageDiscountCampaignPage < SitePrism::Page
  path = "/payment/deals/admin_manage"
  set_url(ENV['BASE_URL'] + path)
  
  #constant
  ACTIVE_FILTER_PATH = ENV['BASE_URL']+'/payment/campaigns?utf8=✓&active=1&commit=Filter'
  XPATH_CAMPAIGN_LIST_DASHBOARD=".//*[@id='manage-deals']/table/tbody[2]/tr/td[5]/a[1]"
  SUCCESS_DELETE_CAMPAIGN_MESSAGE="Campaign was successfully destroyed."
  SUCCESS_CREATE_CAMPAIGN_MESSAGE="Campaign was successfully created."
  SUCCESS_EDIT_CAMPAIGN_MESSAGE="Campaign was successfully updated."

  #field
  element :popular_product_dashboard, :xpath,"//*[@id='manage-popular']/section[1]/div/div[1]/div[2]/div"
  element :campaign_open_new_window,:xpath,".//*[@id='payment_campaign_banner_url_target_chzn']/a/span"
  element :campaign_list_dashboard, :xpath, ".//*[@id='manage-deals']/table/tbody[2]/tr/td[5]/a[1]"
  element :campaign_yes_option, :xpath,  ".//*[@id='payment_campaign_banner_url_target_chzn_o_1']"
  element :campaign_meta_desc,:xpath,".//*[@id='payment_campaign_meta_description']"
  element :campaign_sub_desc, :xpath,".//*[@id='payment_campaign_sub_description']"
  element :campaign_redirect_url, :xpath,".//*[@id='payment_campaign_banner_url']"
  element :campaign_desc,:xpath, ".//*[@id='payment_campaign_long_description']"
  element :campaign_tooltip, :xpath,".//*[@id='payment_campaign_description']"
  element :campaign_banner, :xpath,".//*[@id='payment_campaign_banner']"
  element :campaign_title, :xpath, ".//*[@id='payment_campaign_title']"
  element :checkbox_active_campaign, :xpath, ".//*[@id='active']"
  element :flash_message,".flash__text"

  #button
  element :delete_campaign_button,:xpath,".//*[@id='manage-deals']/table/tbody[2]/tr[1]/td[5]/a[1]"
  element :edit_campaign_button, :xpath, ".//*[@id='manage-deals']/table/tbody[2]/tr[1]/td[5]/a[2]"
  element :filter_campaign_button,:xpath,".//*[@id='manage-deals']/form/input[2]"
  element :create_new_campaign_button,:xpath,".//*[@id='manage-deals']/a"
  element :save_new_campaign_button,".btn.button.green"
  
  #link
  element :link_campaign_setting,:xpath, ".//*[@id='tab-nav']/ul/li[10]/a"
end
