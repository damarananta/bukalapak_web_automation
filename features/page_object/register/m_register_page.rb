class MobileRegisterPage < SitePrism::Page
  path = "/register"
  set_url(ENV["MOBILE_URL"] + path)
  element :register_link, :xpath,     "//a[text()='Daftar dengan Email']"
  element :form_registration_section, "#new_user"
  element :fullname_field,            "input[name='user[name]']"
  element :username_field,            "input[name='user[username]'"
  element :email_field,               "input[name='user[email]']"
  element :password_field,            "input[name='user[password]']"
  element :gender_male, :xpath,       "//*[@id='new_user']/div[1]/div[1]"
  element :gender_female, :xpath,     "//*[@id='new_user']/div[1]/div[2]"
  element :register_button,           "#new-registration"

  def choose_gender(gender)
    if gender == "user_gender_perempuan"
      self.gender_female.click
    else
      self.gender_male.click
    end
  end

  def register_random_user(email, fullname, password, username, gender)
    self.register_link.click
    self.wait_for_form_registration_section
    self.fullname_field.set(fullname)
    self.username_field.set(username)
    self.email_field.set(email)
    self.password_field.set(password)
    self.choose_gender(gender)
    self.register_button.click
  end
end
