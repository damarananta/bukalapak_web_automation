class RegisterPage < SitePrism::Page
  path = "register"
  set_url(ENV["BASE_URL"] + path)
  element :email_field,                 "input[name='user[email]']"
  element :fullname_field,              "input[name='user[name]'"
  element :password_field,              "input[name='user[password]'"
  element :password_confirmation_field, "input[name='user[password_confirmation]']"
  element :gender_male, :xpath,         "//*[@id='new_user']/div[4]/label[1]"
  element :gender_female, :xpath,       "//*[@id='new_user']/div[4]/label[2]"
  element :username_field,              "input[name='user[username]']"
  element :button_daftar,               "input[value='Daftar']"

  def choose_gender(gender)
    if gender == "user_gender_perempuan"
      self.gender_female.click
    else
      self.gender_male.click
    end
  end

  def register_random_user(email, fullname, password, username, gender)
    self.email_field.set(email)
    self.fullname_field.set(fullname)
    self.password_field.set(password)
    self.choose_gender(gender)
    self.username_field.set(username)
    self.button_daftar.click
  end
end
