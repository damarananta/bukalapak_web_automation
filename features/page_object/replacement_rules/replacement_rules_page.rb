class ReplacementRules < SitePrism::Page
  path ="/replacements_rules"
  set_url(ENV['BASE_URL'] + path)

  def assign_administrator
    visit_url('/replacement_rules/prepare_mass_assign?controller=replacement_rules&type=assignment')
    @countreplacement_regex= find(:xpath, '//*[@id="new_action_log_replacement_admin_assignment"]/div[2]/p/strong').text
    @countreplacement = change_into_integer(@countreplacement_regex.scan(/\d/).join)

    fill_in('action_log_replacement_admin_assignment_trx_per_assignee', :with => @countreplacement)
    sleep 3
    find(:xpath, '//*[@id="action_log_replacement_admin_assignment_assignee_ids_chzn"]/ul').click
    find(:xpath, '//*[@id="action_log_replacement_admin_assignment_assignee_ids_chzn"]/ul/li/input').set(ENV['CUSTOMER_SUPPORT_STAFF_USERNAME'])
    find(:id, 'action_log_replacement_admin_assignment_assignee_ids_chzn').native.send_keys(:enter)
    sleep 2
    click_button('Save')
    visit_url('/')
    logout
  end
end
