class MobileGameVoucherNew < SitePrism::Page

  element :lanjut_button, "#js-progress-page--next"

  element :bukadompet_radio, :xpath, "//*[@id='choose_payment_methods']/section[1]/div/label"
  element :transfer_radio, :xpath, "//*[@id='choose_payment_methods']/section[2]/div/label"
  element :creditcard_radio, :xpath, "//*[@id='choose_payment_methods']/section[3]/div/label"
  element :bcaklikpay_radio, :xpath, "//*[@id='choose_payment_methods']/section[4]/div/label"
  element :mandiriclickpay_radio, :xpath, "//*[@id='choose_payment_methods']/section[5]/div/label"
  element :mandiriecash_radio, :xpath, "//*[@id='choose_payment_methods']/section[6]/div/label"
  element :cimbclicks_radio, :xpath, "//*[@id='choose_payment_methods']/section[7]/div/label"
  element :indomaret_radio, :xpath, "//*[@id='choose_payment_methods']/section[8]/div/label"
  element :alfamart_radio, :xpath, "//*[@id='choose_payment_methods']/section[9]/div/label"
  element :kredivo_radio, :xpath, "//*[@id='choose_payment_methods']/section[10]/div/label"
  
  element :bukadompet_password_field, "#js-iv-deposit"

  element :lanjut_alfamart_button, :xpath, "//*[@id='new_payment_game_voucher_transaction']/div/section[2]/div/div[2]/input[1]"
  element :lanjut_transfer_button, :xpath, "//*[@id='new_payment_game_voucher_transaction']/div/section[2]/div/div[2]/input[1]"
  element :lanjut_bukadompet_button, :xpath, "//*[@id='new_payment_game_voucher_transaction']/div/section[2]/div/div[2]/input[2]"

end
