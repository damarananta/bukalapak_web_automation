# @electricity_new_page
# @vp_new_page
class GameVoucherNew < SitePrism::Page
  DEPOSIT_DESC_SECTION = lambda do |s|
    s = "#{s}/" unless s.nil?
    "//*[contains(@class, 'payment-method__option--deposit')]" \
    "/div[contains(@class, 'active')]/#{s}div[contains(@class, 'active')]"
  end
  
  element :bukadompet_radio,      "input[value='deposit']"
  element :transfer_radio,        "input[value='atm']"
  element :creditcard_radio,      "input[value='credit_card']"
  element :bcaklikpay_radio,      "input[value='bca_klikpay']"
  element :mandiriclickpay_radio, "input[value='mandiri_clickpay']"
  element :mandiriecash_radio,    "input[value='mandiri_ecash']"
  element :cimbclicks_radio,      "input[value='cimb_clicks']"
  element :indomaret_radio,       "input[value='indomaret']"
  element :alfamart_radio,        "input[value='alfamart']"
  element :kredivo_radio, :xpath, "input[value='kredivo']"

  element :alfamart_list_item,
    :xpath, "//li[contains(@class, 'payment-method__option--alfamart')]"
  element :atm_list_item,
    :xpath, "//li[contains(@class, 'payment-method__option--atm')]"
  element :bca_klikpay_list_item,
    :xpath, "//li[contains(@class, 'payment-method__option--bca_klikpay')]"
  element :deposit_list_item,
    :xpath, "//li[contains(@class, 'payment-method__option--deposit')]"
  element :indomaret_list_item,
    :xpath, "//li[contains(@class, 'payment-method__option--indomaret')]"

  element :insufficient_deposit_section,
    :xpath, DEPOSIT_DESC_SECTION.call(nil)
  element :sufficient_deposit_section,
    :xpath, DEPOSIT_DESC_SECTION.call("div")
  element :deposit_error_notification, :xpath, "//*[@class='msg']"

  element :deposit_password_field, "#js-iv-deposit"
  element :deposit_payment_button,
    :xpath, "//*[@class='js-payment-deposit-method']/button[2]"
  element :normal_payment_button,
    :xpath, "//*[@class='js-payment-normal-method']/button"

  element :button_pay, :xpath, "//button[text()='Bayar']"


  def pick_payment_method(pm)
    case pm
    when "alfamart" then self.alfamart_list_item.click
    when "bukadompet" then self.deposit_list_item.click
    when "bca klikpay" then self.bca_klikpay_list_item.click
    when "indomaret" then self.indomaret_list_item.click
    when "transfer" then self.atm_list_item.click
    end
  end

  def alfamart_payment_channel
    self.wait_for_alfamart_radio
    self.alfamart_radio.click
    self.button_pay.click
    sleep 4
  end

  def indomaret_payment_channel
    self.indomaret_radio.click
    self.button_pay.click
    sleep 4
  end

  def atm_payment_channel
    self.transfer_radio.click
    self.button_pay.click
    sleep 4
  end

  def bukadompet_payment_channel
    self.bukadompet_radio.click
    fill_in 'deposit_password', :with => ENV['BUYER_PASSWORD']
    self.button_pay.click
    sleep 4
  end

  def klikpay_payment_channel
    self.bcaklikpay_radio.click
    self.button_pay.click
    sleep 4
  end
end
