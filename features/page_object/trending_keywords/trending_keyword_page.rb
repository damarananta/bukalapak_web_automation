class TrendingKeywordPage < SitePrism::Page
  path = "/trending_keywords"
  set_url(ENV['BASE_URL'] + path)

  TRENDING_LIST_DASHBOARD='//tbody/tr/td[4]/a[2]'
  EDIT_ELEMENT='.green'

  element :add_new_trending_button,".button.on-left"
  element :trending_keyword_field,"#trending_keyword_keyword"
  element :trending_url_field,"  #trending_keyword_url"
  element :save_button,".green"
  element :back_button,:xpath,".//*[@id='manage-categories']/p/a"

  element :keyword_name,:xpath,".//tbody/tr[1]/td[1]"
  element :keyword_url,:xpath,".//tbody/tr[1]/td[1]"

  def delete_trending
    first(:xpath,TRENDING_LIST_DASHBOARD).click
    page.driver.browser.switch_to.alert.accept
    sleep 2
  end

  def verify_trending_modified_result(keyword,url)
    keyword_text= find(:xpath, './/tbody/tr[1]/td[1]').text
    keyword_text.should==keyword
    url_text= find(:xpath, './/tbody/tr[1]/td[2]').text
    url_text.should==url
  end

  def count_trending_keyword
    if page.has_xpath?(TRENDING_LIST_DASHBOARD)
      @trendingCount=page.all(:xpath,TRENDING_LIST_DASHBOARD).count
    else
      @trendingCount=0
    end
  end

  def clear_trending_if_exist
    while @trendingCount > 0
      self.delete_trending
      @trendingCount -=1
    end
  end

end
