class MobileSubscriptionsPage < SitePrism::Page

  element :lapak_langganan_tab,:xpath,    '//*[@id="page"]/ul/li[1]/a'
  element :timeline_tab,:xpath,           '//*[@id="page"]/ul/li[2]/a'
  element :daftar_pelanggan_tab,:xpath,   '//*[@id="page"]/ul/li[3]/a'
  element :daftar_pelanggan_tab_active,   '.active.big-tab-toggle'
  element :empty_text,                    '.list-not-found'
  element :breadcum_level2,               '#breacrumb_list > ol > li:nth-child(2)'
  element :breadcum_level1,               '#breacrumb_list > ol > li:nth-child(1)'
  element :search_box,                    '#keywords'

  #element list lapak langganan
  element :pelapak_link,:xpath,             '//*[@id="page"]/div/div/div/div[2]/a'
  element :user_activity,:xpath,            '//*[@id="page"]/div/div/div/div[2]/div'
  element :pelapak_image,:xpath,            '//*[@id="page"]/div/div/div/div[1]/img'
  element :product_box,:xpath,              '//*[@id="page"]/div/div/ul/li/article/div/a/picture'

  #element list timeline
  element :timeline_pelapak_link,:xpath,    '//*[@id="page"]/div/div/div/div[2]/h5/a'
  element :timeline_pelapak_image,:xpath,   '//*[@id="page"]/div/div/div/div[1]/img'
  element :timeline_user_activity,:xpath,   '//*[@id="page"]/div/div/div/div[2]/div'

  #element list pelanggan
  element :header_pelanggan,:xpath,         '//*[@id="page"]/div/table/thead/tr/th[1]'
  element :header_subs_date,:xpath,         '//*[@id="page"]/div/table/thead/tr/th[2]'
  element :value_image_pelanggan,:xpath,    '//*[@id="page"]/div/table/tbody/tr/td[1]/img'
  element :value_link_pelanggan,:xpath,     '//*[@id="page"]/div/table/tbody/tr/td[2]/a'
  element :value_subs_date,:xpath,          '//*[@id="page"]/div/table/tbody/tr/td[3]'

end
