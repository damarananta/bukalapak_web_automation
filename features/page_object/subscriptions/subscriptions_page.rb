class SubscriptionsPage < SitePrism::Page

  element :page_title,                  	'#page_title'
  element :lapak_langganan_tab,:xpath,  	'//*[@id="page_header"]/div/div/ul/li[1]/a'
  element :timeline_tab,:xpath,         	'//*[@id="page_header"]/div/div/ul/li[2]/a'
  element :daftar_pelanggan_tab,:xpath, 	'//*[@id="page_header"]/div/div/ul/li[3]/a'
  element :daftar_pelanggan_tab_active, 	'#page_header > div > div > ul > li.active.tab-trigger > a'
  element :empty_text,                  	'#normal_page > section > div > div > div.content-segment.blank-slate > p > strong'
  element :empty_icon,                  	'#normal_page > section > div > div > div.content-segment.blank-slate > span'
  element :preview_pelapak_button,      	'#normal_page > section > div > nav > ul > li:nth-child(23) > a'
  element :breadcum_level3,             	'#breadcrumb > ol > li:nth-child(3)'
  element :breadcum_level2,             	'#breadcrumb > ol > li:nth-child(2) > a'
  element :breadcum_level1,             	'#breadcrumb > ol > li:nth-child(1) > a'
  element :search_box,:xpath, 			      '//div//*[@id="keywords"]'
  
  #element list lapak langganan 
  element :button_lihat_semua_barang,:xpath,'//*[@id="normal_page"]/section/div/div/div/div[2]/a'
  element :pelapak_link,:xpath,             '//*[@id="normal_page"]//div/div[1]/div/div[2]/h5/a'
  element :user_activity,:xpath,            '//*[@id="normal_page"]/section/div/div/div/div[1]/div/div[2]/div'
  element :product_box,:xpath,              '//div/ul/li/div/article/div/a/picture/img'

  #element list timeline
  element :timeline_pelapak_link,:xpath,    '//*[@id="normal_page"]/section/div/div/div/div/div[1]/div/div[2]/h5/a'
  element :timeline_user_activity,:xpath,   '//*[@id="normal_page"]/section/div/div/div/div/div[1]/div/div[2]/div'
  element :modify_date,:xpath,              '//*[@id="normal_page"]/section/div/div/div/div/div[2]'

  #element list pelanggan
  element :header_pelanggan,:xpath,         '//*[@id="normal_page"]//table/thead/tr/th[1]'
  element :header_subs_date,:xpath,         '//*[@id="normal_page"]//table/thead/tr/th[2]'
  element :header_transaksi_total,:xpath,   '//*[@id="normal_page"]//table/thead/tr/th[3]'
  element :value_image_pelanggan,:xpath,    '//*[@id="normal_page"]/section/div/div/table/tbody/tr/td[1]/img'
  element :value_link_pelanggan,:xpath,     '//*[@id="normal_page"]/section/div/div/table/tbody/tr/td[2]/a'
  element :value_subs_date,:xpath,          '//*[@id="normal_page"]/section/div/div/table/tbody/tr/td[3]'
  element :value_transaksi_total,:xpath,    '//*[@id="normal_page"]/section/div/div/table/tbody/tr/td[4]'

end
