class VPDataPlanLandingPage < SitePrism::Page
  require_relative "../pulsa/sidebar_section.rb"

  set_url "/paket-data{/seo}{?query*}"

  section :tabs_section, SideBarSection, "#reskinned_page"
  element :onesignal_popover, ".onesignal-popover-dialog"

  element :phone_number_field, :xpath, "//*[@id='phone_number']"
  element :beli_button, ".c-btn.c-btn--green.c-btn--large.js-submit-button"

  def dismiss_one_signal_popup_homepage
    click_button("No Thanks") if self.has_onesignal_popover?
    sleep 2
  end

  def load_page(seo: nil, main_focus: true)
    query = {}

    self.load(seo: seo, query: query) do |page|
      page.dismiss_one_signal_popup_homepage
    end
    self.wait_for_tabs_section

    page.execute_script("window.scrollBy(0,400)") if main_focus
  end

  def load_old_url(seo: nil)
    visit("/vp/voucher/data/#{seo}")
  end
end
