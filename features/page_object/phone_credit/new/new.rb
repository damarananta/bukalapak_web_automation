# @phone_credit_new_page
# @vp_new_page

class PhoneCreditNew < SitePrism::Page
  DEPOSIT_DESC_SECTION = lambda do |s|
    s = "#{s}/" unless s.nil?
    "//*[contains(@class, 'payment-method__option--deposit')]" \
    "/div[contains(@class, 'active')]/#{s}div[contains(@class, 'active')]"
  end

  element :alfamart_list_item,         "input[value='alfamart']"
  element :atm_list_item,              "input[value='atm']"
  element :bca_klikpay_list_item,      "input[value='bca_klikpay']"
  element :deposit_list_item,          "input[value='deposit']"
  element :indomaret_list_item,        "input[value='indomaret']"

  element :insufficient_deposit_section, :xpath, DEPOSIT_DESC_SECTION.call(nil)
  element :sufficient_deposit_section, :xpath, DEPOSIT_DESC_SECTION.call("div")
  element :deposit_error_notification, :xpath, "//*[@class='msg']"

  element :deposit_password_field, "#js-iv-deposit"
  element :deposit_payment_button, :xpath, "//*[@class='js-payment-deposit-method']/button[2]"
  element :normal_payment_button, :xpath, "//*[@class='js-payment-normal-method']/button"

  element :button_pay, :xpath, "//button[text()='Bayar']"
  
  element :otp_button,          :xpath,            "//div[contains(@class,'js-tfa-request__button') and text()='Kirim Kode Otentikasi']"
  element :otp_field,                              "#otp"
  element :submit_otp_button,   :xpath,            "//div[contains(@class,'js-tfa-submit') and text()='Lanjut']"
  element :finish_otp_button,   :xpath,            "//div[contains(@class,'js-tfa-finish') and text()='Lanjut']"

  def tfa_alert(token)
    self.wait_for_otp_button
    self.otp_button.click
    self.wait_for_otp_field
    self.otp_field.set(token)
    self.submit_otp_button.click
    self.finish_otp_button.click
  end

  def alfamart_payment_channel
    self.alfamart_list_item.click
    self.button_pay.click
    sleep 4
  end

  def indomaret_payment_channel
    self.indomaret_list_item.click
    self.button_pay.click
    sleep 4
  end

  def atm_payment_channel
    self.atm_list_item.click
    self.button_pay.click
    sleep 4
  end

  def bukadompet_payment_channel(password)
    self.deposit_list_item.click
    fill_in 'deposit_password', :with => password
    self.button_pay.click
    sleep 4
  end

  def klikpay_payment_channel
    self.bca_klikpay_list_item.click
    self.button_pay.click
    sleep 4
  end

end
