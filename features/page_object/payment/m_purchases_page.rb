class MobilePurchasesPage < SitePrism::Page
  
  element :invoice_number_link, :xpath, "//*[@id='page']/section/div[1]/a"
  element :invoice_number, :xpath, "//*[@id='page']/section/div[1]/a"
  
  element :lihat_tagihan_pembayaran_link, :xpath, "//*[@id='page']/section/div[3]/p[2]/a"

end