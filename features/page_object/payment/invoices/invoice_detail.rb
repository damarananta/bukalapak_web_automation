# @invoice_detail_page
class InvoiceDetailPage < SitePrism::Page
  PAID_STATUS    = "Dibayar"
  WAITING_STATUS = "Menunggu Pembayaran"

  element :invoice_number_text,   :xpath, "//*[@id='page_title']/small"
  element :payment_status_text,   :xpath, "//*[@class='memo-column memo-left clearfix']/div[1]"
  element :unique_code_text,              ".block-payment-code--highlight"
  element :payment_status_non_vp, :xpath, "//*[@class='memo-header clearfix']/div[1]"

  def vp_invoice_should_be_in_waiting
    self.wait_for_payment_status_text
    expect(self.payment_status_text).to have_content(WAITING_STATUS)
  end

  def vp_invoice_should_be_paid
    self.wait_for_payment_status_text
    expect(self.payment_status_text).to have_content(PAID_STATUS)
  end

  def invoice_should_be_paid
    self.wait_for_payment_status_non_vp
    expect(self.payment_status_non_vp).to have_content(PAID_STATUS)
  end
end
