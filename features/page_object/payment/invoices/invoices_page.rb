class InvoicesPage < SitePrism::Page

  set_url "/payment/invoices"

  # invoice number
  element :invoice_number, :xpath, "//*[@id='page_title']/small"  
  
  # Button invoice list
  element :button_select_invoice, :xpath, "//*[@id='nego_list']/ul/li[1]/div[1]/h3/a"

  # View invoice
  element :button_view_invoice, :xpath, "//a[text()='Lihat Tagihan']"
  element :button_reskinned_view_invoice, :xpath, "//a[text()='Lihat Tagihan Pembayaran']"

  element :search_field, "#keywords"
  element :invoice_list, :xpath, "//*[@id='nego_list']/ul/li/div/h3/a"

  # search field
  element :search_field, "#keywords"  
  
  # trx result
  element :trx_result, :xpath, "//*[@id='nego_list']/ul/li/div[1]/h3/a"

  def view_invoice
    if(page.has_content?('Lihat Tagihan Pembayaran'))
      self.wait_for_button_reskinned_view_invoice
      self.button_reskinned_view_invoice.click
    else
      self.wait_for_button_view_invoice
      self.button_view_invoice.click
    end
  end

  def invoices_page_search
    visit_url '/payment/invoices?utf8=%E2%9C%93&payment_invoice_search_search%5Bkeywords%5D=&payment_invoice_search_search%5Bmode%5D=menunggu&payment_invoice_search_search%5Bhas_voucher%5D=0'
    self.button_select_invoice.click
  end

  def get_invoice_id
    @invoice_id_regex = find(:xpath, '//*[@id="normal_page"]/section/div/div/div[3]/section[3]/div[3]/div/div/a').text
    @invoice_id = @invoice_id_regex.scan(/\d/).join
  end

end
