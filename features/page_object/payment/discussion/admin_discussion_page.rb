class AdminDiscussion < SitePrism::Page
  path ="payment/discussions/admin"
  set_url(ENV['BASE_URL'] + path)


  def admin_approve_retur
    visit_url '/payment/discussions/admin?utf8=%E2%9C%93&transaction_id=&state=all&return_type=all&mode=&sorting=desc&commit=Search'

    new_window = window_opened_by do
      first(:link, 'Lihat Diskusi').click
    end

    within_window new_window do
      click_link 'Setujui Tipe Retur'
      click_link 'Setujui Retur'
      close_window
    end
  end
end