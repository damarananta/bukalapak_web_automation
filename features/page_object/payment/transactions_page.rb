class TransactionPage < SitePrism::Page

  path ="/payment/transactions?active_tab={account_type}&payment_transaction_searchable_search[filter_by]={account_type}"

  set_url(ENV['BASE_URL'] + path)
  element :invoice_tab,                   :xpath,         "//*[@id='page_header']/div[2]/div/div/ul/li[1]/a"
  element :seller_tab,                    :xpath,         "//*[@id='page_header']/div[2]/div/div/ul/li[3]/a"
  element :buyer_tab,                     :xpath,         "//*[@id='page_header']/div[2]/div/div/ul/li[2]/a"
  element :discussion_tab,                :xpath,         "//*[@id='normal_page']/section/div/div/div[2]/header/div[2]/div/div/ul/li[2]/a"
  element :filter_transaction,                            "#keywords"
  element :reject_filtered_trx,           :xpath,         "//*[@id='nego_list']/ul/li[1]/div[6]/a[2]"
  element :otp_button,                    :xpath,         "//div[contains(@class,'js-tfa-request__button') and text()='Kirim Kode Otentikasi']"
  element :otp_field,                                     "#otp"
  element :submit_otp_button,             :xpath,         "//div[contains(@class,'js-tfa-submit') and text()='Lanjut']"
  element :finish_otp_button,             :xpath,         "//div[contains(@class,'js-tfa-finish') and text()='Lanjut']"
  element :buyer_confirm_send_item,       :xpath,         "//*[@id='transaction_return_address']/div[2]/a"
  element :buyer_select_send_item,        :xpath,         "//*[@id='nego_list']/ul/li/div[2]/h3/a"
  element :transaction_id_filter,                         "#keywords"
  element :confirm_delivered,             :xpath,         "//a[text()='Konfirmasi Terima Barang']"
  element :thumb_up,                      :css,           ".c-icon.c-icon--thumb-up"
  element :auto_feedback,                 :xpath,         "//*[@id='js-recomendation-form']/div/div[3]/div[2]/div[2]/div[7]"
  element :confirm_remitted_checkbox,     :xpath,         "//*[@id='js-recomendation-form']/div/div[6]/div[1]/label"
  element :save_confirmed,                :css,           ".c-btn.c-btn--red.js-recomendation-submit"

  element :seller_reject_button,          :xpath,         "//a[text()='Tolak']"
  element :reject_radio_button,           :xpath,         "//*[@id='payment_rejection_reason_stok_habis']"
  element :confirm_reject_button,         :xpath,         "//*[@id='new_payment_rejection']/div/input"

  element :seller_process_order,          :xpath,         "//a[text()='Proses']"
  element :confirm_process_order,         :xpath,         "//a[text()='Konfirmasi Proses Pesanan']"
  element :re_confirm_process_order,      :xpath,         "//*[@id='accept-order__confirm-content']/div/div/form/button"
  element :payment_shipping_field,        :xpath,         "//*[@id='payment_shipping_shipping_code']"
  element :save_shipping_code,            :xpath,         "//*[@id='new_payment_shipping']/fieldset[3]/dl/dd/input"

  element :complaint_button,			  :xpath,		  "//*[@id='nego_list']/ul/li/div[5]/a[3]"
  element :auto_feedback_negative_1,	  :xpath,		  "//*[@id='js-recomendation-form']/div/div[3]/div[2]/div[2]/div[1]"
  element :auto_feedback_negative_2,	  :xpath,		  "//*[@id='js-recomendation-form']/div/div[3]/div[2]/div[2]/div[5]"
  element :thumb_down,					  :css,			  ".c-icon.c-icon--thumb-down"

  element :discussion_tab_retur,		  :xpath,		  "//a[text()='Lihat Diskusi']"
  element :discussion_tab_send_item,	  :xpath,		  "//a[text()='Konfirmasi Kirim Barang']"
  element :shipping_courier_retur,        :css,			  "#payment_shipping_delivery_service"
  element :shipping_number_confirm,	   	  :xpath,	 	  "//*[@id='new_payment_shipping']/fieldset[3]/dl/dd/input"


  def tfa_alert_bukadompet(token)
    self.otp_button.click
    sleep 2
    self.otp_field.set(token)
    self.submit_otp_button.click
  end

  def tfa_alert_bukadompet_ts(token)
    self.otp_button.click
    sleep 2
    self.otp_field.set(token)
    self.submit_otp_button.click
    self.finish_otp_button.click
    sleep 3
  end

  def filter_transactions(invoice_id)
    self.wait_for_transaction_id_filter
    self.transaction_id_filter.set(invoice_id+'A')
    find(:id, 'keywords').native.send_keys(:enter)
    sleep 3
  end

  def buyer_confirm_to_remitted
    self.wait_until_confirm_delivered_visible
    self.confirm_delivered.click
    self.wait_for_thumb_up
    self.thumb_up.click
    self.wait_for_auto_feedback
    self.auto_feedback.click
    self.wait_for_confirm_remitted_checkbox
    self.confirm_remitted_checkbox.click
    self.wait_for_save_confirmed(5)
    self.save_confirmed.click
  end

  def seller_reject_orders(invoice_id)
    visit_url '/payment/transactions?active_tab=sell&payment_transaction_searchable_search[filter_by]=sell'
    filter_transactions(invoice_id)
    self.wait_for_seller_reject_button(3)
    self.seller_reject_button.click
    self.wait_for_reject_radio_button(5)
    self.reject_radio_button.click
    self.wait_for_confirm_reject_button(5)
    self.confirm_reject_button.click
  end

  def search_transaction_replacement(invoice_id)
    visit_url('/replacements?utf8=%E2%9C%93&mode=&transaction_id=&product_name=&amount=&seller_name=&per_page=25&state=all&reason=&replacement_type=all&start_date[day]=2&start_date[month]=12&start_date[year]=2016&finish_date[day]=2&finish_date[month]=12&finish_date[year]=2020&commit=Search')
    filter_transactions(invoice_id)
  end

  def search_new_transaction_after_replacements(new_transaction_id)
    visit_url '/payment/transactions?active_tab=buy&payment_transaction_searchable_search%5Bfilter_by%5D=buy'
    filter_transactions(new_transaction_id)
  end

  def seller_accept_orders(new_transaction_id)
    filter_transactions(new_transaction_id)
    sleep 3
    self.wait_for_seller_process_order(5)
    self.seller_process_order.click
    self.wait_for_confirm_process_order(5)
    self.confirm_process_order.click
    self.wait_for_re_confirm_process_order(5)
    self.re_confirm_process_order.click
    self.payment_shipping_field.click
    self.payment_shipping_field.set "CGK123AUTOMATION"
    self.wait_for_save_shipping_code
    self.save_shipping_code.click
  end


  def buyer_complains(invoice_id)
    visit_url '/payment/transactions?active_tab=buy&payment_transaction_searchable_search%5Bfilter_by%5D=buy'
    filter_transactions(invoice_id)
    self.wait_for_complaint_button
    self.complaint_button.click
    self.wait_for_thumb_down
    self.thumb_down.click
    self.auto_feedback_negative_1.click
    self.auto_feedback_negative_2.click
    click_button('Simpan dan Lanjutkan Komplain')
    #buyer_complain
  end

  def get_transaction_id
    @transaction_id = change_into_integer(find(:xpath, '//*[@id="nego_list"]/ul/li[1]/div[2]/h3/a').text)
    find(:xpath, '//*[@id="nego_list"]/ul/li[1]/div[2]/h3/a').click
    sleep 7
    url = URI.parse(current_url)
    puts("URL = " + url.to_s)
    @newurl = url.to_s+"/arrival_confirmation?complain=true"
    visit_url(@newurl)
    #wait_for_ajax
    sleep 3
  end

  def replace_product
    find('.btn.btn--outline-green.btn--large.radio-as-btn__btn.pull-right.js-radio-as-btn__label').click
    choose('Barang rusak/cacat/pecah')
    click_hidden('#payment_return_return_type_replace')
    attach_file('attachment', File.absolute_path('./features/support/image/crushed-boxes.jpg'))
    check('accept_agreement')
    click_button('Kirim Komplain')
  end

  def buyer_deliver_product_retur(invoice_id)
    visit_url 'payment/transactions?active_tab=buy&payment_transaction_searchable_search[filter_by]=buy'
    filter_transactions(invoice_id)
    self.wait_for_discussion_tab_retur
    self.discussion_tab_retur.click
    self.wait_for_discussion_tab_send_item
    click_hidden(".btn.btn--red.btn--medium.cbox-confirm.cboxElement")
    self.wait_for_payment_shipping_field
    self.payment_shipping_field.set "123BUYERS"
    self.shipping_courier_retur.set "JNE"
    self.shipping_number_confirm.click
  end

  def seller_accept_the_product_retur(invoice_id)
    visit_url 'payment/transactions?active_tab=sell&payment_transaction_searchable_search[filter_by]=sell'
    filter_transactions(invoice_id)
    sleep 3
    find(:xpath, "//*[@id='nego_list']/ul/li[1]/div[3]/h3/a").click
    sleep 3
    find(:xpath,"//*[@id='normal_page']/section/div/div/div[2]/header/div[2]/div/div/ul/li[2]/a").click
    sleep 3
    click_hidden(".btn.btn--red.btn--medium.cbox-confirm.cboxElement")
    sleep 3
    click_button('Ya')
  end

  def seller_deliver_product_retur
    click_hidden('.btn.btn--red.btn--medium.cbox-confirm.cboxElement')
    self.wait_for_payment_shipping_field
    self.payment_shipping_field.set "123SELLERS"
    self.shipping_courier_retur.set "JNE"
    self.shipping_number_confirm.click
    logout
  end

  def buyer_accept_product_retur(invoice_id)
    visit_url 'payment/transactions?active_tab=buy&payment_transaction_searchable_search[filter_by]=buy'
    filter_transactions(invoice_id)
    sleep 3
    find(:xpath, "//*[@id='nego_list']/ul/li/div[2]/h3/a").click
    sleep 3
    find(:xpath,"//*[@id='normal_page']/section/div/div/div[2]/header/div[2]/div/div/ul/li[2]/a").click
    sleep 3
    click_hidden(".btn.btn--red.btn--medium.cbox-confirm.cboxElement")
  end
end
