class KredivoPaymentPage < SitePrism::Page

  element :button_cancel_and_back_to_bukalapak, "button[name='cancel']"
  element :text_invalid_login_credential, "p.error-message.mb-20"

  class LoginSection < SitePrism::Section
    element :field_mobile_number, "#inputMobileNumber"
    element :field_password, "#inputPassword"
    element :button_login, "button[name='login']"
  end

  section :login_section, LoginSection, ".box.mb-20"

  class PaymentOptionSection < SitePrism::Section
    element :selected_radio_30_days, "input[checked='checked'][value='30_days']"
    element :selected_radio_3_months, "input[checked='checked'][value='3_months']"
    element :selected_radio_6_months, "input[checked='checked'][value='6_months']"
    element :selected_radio_12_months, "input[checked='checked'][value='12_months']"
  end

  section :payment_option_section, PaymentOptionSection, ".box.mb-30:nth-child(1)"

  def verify_selected_payment_option(payment_type)
    radio_value = case payment_type
      when '30_days'
        expect(self.payment_option_section).to have_selected_radio_30_days
      when '3_months'
        expect(self.payment_option_section).to have_selected_radio_3_months
      when '6_months'
        expect(self.payment_option_section).to have_selected_radio_6_months
      when '12_months'
        expect(self.payment_option_section).to have_selected_radio_12_months
    end
  end

  def verify_invalid_login_message
    expect(self).to have_text_invalid_login_credential
  end
  
  def login(mobile_number, password)
    self.login_section.field_mobile_number.set mobile_number
    self.login_section.field_password.set password
    self.login_section.button_login.click
  end

  def cancel_and_go_back_to_bukalapak
    self.button_cancel_and_back_to_bukalapak.click
  end
end
