class MobileInvoicesPage < SitePrism::Page
  path = "/payment/invoices/"
  set_url(ENV['MOBILE_URL'] + path)

  element :search_trx_field, :xpath, "//*[@id='keywords']"
  element :trx_result, :xpath, "/html/body/div[2]/div/div[2]/div/div/div[2]/a/div"
end