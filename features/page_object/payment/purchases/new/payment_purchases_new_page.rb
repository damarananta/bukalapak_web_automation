class PaymentPurchasesNewPage < SitePrism::Page

  # Button Payment Methods
  element :button_alfamart, "input[value='alfamart']"
  element :button_indomaret, "input[value='indomaret']"
  element :button_atm, "input[value='atm']"
  element :button_bca_klikpay, "input[value='bca_klikpay']"
  element :button_mandiri_clickpay, "input[value='mandiri_clickpay']"
  element :button_cimb, "input[value='cimb_clicks']"
  element :button_cc, "input[value='credit_card']"
  element :button_bukadompet, ".payment-method__option.payment-method__option--deposit"

  element :next_page_button,           "#js-purchase-next"
  element :deposit_password_field,     "#js-iv-deposit"
  element :pay_button, :button,        "Bayar"

  # Unique Code
  element :unique_code, ".block-payment-code--highlight"

  # Next to choose payment method after checkout 1 for force bukadompet mode
  element :button_other_payment, :xpath, "//a[text()='Pilih metode pembayaran lain']"
  
  # Automatically pay using bukadompet
  element :button_force_bukadompet, :button, "Bayar dengan BukaDompet"

  # Fields
  element :card_number, "#payment_payment_card_number"
  element :token_response, "#token_response"

  # Change Address
  element :button_change_address, "#js_iv_address_id_chzn"
  element :second_address, "#js_iv_address_id_chzn_o_1"  

  # Mix Payment
  element :mix_payment, :xpath, "//*[@id='mix-payment-wrapper']/fieldset[1]/a"
  element :fill_nominal_mixed, "#js-iv-mix-input-preview"
  element :use_balance, "#js-iv-mix-action"
  
  element :button_force_bukadompet_ts, ".js-encourage-deposit .was-tfa-button"

  # OTP
  element :send_otp, ".btn.btn--red.btn--medium.js-tfa-request__button.js-tfa-step.js-tfa-step--request"
  element :input_otp, "#otp"
  element :next_otp, ".btn.btn--green.btn--medium.js-tfa-submit.js-tfa-step.js-tfa-step--input1.js-tfa-step--input2"

  # Adjust Quantity
  element :increase_in_checkout, ".btn.btn--gray.btn--increase.btn--medium.js-btn-increase"
  element :product_price_before, ".js-iv-review__product"


  def paid_with_bukadompet(password)
    self.next_page_button.click
    self.wait_for_button_bukadompet
    self.button_bukadompet.click
    self.wait_for_deposit_password_field
    self.deposit_password_field.set(password)
    self.pay_button.click
  end

  def choose_alfamart
    self.next_page_button.click
    self.button_alfamart.click
    self.pay_button.click
  end

  def choose_indomaret
    self.next_page_button.click
    self.button_indomaret.click
    self.pay_button.click
  end

  def choose_atm
    self.next_page_button.click
    self.button_atm.click
    self.pay_button.click
  end

  def choose_bca_klikpay
    self.next_page_button.click
    self.button_bca_klikpay.click
    self.pay_button.click
  end

  def choose_mandiri_clickpay(card_number, token_response)
    self.next_page_button.click
    self.button_mandiri_clickpay.click
    self.pay_button.click
    self.card_number.set(card_number)
    self.token_response.set(token_response)
  end

  def choose_cimb
    self.next_page_button.click
    self.button_cimb.click
    self.pay_button.click
  end

  def change_address    
    self.button_change_address.click  
    self.second_address.click
  end

  def choose_payment_but_dicontinue(payment_method)
    if(payment_method == "alfamart")
      self.next_page_button.click
      self.wait_for_button_alfamart
      self.button_alfamart.click
    elsif(payment_method == "bukadompet")
      self.next_page_button.click
      self.wait_for_button_bukadompet
      self.button_bukadompet.click
    elsif (payment_method == "indomaret")
      self.next_page_button.click
      self.wait_for_button_indomaret
      self.button_indomaret.click
    elsif (payment_method == "atm")
      self.next_page_button.click
      self.wait_for_button_atm
      self.button_atm.click
    elsif (payment_method == "bca")
      self.next_page_button.click
      self.wait_for_button_bca_klikpay
      self.button_bca_klikpay.click
    elsif (payment_method == "cimb")
      self.next_page_button.click
      self.wait_for_button_cimb
      self.button_cimb.click
    elsif (payment_method == "mandiri_clickpay")
      self.next_page_button.click
      self.wait_for_button_mandiri_clickpay
      self.button_mandiri_clickpay.click
    elsif (payment_method == "credit_card")
      self.next_page_button.click
      self.wait_for_button_cc
      self.button_cc.click
    else
      return false
    end
  end

  def choose_mix_payment(payment_method)
    if(payment_method == "atm")
      self.next_page_button.click
      self.wait_for_button_atm
      self.button_atm.click
      self.mix_payment.click
      self.fill_nominal_mixed.set('20000')
      self.use_balance.click
      self.pay_button.click
    elsif (payment_method == "alfamart")
      self.next_page_button.click
      self.wait_for_button_alfamart
      self.button_alfamart.click
      self.mix_payment.click
      self.fill_nominal_mixed.set('20000')
      self.use_balance.click
      self.pay_button.click
    elsif (payment_method == "indomaret")
      self.next_page_button.click
      self.wait_for_button_indomaret
      self.button_indomaret.click
      self.mix_payment.click
      self.fill_nominal_mixed.set('20000')
      self.use_balance.click
      self.pay_button.click
    elsif (payment_method == "bca")
      self.next_page_button.click
      self.wait_for_button_bca_klikpay
      self.button_bca_klikpay.click
      self.mix_payment.click
      self.fill_nominal_mixed.set('20000')
      self.use_balance.click
      self.pay_button.click
    elsif (payment_method == "mandiri")
      self.next_page_button.click
      self.wait_for_button_mandiri_clickpay
      self.button_mandiri_clickpay.click
      self.mix_payment.click
      self.fill_nominal_mixed.set('20000')
      self.use_balance.click
      self.pay_button.click
    else
      return false
    end
  end

  def choose_bukadompet_tfa(choose_button)
    if(choose_button == "force_bukadompet")
      self.button_force_bukadompet.click
    elsif (choose_button == "encourage_bukadompet")
      if(page.has_content?('Pilih metode pembayaran lain'))
        self.button_other_payment.click
        sleep 2
        self.pay_button.click
        self.send_otp.click
        self.input_otp.set("12345")
        self.next_otp.click
        self.pay_button.click
      else
        self.next_page_button.click
      end
    else
      return false
    end
  end

  def adjust_quantity
    
    @one_product_price = product_price_before.text
    @first_price = @one_product_price.delete("Rp.").to_i
    
    self.wait_for_increase_in_checkout
    self.increase_in_checkout.click
    # self.wait_until_product_price_before_visible
    sleep 3

    @increased_price = product_price_before.text
    @increased_price = @one_product_price.delete("Rp.").to_i

    @final_price = @first_price < @increased_price
  end           
end
