class CheckoutPage < SitePrism::Page

  # Next to choose payment method after checkout 1 for force bukadompet mode
  element :button_choose_payment, "#js-purchase-next"

  # Next to choose payment method after checkout 1 for non-force bukadompet mode
  element :button_choose_other_payment, "#js-purchase-encourage-deposit-next"
  
  # Automatically pay using bukadompet
  element :button_force_bukadompet, ".btn.btn--red btn--large was-tfa-button"

  element :button_force_bukadompet_ts, ".js-encourage-deposit .was-tfa-button"
  def checkout
    neutralize_ab_test("&ab_bukadompet=normal&ab_payment_view_v2=normal&ab_checkout_button=normal&checkout_button=normal&ab_test[btp_checkout_reskin]=normal")
    scroll_down(100)
    sleep 3
    if(page.has_content?('Pilih metode pembayaran lain'))
      self.button_choose_other_payment.click
    else
      self.button_choose_payment.click
    end
  end

  def pay_force_bukadompet
    neutralize_ab_test("&ab_bukadompet=normal&ab_payment_view_v2=normal&ab_checkout_button=normal&checkout_button=normal")
    scroll_down(100)
    sleep 3
    self.button_force_bukadompet.click
  end
end