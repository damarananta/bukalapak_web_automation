# @confirmation_page
class PurchaseConfirmationPage < SitePrism::Page
  element :tab_phone_credit,       :xpath, "//*[@id='reskinned_page']/section/div[1]/div/div/div/ul/li[1]/a"
  element :tab_dataplan,           :xpath, "//*[@id='reskinned_page']/section/div[1]/div/div/div/ul/li[2]/a"
  element :tab_electricity,        :xpath, "//*[@id='reskinned_page']/section/div[1]/div/div/div/ul/li[3]/a"
  element :tab_gamevoucher,        :xpath, "//*[@id='reskinned_page']/section/div[1]/div/div/div/ul/li[4]/a"

  element :phone_number_field,     "#phone_number"
  element :customer_number_field,  "#customer_number"

  element :buy_button_phonecredit, :xpath, "//*[@id='mod-virtual-credit-1']/form/div[2]/div/div[3]/div/input"
  element :buy_button_dataplan,    :xpath, "//*[@id='mod-virtual-credit-2']/form/div[2]/div/div[3]/div/input"
  element :buy_button_electricity, :xpath, "//*[@id='mod-virtual-credit-3']/form/div[2]/div/div[3]/div/input"
  element :buy_button_gamevoucher, :xpath, "//*[@id='mod-virtual-credit-4']/form/div[2]/div/div[3]/div/input"

  element :display_invoice_button, ".c-btn.c-btn--red.c-btn--block.u-mrgn-top--3.u-mrgn-bottom--3"
  element :unique_code_text,       :xpath, "//*[@id='reskinned_page']/section/div/div/div/div[2]/div/div[2]/div[2]/span[3]"
  element :invoice_number, ".u-txt--bold"

  element :ya_button,              ".btn.btn--red.btn--large.btn--block.js-btn-phone-confirm"
  element :bukan_button,           ".btn.btn--gray.btn--large.btn--block.js-btn-phone-another"

  element :lewati_proses_link,     ".pull-right.js-btn-phone-cancel"



  def ignore_phonenumber_confirmation(payment_method)
    if page.has_css?('#cboxWrapper')
  	  self.bukan_button.click
  	  sleep 2
  	  self.lewati_proses_link.click
  	else
      neutralize_ab_test('ab_test[btp_checkout_reskin]=reskinned')
  	  self.display_invoice_button.click
    end
  end
end
