# @ebanking_redirect_page
class EbankingRedirectPage < SitePrism::Page
  element :ebanking_page_button,
    :xpath, "//input[@value='Pindah ke halaman e-banking']"
end
