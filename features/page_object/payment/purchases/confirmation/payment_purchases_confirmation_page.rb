class PaymentPurchasesConfirmationPage < SitePrism::Page

  element :payment_confirmation_header,  ".payment-confirmation__header.clearfix"
  
  # View invoice
  element :button_view_invoice, :xpath, "//a[text()='Lihat Tagihan']"

  # Button invoice list
  element :button_select_invoice, :xpath, "//*[@id='nego_list']/ul/li[1]/div[1]/h3/a"

  def view_invoice
    self.wait_for_button_view_invoice
    self.button_view_invoice.click
  end

  def invoices_page_search
    visit_url '/payment/invoices?utf8=%E2%9C%93&payment_invoice_search_search%5Bkeywords%5D=&payment_invoice_search_search%5Bmode%5D=menunggu&payment_invoice_search_search%5Bhas_voucher%5D=0'
    self.wait_for_button_select_invoice
    self.button_select_invoice.click
  end
end
