class PaymentChannel < SitePrism::Page

  # Button Payment Methods
  element :button_alfamart, "input[value='alfamart']"
  element :button_indomaret, "input[value='indomaret']"
  element :button_atm, "input[value='atm']"
  element :button_mandiri_clickpay, "input[value='mandiri_clickpay']"
  element :button_mandiri_ecash, "input[value='mandiri_ecash']"
  element :button_bca_klikpay, "input[value='bca_klikpay']"
  element :button_cimb, "input[value='cimb_clicks']"
  element :button_cc, "input[value='credit_card']"
  element :button_bukadompet, ".payment-method__option.payment-method__option--deposit.active"
  element :button_kredivo, "input[value='kredivo']"
  
  # Button pay 
  element :button_pay, :xpath, "//button[text()='Bayar']"
  element :button_pay_using_tfa_ts, ".btn.btn--red.btn--large.btn--block.js-iv-submit-deposit.js-iv-submit-unpassword.js-tfa-required-button"
  element :button_pay_using_tfa, ".btn.btn--red.btn--large.btn--block.js-iv-submit-deposit.was-tfa-button"
  element :button_pay_after_otp, ".btn.btn--red.btn--large.btn--block.js-iv-submit-deposit.js-iv-submit-unpassword.was-tfa-button"

  # Cancel button
  element :button_cancel_ecash, ".btn.btn-default.gradient.pull-right"  

  # Unique Code
  element :unique_code, ".block-payment-code--highlight"

  # Fields
  element :card_number, "#payment_payment_card_number"
  element :token_response, "#token_response"
  element :cc_number, "#cc_number_input"
  element :cc_cvv, "#cc_cvv"
  element :deposit_password, "#js-iv-deposit"
  element :month_expiry_cc, "#cc_expiry_month_chzn"
  element :choose_month_cc, "#cc_expiry_month_chzn_o_1"
  element :year_expiry_cc, "#cc_expire_year_chzn"
  element :choose_year_cc, "#cc_expire_year_chzn_o_4"
  element :pay_cc, ".btn.btn--large.btn--red.pull-right.js-cc-submit"
  element :input_res_cc, "PaRes"
  element :ok_button_cc, "//button[text()='OK']"

  # Fields for Kredivo
  element :kredivo_payment_type_dropdown, '#kredivo_payment_type_chzn'
  element :kredivo_30_days, 'li#kredivo_payment_type_chzn_o_0'
  element :kredivo_3_months, 'li#kredivo_payment_type_chzn_o_1'
  element :kredivo_6_months, 'li#kredivo_payment_type_chzn_o_2'
  element :kredivo_12_months, 'li#kredivo_payment_type_chzn_o_3'

  def choose_alfamart
    self.button_alfamart.click
    self.button_pay.click
  end

  def choose_indomaret
    self.button_indomaret.click
    self.button_pay.click
  end

  def choose_atm
    self.button_atm.click
    self.button_pay.click
  end

  def choose_mandiri_clickpay(card_number, token_response)
    self.button_mandiri_clickpay.click
    self.button_pay.click
    self.card_number.set(card_number)
    self.token_response.set(token_response)
  end

  def choose_mandiri_ecash
    self.button_mandiri_ecash.click
    self.button_pay.click
    self.button_cancel_ecash.click
  end

  def choose_bca
    self.button_bca_klikpay.click
    self.button_pay.click
    sleep 8
  end

  def choose_cimb
    self.button_cimb.click
    self.button_pay.click
    sleep 8
  end

  def choose_credit_card
    self.button_cc.click
    self.button_pay.click
  end

  def choose_kredivo(payment_type)
    #payment_type: 30_days, 3_months, 6_months, 12_months

    #scroll down the page
    within('.section-page.section-purchase.section-purchase__payment.active') do
      scroll_down(100)
    end

    #and select kredivo
    self.button_kredivo.click
    
    #wait for payment type dropdown item
    self.wait_for_kredivo_payment_type_dropdown(10)
  
    #scroll down the page again, so the dropdown and it list can be visible
    scroll_down(10000)

    within('.payment-method-info.payment-info--kredivo.u-overflow-visible.active') do

      #clicking the payment type dropdown
      self.kredivo_payment_type_dropdown.click

      within('.chzn-drop') do
        #selecting a payment type dopdown item
        case payment_type
          when '30_days'
            self.kredivo_30_days.click
          when '3_months'
            self.kredivo_3_months.click
          when '6_months'
            self.kredivo_6_months.click
          when '12_months'
            self.kredivo_12_months.click
        end

      end

    end

    self.button_pay.click

    #clicking 'yes' option if kredivo account confirmation pop-up appeared
    btn = '.btn.btn--gray.btn--medium.js-kredivo-with-account-btn'
    find(btn).click if page.has_css?(btn)  
  end

  def choose_bukadompet(password)
    self.button_bukadompet.click
    within('.text-green') do
      @deposit = change_into_integer(find('.amount').text)
    end
    
    @price = change_into_integer(find('.js-iv-review__deposit').text)
    @count_process = @deposit - @price
    @current_deposit = add_delimiter(@count_process)

    within('.form-field-item.payment-info-content__deposit__password') do
      self.deposit_password.set(password)
    end
    
    self.button_pay.click    
  end

  def choose_bukadompet_unpassword
    @price = change_into_integer(find('.js-iv-review__deposit').text)    
    self.button_pay_using_tfa.click
    return @price
  end

  def choose_bukadompet_unpassword_ts
    @price = change_into_integer(find('.js-iv-review__deposit').text)    
    self.button_pay_using_tfa_ts.click
    return @price
  end

  def choose_bukadompet_voucher(password)
    self.button_bukadompet.click
   
    within('.form-field-item.payment-info-content__deposit__password') do
      self.deposit_password.set(password)
    end
    
    self.button_pay.click    
  end

  def choose_bukadompet_after_otp
  	self.button_pay_after_otp.click
    #@price = change_into_integer(find('.js-iv-review__deposit').text)    
  end

  def fill_data_credit_card(cc_number, cc_cvv)
    self.cc_number.set(cc_number)
    self.cc_cvv.set(cc_cvv)    
  end

  def fill_data_cc_staging(cc_number, cc_cvv, res)
    self.cc_number.set(cc_number)
    self.cc_cvv.set(cc_cvv)
    self.month_expiry_cc.click
    self.choose_month_cc.click
    self.year_expiry_cc.click
    self.choose_year_cc.click
    self.pay_cc.click
    self.wait_until_input_res_cc_visible
    self.input_res_cc.set(res)
    self.wait_until_ok_button_cc.click_visible
    self.ok_button_cc.click    
  end
end
