# @electricity_new_page
# @vp_new_page
class ElectricityNew < SitePrism::Page
  DEPOSIT_DESC_SECTION = lambda do |s|
    s = "#{s}/" unless s.nil?
    "//*[contains(@class, 'payment-method__option--deposit')]" \
    "/div[contains(@class, 'active')]/#{s}div[contains(@class, 'active')]"
  end

  element :deposit_list_item,     "input[value='deposit']"
  element :atm_list_item,         "input[value='atm']"
  element :creditcard_radio,      "input[value='credit_card']"
  element :bca_klikpay_list_item, "input[value='bca_klikpay']"
  element :mandiriclickpay_radio, "input[value='mandiri_clickpay']"
  element :mandiriecash_radio,    "input[value='mandiri_ecash']"
  element :cimbclicks_radio,      "input[value='cimb_clicks']"
  element :indomaret_list_item,   "input[value='indomaret']"
  element :alfamart_list_item,    "input[value='alfamart']"
  element :kredivo_radio, :xpath, "input[value='kredivo']"

  element :insufficient_deposit_section,
    :xpath, DEPOSIT_DESC_SECTION.call(nil)
  element :sufficient_deposit_section,
    :xpath, DEPOSIT_DESC_SECTION.call("div")
  element :deposit_error_notification, :xpath, "//*[@class='msg']"

  element :deposit_password_field, "#js-iv-deposit"
  element :deposit_payment_button, :xpath, "//*[@class='js-payment-deposit-method']/button[2]"
  element :normal_payment_button,  :xpath, "//*[@class='js-payment-normal-method']/button"

  element :button_pay, :xpath, "//button[text()='Bayar']"
  

  def pick_payment_method(pm)
    case pm
    when "alfamart" then self.alfamart_list_item.click
    when "bukadompet" then self.deposit_list_item.click
    when "bca klikpay" then self.bca_klikpay_list_item.click
    when "indomaret" then self.indomaret_list_item.click
    when "transfer" then self.atm_list_item.click
    end
  end

  def alfamart_payment_channel
    self.wait_for_alfamart_list_item(30)
    self.alfamart_list_item.click
    self.button_pay.click
    sleep 4
  end

  def indomaret_payment_channel
    self.wait_for_indomaret_list_item
    self.indomaret_list_item.click
    self.button_pay.click
    sleep 4
  end

  def atm_payment_channel
    self.wait_for_atm_list_item
    self.atm_list_item.click
    self.button_pay.click
    sleep 4
  end

  def bukadompet_payment_channel
    self.wait_for_deposit_list_item
    self.deposit_list_item.click
    fill_in 'deposit_password', :with => ENV['BUYER_PASSWORD']
    self.button_pay.click
    sleep 4
  end

  def klikpay_payment_channel
    self.wait_for_bca_klikpay_list_item
    self.bca_klikpay_list_item.click
    self.button_pay.click
    sleep 4
  end

end
