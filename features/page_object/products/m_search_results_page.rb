class MobileSearchResultsPage < SitePrism::Page

  elements :promoted_push_top_product_names,           ".endorsed-products .product__name"
  element  :promoted_push_top_product_section,         "#page > section:nth-child(1) > ul:nth-child(2)"
  element  :search_bar,                                '.search-bar__input'
  element  :search_box,                                '#search-form-new-input'
  elements :product_names,                             '.product__name'


  def search_product(keyword)
    visit '/'
    dismiss_one_signal_popup
    self.wait_until_search_bar_visible
    self.search_bar.click
    find('#search-form-new-input').send_keys(keyword, :enter)
    self.wait_until_product_names_visible(3)
  end

  def verify_search_result(result)
    self.product_names.each do |search_result|
      productName=search_result.text()
      productName=productName.downcase
      puts productName
      is_true=productName.include? "#{result}"
      is_true.should==true
    end
  end

end
