class SearchResultsPage < SitePrism::Page

  elements :promoted_push_top_product_names,   ".endorsed-products .product__name"
  element  :promoted_push_top_product_section, "[data-sem-section*=' top'].endorsed-products--sem"
  element  :search_box,                        '#search_keywords'
  element  :keyword_title,                     '.key-page-title'

  def search_product(keyword)
    visit_split_true '/'
    dismiss_one_signal_popup
    self.wait_until_search_box_visible(3)
    find('#search_keywords').send_keys(keyword, :enter)
    self.wait_until_keyword_title_visible(8)
  end

  def verify_search_result(result)
    keyword_result=self.keyword_title.text
    is_true=keyword_result.include? "#{result}"
    is_true.should==true
  end

end
