class MobileProductsNewPage < SitePrism::Page
  set_url("/products/new")

  element :product_name_field,                           "#product_name"
  element :set_own_category_button, :button,             "Atur Kategori Sendiri"
  element :first_category_dropdown,                      "#first_category select"
  element :second_category_dropdown,                     "#second_category select"
  element :third_category_dropdown,                      "#third_category select"
  element :new_item_radio_button,                        "#product_new_true"
  element :old_item_radio_button,                        "#product_new_false"
  element :product_weight_field,                         "#product_weight"
  element :product_stock_field,                          "#product_stock"
  element :product_price_field,                          "#product_price"
  element :product_description_field,                    "#product_description_bb"
  element :save_button, :button,                         "Simpan"

  def upload_image
    page.driver.browser.all(:xpath, '//input[@name="product_images_upload[data]"]')[0].send_key(File.absolute_path ('./features/support/image/tutup botol.jpg'))
  end

  def select_category(first_category, second_category, third_category)
    self.first_category_dropdown.select(first_category)
    self.second_category_dropdown.select(second_category)
    self.third_category_dropdown.select(third_category)
  end

  def upload_new_product(product_name, first_category, second_category, third_category, weight, stock, price, description)
    self.product_name_field.set(product_name)
    self.wait_for_set_own_category_button
    self.set_own_category_button.click
    self.select_category(first_category, second_category, third_category)
    scroll_down(500)
    self.new_item_radio_button.click
    self.product_weight_field.set(weight)
    self.product_stock_field.set(stock)
    self.product_price_field.set(price)
    self.upload_image
    self.product_description_field.set(description)
    self.save_button.click
    modal_accept_confirm
  end
end
