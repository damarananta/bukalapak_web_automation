class ProductsNewPage < SitePrism::Page
  path = "/products/new"
  set_url(ENV['BASE_URL'] + path)

  element :product_name_field,                           "#product_name"
  element :suggested_category_radio_button, :xpath,      "//*[@id='category_suggestion']/div/div/div[2]/div/fieldset/div"
  element :set_own_category_button, :button,             "Atur Kategori Sendiri"
  element :first_category_dropdown, :xpath,              "//div[contains(@class, 'qa-first-category')]"
  element :second_category_dropdown, :xpath,             "//div[contains(@class, 'qa-second-category')]"
  element :third_category_dropdown, :xpath,              "//div[contains(@class, 'qa-third-category')]"
  element :new_item_radio_button,                        "#product_new_true"
  element :old_item_radio_button,                        "#product_new_false"
  element :product_weight_field,                         "#product_weight"
  element :product_stock_field,                          "#product_stock"
  element :product_price_field,                          "#product_price"
  element :product_description_field,                    "#product_description_bb"
  element :save_button, :button,                         "Simpan"

  def upload_image
    page.driver.browser.all(:xpath, '//input[@name="product_images_upload[data]"]')[0].send_key(File.absolute_path ('./features/support/image/tutup botol.jpg'))
  end

  def select_category(first_category, second_category, third_category)
    self.first_category_dropdown.click
    find(:xpath, "//li[text()='#{first_category}']").click
    self.second_category_dropdown.click
    scroll_down(200)
    find(:xpath, "//li[text()='#{second_category}']").click
    self.third_category_dropdown.click
    find(:xpath, "//li[text()='#{third_category}']").click
  end

  def upload_new_product(product_name, first_category, second_category, third_category, weight, stock, price, description)
    self.product_name_field.set(product_name)
    self.wait_for_set_own_category_button
    self.set_own_category_button.click
    self.select_category(first_category, second_category, third_category)
    self.new_item_radio_button.click
    self.product_weight_field.set(weight)
    self.product_stock_field.set(stock)
    self.product_price_field.set(price)
    self.upload_image
    self.product_description_field.set(description)
    self.save_button.click
  end
end
