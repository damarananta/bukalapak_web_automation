class BeliPaketPushPage < SitePrism::Page
  path = "/push_package_histories/new"
  set_url(ENV['BASE_URL'] + path)

  element :saldo_push_text,                        ".qa-push-balance"
  element :saldo_bukadompet_text,                  "#current-user-balance .amount"
  element :package_price_text,                     ".u-txt-price .amount"
  element :package_active_until_date_text,         "dd.c-deflist__value:nth-child(6) > strong:nth-child(1)"
  element :beli_paket_push_button,         :xpath, "//*[normalize-space(text())='Beli Paket Push']"
  element :push_info_section,                      ".c-panel__head"

  def get_pilih_push_package_button(total_push)
    return find(:css, ".qa-buy-push-#{total_push}")
  end

  def get_bukadompet_balance
    return change_into_integer(self.saldo_bukadompet_text.text)
  end

  def get_push_balance
    return self.saldo_push_text.text.to_i
  end

end
