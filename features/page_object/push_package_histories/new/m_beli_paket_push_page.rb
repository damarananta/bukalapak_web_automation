class MobileBeliPaketPushPage < SitePrism::Page
  path = "/push_package_histories/new"
  set_url(ENV['MOBILE_URL'] + path)

  element :saldo_bukadompet_text, "#current-user-balance .amount"
  element :beli_button,           "input[value=Beli]"
  element :page_notif_section,    ".page-notif"

  def get_push_package_radiobutton(total_push)
    return find("input#name_#{total_push}")
  end

  def get_push_package_price(total_push)
    if (page.has_css?("div[for=name_#{total_push}].push__price .push__price--reduced .amount"))
      return change_into_integer(find("div[for=name_#{total_push}].push__price .push__price--reduced .amount").text)
    else
      return change_into_integer(find("div[for=name_#{total_push}].push__price .amount").text)
    end
  end

  def get_push_package_active_until(total_push)
    return find("[for=name_#{total_push}].push__expiry").text.gsub(/.+:/, "").strip
  end

end
