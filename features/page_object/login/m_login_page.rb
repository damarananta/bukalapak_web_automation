class MobileLoginPage < SitePrism::Page
  path = "/login"
  set_url(ENV['BASE_URL'] + path)
  element :username_field,        "input[name='user_session[username]']"
  element :password_field,        "input[name='user_session[password]']"
  element :login_button,          ".js-btn-menu-login"
  element :flash_message,        ".flash-container.js-flash-container"

  def dismiss_one_signal_popup
    click_button("No Thanks") if page.has_css?(".onesignal-popover-dialog")
  end

  def login(username, password)
    self.dismiss_one_signal_popup
    self.username_field.set(username)
    self.password_field.set(password)
    page.execute_script("window.scrollBy(0,200)")
    self.login_button.click
    sleep 3
    self.wait_for_flash_message(10)
  end
end
