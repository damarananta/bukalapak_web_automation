class LoginPage < SitePrism::Page
  path = "/login"
  set_url(ENV['BASE_URL'] + path)
  element :username_field,        "input[name='user_session[username]']"
  element :password_field,        "input[name='user_session[password]']"
  element :login_button, :xpath,  "//input[@value='Login']"
  element :username_link,         ".c-nav-list__link.u-fg--yellow"
  element :form_login,            '.js-form-login'


  def dismiss_one_signal_popup
    find(".onesignal-popover-cancel-button").click if page.has_css?(".onesignal-popover-cancel-button")
  end

  def login(username, password)
    self.dismiss_one_signal_popup
    self.username_field.set(username)
    self.password_field.set(password)
    self.login_button.click
    self.wait_for_username_link(10)
  end

  #verify user should redirect to login form
  def verify_login_form
     expect(self).to have_form_login
  end
end
