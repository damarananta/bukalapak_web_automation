# @game_voucher_page
class MobileGameVoucherPage < SitePrism::Page
  path = "/voucher-game"
  set_url(ENV['MOBILE_URL'] + path)

  elements :publishers, ".product-media__img"
  
end
