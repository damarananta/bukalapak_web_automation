class ProductKeywordsNewPage < SitePrism::Page
  path = "/fraud/product_keywords/new"
  set_url(ENV['BASE_URL'] + path)

  element :context_field,       "input[name='fraud_product_keyword[context]']"
  element :save_button, :xpath, "//input[@value='Save']"
  element :keywords_field,      "input[name='fraud_product_keyword[keyword]']"

  element :category_field, :xpath,      "//select[@name='fraud_product_keyword[category]']/option[@value='4']"
  element :minimal_price_field, "input[name='fraud_product_keyword[minimal_price]']"
  element :maximum_price_field, "input[name='fraud_product_keyword[maximum_price]']"
  element :action_field, :xpath,       "//select[@name='fraud_product_keyword[action]']/option[@value='report']"
  element :reason,              "input[id='fraud_product_keyword_action_params_bad_description']"        

  def add_suspect_keyword(context,key,min,max)
    self.context_field.set(context,)
    self.keywords_field.set(key)
    sleep 2
    page.execute_script("window.scrollBy(0,100)")
    self.category_field.click
    
    sleep 2
    self.minimal_price_field.set(min)
    self.maximum_price_field  .set(max)
    page.execute_script("window.scrollBy(0,120)")
    sleep 2
    self.action_field.click
    self.reason.click
    self.save_button.click
  end
end
