class GreyKeywordsNewPage < SitePrism::Page
  path = "/fraud/grey_keywords/new"
  set_url(ENV['BASE_URL'] + path)

  element :grey_keyword_field,         "input[name='fraud_grey_keyword[keyword]'"
  element :add_keyword_button, :xpath, "//input[@value='Buat Grey keyword']"

  def add_grey_keyword(keyword)
  	@keyword = keyword + Random.new.rand(10000).to_s 
    self.grey_keyword_field.set(@keyword)
    self.add_keyword_button.click
  end
end
