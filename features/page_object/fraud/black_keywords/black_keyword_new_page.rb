class BlackKeywordNewPage < SitePrism::Page
  path = "/fraud/black_keywords/new"
  set_url(ENV['BASE_URL'] + path)

  element :black_keyword_field,        "input[name='fraud_black_keyword[keyword]'"
  element :reason_field,               "input[name='fraud_black_keyword[reason]'"
  element :add_keyword_button, :xpath, "//input[@value='Buat Black keyword']"

  def add_blacklist_keyword(keyword,reason)
    @keyword = keyword + Random.new.rand(10000).to_s 
    self.black_keyword_field.set(@keyword)
    self.reason_field.set(reason)
    self.add_keyword_button.click
  end
end
