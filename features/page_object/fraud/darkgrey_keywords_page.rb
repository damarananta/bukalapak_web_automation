class DarkgreyKeywordsPage < SitePrism::Page
  path = "/fraud/darkgrey_keywords"
  set_url(ENV['BASE_URL'] + path)

  element :new_keyword_button, :xpath,     "//a[text()='New Keyword']"
  element :delete_keyword_button, :xpath,  "//a[text()='Delete']"

  def delete_darkgrey_keyword
    self.delete_keyword_button.click
    page.driver.browser.switch_to.alert.accept
  end
end
