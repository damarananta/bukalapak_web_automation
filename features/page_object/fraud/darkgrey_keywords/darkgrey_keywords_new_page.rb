class DarkgreyKeywordsNewPage < SitePrism::Page
  path = "/fraud/darkgrey_keywords/new"
  set_url(ENV['BASE_URL'] + path)

  element :darkgrey_keyword_field,     "input[name='fraud_darkgrey_keyword[keyword]'"
  element :reason_field, :xpath,       "//div/*[@class='controls']/select/option[@value='6']"
  element :add_keyword_button,  ".c-btn.c-btn--green.js-dashed-filter-description-form__submit"

  def add_darkgrey_keyword(keyword)
    @keyword = keyword + Random.new.rand(10000).to_s 
    self.darkgrey_keyword_field.set(@keyword)
    self.reason_field.click
    self.add_keyword_button.click
  end
end
