class ProductKeywordsPage < SitePrism::Page
  path = "/fraud/product_keywords"
  set_url(ENV['BASE_URL'] + path)

  element :new_keyword_button, :xpath,     "//a[text()='New Keyword']"
  element :delete_keyword_button, :xpath,  "//a[text()='Deactivate']"

  def delete_black_keyword
    self.delete_keyword_button.click
    page.driver.browser.switch_to.alert.accept
  end
end
