class MobileBarangDijualPage < SitePrism::Page
  path = "/my_products/stocked"
  set_url(ENV['MOBILE_URL'] + path)

  elements :product_names,                                "a.product__name"
  element  :promoted_push_button,                 :xpath, "//*[normalize-space(text()) = 'Promoted Push']"
  elements :pengaturan_barang_buttons,                    "a.product-actions__drawer-toggle"
  element  :cost_per_click_field,                         "input#product_bidding_bid_value"
  element  :active_date_start_day_select,                 "select#product_bidding_sem_start_3i"
  element  :active_date_start_month_select,               "select#product_bidding_sem_start_2i"
  element  :active_date_start_year_select,                "select#product_bidding_sem_start_1i"
  element  :active_date_end_day_select,                   "select#product_bidding_sem_end_3i"
  element  :active_date_end_month_select,                 "select#product_bidding_sem_end_2i"
  element  :active_date_end_year_select,                  "select#product_bidding_sem_end_1i"
  element  :active_promoted_push_radio_button,    :xpath, "//*[normalize-space(text()) = 'Aktif']"
  element  :nonactive_promoted_push_radio_button, :xpath, "//*[normalize-space(text()) = 'Nonaktif']"
  element  :save_promoted_push_setting_button,    :xpath, "//*[@value='Simpan']"
  element  :flash_message,                                ".flash__text"
  element  :agreement_text,                               ".js-product-bid__agreement"
  element  :cari_barang_field,                            "#search_keywords"
  element  :search_button,                                ".entry-nav__search-btn"
  element  :pengaturan_barang_button,                     ".product-actions__drawer-toggle"


  FLASH_MESSAGE_SUCCESS_ACTIVE_PROMOTED_PUSH    = "Promoted Push diaktifkan"
  FLASH_MESSAGE_SUCCESS_NONACTIVE_PROMOTED_PUSH = "Promoted Push dinonaktifkan"

  def click_promoted_push_on_a_product(a_product_name)
    self.product_names.each_with_index do |product_name, index|
      if (product_name.has_text?(a_product_name))
        scroll_to(product_name, true)
        sleep 1
        self.pengaturan_barang_buttons[index].click
        self.promoted_push_button.click
      end
    end
  end

  def set_promoted_push_product_active_for_today(cost_per_click)
    self.cost_per_click_field.set(cost_per_click)
    now = Time.now
    
    # fill active start date
    self.active_date_start_day_select.select("#{now.day}")
    self.active_date_start_month_select.find("option[value='#{now.month}']").click
    self.active_date_start_year_select.select("#{now.year}")
    
    # fill active end date
    self.active_date_end_day_select.select("#{now.day}")
    self.active_date_end_month_select.find("option[value='#{now.month}']").click
    self.active_date_end_year_select.select("#{now.year}")

    self.active_promoted_push_radio_button.click
    self.wait_until_agreement_text_visible
    self.save_promoted_push_setting_button.click
  end

  def set_promoted_push_product_nonactive
    self.nonactive_promoted_push_radio_button.click
    self.wait_until_agreement_text_invisible
    self.save_promoted_push_setting_button.click
  end

end
