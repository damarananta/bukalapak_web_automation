class BarangDijualPage < SitePrism::Page
  path = "/my_products/stocked"
  set_url(ENV['BASE_URL'] + path)

  elements :product_names,                                "a.product__name"
  elements :promoted_push_buttons,                        "a.cbox-bid"
  element  :cost_per_click_field,                         "input#product_bidding_bid_value"
  element  :active_promoted_push_radio_button,    :xpath, "//*[normalize-space(text()) = 'Aktif']"
  element  :nonactive_promoted_push_radio_button, :xpath, "//*[normalize-space(text()) = 'Nonaktif']"
  element  :active_date_start_day_section,                "div#product_bidding_sem_start_3i_chzn"
  element  :active_date_start_month_section,              "div#product_bidding_sem_start_2i_chzn"
  element  :active_date_start_year_section,               "div#product_bidding_sem_start_1i_chzn"
  element  :active_date_end_day_section,                  "div#product_bidding_sem_end_3i_chzn"
  element  :active_date_end_month_section,                "div#product_bidding_sem_end_2i_chzn"
  element  :active_date_end_year_section,                 "div#product_bidding_sem_end_1i_chzn"
  element  :save_promoted_push_setting_button,    :xpath, "//*[@value='Simpan']"
  element  :flash_message,                                ".flash__text"
  element  :tambah_budget_text,                   :xpath, "//*[normalize-space(text()) = 'Tambah Budget Promoted Push']"
  element  :agreement_text,                               ".js-product-bid__agreement"
  element  :check_all_products_checkbox,                  "input#check_all"
  element  :product_action_combobox,                      "div#toggler_select_action_chzn"
  element  :push_combobox_option,                 :xpath, "//li[normalize-space(text()) = 'Push']"
  element  :product_count_text,                           "#mass_push_notice strong.product-count"
  element  :remaining_push_text,                          "#mass_push_notice strong.product-count + br + strong"
  element  :push_button_on_popup_button,                  "button#push-button-continue"
  element  :push_popup_section,                           "#mass_push_notice"
  element  :amount_negative_text,                         ".amount.negative"

  ACTIVE_DATE_START_DAY_SELECT_ID_PREFIX        = "product_bidding_sem_start_3i_chzn_o_"
  ACTIVE_DATE_START_MONTH_SELECT_ID_PREFIX      = "product_bidding_sem_start_2i_chzn_o_"
  ACTIVE_DATE_END_DAY_SELECT_ID_PREFIX          = "product_bidding_sem_end_3i_chzn_o_"
  ACTIVE_DATE_END_MONTH_SELECT_ID_PREFIX        = "product_bidding_sem_end_2i_chzn_o_"
  FLASH_MESSAGE_SUCCESS_ACTIVE_PROMOTED_PUSH    = "Promoted Push diaktifkan"
  FLASH_MESSAGE_SUCCESS_NONACTIVE_PROMOTED_PUSH = "Promoted Push dinonaktifkan"

  def click_promoted_push_on_a_product(a_product_name)
    product_index = 0
    self.product_names.each_with_index do |product_name, index|
      if (product_name.has_text?(a_product_name))
        scroll_to(product_name, true)
        self.promoted_push_buttons[index].click
        product_index = index
      end
    end
    product_index
  end

  def set_promoted_push_product_active_for_today(cost_per_click)
    self.cost_per_click_field.set(cost_per_click)
    now = Time.now
    
    # fill active start date
    scroll_to(self.tambah_budget_text, true)
    self.active_date_start_day_section.click
    find("##{ACTIVE_DATE_START_DAY_SELECT_ID_PREFIX}#{now.day-1}").click
    scroll_to(self.tambah_budget_text, true)
    self.active_date_start_month_section.click
    find("##{ACTIVE_DATE_START_MONTH_SELECT_ID_PREFIX}#{now.month-1}").click
    self.active_date_start_year_section.click
    find(:xpath, "//li[text()=#{now.year}]").click
    
    # fill active end date
    self.active_date_end_day_section.click
    find("##{ACTIVE_DATE_END_DAY_SELECT_ID_PREFIX}#{now.day-1}").click
    scroll_to(self.tambah_budget_text, true)
    self.active_date_end_month_section.click
    find("##{ACTIVE_DATE_END_MONTH_SELECT_ID_PREFIX}#{now.month-1}").click
    self.active_date_end_year_section.click
    find(:xpath, "//li[text()=#{now.year}]").click

    self.active_promoted_push_radio_button.click
    self.wait_until_agreement_text_visible
    self.save_promoted_push_setting_button.click
  end

  def set_promoted_push_product_nonactive
    self.nonactive_promoted_push_radio_button.click
    self.wait_until_agreement_text_invisible
    self.save_promoted_push_setting_button.click
  end

end
