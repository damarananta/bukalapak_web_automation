class PoliciesPage < SitePrism::Page
  path = "/policies"
  set_url(ENV['BASE_URL'] + path)

  element :search_field,            "input[name='search[keywords]']"
  element :search_button,   :xpath, "//input[@value='Cari']"
  element :arrow,           :xpath, "//div/*[@id='search_field_chzn']/a/div/b"
  element :dropdown_field,  :xpath, "//div/*[@id='search_field_chzn']/div/ul/li[text()='name']"
  element :reported_button, :xpath, "//div/*[@class='policy__actions']/a[text()='Laporkan']"
  element :unreported_button, :xpath, "//div/*[@class='policy__actions']/a[text()='Unhide Barang']"
  element :reason_field,            "input[id='product_product_sin_attributes_bad_description']"
  element :lapor_button,    :xpath, "//input[@value='lapor']"

  def search_product(nama)
    self.search_field.set(nama)
    self.arrow.click
    self.dropdown_field.click
    sleep 2
    self.search_button.click
    sleep 4
  end

  def reported_action
    page.execute_script "window.scrollBy(0,500)"
    self.reported_button.click
    self.reason_field.click
    self.lapor_button.click
    sleep 4
  end

  def unreported_action
    page.execute_script "window.scrollBy(0,500)"
    self.unreported_button.click
    sleep 2
  end
end
