class PremiumPackagesAvailablePage < SitePrism::Page
  path = "/premium/packages/available"
  set_url(ENV["BASE_URL"] + path)

  element :subscribe_basic_button,          :xpath, "(//*[contains(@class,'qa-premium-subscribe--basic')])[1]"
  element :subscribe_pro_button,            :xpath, "(//*[contains(@class,'qa-premium-subscribe--professional')])[1]"
  element :one_month_period_radiobutton,    :xpath, "//*[contains(@class,'pricing-period__option')][1]"
  element :six_month_period_radiobutton,    :xpath, "//*[contains(@class,'pricing-period__option')][2]"
  element :twelve_month_period_radiobutton, :xpath, "//*[contains(@class,'pricing-period__option')][3]"

end
