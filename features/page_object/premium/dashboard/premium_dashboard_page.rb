class PremiumDashboardPage < SitePrism::Page
  path = "/premium/dashboard"
  set_url(ENV["BASE_URL"] + path)

  element :package_name_text,                          ".premium-color"
  element :subscription_auto_extend_date_text, :xpath, "//div[@class='premium-content']/p"

end
