
class PromoPage < SitePrism::Page
  path = "/promo"
  set_url(ENV['SMOKE_TESTING_URL'] + path)
  #button
  PROMO_IMAGE='.js-lazy-load'
  element :sample_promo,:xpath, ".//*[@id='normal_page']/section/div/div[3]/p/a/img"

  def visit_sample_promo
    visit '/promo'
    accept_onboarding
    self.wait_for_sample_promo
    page.execute_script "window.scrollBy(0,400)"
    first(PROMO_IMAGE).click
  end
end
