class MobilePromoPage < SitePrism::Page
  path = "/cart/carts"
  set_url('https://m.bukalapak.com'+ path)
  #button
  element :sample_promo,:xpath, ".//*[@id='campaign-page']/div/div[4]/p[1]/a/img"

  def visit_sample_promo
    visit '/promo'
    accept_onboarding
    self.wait_for_sample_promo
    page.execute_script "window.scrollBy(0,400)"
    self.sample_promo.click
    sleep 1
  end

end
