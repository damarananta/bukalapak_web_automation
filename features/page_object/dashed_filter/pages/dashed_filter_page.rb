class DashedFilterPage < SitePrism::Page
  path = "/dashed_filter/pages"
  set_url(ENV['BASE_URL'] + path)

  DELETE_DASHED_FILTER_BUTTON='.u-float-right.u-mrgn-right--2.c-btn.c-btn--tiny.c-btn--red'
  EDIT_DASHED_FILTER_BUTTON='.//td[8]/a[1]'
  PATH_IMAGE='./features/support/image/'
  COMBINATION_NAME_EXP='Jual Beli Handphone (HP) Acer Baru Jakarta Selatan dari Murah hanya di Bukalapak'
  COMBINATION_META_DESC_EXP='Jual beli Handphone (HP) Acer. Tersedia Acer Baru Murah aman dan mudah di Jakarta Selatan, jaminan uang kembali 100% di Bukalapak.'

  element :add_dashed_filter_button,       ".c-btn.c-btn--tiny.c-btn--green"
  element :save_dashed_filter_button,      ".c-btn.c-btn--green"
  element :save_dashed_filter_page_button, ".c-btn.c-btn--green.js-dashed-filter-description-form__submit"
  element :dashed_filter_button,           ".c-btn.c-btn--small"
  element :form_field,                     '.c-fld__row'

  element :filter_general_url,:xpath,      '//div[2]/input'
  element :filter_condition,:xpath,        '//div[4]/input'
  element :filter_price,:xpath,            '//div[6]/input'
  element :filter_location,:xpath,         '//div[8]/input'
  element :filter_merk,:xpath,             '//div[10]/input'

  element :category_list_name,:xpath,      '//tr[1]/td[1]'
  element :condition_list_name,:xpath,     '//tr[1]/td[2]'
  element :price_list_name,:xpath,         '//tr[1]/td[3]'
  element :location_list_name,:xpath,      '//tr[1]/td[4]'
  element :merk_list_name,:xpath,          '//tr[1]/td[5]'

  element :category_field,                 '.chzn-single.chzn-default'
  element :price_page,                     '#dashed_filter_page_price'
  element :location_page,                  '#dashed_filter_page_location'
  element :brand_page,                     '#dashed_filter_page_brand'
  element :attach_banner_page,             '#dashed_filter_page_banner'
  element :meta_desc_page,                 '#dashed_filter_page_meta_description'
  element :desc_page,                      '#dashed_filter_page_description'

  element :flash_success,                  '.flash__text'
  element :url_dash_filter_list,:xpath,    '//tr[1]/td[6]'
  element :description_detail_text,        '.bhlm-description'

  def search_dashed_filter(category,condition,price,location,merk)
    self.filter_general_url.set("#{category}")
    self.filter_condition.set("#{condition}")
    self.filter_price.set("#{price}")
    self.filter_location.set("#{location}")
    self.filter_merk.set("#{merk}")
  end

  def verify_search_result(category,condition,price,location,merk)
    category_result=self.category_list_name.text
    category_result.should==category
    condition_result=self.condition_list_name.text
    condition_result.should==condition
    price_result=self.price_list_name.text
    price_result.should==price
    location_result=self.location_list_name.text
    location_result.should==location
    merk_result=self.merk_list_name.text
    merk_result.should==merk
    @listCount = page.all(DELETE_DASHED_FILTER_BUTTON).count
    @listCount.should==1
  end

  def verify_success_message
    sleep 3
    self.wait_for_flash_success(10)
    flash_success=self.flash_success.text
    flash_success.should=='Berhasil'
  end

end
