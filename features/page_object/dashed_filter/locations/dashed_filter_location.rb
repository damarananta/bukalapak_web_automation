class DashedFilterLocationPage < SitePrism::Page
  path = "/dashed_filter/locations"
  set_url(ENV['BASE_URL'] + path)

  EDIT_BUTTON='//tr[1]/td[4]/a[1]'

  element :link_location,:xpath,     		    '//section/ul/li[3]/a'
  element :location_url,             		    '#dashed_filter_location_location_url'
  element :location_province,        		    '#dashed_filter_location_province'
  element :location_city,            		    '#dashed_filter_location_city'
  
  element :location_url_list,:xpath,        '//tbody/tr[1]/td[1]'
  element :location_province_list,:xpath,   '//tbody/tr[1]/td[2]'
  element :location_city_list,:xpath,       '//tbody/tr[1]/td[3]'

  def update_dashed_filter_location_should_success(url,province,city)
    @url_update=self.location_url_list.text
    @url_update.should==url
    @province_update=self.location_province_list.text
    @province_update.should==province
    @city_update=self.location_city_list.text
    @city_update.should==city
  end
  
end
