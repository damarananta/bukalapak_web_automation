class DashedFilterMerkPage < SitePrism::Page
  path = "/dashed_filter/brands"
  set_url(ENV['BASE_URL'] + path)

  EDIT_BUTTON='//tr[1]/td[3]/a[1]'

  element :link_merk,:xpath,         '//section/ul/li[2]/a'
  element :brand_url,                '#dashed_filter_brand_brand_url'
  element :brand_name,               '#dashed_filter_brand_brand_name'
 
  element :brand_url_list,:xpath,    '//tbody/tr[1]/td[1]'
  element :brand_name_list,:xpath,   '//tbody/tr[1]/td[2]'

  def update_dashed_filter_merk_should_success(url,name)
    @url_update=self.brand_url_list.text
    @url_update.should==url
    @name_update=self.brand_name_list.text
    @name_update.should==name
  end

end
