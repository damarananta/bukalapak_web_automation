class DashedFilterPricePage < SitePrism::Page
  path = "/dashed_filter/prices"
  set_url(ENV['BASE_URL'] + path)

  EDIT_BUTTON='//tr[1]/td[4]/a[1]'

  element :link_price,:xpath,     		      '//section/ul/li[4]/a'
  element :price_url,             		      '#dashed_filter_price_price_url'
  element :price_price_min,        		      '#dashed_filter_price_price_min'
  element :price_price_max,            		  '#dashed_filter_price_price_max'

  element :price_url_list,:xpath,           '//tbody/tr[1]/td[1]'
  element :price_price_min_list,:xpath,     '//tbody/tr[1]/td[2]'
  element :price_price_max_list,:xpath,     '//tbody/tr[1]/td[3]'

  def update_dashed_filter_price_should_success(url,price_min,price_max)
    @url_update=self.price_url_list.text
    @url_update.should==url
    @price_min_update=self.price_price_min_list.text
    @price_min_update.should==price_min
    @price_max_update=self.price_price_max_list.text
    @price_max_update.should==price_max
  end
  
end
