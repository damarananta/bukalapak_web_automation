class MobileBookmarksPage < SitePrism::Page
  path = "/bookmarks"

  element  :favorite_count_dashboard_mweb,       "#page>h5"
  elements :favorites_product_homepage,:xpath,   '//section[11]//h3'

  def verify_favorite_count_homepage_mweb
    visit_split_true '/'
    accept_onboarding
    expect(self.favorites_product_homepage.size).to eq 2
  end

  def verify_favorite_count_should_decrease_mweb
    visit "/bookmarks"
    fav_count=favorite_count_dashboard_mweb.text
    fav_count=='1'
  end

  def verify_favorite_count_dashboard_mweb
    visit "/bookmarks"
    fav_count=favorite_count_dashboard_mweb.text
    fav_count=='2'
  end

  def verify_favorite_should_increase
    visit "/bookmarks"
    close_general_cart_popup
    fav_count=favorite_count_dashboard_mweb.text
    fav_count=='1'
  end

end
