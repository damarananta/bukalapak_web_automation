class BookmarksPage < SitePrism::Page
  path = "/bookmarks"
  set_url(ENV['BASE_URL'] + path)

  #constant
  BUTTON_SET_UNFAVORITE                           ='Set tidak favorit'
  CHECK_BOX_SET_UNFAVORITE                        = '.c-inp__inner-lbl'
  FAVORITE_EMPTY_TEXT                             ='Belum ada barang favorit'

  #button
  element :favorite_count_dashboard,              ".dotted-tooltip-link"
  element :favorite_empty_label,                  ".blank-slate.blank-slate--large>p"
  elements :favorites_product_homepage,:xpath,    ".//*[@id='main']/div/section/ul/li/div/article/div[3]/h3/a"

  def set_all_unfavorite
    visit "/bookmarks"
    if page.has_css?(CHECK_BOX_SET_UNFAVORITE)
      find(CHECK_BOX_SET_UNFAVORITE).click
      sleep 1
      click_button(BUTTON_SET_UNFAVORITE)
      sleep 3
    else
      puts "No favorite record"
    end
  end

  def favorite_list_should_empty
    fav_empty_text=self.favorite_empty_label.text
    fav_empty_text==FAVORITE_EMPTY_TEXT
  end

  def verify_favorite_count_homepage
    visit_split_true '/'
    expect(self.favorites_product_homepage.size).to eq 2
  end

  def verify_favorite_count_should_decrease
    visit "/bookmarks"
    fav_count=self.favorite_count_dashboard.text
    fav_count=='1'
  end

  def verify_favorite_count_dashboard
    visit "/bookmarks"
    fav_count=self.favorite_count_dashboard.text
    fav_count=='2'
  end

  def verify_favorite_should_increase
    visit "/bookmarks"
    fav_count=self.favorite_count_dashboard.text
    fav_count=='1'
  end

end
