class DiskonPage < SitePrism::Page

  element :search_box,:xpath,            '//div/*[@id="search_keywords"]'
  element :product_name,                 '.product__name'
  element :button_diskon_spesial,:xpath, '//*[@id="normal_page"]//div[1]/nav/div/a[1]'
  element :search_tooltip_result,        '#normal_page > nav > div > div > div:nth-child(1) > div > h1 > a'

end
