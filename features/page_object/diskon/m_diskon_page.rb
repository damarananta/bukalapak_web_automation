class MobileDiskonPage < SitePrism::Page

  element :page_tilte,:xpath,            '//*[@id="main"]/nav/div/h1'
  element :product_name,                 '.product__name'
  element :button_diskon_spesial,:xpath, '//*[@id="main"]/nav/div/div[1]/a[1]'
  element :button_diskon_hari_ini,:xpath,'//*[@id="main"]/nav/div/div[1]/a[2]'
  element :discount_badge,               '.product-discount-percentage-amount'
  element :reduce_price,:xpath,          '//*[@id="page"]/div/div[1]/ul/li//span[2]/span[2]'
  element :original_price,:xpath,        '//*[@id="page"]/div/div[1]/ul/li//span[1]/span[2]'
  element :card_product,:xpath,          '//*[@id="page"]/div/div[1]/ul/li//div[1]/a/picture'

end
