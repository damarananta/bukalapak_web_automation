class MobileElectricityPage < SitePrism::Page
  path = "/listrik-pln/token-listrik"
  set_url(ENV['MOBILE_URL'] + path)

  element :meter_number_field, "#js-vp-customer-no"
  element :beli_button, :xpath, "//*[@id='main']/div[1]/div[2]/form/div/div/input"
  
end
