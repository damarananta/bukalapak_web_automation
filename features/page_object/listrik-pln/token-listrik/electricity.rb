# @electricity_page
class VPElectricityLandingPage < SitePrism::Page
  require_relative "../../pulsa/sidebar_section.rb"

  set_url "/listrik-pln{/seo}{?query*}"

  section :tabs_section, SideBarSection, "#reskinned_page"

  element :onesignal_popover, ".onesignal-popover-dialog"
  
  element :customer_number_field, "#customer_number"

  element :box_customer_info, ".c-notification.c-notification--gray.c-notification--dashed.c-notification--block.u-mrgn-bottom--3"

  element :buy_button, ".c-btn.c-btn--green.c-btn--large.js-submit-button"

  def dismiss_one_signal_popup_homepage
    click_button("No Thanks") if self.has_onesignal_popover?
    sleep 2
  end

  def load_page(seo: "token-listrik", main_focus: true)
    query = {}

    self.load(seo: seo, query: query) do |page|
      page.dismiss_one_signal_popup_homepage
    end
    self.wait_for_tabs_section

    page.execute_script("window.scrollBy(0,400)") if main_focus
  end

  def load_old_url(seo: nil)
    visit("/vp/voucher/listrik")
  end
end
