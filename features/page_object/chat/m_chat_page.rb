class MobileChatPage < SitePrism::Page
  #general element
  CHAT_CONTENT                              ='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
  NEW_MESSAGE_NOTIF                         ='.m-icon.m-icon--textsms.user-panel__menu-link__icon.user-panel__menu-link__icon--has-new'
  ACCOUNT_LINK1                             ='.m-icon.m-icon--person'
  ACCOUNT_LINK2                             ='.misc-nav-item__count'

  element :account_link,                   '.misc-nav-item__link--userpanel'
  element :oke_onboarding_button,          '.btn.btn--red.search-bar-overlay__button.js-search-overlay-close'

  #chat element
  element :message_content,                '.js-char-counter--tobe-counted'
  element :send_button,                    '.btn.btn--medium.btn--red.btn--right'
  element :new_message_notif,              '.m-icon.m-icon--textsms.user-panel__menu-link__icon.user-panel__menu-link__icon--has-new'
  element :user_panel_info,                '.user-panel__userinfo'


  def create_chat(receiver)
    visit "/messages/#{receiver}"
    accept_onboarding
    sleep 2
    page.execute_script "window.scrollBy(0,400)"
    self.message_content.set(CHAT_CONTENT)
    sleep 4
    self.send_button.click
  end

  def set_read_all_chat(id_user)
    visit "messages/#{id_user}"
    sleep 1
  end

  def visit_user_panel
    self.wait_for_account_link(4)
    self.account_link.click
    self.wait_for_user_panel_info(3)
  end

end
