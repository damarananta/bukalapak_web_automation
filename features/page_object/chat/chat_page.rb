class ChatPage < SitePrism::Page

  CHAT_CONTENT=                         'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
  NEW_NOTIF_CHAT=                       '//*[@id="reskinned_page"]/section/div/nav/ul/li[18]/div/div/div'

  element :message_content,             '#new_messages_message_message'
  element :send_button,                 '.btn.btn--red.btn--large'
  element :new_notif_chat, :xpath,      '//*[@id="reskinned_page"]/section/div/nav/ul/li[18]/div/div/div'

  def create_chat(receiver)
    visit "/messages/#{receiver}"
    sleep 2
    page.execute_script "window.scrollBy(0,500)"
    self.message_content.set(CHAT_CONTENT)
    self.send_button.click
  end

  def set_read_all_chat(id_user)
    visit "messages/#{id_user}"
    sleep 1
  end

end
