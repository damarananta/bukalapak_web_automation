class MobileRingkasanAkunPage < SitePrism::Page
  path = "/my_lapak"
  set_url(ENV['MOBILE_URL'] + path)

  element :push_balance_text,      ".summary--push-info h1"
  element :push_active_until_text, ".summary--push-info .text-green"

  def get_push_balance
    return self.push_balance_text.text.delete("Push ").to_i
  end

end
