class MobileProductDetailPage < SitePrism::Page
  TERM_NOTES                                        ="Catatan Pelapak tetap tunduk terhadap Aturan penggunaan Bukalapak."
  FORUM_INFO_TEXT                                   ="Dapatkan tips belanja dan informasi terkini seputar Bukalapak"

  element :button_buy, :xpath,                      "//button[contains(@class, 'qa-main-buy-button')]"
  element :breadcrumb,                              "#breadcrumb"
  element :product_name,                            ".product-detailed__name"
  element :product_category,                        ".qa-product-detail-category"
  element :product_weight,                          ".qa-product-detail-weight"
  element :product_stock,                           ".product-statistics__stock span.detail-stat__val"
  element :product_price,                           ".product-detailed-price span.amount.positive"
  element :product_description,                     ".product-tab__desc p"

  element :product_availability_label,              '.product__availability'
  #general element
  element :favorite_button,                         '.product-action-favorite'
  element :unfavorite_button,                       '.product-action-favorite--active'
  element :decrease_qty_btn,                        '.btn--decrease'
  element :increase_qty_btn,                        '.btn--increase'
  element :qty_buy_product,                         '.item-quantity'
  element :product_detail_image,:xpath,             '//div[1]/ul/li[2]/picture/img'
  element :product_stock_info,                      '.product-stock'
  element :beli_qty_label,                          '.product-buy-input label'
  element :barang_qty_label,                        '.product-buy-input'

  element :laporkan_link,                           ".product-action--report.cbox-report"
  element :bottom_beli_sekarang_button,:xpath,      '//*[@id="mod-product-detail-1"]//form/p/input'
  element :anda_berminat_text,:xpath,               '//section/div/form/p'

  #free shipping
  element :free_shipping_label,:xpath,              "//div[1]/div[contains(@class, 'product-delivery')]/span[1]"
  element :free_shipping_tooltip,:xpath,            "//div[5]//span[contains(@class, 'tooltipstered')]"
  element :free_shipping_tooltip_content,           '.tooltipster-content'
  element :gratis_ongkos_kirim_label,               '.product-det-stat__free-shipping h3.header__title'
  element :gratis_ongkos_kirim_value,               '.product-det-stat__free-shipping p.product-det__cities'

  #product with rating/review
  element :jumlah_ulasan,                           ".product-detailed__rating a"
  element :rating_icon_main,                        ".product-detailed__rating span span"
  element :ulasan_label,                            '.product-tab__review h3'
  element :rating_icon_review,:xpath,               '//div[8]/span/span'
  element :rating_icon_review_content, :xpath,      '//ul/li/article/span/span'
  element :review_content,                          '.review-body'
  element :reviewer_name,                           '.review-sender__name'
  element :review_date,                             '.review-meta__pubdate'

  #product with feedback
  element :feedback_sender,                         '.user-feedback-sender'
  element :user_feedback_content,                   '.user-feedback-content-body blockquote p'

  #installment price >= Rp.500.00
  element :intallment_percentage,                   '.product-detailed-price__installment-percentage'
  element :detail_installment_label,                '.product-det-stat__installment h3.header__title'
  elements :cicilan_label,                          '.product-detailed__installment dl dt span'

  #premium seller
  element :pelapak_premium_label,                   '.premium__text span'

  #product discount
  element :original_price,                          '.product-detailed-price__original'
  element :reduce_price,                            '.product-detailed-price__reduced'
  element :discount_badge,                          '.product-detailed-discount-badge--active'

  #wholesale product
  element :detail_harga_grosir_label,               '.product-det-stat__product-detailed h3.header__title'
  element :qty_wholesale_product,                   '.product-detailed__wholesales span'
  element :price_wholesale_product,                 '.product-detailed__wholesales dd span span'

  ##Informasi
  element :stock_label,                             '.product-statistics__stock'
  element :terjual_label,                           ".product-statistics__sold"
  element :terjual_value,                           ".product-statistics__sold span.detail-stat__val"
  element :peminat_label,                           '.product-statistics__interest'
  element :peminat_value,                           '.product-statistics__interest span.detail-stat__val'
  element :dilihat_label,                           '.product-statistics__viewed'
  element :dilihat_value,                           '.product-statistics__viewed span.detail-stat__val'

  ##Spesifikasi
  element :spesifikasi_label,                       '.product-tab__spec h3'
  element :kategori_label, :xpath,                  '//dl/dt[1]'
  element :product_condition_icon,                  '//dl/dt[2]'
  element :product_condition_label, :xpath,         "//dl/dt[2]"
  element :product_condition_value,                 ".qa-product-detail-condition"
  element :berat_label, :xpath,                     '//dl/dt[3]'
  element :berat_value, :xpath,                     "//div[5]/div[2]/div/dl/dd[2]"

  # cek ongkos kirim
  element :ongkos_kirim_label,                      '.collapsible.collapse--shipping-estimation'
  element :pilih_lokasi_label,:xpath,               '//*[@id="product_shipping_est"]/p/strong'

  ## Masukkan jumlah
  element :masukkan_jumlah_value,                   '#payment_transaction_quantity'
  element :masukkan_jumlah_label,:xpath,            '//*[@id="product_shipping_est"]//label/strong'

  ## Masukkan tujuanwh
  element :province_address,                        '.product-shipping-dest__province'
  element :city_address,                            '.product-shipping-dest__city'
  element :area_address,                            '.product-shipping-dest__area'
  element :biaya_estimasi,                          '.product-shipping-costs'
  element :service_logo,:xpath,                     '//*[@id="product_shipping_est"]//img'
  element :service_value,                           '.product-shipping-estimation__service-name'
  element :waktu_pengiriman_value,                  '.product-shipping-estimation__service-eta'
  element :ongkos_kirim_value,                      '.product-shipping-estimation__fee'
  element :product_last_update,                     '.kvp--compact.kvp__key--i-updated'

  ##Deskripsi
  element :deskripsi_label,                         '.product-tab__desc h3'

  #selller note
  element :catatan_label,                           '.product-tab__term h3'
  element :terms_notes_text,:xpath,                 '//p[1]/small'
  element :notes_content,:xpath,                    '//div[7]/div/p'
  element :last_update_notes,:xpath,                '//p[2]/small'

  #social media icon
  element :fb_share_icon,                            '.facebook-button'
  element :twitter_share_icon,                       '.twitter-button'
  element :g_plus_share_icon,                        '.gplus-button'
  element :pinterest_share_icon,                     '.pin-button'

  #pelapak
  ##pelapak reputation
  element :pelapak_reputation,:xpath,                '//*[@id="mod-product-detail-1"]/div//a/span'
  element :jumlah_ulasan,:xpath,                     '//*[@id="mod-product-detail-1"]/div[1]//div[1]//div[2]/a'

  element :pelapak_title, :xpath,                    '//*[@id="mod-product-detail-1"]/div/div/h3'
  element :pelapak_name,                             '.user-description h5.user__name a'
  element :pelapak_location,                         '.user-location div.user-address'
  element :jasa_pengiriman_title,                    '.product-detailed-shipping h4'
  element :pelapak_pengiriman_header,:xpath,         '//table[1]/thead/tr/th[1]'
  element :pelapak_regular_header,:xpath,            '//table[1]/thead/tr/th[2]'
  element :pelapak_kilat_header,:xpath,              '//table[1]/thead/tr/th[3]'
  elements :pelapak_pengiriman_value,:xpath,         "//table[1]/tbody/tr/td[1]/img"
  elements :pelapak_regular_value,:xpath,            "//table[1]/tbody/tr/td[2]/em"
  elements :pelapak_kilat_value,:xpath,              "//table[1]/tbody/tr/td[3]/em"
  element :tinggalkan_pesan,                         '.user-detailed-chat a'
  element :huge_forum_icon,                          '.forum-referrer__icon'
  element :forum_info_text,                          '.forum-referrer__text'
  element :kunjungi_forum_button,                    '.forum-referrer__button a'

  def load_product(url)
    visit_url url
  end

  def click_beli
    neutralize_ab_test("&one_click_payment=normal&ab_test[btp_beli_button]=normal")
    self.button_buy.click
    sleep 2
  end

  def favorite
    page.execute_script "window.scrollBy(0,400)"
    self.favorite_button.click
    self.wait_until_unfavorite_button_visible(5)
  end

  def unfavorite
    page.execute_script "window.scrollBy(0,400)"
    self.unfavorite_button.click
    self.wait_until_favorite_button_visible(5)
  end

  def assert_uploaded_product(product_name, product_first_category, product_second_category, product_third_category, product_weight, product_stock, product_price, product_description)
    product_name = capitalize_each_word(product_name)
    product_weight = weight_normalizer(product_weight)
    product_stock = stock_normalizer(product_stock)
    product_price = price_delimiter(product_price)

    expect(self.product_name).to have_text(product_name)
    expect(self.product_category).to have_text(product_third_category)
    expect(self.product_weight).to have_text(product_weight)
    expect(self.product_stock).to have_text(product_stock)
    expect(self.product_price).to have_text(product_price)
    expect(self.product_description).to have_text(product_description)
  end

  def verify_basic_information_about_product
    expect(self).to have_product_stock
    expect(self).to have_favorite_button
    expect(self).to have_decrease_qty_btn
    expect(self).to have_increase_qty_btn
    expect(self).to have_qty_buy_product
    expect(self).to have_product_detail_image
    expect(self.laporkan_link).to have_text('Laporkan Barang')
    expect(self).to have_bottom_beli_sekarang_button
  end

  def verify_pelapak_info
    expect(self.pelapak_title).to have_text('Tentang Pelapak')
    expect(self).to have_pelapak_name
    expect(self).to have_pelapak_location
    expect(self.jasa_pengiriman_title).to have_text('JASA PENGIRIMAN')
    expect(self.pelapak_pengiriman_header).to have_text('PENGIRIMAN')
    expect(self.pelapak_regular_header).to have_text('REGULER')
    expect(self.pelapak_kilat_header).to have_text('KILAT')
    expect(self).to have_pelapak_pengiriman_value
    expect(self).to have_pelapak_regular_value
    expect(self).to have_pelapak_kilat_value
    expect(self.tinggalkan_pesan).to have_text('Tinggalkan Pesan')
    expect(self.forum_info_text).to have_text(FORUM_INFO_TEXT)
    self.huge_forum_icon.should be_visible
  end

  def verify_sosial_media_icon_list
    expect(self).to have_fb_share_icon
    expect(self).to have_twitter_share_icon
    expect(self).to have_g_plus_share_icon
    expect(self).to have_pinterest_share_icon
  end

  def verify_product_information
    expect(self.stock_label).to have_text("STOK")
    expect(self).to have_product_stock
    expect(self.terjual_label).to have_text("TERJUAL")
    expect(self).to have_terjual_value
    expect(self.dilihat_label).to have_text("DILIHAT")
    expect(self).to have_dilihat_value
    expect(self.peminat_label).to have_text("PEMINAT")
    expect(self).to have_peminat_value
    expect(self).to have_product_last_update
  end

  def verify_product_specification
    expect(self.spesifikasi_label).to have_text("Spesifikasi")
    expect(self.kategori_label).to have_text("Kategori")
    expect(self.berat_label).to have_text("Berat")
    expect(self.product_condition_label).to have_text("Kondisi")
    expect(self).to have_product_condition_value
    expect(self).to have_product_weight
    expect(self).to have_product_category
  end

  def verify_product_description
    expect(self.deskripsi_label).to have_text("Deskripsi")
    expect(self).to have_product_description
  end

  def verify_product_notes
    expect(self.catatan_label).to have_text("Catatan Pelapak")
    expect(self.terms_notes_text).to have_text(TERM_NOTES)
    expect(self).to have_notes_content
    expect(self).to have_last_update_notes
  end

  def verify_product_detail_discount_info
    expect(self).to have_original_price
    expect(self).to have_reduce_price
    expect(self).to have_discount_badge
  end

  def verify_product_detail_average_shipping_time_info
    expect(self).to have_waktu_kirim_tooltip
    self.waktu_kirim_tooltip.click
    self.wait_for_waktu_kirim_tooltip_content(3)
    expect(self.waktu_kirim_tooltip_content).to have_text(WAKTU_KIRIM_TOOLTIP_CONTENT)
  end

  def verify_product_detail_free_shipping_info
    expect(self.free_shipping_label).to have_text("GRATIS BIAYA KIRIM")
    self.free_shipping_tooltip.click
    self.wait_for_free_shipping_tooltip_content(3)
    expect(self.free_shipping_tooltip_content).to have_text("Khusus DKI Jakarta")
    expect(self.gratis_ongkos_kirim_label).to have_text("Gratis Ongkos Kirim")
    expect(self.gratis_ongkos_kirim_value).to have_text("DKI Jakarta")
  end

  def verify_pelapak_reputation_info
    expect(self).to have_pelapak_reputation
  end

  def verify_shipping_estimation_info
    expect(self.ongkos_kirim_label).to have_text("Cek Ongkos Kirim")
    expect(self.pilih_lokasi_label).to have_text("Pilih Lokasi")
    expect(self).to have_province_address
    expect(self).to have_city_address
    expect(self).to have_area_address
    expect(self.masukkan_jumlah_label).to have_text("Jumlah Barang")
    expect(self).to have_masukkan_jumlah_value
    expect(self.biaya_estimasi).to have_text("Biaya Kirim")
    expect(self).to have_service_logo
    expect(self).to have_service_value
    expect(self).to have_waktu_pengiriman_value
    expect(self).to have_ongkos_kirim_value
  end

  def verify_feedback_list
    expect(self).to have_feedback_sender
    expect(self).to have_user_feedback_content
  end

  def verify_review_list
    expect(self).to have_jumlah_ulasan
    expect(self).to have_rating_icon_main
    expect(self.ulasan_label).to have_text("Ulasan Barang")
    expect(self).to have_jumlah_ulasan
    expect(self).to have_rating_icon_review
    expect(self).to have_rating_icon_review_content
    expect(self).to have_reviewer_name
    expect(self).to have_review_date
  end

  def verify_product_detail_wholesale_info
    expect(self.detail_harga_grosir_label).to have_text("Harga Grosir")
    expect(self).to have_qty_wholesale_product
    expect(self).to have_price_wholesale_product
  end

  def verify_installment_info_price_500000
    expect(self.intallment_percentage).to have_text("Cicilan 0%")
    expect(self.detail_installment_label).to have_text("Cicilan 0%")
    expect(self).to have_cicilan_label
  end

  def verify_premium_seller_info
    expect(self.pelapak_premium_label).to have_text("PELAPAK PREMIUM")
  end

  def verify_product_availability(availability_label)
    expect(self.product_availability_label).to have_text(availability_label)
  end

  def load_product(url)
    visit_url url
  end

  def click_beli
    neutralize_ab_test("&one_click_payment=normal&ab_test[btp_beli_button]=normal")
    self.button_buy.click
    sleep 2
  end

  def assert_uploaded_product(product_name, product_first_category, product_second_category, product_third_category, product_weight, product_stock, product_price, product_description)
    product_weight = weight_normalizer(product_weight)
    product_price = price_delimiter(product_price)

    expect(self.product_name).to have_text(product_name)
    expect(self.product_category).to have_text(product_third_category)
    expect(self.product_weight).to have_text(product_weight)
    expect(self.product_stock).to have_text(product_stock)
    expect(self.product_price).to have_text(product_price)
    expect(self.product_description).to have_text(product_description)
  end
end
