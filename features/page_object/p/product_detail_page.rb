class ProductDetailPage < SitePrism::Page

  FLASH_MESSAGE_SUCCESS_PUSH_PRODUCT                ="Barang telah berhasil di-push"
  WAKTU_KIRIM_TOOLTIP_CONTENT                       ="Waktu rata-rata yang diperlukan pelapak untuk mengirim pesanan yang sudah terbayar, ke pihak ekspedisi/kurir"
  TERM_NOTES                                        ="Catatan Pelapak tetap tunduk terhadap Aturan penggunaan Bukalapak."
  JAMINAN_TEXT                                      ="Jaminan 100% Aman Uang pasti kembali. Sistem pembayaran bebas penipuan. Selengkapnya."
  MASUKKAN_JUMLAH_TEXT                              ="Masukkan jumlah yang diinginkan"
  FORUM_INFO_TEXT                                   ="Dapatkan tips belanja dan informasi terkini seputar Bukalapak"

  element :button_buy, :xpath,                      "//button[contains(@class, 'qa-main-buy-button')]"
  element :breadcrumb,                              ".c-breadcrumb"
  element :product_name,                            ".c-product-detail__name"
  element :product_category,                        ".qa-product-detail-category"
  element :product_weight,                          ".qa-product-detail-weight"
  element :product_stock,                           ".c-product-buy__stock"
  element :product_price,                           ".c-product-detail-price .amount.positive"
  element :product_description,                     ".qa-product-detail-description"
  element :product_description_reskin,:xpath,       '//li[3]//p'
  element :product_push_button, :xpath,             "//a[normalize-space(text())='Push']"
  element :flash_message,                           ".flash__text"

  element :add_to_cart_button_login,:xpath,         '//div/button/span[contains(@class,"c-icon--cart")]'
  element :add_to_cart,                             '.ion-android-cart'

  element :product_availability_label,              '.u-mrgn-bottom--3 span.c-label'

  #general element
  element :favorite_button,                         '.product-action-favorite'
  element :unfavorite_button,                       '.product-action-favorite--active'
  element :jaminan_text,                            '.c-guarantee__body'
  element :masukkan_jumlah_text,                    '.string.optional.control-label.c-product-buy-qty__lbl'
  element :decrease_qty_btn,                        '.js-qty-field__btn-dec'
  element :increase_qty_btn,                        '.js-qty-field__btn-inc'
  element :qty_buy_product,                         '.string.optional.js-qty-field__input.c-inp.c-product-buy-qty__input'
  element :product_detail_image,                    '.c-thumb.c-thumb--huge'

  element :laporkan_link,                           ".js-product-report-modal-trigger.c-link-rd"
  element :bottom_beli_sekarang_button,             '.u-mrgn-left--4'
  element :anda_berminat_text,:xpath,               '//*[@id="item_quantity_2"]'

  #product detail average shipping
  element :waktu_kirim_tooltip,:xpath,              "//div/span[contains(@class,'c-icon--help')]"
  element :waktu_kirim_tooltip_content,:xpath,      '//*[@id="user_delivery_tooltip"]/div/div'

  #free shipping
  element :free_shipping_label,                     '.c-product__free-shipping'
  element :free_shipping_tooltip,                   '.c-product__free-shipping-icon'
  element :free_shipping_tooltip_content,:xpath,    '//*[@id="free_shipping_tooltip"]/div/div[2]'

  #product with rating/review
  element :jumlah_ulasan,:xpath,                    "//a[contains(@class, 'c-product-rating__count')]"
  element :tab_ulasan_barang,:xpath,                '//*[@id="mod-product-detail-1"]/div[1]/div[2]/ul/li[4]/a'
  element :rating_icon_ulasan,:xpath,               "//*[@id='product_review_panel2']//div[contains(@class, 'c-rating__fg')]"
  element :ulasan_date,:xpath,                      '//*[@id="product_review_panel2"]//div[2]/div[2]/span[2]'
  element :ulasan_content,:xpath,                   '//*[@id="product_review_panel2"]//div[2]/p'

  #product with feedback
  element :pelapak_reputation,                      '.c-user-identification__reputation'
  element :tab_feedback,:xpath,                     '//*[@id="mod-product-detail-1"]/div[1]/div[2]/ul/li[3]/a'
  element :user_feedback_flag,                      '.c-icon--thumb-up'
  element :user_feedback_date, :xpath,              '//div/time'
  element :user_feedback_content,:xpath,            '//*[@id="product_seller_feedback_panel2"]//div[2]/p'

  #click image product detail
  element :beli_popup_btn,:xpath,                   '//div[2]/div/div[1]/div/div/div/form[1]/button'
  element :add_to_cart_popup_btn,:xpath,            '//div[2]/div/div[1]/div/div/div/form[2]/button'
  element :right_slide_image,                       '.mfp-arrow.mfp-arrow-right.mfp-prevent-close'
  element :left_slide_image,                        '.mfp-arrow.mfp-arrow-left.mfp-prevent-close'
  element :close_popup_prod_detail,:xpath,          '//body/div[2]/div/div[1]/div/button'

  #installment price < Rp.500.00
  element :cicilan_info_text,                       '.c-notification.js-exposure-installment.u-mrgn-bottom--3.u-mrgn-top--3'

  #installment price >= Rp.500.00
  element :cicilan_option_text,                     '.c-product-installment__option'
  element :intallment_percentage,                   '.c-product-detail-price__installment-percentage'
  element :bank_penyedia_cicilan_tooltip,:xpath,    "//span/small"
  element :cicilan_tanpa_kartu_kredit_text,         '.c-link-rd.u-mrgn-left--3'
  element :bca_logo,                                '.c-logo-external-bank--bca'

  #having subscriber
  element :lapak_langganan_icon,                    '.c-icon--store'
  element :pelanggan_label,:xpath,                  '//div[6]/table/tbody/tr[2]/td[1]'
  element :pelanggan_value,:xpath,                  '//div[6]/table/tbody/tr[2]/td[2]'

  #premium seller
  element :pelapak_premium_badge,:xpath,            "//div[2]/div/div[1]/div/div[1]/span[contains(@class,'c-seller-badge--premium')]"
  element :pelapak_premium_label,:xpath,            '//*[@id="mod-product-detail-1"]//div[4]/div[1]/div/div[2]'

  #product discount
  element :original_price,                          '.c-product-detail-price__original'
  element :reduce_price,                            '.c-product-detail-price__reduced'
  element :discount_badge,                          '.c-badge.c-badge--large.c-badge--pink'

  #wholesale product
  element :wholesale_badge_tooltip,:xpath,          '//*[@id="mod-product-detail-1"]//div[contains(@class, "js-wholesale-tooltip")]'
  element :detail_harga_grosir_label,               '.product_wholesale_price_panel__title'
  element :ws_beli_header,:xpath,                   '//div[1]/div/div/div[2]/table/thead/tr/th[1]'
  element :ws_harga_satuan_header,:xpath,           '//div[1]/div/div/div[2]/table/thead/tr/th[2]'
  element :ws_beli_value,:xpath,                    '//div[1]/div/div/div[2]/table/tbody/tr/td[1]'
  element :ws_harga_satuan_value,:xpath,            '//div[1]/div/div/div[2]/table/tbody/tr/td[2]'
  element :beli_lebih_banyak_tooltip,               '.u-mrgn-left--1.autooltip.c-tooltip__cue.tooltipstered'

  #detail barang tab elements
  ##Informasi
  element :tab_detail_barang,:xpath,                '//*[@id="mod-product-detail-1"]/div[1]/div[2]/ul/li[1]/a'
  element :informasi_label, :xpath,                 '//*[@id="product_detail_data_panel2"]/div/div[1]//div[3]/div[1]'
  element :product_condition_icon,                  '.c-icon.c-icon--label'
  element :product_condition_label, :xpath,         "//div[3]/div[2]//div[1]/dl/dt[1]"
  element :product_condition_value, :xpath,         "//dd/span[contains(@class, 'c-label')]"
  element :terjual_icon, :xpath,                    "//dt[2]/span[contains(@class,'c-icon--cart')]"
  element :terjual_label, :xpath,                   "//div[3]/div[2]//div[1]/dl/dt[2]"
  element :terjual_value, :xpath,                   "//div[3]/div[2]//div[1]/dl/dd[2]"
  element :dilihat_icon,                            ".c-icon--visibility"
  element :dilihat_label, :xpath,                   "//div[3]/div[2]//div[1]/dl/dt[3]"
  element :dilihat_value, :xpath,                   "//div[3]/div[2]//div[1]/dl/dd[3]"
  element :difavoritkan_icon, :xpath,               "//dt/span[contains(@class,'c-icon--favorite')]"
  element :difavoritkan_label, :xpath,              '//div[2]/dl/dt[1]'
  element :difavoritkan_value, :xpath,              '//div[2]/dl/dd[1]'
  element :diperbarui_icon,                         '.c-icon--time'
  element :diperbarui_label, :xpath,                '//div[2]/dl/dt[2]'
  element :diperbarui_value, :xpath,                '//div[2]/dl/dd[2]'

  ##Spesifikasi
  element :spesifikasi_label, :xpath,               '//*[@id="product_detail_data_panel2"]/div//li[2]/div/div[1]'
  element :kategori_label, :xpath,                  '//div[2]/div/dl/dt[1]'
  element :berat_label, :xpath,                     '//div[2]/div/dl/dt[2]'
  element :berat_value, :xpath,                     "//div[5]/div[2]//div/dl/dd[2]"
  element :type_label,                              ".qa-product-detail-weight"
  element :type_value, :xpath,                      "//div[5]/div[2]//div/dl/dd[3]"

  ##Deskripsi
  element :deskripsi_label, :xpath,                 '//*[@id="product_detail_data_panel2"]/div//li[3]/div/div[1]'

  #Estimasi biaya kirim
  element :tab_estimasi_biaya_kirim,:xpath,         '//*[@id="mod-product-detail-1"]/div[1]/div[2]/ul/li[2]/a'

  ## Masukkan jumlah
  element :masukkan_jumlah_value,                   '.c-product-shipping-est__inp-qty'
  element :jumlah_value_1,:xpath,                   '//*[@id="payment_transaction_quantity"]'
  element :masukkan_jumlah_label,:xpath,            '//ul/li[1]/div/div[1]/label'
  element :decrease_jumlah_button,:xpath,           '//*[@id="product_shipping_est"]//div[1]/button'
  element :increase_jumlah_button,:xpath,           '//*[@id="product_shipping_est"]//div[2]/button'

  ## Masukkan tujuan
  element :masukkan_tujuan,:xpath,                 '//ul/li[2]/div/div[1]/label'
  element :province_address,                       '#payment_transaction_address_attributes_province_chzn'
  element :city_address,                           '#payment_transaction_address_attributes_city_chzn'
  element :area_address,                           '#payment_transaction_address_attributes_area_chzn'

  element :biaya_estimasi_ke,:xpath,                '//div[@class="o-flag o-flag--top o-flag--tab-shipping js-estimation-tab"]/div[1]'
  element :service_header,:xpath,                   '//div[1]/div[2]/table/thead/tr/th[1]'
  element :waktu_pengiriman_header,:xpath,          '//div[1]/div[2]/table/thead/tr/th[2]'
  element :ongkos_kirim_header,:xpath,              '//div[1]/div[2]/table/thead/tr/th[3]'
  element :ongkos_kirim_plus_price_header,:xpath,   '//div[1]/div[2]/table/thead/tr/th[4]'
  element :service_logo,                            '.c-product-shipping-est__courier'
  element :service_value,:xpath,                    '//div[2]/div/div[2]/table/tbody/tr/td[1]'
  element :waktu_pengiriman_value,:xpath,           '//div[2]/div/div[2]/table/tbody/tr/td[2]'
  element :ongkos_kirim_value,:xpath,               '//div[2]/div/div[2]/table/tbody/tr/td[3]'
  element :ongkos_kirim_plus_price_value,:xpath,    '//div[2]/div/div[2]/table/tbody/tr/td[4]'

  #selller note
  element :catatan_label, :xpath,                   '//*[@id="product_detail_data_panel2"]/div//li[4]/div/div[1]'
  element :terms_notes_text,:xpath,                 '//div/p[1]/small'
  element :notes_content,:xpath,                    '//div[2]/div/div/p'
  element :last_update_notes,:xpath,                '//div/p[2]/small'

  #social media icon
  element :fb_share_icon,                            '.c-btn--share-facebook'
  element :twitter_share_icon,                       '.c-btn--share-twitter'
  element :g_plus_share_icon,                        '.c-btn--share-gplus'
  element :pinterest_share_icon,                     '.c-btn--share-pinterest'

  #pelapak info
  element :pelapak_label_text,                       '.u-txt--small.u-txt--small-upcase.u-mrgn-bottom--2'
  element :pelapak_name,                             '.c-user-identification__name'
  element :pelapak_location,                         '.c-user-identification-location__txt'
  element :pelapak_badge_icon,:xpath,                '//div[4]/div/div[1]/span'
  element :pelapak_badge_label,:xpath,               '//div/div[4]/div/div[2]'

  element :waktu_kirim_label,:xpath,                 '//div[6]/table/tbody/tr[1]/td[1]'
  element :pesanan_diterima_label,:xpath,            '//div[6]/table/tbody/tr[2]/td[1]'
  element :login_terakhir_label,:xpath,              '//div[6]/table/tbody/tr[3]/td[1]'
  element :bergabung_label,:xpath,                   '//div[6]/table/tbody/tr[4]/td[1]'

  element :waktu_kirim_value,:xpath,                 '//div[6]/table/tbody/tr[1]/td[2]'
  element :pesanan_diterima_value,:xpath,            '//div[6]/table/tbody/tr[2]/td[2]'
  element :login_terakhir_value,:xpath,              '//div[6]/table/tbody/tr[3]/td[2]'
  element :bergabung_value,:xpath,                   '//div[6]/table/tbody/tr[4]/td[2]'

  element :pelapak_pengiriman_header,:xpath,         '//div[2]/table[1]/thead/tr/th[1]'
  element :pelapak_regular_header,:xpath,            '//div[2]/table[1]/thead/tr/th[2]'
  element :pelapak_kilat_header,:xpath,              '//div[2]/table[1]/thead/tr/th[3]'

  elements :pelapak_pengiriman_value,:xpath,         "//div[2]/table[1]/tbody/tr/td[1]/span"
  elements :pelapak_regular_value,:xpath,            "//div[2]/table[1]/tbody/tr/td[2]/span"
  elements :pelapak_kilat_value,:xpath,              "//div[2]/table[1]/tbody/tr/td[3]/span"

  element :tinggalkan_pesan,                         '.js-td__message-product-button'
  element :berlangganan_button,                      '.btn__subscription'
  element :semua_barang_link,:xpath,                 '//ul[contains(@class, "c-user__links")]/li[1]/a'
  element :barang_favorit_link,:xpath,               '//ul[contains(@class, "c-user__links")]/li[2]/a'
  element :lihat_feedback_link,:xpath,               '//ul[contains(@class, "c-user__links")]/li[3]/a'
  element :laporkan_pelanggan_link,:xpath,           '//ul[contains(@class, "c-user__links")]/li[4]/a'
  element :huge_forum_icon,                          '.c-icon--forum.c-icon--huge'
  element :forum_info_text,:xpath,                   '//div[2]/div[9]/div[2]'

  element :kunjungi_forum_button,                    '.c-btn.c-btn--block.u-mrgn-bottom--4'
  element :tooltip_kecepatan_pengiriman,             '.c-icon.c-icon--help.c-tooltip__cue.autooltip.tooltipstered'

  def load_product(url)
    visit_url url
  end

  def click_beli
    neutralize_ab_test("&one_click_payment=normal&ab_test[btp_beli_button]=normal")
    self.button_buy.click
    sleep 2
  end

  def favorite
    self.favorite_button.click
    self.wait_until_unfavorite_button_visible(5)
  end

  def unfavorite
    self.unfavorite_button.click
    self.wait_until_favorite_button_visible(5)
  end

  def assert_uploaded_product(product_name, product_first_category, product_second_category, product_third_category, product_weight, product_stock, product_price, product_description)
    product_name = capitalize_each_word(product_name)
    product_weight = weight_normalizer(product_weight)
    product_stock = stock_normalizer(product_stock)
    product_price = price_delimiter(product_price)

    expect(self.product_name).to have_text(product_name)
    expect(self.product_category).to have_text(product_third_category)
    expect(self.product_weight).to have_text(product_weight)
    expect(self.product_stock).to have_text(product_stock)
    expect(self.product_price).to have_text(product_price)
    expect(self.product_description).to have_text(product_description)
  end

  def verify_basic_information_about_product
    expect(self).to have_product_stock
    expect(self).to have_favorite_button
    expect(self).to have_jaminan_text
    expect(self.masukkan_jumlah_text).to have_text(MASUKKAN_JUMLAH_TEXT)
    expect(self).to have_decrease_qty_btn
    expect(self).to have_increase_qty_btn
    expect(self).to have_qty_buy_product
    expect(self).to have_product_detail_image
    expect(self.laporkan_link).to have_text('Laporkan Barang')
    expect(self).to have_bottom_beli_sekarang_button
  end

  def verify_pelapak_info
    expect(self.pelapak_label_text).to have_text('PELAPAK')
    self.pelapak_name.should be_visible
    self.pelapak_location.should be_visible
    self.pelapak_badge_icon.should be_visible
    self.pelapak_badge_label.should be_visible
    expect(self.waktu_kirim_label).to have_text('Waktu kirim')
    expect(self.pesanan_diterima_label).to have_text('Pesanan diterima')
    expect(self.login_terakhir_label).to have_text('Login terakhir')
    expect(self.bergabung_label).to have_text('Bergabung')
    self.waktu_kirim_value.should be_visible
    self.pesanan_diterima_value.should be_visible
    self.login_terakhir_value.should be_visible
    self.bergabung_value.should be_visible
    expect(self.pelapak_pengiriman_header).to have_text('PENGIRIMAN')
    expect(self.pelapak_regular_header).to have_text('REGULER')
    expect(self.pelapak_kilat_header).to have_text('KILAT')
    expect(self).to have_pelapak_pengiriman_value
    expect(self).to have_pelapak_regular_value
    expect(self).to have_pelapak_kilat_value
    expect(self.tinggalkan_pesan).to have_text('Tinggalkan Pesan')
    expect(self.berlangganan_button).to have_text('Berlangganan')
    expect(self.semua_barang_link).to have_text('Semua Barang')
    expect(self.barang_favorit_link).to have_text('Barang Favorit')
    expect(self.lihat_feedback_link).to have_text('Lihat Feedback')
    expect(self.forum_info_text).to have_text(FORUM_INFO_TEXT)
    expect(self.laporkan_pelanggan_link).to have_text('Laporkan Pelanggaran')
    self.huge_forum_icon.should be_visible
  end

  def verify_sosial_media_icon_list
    expect(self).to have_fb_share_icon
    expect(self).to have_twitter_share_icon
    expect(self).to have_g_plus_share_icon
    expect(self).to have_pinterest_share_icon
  end

  def verify_product_information
    expect(self).to have_tab_detail_barang
    expect(self.informasi_label).to have_text("Informasi")
    expect(self).to have_product_condition_icon
    expect(self.product_condition_label).to have_text("Kondisi")
    expect(self).to have_product_condition_value
    expect(self).to have_terjual_icon
    expect(self.terjual_label).to have_text("Terjual")
    expect(self).to have_terjual_value
    expect(self).to have_dilihat_icon
    expect(self.dilihat_label).to have_text("Dilihat")
    expect(self).to have_dilihat_value
    expect(self).to have_difavoritkan_icon
    expect(self.difavoritkan_label).to have_text("Difavoritkan")
    expect(self).to have_difavoritkan_value
    expect(self).to have_diperbarui_icon
    expect(self.diperbarui_label).to have_text("Diperbarui")
    expect(self).to have_diperbarui_value
  end

  def verify_product_specification
    expect(self.spesifikasi_label).to have_text("Spesifikasi")
    expect(self.kategori_label).to have_text("Kategori")
    expect(self.berat_label).to have_text("Berat")
    expect(self).to have_product_weight
    expect(self).to have_product_category
  end

  def verify_product_description
    expect(self.deskripsi_label).to have_text("Deskripsi")
    expect(self).to have_product_description_reskin
  end

  def verify_product_notes
    expect(self.catatan_label).to have_text("Catatan")
    expect(self.terms_notes_text).to have_text(TERM_NOTES )
    expect(self).to have_notes_content
    expect(self).to have_last_update_notes
  end

  def verify_product_detail_discount_info
    expect(self).to have_original_price
    expect(self).to have_reduce_price
    expect(self).to have_discount_badge
  end

  def verify_product_detail_average_shipping_time_info
    expect(self).to have_waktu_kirim_tooltip
    self.waktu_kirim_tooltip.click
    self.wait_for_waktu_kirim_tooltip_content(3)
    expect(self.waktu_kirim_tooltip_content).to have_text(WAKTU_KIRIM_TOOLTIP_CONTENT)
  end

  def verify_product_detail_free_shipping_info
    expect(self.free_shipping_label).to have_text("Gratis Biaya Kirim")
    expect(self).to have_free_shipping_tooltip
    self.free_shipping_tooltip.click
    self.wait_for_free_shipping_tooltip_content(3)
    expect(self.free_shipping_tooltip_content).to have_text("Khusus DKI Jakarta")
  end

  def verify_product_detail_rating_info
    expect(self).to have_jumlah_ulasan
  end

  def verify_pelapak_reputation_info
    expect(self).to have_pelapak_reputation
  end

  def installment_info_price_less_than_500000
    expect(self).to have_cicilan_info_text
  end

  def verify_shipping_estimation_info
    expect(self.masukkan_jumlah_label).to have_text("Masukkan jumlah")
    self.increase_jumlah_button.click
    self.decrease_jumlah_button.click
    expect(self).to have_jumlah_value_1
    expect(self.masukkan_tujuan).to have_text("Masukkan tujuan")
    expect(self).to have_province_address
    expect(self).to have_city_address
    expect(self).to have_area_address
    expect(self).to have_biaya_estimasi_ke
    expect(self.service_header).to have_text("SERVIS")
    expect(self.waktu_pengiriman_header).to have_text("WAKTU PENGIRIMAN")
    expect(self.ongkos_kirim_header).to have_text("ONGKOS KIRIM")
    expect(self.ongkos_kirim_plus_price_header).to have_text("HARGA + ONGKOS KIRIM")
    expect(self).to have_service_logo
    expect(self).to have_service_value
    expect(self).to have_ongkos_kirim_value
    expect(self).to have_ongkos_kirim_plus_price_value
  end

  def verify_feedback_list
    expect(self).to have_user_feedback_flag
    expect(self).to have_user_feedback_date
    expect(self).to have_user_feedback_content
  end

  def verify_review_list
    expect(self).to have_rating_icon_ulasan
    expect(self).to have_ulasan_date
    expect(self).to have_ulasan_content
  end

  def verify_product_detail_wholesale_info
    self.wholesale_badge_tooltip.click
    expect(self.detail_harga_grosir_label).to have_text("Detail Harga Grosir")
    expect(self.ws_beli_header).to have_text("Beli")
    expect(self.ws_harga_satuan_header).to have_text("Harga Satuan")
    expect(self).to have_ws_beli_value
    expect(self).to have_ws_harga_satuan_value
    self.wholesale_badge_tooltip.click
    expect(self.beli_lebih_banyak_tooltip).to have_text("Beli banyak lebih murah")
  end

  def verify_installment_info_price_500000
    expect(self).to have_cicilan_option_text
    expect(self.intallment_percentage).to have_text("Cicilan 0%")
    expect(self.bank_penyedia_cicilan_tooltip).to have_text("Bank penyedia cicilan")
    self.bank_penyedia_cicilan_tooltip.click
    self.wait_for_bca_logo(3)
    self.bca_logo.should be_visible
    self.bank_penyedia_cicilan_tooltip.click
    expect(self.cicilan_tanpa_kartu_kredit_text).to have_text("Cicilan tanpa kartu kredit")
  end

  def verify_seller_subscriber_info
    expect(self.pelanggan_label).to have_text("Pelanggan")
    expect(self).to have_pelanggan_value
    self.lapak_langganan_icon.should be_visible
  end

  def verify_premium_seller_info
    self.pelapak_premium_badge.should be_visible
    expect(self.pelapak_premium_label).to have_text("Pelapak Premium")
  end

  def verify_product_availability(availability_label)
    expect(self.product_availability_label).to have_text(availability_label)
  end

end
