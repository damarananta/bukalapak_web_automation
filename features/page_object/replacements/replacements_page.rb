class ItemReplacements < SitePrism::Page
  path ="/replacements"
  set_url(ENV['BASE_URL'] + path)
  element  :submit_notes_replaced_item, :xpath ,		"//*[@id='cboxLoadedContent']/form/input[6]"

  def search_transaction_replacements(invoice_id)
    visit_url('/replacements?utf8=%E2%9C%93&mode=&transaction_id=&product_name=&amount=&seller_name=&per_page=25&state=all&reason=&replacement_type=all&start_date[day]=2&start_date[month]=12&start_date[year]=2016&finish_date[day]=2&finish_date[month]=12&finish_date[year]=2020&commit=Search')
    fill_in('transaction_id', :with => invoice_id)
    find(:id, 'transaction_id').native.send_keys(:enter)
    sleep 2
  end

  def get_replacements(invoice_id)
    scroll_down(50)
    click_link('Book')
    page.driver.browser.switch_to.alert.accept
    click_link('Find Replacement')
    within ('.box-form') do
      fill_in('url', :with => ENV['SELLER4_PRODUCT_URL'])
      find(:id, 'url').native.send_keys(:enter)
    end
    within('.replacement-list__product') do
      @new_price = change_into_integer(find(:xpath, '//*[@id="detail_transactions"]/div[4]/ul/li/div/div/div[1]/div[2]/dl[3]/dd/span[2]').text)
      click_link('Set As Replacement Item')
      self.submit_notes_replaced_item.click
    end

    search_transaction_replacements(invoice_id)
    first(:xpath, '//*[@id="item-lists"]/table/tbody/tr/td[7]/a').click
    @new_transaction_id = find(:xpath, '//*[@id="page_title"]/small').text
    [@new_price,@new_transaction_id]
  end
end
