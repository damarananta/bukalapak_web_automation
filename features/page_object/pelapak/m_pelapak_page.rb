class MobilePelapakPage < SitePrism::Page

  element :search_box,                        '#search_keywords'
  element :sort_by_box,:xpath,                '//*[@id="search_sort_by"]'
  element :select_by_label_box,:xpath,        '//*[@id="label_slug"]'
  element :label1,                            '#label_slug > option:nth-child(2)'
  element :product_card,:xpath,               '//*[@id="page"]/div[4]//div[1]/a/picture/img'
  element :product_name,                      '.product__name.line-clamp--2'

  element :pelapak_name_link,:xpath,          '//*[@id="page"]/div[1]/article/div[2]/h5/a'
  element :pelapak_feedback_link,:xpath,      '//*[@id="page"]/div[1]/article/div[2]/a'
  element :pelapak_image,:xpath,              '//*[@id="page"]/div[1]/article/div[1]/a/img'
  element :bagikan_text,:xpath,               '//*[@id="page"]/div[1]/article/div[3]/span'
  element :share_product_ke_text,:xpath,      '//*[@id="page"]/div[1]/article/div[3]/div/h3'
  element :fb_icon_link,                      '.socmed-button.socmed-button--bacefook'
  element :twitter_icon_link,                 '.socmed-button.socmed-button--twitter'
  element :google_icon_link,                  '.socmed-button.socmed-button--gplus'
  element :pinterest_icon_link,               '.socmed-button.socmed-button--pinterest'
  element :pelapak_location_link,             '.user-address'
  element :pelapak_join_date,                 '.user-meta-join-at'
  element :pelapak_last_login,                '.user-meta-last-login'
  element :pelapak_subscriber_count,          '.user-meta-subscribers-total'
  element :pelapak_rejection_worrisome,       '.rejection-rate-worrisome'
  element :jasa_pengiriman_title,             '.section__title'
  element :pengiriman_header,:xpath,          '//table[1]/thead/tr/th[1]'
  element :regular_header,:xpath,             '//table[1]/thead/tr/th[2]'
  element :kilat_header,:xpath,               '//table[1]/thead/tr/th[3]'
  element :jasa_pengiriman_value,:xpath,      '//*[@id="page"]//table/tbody/tr/td[1]/img'
  element :regular_value,:xpath,              '//*[@id="page"]//table/tbody/tr/td[2]/em'
  element :kilat_value,:xpath,                '//*[@id="page"]//table/tbody/tr/td[3]/em'
  element :tinggalkan_pesan_button,           '.user-detailed-chat'
  element :berlangganan_button,               '.btn--medium.btn__subscription'
  element :laporkan_pelapak_button,           '.btn.btn--red2.btn--medium.btn--block'
  element :barang_dari_pelapak_text,:xpath,   '//*[@id="page"]/h4'


  def assert_detail_pelapak_information(pelapak_name)
  	expect(self.pelapak_name_link).to have_text("#{pelapak_name}")
    expect(self).to have_pelapak_feedback_link
    expect(self).to have_pelapak_image
    expect(self.bagikan_text).to have_text('Bagikan')
    expect(self.share_product_ke_text).to have_text('Share Produk Ke:')
    expect(self).to have_fb_icon_link
    expect(self).to have_twitter_icon_link
    expect(self).to have_google_icon_link
    expect(self).to have_pinterest_icon_link
    expect(self).to have_pelapak_location_link
    expect(self).to have_pelapak_join_date
    expect(self).to have_pelapak_last_login
    expect(self).to have_pelapak_subscriber_count
    expect(self).to have_pelapak_rejection_worrisome
    expect(self.jasa_pengiriman_title).to have_text('JASA PENGIRIMAN')
    expect(self.pengiriman_header).to have_text('PENGIRIMAN')
    expect(self.regular_header).to have_text('REGULER')
    expect(self.kilat_header).to have_text('KILAT')
    expect(self).to have_jasa_pengiriman_value
    expect(self).to have_regular_value
    expect(self).to have_kilat_value
    expect(self.tinggalkan_pesan_button).to have_text('Tinggalkan Pesan')
    expect(self.berlangganan_button).to have_text('Berlangganan')
    expect(self.laporkan_pelapak_button).to have_text('Laporkan Pelapak')
    expect(self.barang_dari_pelapak_text).to have_text("Barang dari #{pelapak_name}")
  end

end
