class PelapakPage < SitePrism::Page

  element :search_box,:xpath,    '//div/*[@id="search_keywords"]'
  element :product_name,         '.product__name'

end
