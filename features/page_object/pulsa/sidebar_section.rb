class SideBarSection < SitePrism::Section
  element :phone_credits_link, :xpath, "//span[text()='Pulsa']"
  element :data_plan_link, :xpath, "//span[text()='Paket Data']"
  element :electricity_link, :xpath, "//span[text()='Token Listrik']"
  element :game_voucher_link, :xpath, "//span[text()='Voucher Game']"
end
