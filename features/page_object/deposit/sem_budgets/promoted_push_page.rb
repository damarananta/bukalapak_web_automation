class PromotedPushPage < SitePrism::Page
  path = "/deposit/sem_budgets"
  set_url(ENV['BASE_URL'] + path)

  element  :budget_balance_text,                          ".u-txt--fair span.amount"
  element  :add_budget_amount_field,                      "#budget_amount"
  element  :add_budget_password_field,                    "#budget_password"
  element  :add_budget_tambah_button,             :xpath, "//button[normalize-space(text())='Tambah']"
  element  :add_budget_button,                    :xpath, "//*[normalize-space(text())='Tambah Budget']"
  element  :flash_message,                                ".flash__text"
  element  :budget_balance_section,                       ".c-panel"
  elements :list_product_names,                           ".c-table tbody tr td:nth-child(1)"
  elements :list_product_status,                          ".c-table tbody tr td:nth-child(2)"
  elements :list_product_set_button,                      ".c-table tbody tr td:nth-child(5) a"
  element  :active_date_start_day_section,                "select#product_bidding_sem_start_3i"
  element  :active_date_start_month_section,              "select#product_bidding_sem_start_2i"
  element  :active_date_start_year_section,               "select#product_bidding_sem_start_1i"
  element  :active_date_end_day_section,                  "select#product_bidding_sem_end_3i"
  element  :active_date_end_month_section,                "select#product_bidding_sem_end_2i"
  element  :active_date_end_year_section,                 "select#product_bidding_sem_end_1i"
  element  :cost_per_click_field,                         "input#product_bidding_bid_value"
  element  :active_promoted_push_radio_button,    :xpath, "//span[normalize-space(text()) = 'Aktif']"
  element  :nonactive_promoted_push_radio_button, :xpath, "//span[normalize-space(text()) = 'Nonaktif']"
  element  :agreement_text,                               ".js-product-bid__agreement"
  element  :save_promoted_push_setting_button,    :xpath, "//button[normalize-space(text())='Simpan']"

  def add_budget(amount)
    self.add_budget_amount_field.set(amount)
    self.add_budget_tambah_button.click
  end

  def get_budget_balance
    budget_balance = change_into_integer(self.budget_balance_text.text)
  end

  def click_promoted_push_product_setting(product_name)
    self.list_product_names.each_with_index do |a_product_name, index|
      if (a_product_name.has_text?(product_name))
        scroll_down(100)
        self.list_product_set_button[index].click
        break
      end
    end
  end

  def set_promoted_push_product_active_for_today(cost_per_click)
    self.cost_per_click_field.set(210)
    now = Time.now
    
    # fill active start date
    self.active_date_start_day_section.select(now.day)
    self.active_date_start_month_section.find("option[value='#{now.month}']").select_option
    self.active_date_start_year_section.select(now.year)
    
    # fill active end date
    self.active_date_end_day_section.select(now.day)
    self.active_date_end_month_section.find("option[value='#{now.month}']").select_option
    self.active_date_end_year_section.select(now.year)
    
    self.active_promoted_push_radio_button.click
    self.wait_until_agreement_text_visible
    self.save_promoted_push_setting_button.click
  end

  def set_promoted_push_product_nonactive
    self.nonactive_promoted_push_radio_button.click
    self.wait_until_agreement_text_invisible
    self.save_promoted_push_setting_button.click
  end

end
