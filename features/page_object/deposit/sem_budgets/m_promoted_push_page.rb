class MobilePromotedPushPage < SitePrism::Page
  path = "/deposit/sem_budgets"
  set_url(ENV['MOBILE_URL'] + path)

  element  :budget_balance_section,               :xpath, "//*[@id='page']/div"
  element  :budget_balance_text,                  :xpath, "//*[@id='page']/div/h6/span/span[2]"
  element  :add_budget_button,                    :xpath, "//*[normalize-space(text())='Tambah Budget']"
  element  :add_budget_amount_field,                      "#budget_amount"
  element  :add_budget_password_field,                    "#budget_password"
  element  :add_budget_tambah_button,             :xpath, "//input[@value='Tambah']"
  element  :flash_message,                                ".flash__text"
  element  :cost_per_click_field,                         "input#product_bidding_bid_value"
  element  :active_date_start_day_select,                 "select#product_bidding_sem_start_3i"
  element  :active_date_start_month_select,               "select#product_bidding_sem_start_2i"
  element  :active_date_start_year_select,                "select#product_bidding_sem_start_1i"
  element  :active_date_end_day_select,                   "select#product_bidding_sem_end_3i"
  element  :active_date_end_month_select,                 "select#product_bidding_sem_end_2i"
  element  :active_date_end_year_select,                  "select#product_bidding_sem_end_1i"
  element  :active_promoted_push_radio_button,    :xpath, "//*[normalize-space(text()) = 'Aktif']"
  element  :nonactive_promoted_push_radio_button, :xpath, "//*[normalize-space(text()) = 'Nonaktif']"
  element  :save_promoted_push_setting_button,    :xpath, "//*[@value='Simpan']"
  element  :agreement_text,                               ".js-product-bid__agreement"
  elements :list_product_names,                           "#page ul li div:nth-child(1)"
  elements :list_product_status,                          "#page ul li div:nth-child(2)"
  elements :list_product_set_button,                      "#page ul li div:nth-child(3) a"
  
  def add_budget(amount)
    self.add_budget_amount_field.set(amount)
    self.add_budget_tambah_button.click
  end

  def get_budget_balance
    budget_balance = change_into_integer(self.budget_balance_text.text)
  end

  def click_promoted_push_product_setting(a_product_name)
    self.list_product_names.each_with_index do |product_name, index|
      if (product_name.has_text?(a_product_name))
        self.list_product_set_button[index].click
        break
      end
    end
  end

  def set_promoted_push_product_active_for_today(cost_per_click)
    self.cost_per_click_field.set(cost_per_click)
    now = Time.now
    scroll_to(cost_per_click_field, true)

    # fill active start date
    self.active_date_start_day_select.select("#{now.day}")
    self.active_date_start_month_select.find("option[value='#{now.month}']").select_option
    self.active_date_start_year_select.select("#{now.year}")

    # fill active end date
    self.active_date_end_day_select.select("#{now.day}")
    self.active_date_end_month_select.find("option[value='#{now.month}']").select_option
    self.active_date_end_year_select.select("#{now.year}")
    
    self.active_promoted_push_radio_button.click
    self.wait_until_agreement_text_visible
    self.save_promoted_push_setting_button.click
  end

  def set_promoted_push_product_nonactive
    self.nonactive_promoted_push_radio_button.click
    self.wait_until_agreement_text_invisible
    self.save_promoted_push_setting_button.click
  end

end
