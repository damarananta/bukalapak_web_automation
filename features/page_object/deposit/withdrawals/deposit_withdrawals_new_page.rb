class DepositWithdrawalsPage < SitePrism::Page
  path = "/deposit/withdrawals/new"
  set_url(ENV['BASE_URL'] + path)

  element :amount_field,   :xpath,   	"//*[@id='js-withdrawal-user-amount-delimiter']"
  element :password_field, :xpath,   	"//*[@id='js-withdrawal-user-password']"
  element :withdrawal_button, :xpath,       	"//button[text()='Cairkan']"
  
  def withdrawal_bukadompet(amount,password)
    page.execute_script "window.scrollBy(0,200)"
    self.amount_field.set(amount)
    self.password_field.set(password)
    self.withdrawal_button.click
  end
end
