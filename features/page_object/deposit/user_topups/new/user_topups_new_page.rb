class UserTopupsNewPage < SitePrism::Page
  path = "/deposit/user_topups/new"
  set_url(ENV['BASE_URL'] + path)

  element :saldo_field,                        "#js-topup-amount-delimiter"
  element :payment_method_dropdown,            "#js-topup-payment"
  element :next_button, :button,               "Lanjut"

  def top_up_with_transfer(amount)
    self.saldo_field.set(amount)
    self.payment_method_dropdown.select("Transfer")
    scroll_down(100)
    self.next_button.click
  end
end
