class UserTopupsPage < SitePrism::Page
  path = "/deposit/user_topups"
  set_url(ENV['BASE_URL'] + path)

  element :ticket_number_field,       "#payment_id"
  element :search_button, :xpath,     "//input[@value='Search']"
  element :confirm_link,  :link,      "Confirm"

  def confirm_topup_ticket(ticket_number)
    self.ticket_number_field.set(ticket_number)
    self.search_button.click
    self.confirm_link.click
    modal_accept_confirm
  end
end
