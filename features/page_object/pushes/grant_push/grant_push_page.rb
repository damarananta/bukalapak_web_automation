class GrantPushPage < SitePrism::Page
  path = "/pushes/grant_push"
  set_url(ENV['BASE_URL'] + path)

  element :simpan_button,      "input[type='submit'][value='Simpan']"
  element :username_field,     "input#username"
  element :push_count_field,   "input#action_log_push_earned_count"
  element :aktif_hingga_field, "input#action_log_push_earned_validity_time"
  element :flash_message,      ".flash__text"

end
