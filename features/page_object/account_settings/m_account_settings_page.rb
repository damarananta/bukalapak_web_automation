class MobileAccountSettingsPage < SitePrism::Page
  path = "/account_settings"
  set_url(ENV['MOBILE_URL'] + path)
  element :phone_number_text, :xpath, "//*[@id='setting_account_files']/fieldset[8]/dl/dd"

  def assert_phone_number(phone_number)
    self.load
    self.wait_for_phone_number_text
    self.phone_number_text :text => phone_number
  end
end
