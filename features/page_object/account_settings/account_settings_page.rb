class AccountSettingsPage < SitePrism::Page
  path = "/account_settings"
  set_url(ENV['BASE_URL'] + path)
  element :phone_number_text,   :xpath, "//*[@id='setting_account']/dl[9]/dd"
  element :phone_edit_button,   :xpath, "//*[@id='setting_account']/dl[7]/dd/a[2]"
  element :password_edit_button,:xpath, "//*[@id='setting_account']/dl[10]/dd/a[2]"

  element :deactivated_button,  :xpath, "//*[@id='setting_account']/dl[12]/dd/a[2]"
  element :activated_button,    :xpath, "//*[@id='setting_account']/dl[12]/dd/a"

  element :otp_button,          :xpath, "//div[contains(@class,'js-tfa-request__button') and text()='Kirim Kode Otentikasi']"
  element :otp_field,                   "#otp"
  element :submit_otp_button,   :xpath, "//div[contains(@class,'js-tfa-submit') and text()='Lanjut']"
  element :finish_otp_button,   :xpath, "//div[contains(@class,'js-tfa-finish') and text()='Lanjut']"

  def tfa_alert(token)
    self.wait_for_otp_button
    self.otp_button.click
    self.wait_for_otp_field
    self.otp_field.set(token)
    self.submit_otp_button.click
    self.finish_otp_button.click
  end

  def assert_phone_number(phone_number)
    self.load
    self.wait_for_phone_number_text
    self.phone_number_text :text => phone_number
  end
end
