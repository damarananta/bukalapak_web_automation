class HomePage < SitePrism::Page

  PRODUCT_SAMPLE='/p/fashion-anak/anak-laki-laki/celana-1432/53k8mh-jual-bat-minton-test-2'

  path = "/"
  set_url(path)

  element :login_link, :xpath,                     "//a[text()='Login']"
  element :username_field,                         "input[name='user_session[username]']"
  element :password_field,                         "input[name='user_session[password]']"
  element :login_button, :xpath,                   "//button[text()='Login']"
  element :flash_message,                          ".flash__text"
  element :username_link,                          ".c-nav-list__link.u-fg--yellow"
  element :daftar_link, :xpath,                    "//a[text()='Daftar']"
  element :onb_phone_conf_popup,                   "#ob-popup-1"
  element :onb_phone_conf_skip,                    ".c-btn.c-btn--block.ob-btn--popup-skip"
  element :onb_phone_number_field,                 "#ob-field-phone_number"
  element :onb_phone_simpan_button,                "#ob-btn-submit-phone"
  element :onb_welcome_popup,                      "#ob-popup-2"
  element :onb_welcome_continue_btn, :xpath,       "//a[text()='Lanjutkan ke Halaman Utama']"
  element :onb_email_conf_check, :xpath,           "//a[contains(@class, 'onb-email')]"
  element :onb_save_address_check, :xpath,         "//a[contains(@class, 'onb-address')]"
  element :onb_save_phone_number_check, :xpath,    "//a[contains(@class, 'onb-phone')]"
  element :onb_topup_deposit_check, :xpath,        "//a[contains(@class, 'onb-topup')]"
  element :onb_shop_check, :xpath,                 "//a[contains(@class, 'onb-shop')]"
  element :popular_product_name, :xpath,           ".//*[@id='popular-1-1']/ul/li[1]/div/article/div[3]/h3/a"
  
  element :tab_phonecredit, :xpath,                "//*[@id='mod-tab-1']/li[1]/a"
  element :tab_dataplan,    :xpath,                "//*[@id='mod-tab-1']/li[2]/a"
  element :tab_electricity, :xpath,                "//*[@id='mod-tab-1']/li[3]/a"
  element :tab_gamevoucher, :xpath,                "//*[@id='mod-tab-1']/li[4]/a"
  
  element :phone_number_phone_credit,              "#phone_number_phone_credit"
  element :phone_number_data_plan,                 "#phone_number_data_plan"
  element :customer_number_field,                  "#customer_number"

  element :phonecredit_buy_button,          "input[value='Beli Pulsa']"
  element :dataplan_buy_button,             "input[value='Beli Paket Data']"
  element :electricity_buy_button,          "input[value='Beli Token Listrik']"
  element :gamevoucher_buy_button,          "input[value='Beli Voucher']"

  def dismiss_one_signal_popup_homepage
    click_button("No Thanks") if page.has_css?(".onesignal-popover-dialog")
  end

  def onb_confirm_phone_number(phone_number)
    self.wait_for_onb_phone_conf_popup
    self.onb_phone_number_field.set(phone_number)
    self.onb_phone_simpan_button.click
  end

  def onb_dismiss_phone_confirm
    self.wait_for_onb_phone_conf_skip
    self.onb_phone_conf_skip.click
  end

  def onb_dismiss_welcome_popup
    self.wait_for_onb_welcome_popup
    self.onb_welcome_continue_btn.click
    sleep 1
  end

  def dismiss_onboarding_popup
    self.onb_dismiss_phone_confirm
    self.onb_dismiss_welcome_popup
  end

  def onb_assert_email_checked
    self.onb_email_conf_check :class => "c-list-ui__link ob-link onb-email ob-link__disabled"
  end

  def onb_assert_address_checked
    self.onb_save_address_check :class => "c-list-ui__link ob-link ob-link__disabled onb-address"
  end

  def onb_assert_phone_checked
    self.onb_save_phone_number_check :class => "c-list-ui__link ob-link ob-link__disabled onb-phone"
  end

  def onb_assert_topup_checked
    self.onb_topup_deposit_check :class => "c-list-ui__link ob-link ob-link__disabled onb-topup"
  end

  def onb_assert_shop_checked
    self.onb_shop_check :class => "c-list-ui__link ob-link ob-link__disabled onb-shop"
  end

  def login(username, password)
    dismiss_one_signal_popup_homepage
    self.wait_for_login_link(10)
    self.login_link.click
    self.username_field.set(username)
    self.password_field.set(password)
    self.login_button.click
    self.wait_for_username_link(10)
  end

  def view_detail_product_popular
    page.execute_script "window.scrollBy(0,700)"
    detail_product
    sleep 1
  end

  def verify_product_detail_section
    if page.has_css?(".c-product-card__name")
      @sectionProductsCount=page.all('.c-product-card__name').count
    else
      @sectionProductsCount=page.all('.product__name.line-clamp--2').count
    end
    @sectionProductsCount.should==12
  end

  def buy_vp_home_widget(no_hp, vp, payment, no_meteran)
    case vp
    when "Pulsa"
      self.wait_for_tab_phonecredit
      self.tab_phonecredit.click
      self.phone_number_phone_credit.set no_hp
      self.wait_for_phonecredit_buy_button
      self.phonecredit_buy_button.click
    when "Paket Data"
      self.wait_for_tab_dataplan(5)
      self.tab_dataplan.click
      self.phone_number_data_plan.set no_hp
      self.wait_for_dataplan_buy_button
      self.dataplan_buy_button.click
    when "Token Listrik"
      self.wait_for_tab_electricity
      self.tab_electricity.click
      self.customer_number_field.set no_meteran
      self.wait_for_electricity_buy_button
      sleep 5
      self.electricity_buy_button.click
    when "Voucher Game"
      self.wait_for_tab_gamevoucher
      self.tab_gamevoucher.click
      sleep 3
      self.tab_gamevoucher.click unless self.has_gamevoucher_buy_button?
      self.wait_until_gamevoucher_buy_button_visible
      self.gamevoucher_buy_button.click
    end
    sleep 3
  end
end
