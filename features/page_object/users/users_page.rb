class UsersPage < SitePrism::Page
  path = "/users"
  set_url(ENV['BASE_URL'] + path)

  element  :username_field,        "input[name='keywords']"
  element  :search_button, :xpath, "//input[@value='Search']"
  element  :selection_checklist,   "#user_selected_"
  element  :confirm_account_btn,   "button[id='confirm']"
  element  :flash_message,         ".flash__text"

  elements :user_list, :xpath,     "//*[@id='users-filter']/table/tbody"

  def search_user_by_name_or_username(username)
    self.username_field.set(username)
    self.search_button.click
    self.wait_for_user_list(10)
  end

  def confirmed_user_email(username)
    self.search_user_by_name_or_username(username)
    self.selection_checklist.click
    self.confirm_account_btn.click
    self.wait_for_flash_message
  end
end
