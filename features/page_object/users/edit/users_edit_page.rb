class UsersEditPage < SitePrism::Page
  path = "/users/#{:id}/edit"
  set_url(ENV['BASE_URL'] + path)

  element :email_field,              "input[name='user[new_email]']"
  element :phone_number_field,       "input[name='user[phone]']"
  element :password_field,           "input[name='user[old_password]']"
  element :save_button, :xpath,      "//input[@value='Simpan']"
  element :lanjut_button, :button,   "Lanjutkan"

  def set_phone_number(phone_number, password)
    self.phone_number_field.set(phone_number)
    self.password_field.set(password)
    scroll_down(200)
    self.save_button.click
    self.wait_for_lanjut_button
    self.lanjut_button.click
  end
end
