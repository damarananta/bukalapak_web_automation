class EditUsersPage < SitePrism::Page
  path = "/users/:id/edit"
  set_url(ENV['BASE_URL'] + path)

  element :email_field,           "input[name='user[new_email]'"
  element :phone_field,           "input[name='user[phone]'"
  element :password_field,        "input[name='user[old_password]'"  
  element :simpan_button, :xpath, "//input[@value='Simpan']"
  element :message,       :xpath, "///div[@id='colorbox']"
  element :new_password,          "input[name='user[password]'"
  element :retry_password,        "input[name='user[password_confirmation]'"
  element :old_password,          "input[name='user[old_password]'"
  element :phone_disclaimer, :xpath, "//div/*[@id='cboxContent']"

  def edit_phone_number(phone,password)
    self.wait_for_phone_field
    self.phone_field.set(phone)
    self.password_field.set(password)
    page.execute_script "window.scrollBy(0,200)"
    self.wait_for_simpan_button
    self.simpan_button.click
  end

  def edit_password(new_password,old_password)
    self.new_password.set(new_password)
    self.retry_password.set(new_password)
    self.old_password.set(old_password)
    self.simpan_button.click
  end
end
