class MobileHomePage < SitePrism::Page
  path = "/"
  set_url(path)

  element :login_icon,                         ".icon-login"
  element :login_form,                         ".login-overlay__content"
  element :user_icon,                          ".m-icon.m-icon--person"
  element :username_field,                     "input[name='user_session[username]']"
  element :password_field,                     "input[name='user_session[password]'"
  element :login_button,                       "input[value='Login']"
  element :flash_message,                      ".flash-container.js-flash-container"
  element :register_link, :xpath,              "//a[text()='Daftar sekarang']"
  element :onb_phone_conf_popup,               ".ob-homepage-popup"
  element :onb_phone_number_field,             ".ob-field-phone-number"
  element :onb_phone_simpan_button,            "#ob-btn-submit-phone"
  element :onb_apps_popup, :xpath,             "//*[@id='ob-popup-2']/div[2]"
  element :onb_close_button, :xpath,           "//*[@id='ob-popup-2']/div[1]/div[2]/button"
  element :onb_search_product_ok_btn,          ".btn.btn--red.search-bar-overlay__button.js-search-overlay-close"
  element :onb_email_conf_check, :xpath,       "//li[contains(@class, 'point-email')]"
  element :popular_product_name, :xpath,       ".//*[@id='page']/section[2]/ul/li[2]/article/div[3]/h3/a"

  def dismiss_one_signal_popup_homepage
    click_button("No Thanks") if page.has_css?(".onesignal-popover-dialog")
  end

  def dismiss_search_product_onboarding
    #disable for temporary
    #self.onb_search_product_ok_btn.click
  end

  def visit_register_page
    self.login_icon.click
    self.wait_until_login_form_visible
    self.register_link.click
  end

  def login(username, password)
    self.dismiss_one_signal_popup_homepage
    sleep 1
    self.login_icon.click
    sleep 2
    self.wait_for_login_form(4)
    self.username_field.set(username)
    self.password_field.set(password)
    self.login_button.click
    sleep 3
    self.wait_for_flash_message
  end

  # temporarily disable
  def onb_confirm_phone_number(phone_number)
    # self.wait_for_onb_phone_conf_popup
    # self.onb_phone_number_field.set(phone_number)
    # self.onb_phone_simpan_button.click
  end

  # temporarily disable
  def onb_dismiss_apps_download_popup
    # self.wait_until_onb_apps_popup_visible(3)
    # self.onb_close_button.click
  end

  def onb_assert_email_checked
    self.onb_email_conf_check :class => "point point-disabled point-email"
  end

  def view_detail_product_popular
    sleep 1
    accept_onboarding
    page.execute_script "window.scrollBy(0,500)"
    detail_product
    sleep 1
  end

end
