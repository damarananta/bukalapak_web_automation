class DataNewPage < SitePrism::Page
  
  element :bukadompet_radio,      "input[value='deposit']"
  element :transfer_radio,        "input[value='atm']"
  element :creditcard_radio,      "input[value='credit_card']"
  element :bcaklikpay_radio,      "input[value='bca_klikpay']"
  element :mandiriclickpay_radio, "input[value='mandiri_clickpay']"
  element :mandiriecash_radio,    "input[value='mandiri_ecash']"
  element :cimbclicks_radio,      "input[value='cimb_clicks']"
  element :indomaret_radio,       "input[value='indomaret']"
  element :alfamart_radio,        "input[value='alfamart']"
  element :kredivo_radio, :xpath, "input[value='kredivo']"
  
  element :bukadompet_password_field, "#js-iv-deposit"
  
  element :button_pay, :xpath, "//button[text()='Bayar']"

  element :wording_bukadompet_no_balance, :xpath, "//*[@id='new_payment_invoice']/div/div/div/div[2]/fieldset/div/ul/li[1]/div/div[2]/div/p"

  def alfamart_payment_channel
    self.wait_for_alfamart_radio
    self.alfamart_radio.click
    self.button_pay.click
    sleep 4
  end

  def indomaret_payment_channel
    self.indomaret_radio.click
    self.button_pay.click
    sleep 4
  end

  def atm_payment_channel
    self.transfer_radio.click
    self.button_pay.click
    sleep 4
  end

  def bukadompet_payment_channel
    self.bukadompet_radio.click
    fill_in 'deposit_password', :with => ENV['BUYER_PASSWORD']
    self.button_pay.click
    sleep 4
  end

  def klikpay_payment_channel
    self.bcaklikpay_radio.click
    self.button_pay.click
    sleep 4
  end
  
end
