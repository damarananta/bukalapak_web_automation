class BukaDompet < SitePrism::Page
  path = "/dompet"
  set_url(path)
  element :current_balance,                      ".u-txt--large .amount"

  element :tab_phone_credit, :xpath,             "//*[@id='mod-tab-1']/li[1]/a"
  element :tab_dataplan,     :xpath,             "//*[@id='mod-tab-1']/li[2]/a"
  element :tab_electricity,  :xpath,             "//*[@id='mod-tab-1']/li[3]/a"
  element :tab_gamevoucher,  :xpath,             "//*[@id='mod-tab-1']/li[4]/a"

  element :phone_number_field,                   "#phone_number"
  element :customer_number,                      "#customer_number"

  element :withdrawal_button,      :xpath,       "//a[text()='Cairkan Dana'][2]"
  element :phone_number_field,                   "#phone_number"
  element :customer_number,                      "#customer_number"

  element :phonecredit_buy_button,               "input[value='Beli Pulsa']"
  element :dataplan_buy_button,                  "input[value='Beli Paket Data']"
  element :electricity_buy_button,               "input[value='Beli Token Listrik']"
  element :gamevoucher_buy_button,               "input[value='Beli Voucher']"

  element :otp_button,             :xpath,       "//div[contains(@class,'js-tfa-request__button') and text()='Kirim Kode Otentikasi']"
  element :otp_field,                            "#otp"
  element :submit_otp_button,      :xpath,       "//div[contains(@class,'js-tfa-submit') and text()='Konfirmasi']"
  element :finish_otp_button,      :xpath,       "//div[contains(@class,'js-tfa-finish') and text()='Oke']"

  def withdrawal_deposit(token)
    self.withdrawal_button.click
    self.otp_button.click
    sleep 2
    self.otp_field.set(token)
    self.submit_otp_button.click
    self.finish_otp_button.click
  end

  def get_current_balance
      self.wait_for_current_balance
      @balance=current_balance.text
      @seller_balance_before=@balance.delete("Rp.").to_i
  end

  def get_current_balance_secondary_seller
      self.wait_for_current_balance
      @balance_secondary_seller=current_balance.text
      @secondary_seller_balance_before=@balance_secondary_seller.delete("Rp.").to_i
  end

  def check_buyer_balance_after_replacements(buyer_balance_before,new_price)
    @buyers_dompet_validation = buyer_balance_before - new_price
      expect(page).to have_no_content(add_delimiter(@buyers_dompet_validation))
  end

  def check_secondary_seller_balance_after_replacements(seller_balance_before,new_price)
    @seller_dompet_validation = seller_balance_before+new_price
      expect(page).to have_content(add_delimiter(@seller_dompet_validation))
  end

  def get_current_balance_buyer
      self.wait_for_current_balance
      @balance=current_balance.text
      @buyer_balance_before=@balance.delete("Rp.").to_i
  end


  def buy_vp_bukadompet_widget(phone_number,vp,payment,customer_number)
    case vp
    when "Pulsa"

      self.tab_phone_credit.click
      self.phone_number_field.set phone_number
      sleep 2
      self.phonecredit_buy_button.click
    when "Paket Data"
      self.tab_dataplan.click
      self.phone_number_field.set phone_number
      sleep 2
      self.dataplan_buy_button.click
    when "Token Listrik"
      self.tab_electricity.click
      self.customer_number.set customer_number
      sleep 2
      self.electricity_buy_button.click
    when "Voucher Game"
      self.tab_gamevoucher.click
      sleep 2
      self.gamevoucher_buy_button.click
    end
    sleep 3
  end
end
