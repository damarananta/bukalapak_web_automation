When /^agent buy goods from seller$/ do
  
  @home_page = HomePage.new
  @product_detail = ProductDetailPage.new
  @payment_purchases = PaymentPurchasesNewPage.new
  @confirmation_page = PaymentPurchasesConfirmationPage.new

  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER2_USERNAME'],ENV['BUYER2_PASSWORD'])

  @product_detail.load_product(ENV['SELLER_PRODUCT_URL'])
  @product_detail.click_beli
end

And(/^the product should has administration cost$/) do
  expect(page).to have_content("Biaya Administrasi")
  expect(page).to have_selector('.js-list-item--item.js-iv-review__agent-commission')
end

Then (/^agent pay using "([^"]*)"$/) do |payment_method|
  if payment_method == 'bukadompet'
    @payment_purchases.paid_with_bukadompet(ENV['BUYER_PASSWORD'])
  
  elsif payment_method == 'atm'
    @payment_purchases.choose_atm
  
  elsif payment_method == 'indomaret'
    @payment_purchases.choose_indomaret
  
  elsif payment_method == 'alfamart'
    @payment_purchases.choose_alfamart

  elsif payment_method == 'mandiri_clickpay'
    @payment_purchases.choose_mandiri_clickpay('1234567890', '9810')
    sleep 3
  else
    return false
  end
end
