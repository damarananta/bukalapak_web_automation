Then(/^click dashed filter location link$/) do
  @dashed_filter_location_page.link_location.click
  sleep 1
  @dashed_filter_page.wait_for_add_dashed_filter_button
end

Given(/^url location value (.*)$/) do |url|
  @url=url
  @dashed_filter_location_page.location_url.set("#{@url}")
end

Given(/^province location (.*)$/) do |province|
  @province=province
  @dashed_filter_location_page.location_province.set("#{@province}")
end

Given(/^city location value (.*)$/) do |city|
  @city=city
  @dashed_filter_location_page.location_city.set("#{@city}")
end

Then(/^create dashed filter location should success$/) do
  expect(@dashed_filter_merk_page.current_url).to include "/dashed_filter/locations"
  @dashed_filter_page.verify_success_message
end

When(/^Admin click edit button location dashed filter$/) do
  first(:xpath,DashedFilterLocationPage::EDIT_BUTTON).click
  sleep 1
end

Then(/^update dashed filter location should success$/) do
  @dashed_filter_location_page.update_dashed_filter_location_should_success(@url,@province,@city)
end

Then(/^delete dashed filter location should success$/) do
  @dashed_filter_page.verify_success_message
end
