And(/^Admin go to dashed filter page$/) do
  @dashed_filter_page.load
  @dashed_filter_page.wait_for_add_dashed_filter_button
end

Then(/^Admin click add button$/) do
  @dashed_filter_page.wait_until_flash_success_invisible(5)
  @dashed_filter_page.add_dashed_filter_button.click
  @dashed_filter_page.wait_for_save_dashed_filter_button(20)
end

Then(/^Admin click delete button dashed filter$/) do
  if page.has_css?(DashedFilterPage::DELETE_DASHED_FILTER_BUTTON)
    first(DashedFilterPage::DELETE_DASHED_FILTER_BUTTON).click
    page.driver.browser.switch_to.alert.accept
    @dashed_filter_page.wait_for_flash_success
  end
end

Given(/^Admin click simpan dashed filter$/) do
  @dashed_filter_page.save_dashed_filter_button.click
  @dashed_filter_page.wait_for_flash_success
end

And(/^click button filter$/) do
  @dashed_filter_page.dashed_filter_button.click
  sleep 3
end

When(/^Admin search dashed filter general with url "([^"]*)"$/) do |url|
  @dashed_filter_page.filter_general_url.set("#{url}")
end

Then(/^Admin should get result dashed filter general with url "([^"]*)"$/) do |url|
  url_result= @dashed_filter_page.category_list_name.text
  url_result.should==url
  @listCount = page.all(DashedFilterPage::DELETE_DASHED_FILTER_BUTTON).count
  @listCount.should==1
end
