Then(/^click dashed filter merk link$/) do
  @dashed_filter_merk_page.link_merk.click
  sleep 1
  @dashed_filter_page.wait_for_add_dashed_filter_button
end

Given(/^merk url value (.*)$/) do |url|
  @url=url
  @dashed_filter_merk_page.brand_url.set("#{@url}")
end

Given(/^merk name value (.*)$/) do |name|
  @name=name
  @dashed_filter_merk_page.brand_name.set("#{@name}")
end

Then(/^create dashed filter merk should success$/) do
  expect(@dashed_filter_merk_page.current_url).to include "/dashed_filter/brands"
  @dashed_filter_page.verify_success_message
end

When(/^Admin click edit button merk dashed filter$/) do
  first(:xpath,DashedFilterMerkPage::EDIT_BUTTON).click
  sleep 1
end

Then(/^update dashed filter merk should success$/) do
  @dashed_filter_merk_page.update_dashed_filter_merk_should_success(@url,@name)
end

Then(/^delete dashed filter merk should success$/) do
  @dashed_filter_page.verify_success_message
end
