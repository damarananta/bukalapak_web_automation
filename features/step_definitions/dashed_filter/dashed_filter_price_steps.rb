Then(/^click dashed filter price link$/) do
  @dashed_filter_price_page.link_price.click
  sleep 1
  @dashed_filter_page.wait_for_add_dashed_filter_button
end

Given(/^url price value (.*)$/) do |url|
  @url=url
  @dashed_filter_price_page.price_url.set("#{@url}")
end

Given(/^min price value (.*)$/) do |min_price|
  @min_price=min_price
  @dashed_filter_price_page.price_price_min.set("#{@min_price}")
end

Given(/^max price value (.*)$/) do |max_price|
  @max_price=max_price
  @dashed_filter_price_page.price_price_max.set("#{@max_price}")
end

Then(/^create dashed filter price should success$/) do
  expect(@dashed_filter_price_page.current_url).to include "/dashed_filter/prices"
  @dashed_filter_page.verify_success_message
end

When(/^Admin click edit button price dashed filter$/) do
  first(:xpath,DashedFilterPricePage::EDIT_BUTTON).click
  sleep 1
end

Then(/^update dashed filter price should success$/) do
  @dashed_filter_price_page.update_dashed_filter_price_should_success(@url,@min_price,@max_price)
end

Then(/^delete dashed filter price should success$/) do
  @dashed_filter_page.verify_success_message
end
