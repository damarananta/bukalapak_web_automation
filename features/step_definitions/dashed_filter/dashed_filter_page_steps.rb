Given(/^category page value (.*)$/) do |category|
  sleep 2
  expect(page).to have_selector(:css, "a.chzn-single.chzn-default > span", wait: 3, visible: true)
  @dashed_filter_page.category_field.click
  expect(page).to have_selector(:css, "#dashed_filter_page_category_id_chzn_o_#{category}", wait: 4,visible: true)
  find("#dashed_filter_page_category_id_chzn_o_#{category}").click
end

Given(/^condition page option (.*)$/) do |condition|
  find(:xpath, ".//*[@id]/div[2]/label[#{condition}]/span").click
end

Given(/^price page value (.*)$/) do |price|
  @dashed_filter_page.wait_for_price_page(3)
  @dashed_filter_page.price_page.set("#{price}")
end

Given(/^location page value (.*)$/) do |location|
  @dashed_filter_page.wait_for_location_page(3)
  @dashed_filter_page.location_page.set("#{location}")
end

Given(/^merk page value (.*)$/) do |merk|
  @dashed_filter_page.wait_for_brand_page(3)
  @dashed_filter_page.brand_page.set("#{merk}")
end

Given(/^attach banner page file (.*)$/) do |banner_file|
  @banner_file=DashedFilterPage::PATH_IMAGE+banner_file.gsub(/\s+/, '')
  sleep 1
  @dashed_filter_page.attach_banner_page.set(File.absolute_path(@banner_file))
  sleep 1
end

Given(/^meta desc dashed filter text (.*)$/) do |meta_desc|
  @meta_desc=meta_desc
  @dashed_filter_page.meta_desc_page.set("#{@meta_desc}")
end

Given(/^page dashed filter text (.*)$/) do |desc|
  @desc=desc
  @dashed_filter_page.desc_page.set("#{@desc}")
end

Then(/^create dashed filter page should success$/) do
  current_url=ENV['BASE_URL']+'/dashed_filter/pages'
  expect(page).to have_current_path(current_url, url: true)
end

Then(/^create dashed filter page should fail$/) do
  current_url=ENV['BASE_URL']+'/dashed_filter/pages'
  expect(page).to have_current_path(current_url, url: true)
end

Then(/^delete page dashed filter should success$/) do
  @dashed_filter_page.verify_success_message
end

Then(/^Admin click edit button page dashed filter$/) do
  first(:xpath,DashedFilterPage::EDIT_DASHED_FILTER_BUTTON).click
  sleep 1
end

Then(/^update dashed filter page should success$/) do
  expect(@trending_keyword_page.current_url).to include "/dashed_filter/pages"
end

Then(/^capture one of dashed filter page wording$/) do
  capture_dashed_filter_desc_wording
end

When(/^Admin click detail dashed filter page$/) do
  link=find(:xpath,'//tr[1]/td[6]').text
  visit link
  sleep 1
end

Then(/^dashed filter page detail should contain page wording$/) do
  verify_dashed_filter_detail
end

Then(/^bhlm combine should contain page wording$/) do
  @expect_combination_meta_desc= DashedFilterPage::COMBINATION_META_DESC_EXP
  page.should have_xpath("//meta[2][@content='#{@expect_combination_meta_desc}']",visible: false)
  description_detail=@dashed_filter_page.description_detail_text.text
  description_detail.should==@expect_combination_meta_desc
end

Given(/^Admin click simpan dashed filter page$/) do
  page.execute_script "window.scrollBy(0,500)"
  @dashed_filter_page.save_dashed_filter_page_button.click
  sleep 3
end

When(/^Admin search dashed filter page with param "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)"$/) do |category,condition,price,location,merk|
  @dashed_filter_page.search_dashed_filter(category,condition,price,location,merk)
end

Then(/^Admin should get result dashed filter page with description "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)"$/) do |category, condition,price,location,merk|
  @dashed_filter_page.verify_search_result(category,condition,price,location,merk)
end
