When /^user buy goods from seller$/ do
  @home_page = HomePage.new
  @product_detail = ProductDetailPage.new
  @payment_purchases = PaymentPurchasesNewPage.new
  @confirmation_page = PaymentPurchasesConfirmationPage.new

  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  login(ENV['BUYER7_USERNAME'], ENV['BUYER7_PASSWORD'])

  @product_detail.load_product(ENV['SELLER_PRODUCT_URL'])
  @product_detail.click_beli
end

And (/^use "([^"]*)" button$/) do |choose_button|
  @payment_purchases.choose_bukadompet_tfa(choose_button)
  @confirmation_page.view_invoice
end

Then(/^the invoice's status should be "([^"]*)"$/) do |status|
  sleep 3
  expect(page).to have_content(status)
end
