#Desktop: Scenario 1 Paid
When /^user create transaction using bukadompet at staging$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new
  @invoice_page = InvoicesPage.new
  @status_invoice = InvoiceDetailPage.new
  @bukadompet_page = BukaDompet.new

  visit_url '/'
  login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

  @product_detail.load_product("/p/hobi-hiburan/sport/others-191/4-jual-bat-ping-pong")
  @product_detail.click_beli

  @checkout_page.checkout
end

And /^user has deposit in bukadompet and enough to buy the goods at staging$/ do
  @payment_channel.choose_bukadompet(ENV['BUYER3_PASSWORD'])
  sleep 3
  @invoice_page.view_invoice
end

Then /^invoice status should be paid at staging$/ do
  @status_invoice.invoice_should_be_paid
end

Then(/^E-voucher invoice status should be paid at staging$/) do
  @invoice_detail_page.wait_for_payment_status_text
  @invoice_detail_page.vp_invoice_should_be_paid
end

And /^deposit should reduced by the goods price amount at staging$/ do
  @bukadompet_page.load
  expect(page).to have_content(@current_deposit)
end
