When /^user create transaction using alfamart as payment method at staging$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new
  @invoices_page = InvoicesPage.new

  visit_url '/'
  login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

  @product_detail.load_product("/p/hobi-hiburan/sport/basket/1-jual-bola-basket")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_alfamart
  @invoices_page.view_invoice
end

Then /^invoice status should be in wating until any confirmation from alfamart that has been paid at staging$/ do
  within('.memo-header.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end

Then(/^E-voucher invoice status should be in wating until any confirmation from Alfamart that has been paid at staging$/) do
  @invoice_detail_page.wait_for_payment_status_text
  @invoice_detail_page.vp_invoice_should_be_in_waiting
end

Then /^invoice status in mweb should be in wating until any confirmation from alfamart that has been paid at staging$/ do
  expect(page).to have_content('Menunggu Pembayaran')
end
