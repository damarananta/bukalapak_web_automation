When /^user create transaction using Klik BCA at staging$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new

  visit_url '/'
  login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

  @product_detail.load_product("/p/hobi-hiburan/sport/basket/1-jual-bola-basket")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_bca
end

And /^user go back at staging$/ do
  go_back
end
 
Then /^transaction should not be paid at staging$/ do
  within('.memo-header.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end
