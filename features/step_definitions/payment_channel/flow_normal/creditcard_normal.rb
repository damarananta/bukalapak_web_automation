# Desktop: Scenario 1
When /^user using credit card for the transaction at staging$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new
  @invoices_page = InvoicesPage.new

  visit_url '/'
  login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

  @product_detail.load_product("/p/hobi-hiburan/sport/others-191/4-jual-bat-ping-pong")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_credit_card
end

But /^the data filled inappropriately at staging$/ do
  @payment_channel.fill_data_cc_staging("4411111111111118", "123", "112233")
  # sleep 10
  # fill_in 'PaRes', :with => '112233'
  # click('OK') 
end

Then /^transaction could not continue at staging$/ do
  @invoices_page.invoices_page_search
  sleep 2

  within('.memo-header.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end
