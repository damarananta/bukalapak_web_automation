When /^user create transaction via transfer at staging$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new
  @invoices_page = InvoicesPage.new

  visit_url '/'
  login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

  @product_detail.load_product("/p/hobi-hiburan/sport/basket/1-jual-bola-basket")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_atm
  @invoices_page.view_invoice
  sleep 3
end

Then /^invoice status should be in wating at staging$/ do
  within('.memo-header.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end

And /^unique code should match at staging$/ do
  puts "Actual Unique Code: #{find(".block-payment-code--highlight").text}"
  expect(@payment_channel).to have_unique_code
end

Then(/^E-voucher unique code should match at staging$/) do
  @invoice_detail_page = InvoiceDetailPage.new
  expect(@invoice_detail_page.unique_code_text.text).to eq(@unique_code)
end

Then(/^phone credits invoice status in mweb should be in wating at staging$/) do
  expect(page).to have_content('Menunggu Pembayaran')
end
