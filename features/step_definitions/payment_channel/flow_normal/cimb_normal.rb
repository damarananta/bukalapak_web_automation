When /^user create transaction using CIMB click at staging$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new
  @invoices_page = InvoicesPage.new

  visit_url '/'
  login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])
  
  @product_detail.load_product("/p/hobi-hiburan/sport/others-191/4-jual-bat-ping-pong")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_cimb
end

And /^user cancelled the transaction at staging$/ do
  find('.btn_cancel_cimbclicks').click
  modal_accept_confirm
  sleep 5
end

Then /^transaction will not get paid at staging$/ do
  @invoices_page.invoices_page_search
  sleep 5
  expect(page).to have_content('Menunggu Pembayaran')
end
