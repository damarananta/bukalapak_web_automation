When /^user create transaction using Mandiri clickpay at staging$/ do
  
  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new

  visit_url '/'
  login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

  @product_detail.load_product("/p/hobi-hiburan/sport/basket/1-jual-bola-basket")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_mandiri_clickpay('1234567890', '9810')
end

But /^user do not continue the transaction at staging$/ do
  go_back

  within('.page-bottom.clearfix') do
    find('.back-link.back-link--right').click  
  end
end

Then /^transaction should not getting paid at staging$/ do
  within('.memo-header.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end
