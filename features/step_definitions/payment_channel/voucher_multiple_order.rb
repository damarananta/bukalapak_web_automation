When /^user buy goods from different seller$/ do
  @add_cart = CartsPage.new
  @payment_channel = PaymentChannel.new
  @invoice_page = InvoicesPage.new

  visit_url '/'
  login(ENV['BUYER6_USERNAME'], ENV['BUYER6_PASSWORD'])

  @add_cart.add_to_cart_btp ENV['SELLER_PRODUCT_URL']
  close_cart_pop_up

  @add_cart.add_to_cart_btp ENV['SELLER2_PRODUCT_URL']
  close_cart_pop_up

  payment_multiple_order
end

When(/^use voucher$/) do
  use_voucher(ENV['VOUCHER_NOMINAL_CODE'])
end

When(/^choose "([^"]*)" as the payment method$/) do |payment_method|
  if payment_method == 'atm'
    choose_atm
  elsif payment_method == 'bukadompet'
    @payment_channel.choose_bukadompet_voucher(ENV['BUYER6_PASSWORD'])
    sleep 2
    @invoice_page.view_invoice
  elsif payment_method == 'indomaret'
    choose_indomaret
  elsif payment_method == 'alfamart'
    choose_alfamart
  elsif payment_method == 'bca klikpay'
    choose_bca
    click_link 'Lihat Tagihan'
  elsif payment_method == 'mandiri clickpay'
    choose_mandiri
    click_link 'Lihat Tagihan'
  else
    return false
  end
end

Then(/^the invoice state should be on "([^"]*)"$/) do |state|
  expect(page).to have_content(state, wait: 10)
end

Then(/^the invoice should have unique code$/) do
  within('.block-payment-code--highlight') do
    expect(page).to have_content(@unique_code)
  end
end


Then(/^the voucher should be applied on the invoice$/) do
  @end_total = @total_before - @voucher
  expect(page).to have_content(add_delimiter(@end_total))
end
