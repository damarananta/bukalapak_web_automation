When /^user buys the goods$/ do
  @home_page = HomePage.new
  @product_detail = ProductDetailPage.new
  @payment_purchases = PaymentPurchasesNewPage.new
  @confirmation_page = PaymentPurchasesConfirmationPage.new

  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  login(ENV['BUYER5_USERNAME'],ENV['BUYER5_PASSWORD'])

  @product_detail.load_product(ENV['SELLER_PRODUCT_URL'])
  @product_detail.click_beli
end

And (/^user using mixed payment "([^"]*)" and bukadompet$/) do |payment_method|
  if(payment_method == "bca" || payment_method == "cimb")
    @payment_purchases.choose_mix_payment(payment_method)
    go_back
    @confirmation_page.invoices_page_search
  else      
    @payment_purchases.choose_mix_payment(payment_method)
    @confirmation_page.view_invoice
  end
end

Then(/^the invoice status should be in "([^"]*)"$/) do |status|
  expect(page).to have_content(status, wait: 10)
end

And /^via transfer should has unique code$/ do
  within('.block-payment-code--highlight') do
    expect(page).to have_content(@unique_code)
  end
end 
