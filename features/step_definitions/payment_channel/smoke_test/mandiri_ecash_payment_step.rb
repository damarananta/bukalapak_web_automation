When /^user create transaction using Mandiri e-cash$/ do
  
  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new
  @invoices_page = InvoicesPage.new

  visit_url '/'
  login('kizaru', 'apaajadeh')
  
  @product_detail.load_product("/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_mandiri_ecash
end

And /^user cancel the transaction$/ do
  modal_accept_confirm
  sleep 5
end

Then /^transaction could not be paid$/ do
  @invoices_page.invoices_page_search
  sleep 5
  expect(page).to have_content('Menunggu Pembayaran')
end


# Mobile Web
When /^user create transaction using Mandiri e-cash in mobile web$/ do
  mweb_buy_product('kizaru', 'apaajadeh', '/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas')
  find('.js-collapsible-list.clearfix', :text => 'Mandiri E-Cash').click
  payment_choosen_pay
  sleep 3
end

And /^user cancel the transaction in mobile web$/ do
  if(page.has_content?('Masukkan nomor dan PIN e-cash Anda'))
    go_back
  else
    expect(page).to have_content('Menunggu Pembayaran')
  end
end

Then /^transaction could not be paid in mobile web$/ do
  expect(page).to have_content('Menunggu Pembayaran')
end
