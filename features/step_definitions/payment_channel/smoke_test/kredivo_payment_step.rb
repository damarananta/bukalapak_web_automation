#
# Smoke Test - Desktop Web
#
Given(/^user logged-in to BukaLapak desktop web$/) do

  visit_url('/')
  @home_page = HomePage.new
  @home_page.login(ENV['CREDENTIAL_BUYER_USERNAME'], ENV['CREDENTIAL_BUYER_PASSWORD'])

end

Then(/^user make a transaction$/) do

  p = '/p/rumah-tangga/ruang-makan/5aaat5-jual-gelas-antik-misterius-warna-warni-ceria'
  
  @product_detail=ProductDetailPage.new
  @product_detail.load_product(p)
  @product_detail.click_beli

  @checkout_page = CheckoutPage.new
  @checkout_page.checkout

end

When(/^user chose Kredivo for payment method with "Within 30-days payment" option$/) do

  @kredivo_payment_type = '30_days'

  @payment_channel = PaymentChannel.new
  @payment_channel.choose_kredivo(@kredivo_payment_type)

  sleep 3

end

Then(/^related payment option must be selected on Kredivo payment page$/) do

  @kredivo_payment_page = KredivoPaymentPage.new
  @kredivo_payment_page.verify_selected_payment_option(@kredivo_payment_type)

end

When(/^user entered invalid login credential \(desktop web\)$/) do

  mobile_number = '00012345678'
  password = '000123'

  @kredivo_payment_page.login(mobile_number, password)
  sleep 3

end

Then(/^error message will be appeared \(desktop web\)$/) do

  @kredivo_payment_page.verify_invalid_login_message

end

When(/^user clicked "Cancel and Go Back to BukaLapak" link$/) do

  @kredivo_payment_page.cancel_and_go_back_to_bukalapak
  sleep 3

end

Then(/^related invoice will be appeared with "Waiting for Payment" status$/) do

  #invoice page
  find('.transaction-memo').should have_text('Menunggu Pembayaran')

end

And(/^with "Kredivo" payment method$/) do

  #invoice page
  find('.transaction-memo').should have_text('Kredivo')

end

#
# Smoke Test - Mobile Web
#
When(/^user make a transaction on BukaLapak mobile web$/) do
  mweb_buy_product(ENV['CREDENTIAL_BUYER_USERNAME'], ENV['CREDENTIAL_BUYER_PASSWORD'], '/p/rumah-tangga/ruang-makan/5aaat5-jual-gelas-antik-misterius-warna-warni-ceria')
  sleep 3
end

And(/^chose Kredivo for payment method$/) do
  #previous code
  #find('.js-collapsible-list.clearfix', :text => 'Kredivo').click

  #new temporary code for white-list ab-test
  el = '.js-collapsible-list.clearfix'
  text_kredivo = 'Kredivo'
  text_whitelist = 'Cicilan Tanpa Kartu Kredit'

  if page.has_css?(el, :text => text_kredivo)
    find(el, :text => text_kredivo).click
  else
    find(el, :text => text_whitelist).click
  end
  #--------

  within('.js-kredivo-form-wrapper') do

    within('#kredivo_payment_type-button') do
      find("option[value='30_days']").click
    end

  end

  find('.js-kredivo-form-wrapper').should have_text('Bayar dalam 30 hari')

  payment_choosen_pay
  sleep 3

  #Kredivo account confirmation pop-up (AB TEST)
  btn = '.button.button--mini.white.js-kredivo-with-account-btn.ui-btn.ui-shadow.ui-corner-all'
  find(btn).click if page.has_css?(btn)
  #--------

  sleep 3
end

When(/^user entered invalid login credential$/) do
  within('.box.mb-30:nth-child(1)') do
    find('.radio:nth-child(1)').click
  end

  within('.box.mb-20') do
    find('#inputMobileNumber').set('00012345678')
    find('#inputPassword').set('000123')
    find('.button', :text => 'Login').click
    sleep 3
  end
end

Then(/^error message will be appeared$/) do
  expect(page).to have_content('Invalid mobile number and/or passcode.')
end

When(/^user tapped "Cancel and Go Back to BukaLapak" link$/) do
  find_button('Batalkan dan kembali ke Bukalapak').click
  sleep 3
end

Then(/^related invoice will be appeared with "Waiting for Payment" on "Status" tab$/) do
  #on this step, browser (firefox) will open dweb invoice instead of mweb invoice
  find('.memo-header.clearfix').should have_text('Menunggu Pembayaran')

=begin #code if mweb invoice appeared on browser (still untried)

  within('div#js-tab-invoice-status') do
    find('dl.deflist.clearfix').should have_text('Menunggu Pembayaran')
  end

=end
  
end

And(/^with Kredivo payment guide$/) do
  #on this step, browser (firefox) will open dweb invoice instead of mweb invoice
  find('.payment-total__info').should have_text('Petunjuk Pembayaran Kredivo')

=begin #code if mweb invoice appeared on browser (still untried)

  within('div#js-tab-invoice-status') do
    find('.payment-total__info').should have_text('Petunjuk Pembayaran Kredivo')
  end

=end

end
