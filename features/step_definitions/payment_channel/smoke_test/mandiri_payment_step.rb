When /^user create transaction using Mandiri clickpay$/ do
  
  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new

  visit_url '/'
  login('prakpcd2', 'enggaktaupasswordnya')

  @product_detail.load_product("/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_mandiri_clickpay('1234567890', '9810')
end

But /^user do not continue the transaction$/ do
  go_back

  within('.page-bottom.clearfix') do
    find('.back-link.back-link--right').click  
  end
end

Then /^transaction should not getting paid$/ do
  within('.memo-header.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end
