When /^user create transaction using Klik BCA$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new

  visit_url '/'
  login('mrqae', 'apaajadeh')
  
  @product_detail.load_product("/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_bca
end

And /^user go back$/ do
  go_back
end

Then /^transaction should not be paid$/ do
  within('.memo-header.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end

# Mobile Web
When /^user create transaction using BCA clickpay$/ do
  mweb_buy_product('mrqae', 'apaajadeh', '/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas')
  find('.js-collapsible-list.clearfix', :text => 'BCA KlikPay (KlikBCA Individu)').click
  payment_choosen_pay
  sleep 3
end

And /^user return back$/ do
  go_back
end

Then /^transaction should in waiting and not to be paid/ do
  within('#js-tab-invoice-status') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end
