When /^user create transaction using alfamart as payment method$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new
  @invoices_page = InvoicesPage.new

  visit_url '/'
  login('ahyarah', 'apaajadeh')

  @product_detail.load_product("/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_alfamart
  @invoices_page.view_invoice
end

Then /^invoice status should be in wating until any confirmation from alfamart that has been paid$/ do
  within('.memo-header.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end

Then(/^E-voucher invoice status should be in wating until any confirmation from Alfamart that has been paid$/) do
  @invoice_detail_page.wait_for_payment_status_text
  @invoice_detail_page.vp_invoice_should_be_in_waiting
end

Then /^invoice status in mweb should be in wating until any confirmation from alfamart that has been paid$/ do
  expect(page).to have_content('Menunggu Pembayaran')
end

# Mobile Web
When /^user create transaction using alfamart$/ do
  mweb_buy_product('ahyarah', 'apaajadeh', '/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas')
  find('.js-collapsible-list.clearfix', :text => 'Alfamart').click
  payment_choosen_pay
  sleep 3
end

Then /^invoice status should be in wating until any confirmation from alfamart$/ do
  scroll_down(100)
  click_link('Lihat Tagihan Pembayaran')
  sleep 2
  expect(page).to have_content('Menunggu Pembayaran')
end
