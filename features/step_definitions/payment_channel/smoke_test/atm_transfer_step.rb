When /^user create transaction via transfer$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new
  @invoices_page = InvoicesPage.new

  visit_url '/'
  login('jappri', 'apaajadeh')

  @product_detail.load_product("/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_atm
  @invoices_page.view_invoice
  sleep 3
end

Then /^invoice status should be in wating$/ do
  within('.memo-header.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end

And /^unique code should match$/ do
  puts "Actual Unique Code: #{find(".block-payment-code--highlight").text}"
  expect(@payment_channel).to have_unique_code
end

Then(/^E-voucher unique code should match$/) do
  @invoice_detail_page = InvoiceDetailPage.new
  expect(@invoice_detail_page.unique_code_text.text).to eq(@unique_code)
end

Then(/^phone credits invoice status in mweb should be in wating$/) do
  expect(page).to have_content('Menunggu Pembayaran')
end

# Mobile Web
When /^user create mweb transaction via transfer$/ do
  mweb_buy_product('jappri', 'apaajadeh', '/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas')
  mweb_choose_atm
end

Then /^invoice status should be waiting$/ do
  within('.deflist.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end

And /^unique code should be matching$/ do
  within('.block-payment-code__amount') do
    expect(page).to have_content(@unique_code)
  end
end
