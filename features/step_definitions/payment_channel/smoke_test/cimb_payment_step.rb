When /^user create transaction using CIMB click$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new
  @invoices_page = InvoicesPage.new

  visit_url '/'
  login('gandimustofa', '1234567890')
  
  @product_detail.load_product("/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_cimb
end

And /^user cancelled the transaction$/ do
  find('.btn_cancel_cimbclicks').click
  modal_accept_confirm
  sleep 5
end

Then /^transaction will not get paid$/ do
  @invoices_page.invoices_page_search
  sleep 5
  expect(page).to have_content('Menunggu Pembayaran')
end


# Mobile Web
When /^user create transaction in mobile web using CIMB click$/ do
  mweb_buy_product('gandimustofa', '1234567890', '/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas')
  find('.js-collapsible-list.clearfix', :text => 'CIMB Clicks/Rekening Ponsel').click
  payment_choosen_pay
  sleep 3
end

And /^user cancels the transaction$/ do
  if(page.has_selector?('.btn_cancel_cimbclicks'))
    find('.btn_cancel_cimbclicks').click
    modal_accept_confirm
    sleep 5
  else
    go_back
    expect(page).to have_content('Menunggu Pembayaran')
  end
end

Then /^transaction in mobile web will not get paid$/ do
  expect(page).to have_content('Menunggu Pembayaran')
end
