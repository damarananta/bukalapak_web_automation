# Desktop: Scenario 1
When /^user using credit card for the transaction$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new
  @invoices_page = InvoicesPage.new

  visit_url '/'
  login('stevgerrard', 'apaajadeh')

  @product_detail.load_product("/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_credit_card
end

But /^the data filled inappropriately$/ do
  @payment_channel.fill_data_credit_card("000000000000", "001")
  @payment_page_url = URI.parse(current_url)
  find('.btn.btn--large.btn--red').click
end

Then /^transaction could not continue$/ do
  expect(page).to have_current_path(@payment_page_url, url: true)
  @invoices_page.invoices_page_search
  sleep 2

  within('.memo-header.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end

#Desktop: Scenario 2
When /^user using credit card as payment$/ do
  
  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new
  @invoices_page = InvoicesPage.new

  visit_url '/'
  login('stevgerrard', 'apaajadeh')

  @product_detail.load_product("/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_credit_card
end

But /^the data has not filled$/ do
  @payment_page_url = URI.parse(current_url)
  
  go_back
end

Then /^the status in waiting$/ do 
  expect(page).to have_current_path(@payment_page_url, url: true)

  @invoices_page.invoices_page_search
  sleep 2

  within('.memo-header.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end


# Mobile Web
When /^user using credit card in mobile web for the transaction$/ do
  mweb_buy_product('stevgerrard', 'apaajadeh', '/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas')
  find('.js-collapsible-list.clearfix', :text => 'Kartu Visa/Mastercard').click
  payment_choosen_pay
  sleep 3
end

But /^the data filled in mobile web inappropriately$/ do
  fill_in 'cc_number_input', :with => '000000000000'
  fill_in 'cc_cvv', :with => '001' 
  go_back
  modal_accept_confirm
  sleep 2
end

Then /^transaction in mobile web could not continue$/ do 
  expect(page).to have_content('Menunggu Pembayaran')
end
