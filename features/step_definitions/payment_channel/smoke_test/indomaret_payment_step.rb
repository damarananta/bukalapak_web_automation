When /^user create transaction using indomaret as payment method$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new
  @invoices_page = InvoicesPage.new

  visit_url '/'
  login('adisaputrak', 'apaajadeh')

  @product_detail.load_product("/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas")
  @product_detail.click_beli

  @checkout_page.checkout
  @payment_channel.choose_indomaret
  @invoices_page.view_invoice
end

Then /^invoice status should be in wating until the order has paid via indomaret$/ do
  within('.memo-header.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end

Then(/^E-voucher invoice status should be in wating until the order has paid via Indomaret$/) do
  @invoice_detail_page.wait_for_payment_status_text
  @invoice_detail_page.vp_invoice_should_be_in_waiting
end

Then(/^invoice status in mweb should be in wating until the order has paid via indomaret$/) do
  expect(page).to have_content('Menunggu Pembayaran')
end

# Mobile Web
When /^user create transaction using indomaret as payment method in mobile browser$/ do
  mweb_buy_product('adisaputrak', 'apaajadeh', '/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas')
  find('.js-collapsible-list.clearfix', :text => 'Indomaret').click
  payment_choosen_pay
  sleep 3
end

Then(/^invoice status should be in wating until there is a confirmation from indomaret$/) do
  scroll_down(100)
  click_link('Lihat Tagihan Pembayaran')
  sleep 2
  expect(page).to have_content('Menunggu Pembayaran')
end
