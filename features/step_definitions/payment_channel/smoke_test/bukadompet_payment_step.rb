#Desktop: Scenario 1 Paid
When /^user create transaction using bukadompet$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new
  @invoice_page = InvoicesPage.new
  @status_invoice = InvoiceDetailPage.new
  @bukadompet_page = BukaDompet.new

  visit_url '/'
  login(ENV['CREDENTIAL_BUYER_USERNAME'], ENV['CREDENTIAL_BUYER_PASSWORD'])

  @product_detail.load_product("/p/hobi-koleksi/mainan/lain-lain-345/3p2n83-jual-tutup-botol")
  @product_detail.click_beli

  @checkout_page.checkout
end

And /^user has deposit in bukadompet and enough to buy the goods$/ do
  @payment_channel.choose_bukadompet(ENV['CREDENTIAL_BUYER_PASSWORD'])
  @invoice_page.view_invoice
end

Then /^invoice status should be paid$/ do
  @status_invoice.invoice_should_be_paid
end

And /^deposit should reduced by the goods price amount$/ do
  @bukadompet_page.load
  expect(page).to have_content(@current_deposit)
end

#Desktop: Scenario 2 Not Enough Balance and Unpaid
When /^user create transaction via bukadompet$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new

  visit_url '/'
  login('ahyarah', "apaajadeh")

  @product_detail.load_product("/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas")
  @product_detail.click_beli

  @checkout_page.checkout
end

And /^user has no enough deposit in bukadompet to continue to buy the goods$/ do
  choose "BukaDompet"
  within('.payment-terms.js-payment__deposit--invalid.js-payment__deposit--invalid-max.active') do
    expect(page).to have_content('0')
  end
end

Then /^the goods can not be paid$/ do
  expect(page).to have_no_selector('.form-field-item.payment-info-content__deposit__password')
  expect(page).to have_content('Saldo BukaDompet-mu tidak mencukupi untuk membayar transaksi ini. Silakan pilih metode pembayaran lainnya')
  expect(page).to have_no_content('BERHASIL!')
end

#Desktop: Scenario 3 Wrong Password and Unpaid
When /^user create transaction using bukadompet with wrong password$/ do

  @product_detail = ProductDetailPage.new
  @checkout_page = CheckoutPage.new
  @payment_channel = PaymentChannel.new

  visit_url '/'
  login(ENV['CREDENTIAL_BUYER_USERNAME'], ENV['CREDENTIAL_BUYER_PASSWORD'])

  @product_detail.load_product("/p/hobi-koleksi/mainan/lain-lain-345/3p2n83-jual-tutup-botol")
  @product_detail.click_beli

  @checkout_page.checkout
end

And /^user has enough deposit in bukadompet to continue to buy the goods$/ do
  expect(page).to have_content('Bayar lebih mudah dan cepat dengan menggunakan BukaDompet')
end

But /^the filled password is wrong$/ do
  @payment_channel.choose_bukadompet("salahpassword")

  @payment_page_url = URI.parse(current_url)

  within('.form-field-item.payment-info-content__deposit__password') do
    find('.msg', :text => 'Verifikasi Password Gagal')
  end
end

Then /^the transaction should not be continued$/ do
  sleep 3
  expect(page).to have_current_path(@payment_page_url, url: true)
  page.should have_no_content('BERHASIL!')
end

Then(/^mweb invoice status should be paid$/) do
  expect(page).to have_content('Dibayar')
end

And /^user has no enough deposit in bukadompet to continue to buy the goods via mweb$/ do
  mweb_wording_has_no_balance
end

And /^user has enough deposit in bukadompet to continue to buy the goods via mweb$/ do
  @m_pulsa_new_page = MobileNewPage.new
  @m_pulsa_new_page.bukadompet_radio.click

  expect(page).to have_content('Bayar lebih mudah dan cepat dengan menggunakan BukaDompet')
end


# Mobile Web
When /^user create transaction in mobile web using bukadompet$/ do
  mweb_buy_product(ENV['CREDENTIAL_BUYER_USERNAME'], ENV['CREDENTIAL_BUYER_PASSWORD'], '/p/hobi-koleksi/mainan/lain-lain-345/3p2n83-jual-tutup-botol')
  find('.js-collapsible-list.clearfix', :text => 'BukaDompet').click
end
And /^user has deposit in bukadompet and enough to buy the goods in mobile web$/ do
  choose_bukadompet_mweb(ENV['CREDENTIAL_BUYER_PASSWORD'])
  expect(page).to have_content('BERHASIL')
  click_on('Lihat Tagihan Pembayaran')
  sleep 5
end

Then /^invoice status in mobile web should be paid$/ do
  expect(page).to have_content('Dibayar')
end

And /^deposit would reduced by the goods price amount$/ do
  @current_deposit = @deposit - @price
  visit_url '/dompet'
  expect(page).to have_content(add_delimiter(@current_deposit))
end


When /^user create transaction in mweb via bukadompet$/ do
  mweb_buy_product('ahyarah', 'apaajadeh', '/p/hobi-koleksi/mainan/lain-lain-345/3p2n83-jual-tutup-botol')
  find('.js-collapsible-list.clearfix', :text => 'BukaDompet').click
  sleep 3
end

And /^user has no enough deposit in mweb bukadompet to continue to buy the goods$/ do
  expect(page).to have_no_selector('#js-iv-deposit')
end

Then /^the goods in mweb can not be paid$/ do
  expect(page).to have_no_content('BERHASIL')
  expect(page).to have_no_content('Dibayar')
end

When /^user create transaction in mweb using bukadompet with wrong password$/ do
  mweb_buy_product(ENV['CREDENTIAL_BUYER_USERNAME'], ENV['CREDENTIAL_BUYER_PASSWORD'], '/p/hobi-koleksi/mainan/lain-lain-345/3p2n83-jual-tutup-botol')
  find('.js-collapsible-list.clearfix', :text => 'BukaDompet').click
end

And /^user has enough deposit in bukadompet to continue to buy the goods in mweb$/ do
  expect(page).to have_selector('#js-iv-deposit')
end

But /^the filled password is wrong in mweb$/ do
  choose_bukadompet_mweb "passwordsalah"
end

Then /^the transaction in mweb should not be continued$/ do
  page.should have_no_content('BERHASIL')
end
