Then(/^Admin go to Manage Discount & Campaign page$/) do
  @manage_dicount_campaign_page = ManageDiscountCampaignPage.new
  @manage_dicount_campaign_page.load
end

Then(/^access campaign setting$/) do
  @manage_dicount_campaign_page.link_campaign_setting.click
end

Then(/^click create new campaign button$/) do
  @manage_dicount_campaign_page.create_new_campaign_button.click
  current_url=ENV['BASE_URL']+'/payment/campaigns/new'
  expect(page).to have_current_path(current_url, url: true)
end

Given(/^campaign title "([^"]*)"$/) do |title|
  @title=title + generate_random_string(20)
  @manage_dicount_campaign_page.campaign_title.set("#{@title}")
end

Given(/^campaign upload banner "([^"]*)"$/) do |file_banner|
  path='./features/support/image/'
  @file_banner=path+file_banner.gsub(/\s+/, '')
  @manage_dicount_campaign_page.campaign_banner.set(File.absolute_path("#{@file_banner}"))
end

Given(/^campaign banner open new window "([^"]*)"$/) do |status|
  @manage_dicount_campaign_page.campaign_open_new_window.click
  @manage_dicount_campaign_page.campaign_yes_option.click
end

Given(/^campaign redirect url "([^"]*)"$/) do |redirect_url|
  @manage_dicount_campaign_page.campaign_redirect_url.set(redirect_url)
end

Given(/^campaign tooltip "([^"]*)"$/) do |tooltip|
  @manage_dicount_campaign_page.campaign_tooltip.set(tooltip)
end

Given(/^campaign meta description "([^"]*)"$/) do |meta_desc|
  @manage_dicount_campaign_page.campaign_meta_desc.set(meta_desc)
  page.execute_script "window.scrollBy(0,40)"
end

Given(/^campaign start time "([^"]*)"$/) do |s_time|
  @s_time=s_time
end

Given(/^campaign end time "([^"]*)"$/) do |e_time|
  @e_time=e_time
end

Given(/^campaign description "([^"]*)"$/) do |desc|
  @manage_dicount_campaign_page.campaign_desc.set(desc)
end

Given(/^campaign sub desc "([^"]*)"$/) do |sub_desc|
  @manage_dicount_campaign_page.campaign_sub_desc.set(sub_desc)
end

Given(/^click button save campaign$/) do
  @manage_dicount_campaign_page.save_new_campaign_button.click
  sleep 2
end

Then(/^create new campaign should success$/) do
  message= @manage_dicount_campaign_page.flash_message.text
  message.should==ManageDiscountCampaignPage::SUCCESS_CREATE_CAMPAIGN_MESSAGE
end

When(/^delete campaign if exist$/) do
  @count_campaign= ManageDiscountCampaignPage::XPATH_CAMPAIGN_LIST_DASHBOARD
  if page.has_xpath?("#{@count_campaign}")
    sleep 2
    first(:xpath, "#{@count_campaign}").click
    sleep 1
    page.driver.browser.switch_to.alert.accept
    sleep 2
  end
end

Then(/^click edit campaign button$/) do
  sleep 1
  @manage_dicount_campaign_page.edit_campaign_button.click
  sleep 1
end

Then(/^edit campaign should success$/) do
  message= @manage_dicount_campaign_page.flash_message.text
  message.should==ManageDiscountCampaignPage::SUCCESS_EDIT_CAMPAIGN_MESSAGE
end

Then(/^click checkbox active campaign$/) do
  sleep 1
  @manage_dicount_campaign_page.checkbox_active_campaign.click
  sleep 1
end

Then(/^click filter campaign button$/) do
  sleep 1
  @manage_dicount_campaign_page.filter_campaign_button.click
  sleep 1
end

Then(/^filter active campaign should success$/) do
  @count_campaign= ManageDiscountCampaignPage::XPATH_CAMPAIGN_LIST_DASHBOARD
  @Count = page.all(:xpath,"#{@count_campaign}").count
  @Count.should >= 1
  sleep 1
  expect(page).to have_current_path(ManageDiscountCampaignPage::ACTIVE_FILTER_PATH, url: true)
end

Then(/^delete campaign should success$/) do
  message= @manage_dicount_campaign_page.flash_message.text
  message.should==ManageDiscountCampaignPage::SUCCESS_DELETE_CAMPAIGN_MESSAGE
end
