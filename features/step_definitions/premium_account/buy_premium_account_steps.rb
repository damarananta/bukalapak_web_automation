When(/^user subscribe premium "([^"]*)" for (\d+) with bukadompet on dweb$/) do |package, period|
  @package = package
  @period = period
  premium_package_page = PremiumPackagesAvailablePage.new
  premium_package_page.load
  if (period == '30')
    premium_package_page.one_month_period_radiobutton.click
  elsif (period == '180')
    premium_package_page.six_month_period_radiobutton.click
  elsif (period == '365')
    premium_package_page.twelve_month_period_radiobutton.click
  end
  scroll_down(100)
  @checkout_premium_window = window_opened_by do
    if (package == 'Basic')
      premium_package_page.subscribe_basic_button.click
    elsif (package == 'Professional')
      premium_package_page.subscribe_pro_button.click
    end
  end
  within_window @checkout_premium_window do
    premium_new_subscription_page = PremiumNewSubscriptionPage.new
    premium_new_subscription_page.pay_with_bukadompet_button.click
  end
end

Then(/^user will go to premium dashboard$/) do
  within_window @checkout_premium_window do
    premium_dashboard_page = PremiumDashboardPage.new
    expect(premium_dashboard_page.package_name_text).to(have_text(@package))
    auto_extend_date = Date.today.next_day(@period.to_i + 1)
    auto_extend_date_text = "#{auto_extend_date.day} #{get_month_name_in_bahasa(auto_extend_date.month)} #{auto_extend_date.year}"
    expect(premium_dashboard_page.subscription_auto_extend_date_text).to(have_text(auto_extend_date_text))
  end
end
