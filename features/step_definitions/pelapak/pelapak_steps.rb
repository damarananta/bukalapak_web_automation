When(/^User navigate to "([^"]*)" page$/) do |pelapak|
  visit "/#{pelapak}"
end

When(/^User click label "([^"]*)"$/) do |label|
  find(:xpath,"//*[@id='user_product']/div[1]/div/nav/div/div/div[1]/div/a[#{label}]").click
  sleep 2
end

When(/^User should get product count "([^"]*)"$/) do |qty|
  productCount=page.all(:xpath,'//*[@id="user_product"]/div[2]/ul/li').count
  productCount.should==qty.to_i
end

Given(/"([^"]*)" to the search box seller page$/) do |keyword|
  @pelapak_page=PelapakPage.new
  @pelapak_page.search_box.send_keys(keyword, :enter)
  @pelapak_page.wait_for_product_name(3)
end

Then(/"([^"]*)" should be mentioned in the seller page results$/) do |result|
  product_name_result=@pelapak_page.product_name.text
  is_true=product_name_result.include? "#{result}"
  is_true.should==true
end
