When(/^User navigate to "([^"]*)" page via mweb$/) do |pelapak|
  @pelapak_name=pelapak
  @pelapak_page=MobilePelapakPage.new
  visit "/#{pelapak}"
  dismiss_one_signal_popup
  @pelapak_page.wait_for_jasa_pengiriman_title(6)
end

Then(/^User should see detail information about pelapak via mweb$/) do
  @pelapak_page.assert_detail_pelapak_information(@pelapak_name)
end

Given(/"([^"]*)" to the search box seller page via mweb$/) do |keyword|
  @pelapak_page.search_box.send_keys(keyword, :enter)
  @pelapak_page.wait_for_product_name(3)
end

Then(/"([^"]*)" should be mentioned in the seller page results via mweb$/) do |result|
  product_name_result=@pelapak_page.product_name.text
  is_true=product_name_result.include? "#{result}"
  is_true.should==true
end

And(/^User should get product count "([^"]*)" via mweb$/) do |qty|
  productCount=page.all('.product__name.line-clamp--2').count
  productCount.should==qty.to_i
end

Then(/^User select pelapak label via mweb$/) do
  page.execute_script "window.scrollBy(0,1000)"
  @pelapak_page.select_by_label_box.click
  @pelapak_page.wait_for_label1(3)
  @pelapak_page.label1.click
  @pelapak_page.wait_for_product_name(6)
  page.execute_script "window.scrollBy(0,500)"
end
