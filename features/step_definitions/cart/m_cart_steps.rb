When(/^User add product to cart via mweb$/) do
  @m_carts_page.add_to_cart
end

When(/^User click button buy via mweb$/) do
  @m_carts_page.buy
end

When(/^User click Hapus dan Simpan ke Daftar Barang Favorit via mweb$/) do
  visit '/cart/carts'
  @m_carts_page.delete_cart_and_favorites
end

And(/^product should exist on bookmarks via mweb$/) do
  visit '/'
  @m_bookmarks_page.verify_favorite_should_increase
end

Then(/^verify product should exist popup cart mweb$/) do
  @m_carts_page.verify_poppup_cart_content
end

When(/^User click delete icon from popup cart mweb$/) do
  @m_carts_page.pc_delete_icon.click
  page.driver.browser.switch_to.alert.accept
  @m_carts_page.wait_for_pc_empty_icon(5)
end

Then(/^popup cart mweb should be empty$/) do
  @m_carts_page.verify_pc_should_empty
end

Then(/^product should exist on basket via mweb$/) do
  @m_carts_page.verify_product_exist_on_basket_mweb
end

And(/^cart should be empty via mweb$/) do
  @m_carts_page.verify_cart_should_empty
end

Then(/^delete product from cart via mweb$/) do
  visit '/cart/carts'
  @m_carts_page.delete_one_cart_mobile
end
