When(/^User add product to cart$/) do
  @carts_page.add_to_cart
end

When(/^User click button buy$/) do
  @carts_page.buy
end

When(/^User click Hapus dan Simpan ke Daftar Barang Favorit$/) do
  visit 'cart/carts'
  @carts_page.delete_cart_and_favorites
end

And(/^product should exist on bookmarks$/) do
  visit '/'
  @bookmarks_page.verify_favorite_should_increase
end

Then(/^verify pop-up cart shoud correct$/) do
  @carts_page.verify_poppup_cart_content
end

When(/^User clicked on delete icon popup card$/) do
  @carts_page.click_delete_icon_popup
end

Then(/^User should see product was successfully deleted$/) do
  @carts_page.verify_popup_cart_should_empty
end

And(/^User delete carts if exist$/) do
  visit '/cart/carts'
  @carts_page.delete_cart_desktop
end

Then(/^User click mini cart icon$/) do
  @m_carts_page.icon_mini_cart.click
  @m_carts_page.wait_for_pc_love_icon(5)
end

Then(/^product should exist on basket$/) do
  @carts_page.verify_product_exist_on_basket
end


And(/^cart should be empty$/) do
  @carts_page.verify_cart_should_empty
end

Then(/^delete product from cart$/) do
  @carts_page.delete_one_cart_desktop
end
