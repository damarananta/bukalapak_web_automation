Given(/^a seller as user$/) do
  @username = ENV['SELLER_USERNAME']
  @password = ENV['SELLER_PASSWORD']
end

And(/^user login on dweb$/) do
  home_page = HomePage.new
  home_page.load
  home_page.dismiss_one_signal_popup_homepage
  home_page.login(@username, @password)
  home_page.wait_for_flash_message
end

When(/^user go to Promoted Push on dweb$/) do
  @promoted_push_page = PromotedPushPage.new
  @promoted_push_page.load
end

And(/^click "Tambah Budget" on dweb$/) do
  @budget_balance = @promoted_push_page.get_budget_balance
  @promoted_push_page.add_budget_button.click
end

And(/^fill budget (\d+) and click "Tambah" on dweb$/) do |budget|
  @budget = budget
  @promoted_push_page.add_budget(@budget)
end

Then(/^budget should add up with the previous budget on dweb$/) do
  @promoted_push_page.wait_for_flash_message
  expected_budget_balance = "#{add_delimiter(@budget_balance.to_i + @budget.to_i)}"
  expect(@promoted_push_page.budget_balance_section).to(have_text(expected_budget_balance))
end

Given(/^user login on mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_home_page.load
  @m_home_page.login(@username, @password)
end

When(/^user go to Promoted Push on mweb$/) do
  @m_promoted_push_page = MobilePromotedPushPage.new
  @m_promoted_push_page.load
end

And(/^click "Tambah Budget" on mweb$/) do
  @budget_balance = @m_promoted_push_page.get_budget_balance
  @m_promoted_push_page.add_budget_button.click
end

And(/^fill budget (\d+) and click "Tambah" on mweb$/) do |budget|
  @budget = budget
  @m_promoted_push_page.add_budget(@budget)
end

Then(/^budget should add up with the previous budget on mweb$/) do
  scroll_to(@m_promoted_push_page.add_budget_button, true)
  @m_promoted_push_page.wait_for_flash_message
  expected_budget_balance = "#{add_delimiter(@budget_balance.to_i + @budget.to_i)}"
  expect(@m_promoted_push_page.budget_balance_section).to(have_text(expected_budget_balance))
end
