Given(/^a seller as user with product$/) do
  @username = ENV['SELLER_USERNAME']
  @password = ENV['SELLER_PASSWORD']
end

When(/^user go to list of Barang Dijual on dweb$/) do
  @barang_dijual_page = BarangDijualPage.new
  @barang_dijual_page.load
end

When(/^click Promoted Push on a product with name "([^"]*)" on dweb$/) do |product_name|
  @product_name = product_name
  @product_index = @barang_dijual_page.click_promoted_push_on_a_product(product_name).to_i
end

When(/^fill cost per click with (\d+), active date with today, set status to active, click Simpan on dweb$/) do |cpc|
  @barang_dijual_page.set_promoted_push_product_active_for_today(cpc)
end

Then(/^product will have promoted push active date info$/) do
  within("li.col-12--2:nth-child(#{@product_index.to_i+1})") do
    expect(page).to(have_css(".kvp__value[title='Push Mulai']"))
    expect(page).to(have_css(".kvp__value[title='Push Selesai']"))
  end
end

When(/^click Nonaktif, click Simpan on dweb$/) do
  @barang_dijual_page.set_promoted_push_product_nonactive
end

Then(/^product will not have promoted push active date info$/) do
  within("li.col-12--2:nth-child(#{@product_index.to_i+1})") do
    expect(page).to(have_no_css(".kvp__value[title='Push Mulai']"))
    expect(page).to(have_no_css(".kvp__value[title='Push Selesai']"))
  end
end

When(/^user go to list of Barang Dijual on mweb$/) do
  @m_barang_dijual_page = MobileBarangDijualPage.new
  @m_barang_dijual_page.load
  @m_barang_dijual_page.wait_for_cari_barang_field
end

When(/^user search for "([^"]*)" on Barang Dijual on mweb$/) do |product_name|
  @m_barang_dijual_page.cari_barang_field.set(product_name)
  @m_barang_dijual_page.search_button.click
  @m_barang_dijual_page.wait_for_cari_barang_field
end

When(/^click Promoted Push on a product with name "([^"]*)" on mweb$/) do |product_name|
  scroll_down(150)
  @m_barang_dijual_page.pengaturan_barang_button.click
  @m_barang_dijual_page.promoted_push_button.click
  @m_barang_dijual_page.wait_for_cost_per_click_field
end

When(/^fill cost per click with (\d+), active date with today, set status to active, click Simpan on mweb$/) do |cpc|
  @m_barang_dijual_page.set_promoted_push_product_active_for_today(cpc)
end

Then(/^success message active promoted push appear on mweb$/) do
  scroll_to(@m_barang_dijual_page.product_names[0], true)
  @m_barang_dijual_page.wait_for_flash_message
  expect(@m_barang_dijual_page.flash_message(visible: false)).to(have_text(MobileBarangDijualPage::FLASH_MESSAGE_SUCCESS_ACTIVE_PROMOTED_PUSH))
end

Then(/^click Nonaktif, click Simpan on mweb$/) do
  @m_barang_dijual_page.set_promoted_push_product_nonactive
end

Then(/^success message nonactive promoted push appear on mweb$/) do
  scroll_to(@m_barang_dijual_page.product_names[0], true)
  @m_barang_dijual_page.wait_for_flash_message
  expect(@m_barang_dijual_page.flash_message(visible: false)).to(have_text(MobileBarangDijualPage::FLASH_MESSAGE_SUCCESS_NONACTIVE_PROMOTED_PUSH))
end

When(/^user go to Promoted Push page on dweb$/) do
  @promoted_push_page = PromotedPushPage.new
  @promoted_push_page.load
end

When(/^click Atur Promoted Push on a product with name "([^"]*)" on dweb$/) do |product_name|
  @product_name = product_name
  @promoted_push_page.click_promoted_push_product_setting(product_name)
end

When(/^fill cost per click with (\d+), active date with today, click Aktif, click Simpan on dweb$/) do |cpc|
  @promoted_push_page.set_promoted_push_product_active_for_today(cpc)
end

When(/^click Nonaktif, and click Simpan on dweb$/) do
  @promoted_push_page.set_promoted_push_product_nonactive
end

Then(/^the product's promoted push status will be (.+) on dweb$/) do |status|
  @promoted_push_page.list_product_names.each_with_index do |a_product_name, index|
    if (a_product_name.has_text?(@product_name))
      expect(@promoted_push_page.list_product_status[index]).to(have_text(status))
      break
    end
  end
end

When(/^user go to Promoted Push page on mweb$/) do
  @m_promoted_push_page = MobilePromotedPushPage.new
  @m_promoted_push_page.load
end

When(/^click Atur Promoted Push on a product with name "([^"]*)" on mweb$/) do |product_name|
  @product_name = product_name
  @m_promoted_push_page.click_promoted_push_product_setting(@product_name)
end

When(/^fill cost per click with (\d+), active date with today, click Aktif, click Simpan on mweb$/) do |cpc|
  @m_promoted_push_page.set_promoted_push_product_active_for_today(cpc)
end

Then(/^the product's promoted push status will be (.+) on mweb$/) do |status|
  @m_promoted_push_page.list_product_names.each_with_index do |a_product_name, index|
    if (a_product_name.has_text?(@product_name))
      expect(@m_promoted_push_page.list_product_status[index]).to(have_text(status))
      break
    end
  end
end

Then(/^click Nonaktif, and click Simpan on mweb$/) do
  @m_promoted_push_page.set_promoted_push_product_nonactive
end
