Given(/^user with a promoted product "([^"]*)" with cpc (\d+)$/) do |product_name, cpc|
  @product_name = product_name
  @cpc = cpc
end

Given(/^with promoted push budget on dweb$/) do
  @promoted_push_page = PromotedPushPage.new
  @promoted_push_page.load
  @promoted_push_page.wait_for_budget_balance_text
  @budget_balance_before = @promoted_push_page.get_budget_balance
end

When(/^unregistered user search and click on the promoted product on dweb$/) do
  logout
  search_product(@product_name)
  search_result_page = SearchResultsPage.new
  search_result_page.wait_for_promoted_push_top_product_section
  product_names = search_result_page.promoted_push_top_product_names
  product_names.each_with_index do |product_name, index|
    if (product_name.has_text?(@product_name))
      product_name.click
      product_detail_page = ProductDetailPage.new
      product_detail_page.wait_for_button_buy
      break
    end
  end
end

Then(/^promoted push budget of the seller should be subtracted on dweb$/) do
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(@username, @password)
  @home_page.wait_for_flash_message
  @promoted_push_page.load
  @promoted_push_page.wait_for_budget_balance_text
  expected_budget_balance = "#{add_delimiter(@budget_balance_before.to_i - @cpc.to_i)}"
  expect(@promoted_push_page.budget_balance_text).to(have_text(expected_budget_balance))
end

Given(/^with promoted push budget on mweb$/) do
  @m_promoted_push_page = MobilePromotedPushPage.new
  @m_promoted_push_page.load
  @m_promoted_push_page.wait_for_budget_balance_text
  @budget_balance_before = @m_promoted_push_page.get_budget_balance
end

When(/^unregistered user search and click on the promoted product on mweb$/) do
  logout
  search_product_mobile(@product_name)
  search_result_page = MobileSearchResultsPage.new
  search_result_page.wait_for_promoted_push_top_product_section
  product_names = search_result_page.promoted_push_top_product_names
  product_names.each_with_index do |product_name, index|
    if (product_name.has_text?(@product_name))
      product_name.click
      product_detail_page = ProductDetailPage.new
      product_detail_page.wait_for_button_buy
      break
    end
  end
end

Then(/^promoted push budget of the seller should be subtracted on mweb$/) do
  @m_home_page.load
  @m_home_page.dismiss_one_signal_popup_homepage
  @m_home_page.login(@username, @password)
  @m_home_page.wait_for_flash_message
  @m_promoted_push_page.load
  @m_promoted_push_page.wait_for_budget_balance_text
  expected_budget_balance = "#{add_delimiter(@budget_balance_before.to_i - @cpc.to_i)}"
  expect(@m_promoted_push_page.budget_balance_text).to(have_text(expected_budget_balance))
end
