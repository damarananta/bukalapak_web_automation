When(/^user add darkgrey keyword$/) do
  @darkgrey_keywords_page = DarkgreyKeywordsPage.new
  @darkgrey_keywords_page.load
  @darkgrey_keywords_page.new_keyword_button.click
  @darkgrey_keyword_page_new = DarkgreyKeywordsNewPage.new
  @darkgrey_keyword_page_new.add_darkgrey_keyword("teplok")
end

Then(/^darkgrey keyword will appear on darkgrey keyword page$/) do
  expect(page).to have_content('teplok')
end

When(/^user delete darkgrey keyword$/) do
  @darkgrey_keywords_page = DarkgreyKeywordsPage.new
  @darkgrey_keywords_page.load
  @darkgrey_keywords_page.delete_darkgrey_keyword
end 	

Then(/^darkgrey keyword will not appear on darkgrey keyword page$/) do
  expect(page).to have_no_content('teplok')
end