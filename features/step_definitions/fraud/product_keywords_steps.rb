When(/^user add product keyword$/) do
  @product_keyword_page = ProductKeywordsPage.new
  @product_keyword_page.load
  @product_keyword_page.new_keyword_button.click
  @product_keyword_page_new =  ProductKeywordsNewPage.new
  @product_keyword_page_new.add_suspect_keyword("vimax","obat kuat","50000","100000")
end

When(/^user deactivate product keyword$/) do
  @product_keyword_page = ProductKeywordsPage.new
  @product_keyword_page.load
  @product_keyword_page.delete_keyword_button.click
  page.driver.browser.switch_to.alert.accept
  sleep 2
end

Then(/^product keyword will appear on product keyword page$/) do
  expect(page).to have_content('vimax')
end
