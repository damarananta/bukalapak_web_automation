Given(/^there is admin user$/) do
  login(ENV['ADMIN_USERNAME'], ENV['ADMIN_PASSWORD'])
end

When(/^user add blacklist keyword$/) do
  @black_keywords_page = BlackKeywordsPage.new
  @black_keywords_page.load
  @black_keywords_page.new_keyword_button.click
  @black_keyword_page_new = BlackKeywordNewPage.new
  @black_keyword_page_new.add_blacklist_keyword("teroris","bahaya")
end

Then(/^blacklist keyword will appear on blacklist keyword page$/) do
  expect(page).to have_content('teroris')
end

When(/^user delete blacklist keyword$/) do
  @black_keywords_page = BlackKeywordsPage.new
  @black_keywords_page.load
  @black_keywords_page.delete_black_keyword
end

Then(/^blacklist keyword will not appear on blacklist keyword page$/) do
  expect(page).to have_no_content('teroris')
end
