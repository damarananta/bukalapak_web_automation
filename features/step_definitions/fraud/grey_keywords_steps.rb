When(/^user add grey keyword$/) do
  @grey_keywords_page = GreyKeywordsPage.new
  @grey_keywords_page.load
  @grey_keywords_page.new_keyword_button.click
  @grey_keyword_page_new = GreyKeywordsNewPage.new
  @grey_keyword_page_new.add_grey_keyword("kucing")
end

Then(/^grey keyword will appear on grey keyword page$/) do
  expect(page).to have_content('kucing')
end

When(/^user delete grey keyword$/) do
  @grey_keywords_page = GreyKeywordsPage.new
  @grey_keywords_page.load
  @grey_keywords_page.delete_grey_keyword
end 	

Then(/^grey keyword will not appear on grey keyword page$/) do
  expect(page).to have_no_content('kucing')
end