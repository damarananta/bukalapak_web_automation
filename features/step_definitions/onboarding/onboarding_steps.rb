Then(/^user will get phone number confirmation popup$/) do
  @home_page = HomePage.new
  @home_page.onb_confirm_phone_number(@phone_number)
end

Then(/^welcome popup$/) do
  @home_page.onb_dismiss_welcome_popup
  @account_settings = AccountSettingsPage.new
  @account_settings.assert_phone_number(@phone_number)
end

Then(/^user will get phone number confirmation popup via mobile web$/) do
  @m_home_page = MobileHomePage.new
  @m_home_page.onb_confirm_phone_number(@phone_number)
end

Then(/^android reminder popup via mobile web$/) do
  @m_home_page.onb_dismiss_apps_download_popup
  @m_account_settings = MobileAccountSettingsPage.new
  @m_account_settings.assert_phone_number(@phone_number)
end

When(/^user confirmed email$/) do
  logout
  @home_page = HomePage.new
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['ADMIN_USERNAME'], ENV['ADMIN_PASSWORD'])
  @users_page = UsersPage.new
  @users_page.load
  @users_page.confirmed_user_email(@username)
  logout

  @home_page.load
  @home_page.login(@username, @password)
end

Then(/^the onboarding checklist on "([^"]*)" will be checked$/) do |checklist|
  @home_page.load
  case checklist
  when "email"
    @home_page.onb_assert_email_checked
  when "address"
    @home_page.onb_assert_address_checked
  when "phone"
    @home_page.onb_assert_phone_checked
  when "topup"
    @home_page.onb_assert_topup_checked
  when "shop"
    @home_page.onb_assert_shop_checked
  end
end

Then(/^the onboarding checklist on email will be checked on mobile web$/) do
  @m_home_page.load
  @m_home_page.login(@username, @password)
  @m_home_page.onb_assert_email_checked
end

When(/^user save address$/) do
  @user_addresses_new_page = UserAddressesNewPage.new
  @user_addresses_new_page.load
  @user_addresses_new_page.set_address(@fullname, @phone_number, create_address)
end

When(/^user save phone number$/) do
  @home_page.dismiss_onboarding_popup
  @home_page.onb_save_phone_number_check.click
  @user_edit_page = UsersEditPage.new
  @user_edit_page.set_phone_number(@phone_number, @password)
end

Given(/^user topup deposit "([^"]*)"$/) do |amount|
  amount = amount.to_i
  @user_topups_new_page = UserTopupsNewPage.new
  @user_topups_new_page.load
  @user_topups_new_page.top_up_with_transfer(amount)
  @user_topup_transfer_page = UserTopupsTransferPage.new
  @user_topup_transfer_page.wait_for_ticket_number
  @ticket_number = @user_topup_transfer_page.ticket_number.text
  puts "Ticket Number: " + @ticket_number
  logout

  @home_page.load
  @home_page.login(ENV['CUSTOMER_SUPPORT_USERNAME'],ENV['CUSTOMER_SUPPORT_PASSWORD'])
  @user_topups_page = UserTopupsPage.new
  @user_topups_page.load
  @user_topups_page.confirm_topup_ticket(@ticket_number)
  logout
  @home_page.load
  @home_page.login(@username, @password)
end

When(/^user paid transaction$/) do
  visit_url(ENV['SELLER_PRODUCT_URL'])
  @product_detail_page = ProductDetailPage.new
  @product_detail_page.wait_for_button_buy
  @product_detail_page.button_buy.click
  @payment_purchases_new_page = PaymentPurchasesNewPage.new
  @payment_purchases_new_page.paid_with_bukadompet(@password)
  @payment_purchases_confirmation_page = PaymentPurchasesConfirmationPage.new
  @payment_purchases_confirmation_page.wait_for_payment_confirmation_header(10)
end
