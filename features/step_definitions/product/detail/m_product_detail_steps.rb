Then(/^User should see basic information about product via mweb$/) do
  @m_product_detail_page.wait_for_favorite_button(6)
  @m_product_detail_page.verify_basic_information_about_product
end

And(/^Pelapak info via mweb$/) do
  @m_product_detail_page.verify_pelapak_info
end

And(/^Product detail sosial media icon list via mweb$/) do
  @m_product_detail_page.verify_sosial_media_icon_list
end

And(/^Information product detail via mweb$/) do
  @m_product_detail_page.wait_for_terjual_label(3)
  @m_product_detail_page.verify_product_information
end

And(/^Specification product detail via mweb$/) do
  @m_product_detail_page.verify_product_specification
end
And(/^Description product detail via mweb$/) do
  @m_product_detail_page.verify_product_description
end

And(/^Notes product detail via mweb$/) do
  @m_product_detail_page.verify_product_notes
end

And(/^Product detail discount info via mweb$/) do
  @m_product_detail_page.verify_product_detail_discount_info
end

And(/^Product detail free shipping info via mweb$/) do
  page.execute_script "window.scrollBy(0,500)"
  @m_product_detail_page.verify_product_detail_free_shipping_info
end

And(/^Seller reputation info via mweb$/) do
  @m_product_detail_page.verify_pelapak_reputation_info
end

When(/^User click Cek Ongkos Kirim link via mweb$/) do
  @m_product_detail_page.ongkos_kirim_label.click
  @m_product_detail_page.wait_for_pilih_lokasi_label(10)
end

Then(/^User should see shipping estimation info via mweb$/) do
  @m_product_detail_page.verify_shipping_estimation_info
end

And(/^Product detail feedback list via mweb$/) do
  @m_product_detail_page.verify_feedback_list
end

And(/^Product detail rating & review list via mweb$/) do
  @m_product_detail_page.verify_review_list
end

Then(/^User should see product detail wholesale info via mweb$/) do
  @m_product_detail_page.verify_product_detail_wholesale_info
end

And(/^Product detail installment info with price Rp.500.000 via mweb$/) do
  @m_product_detail_page.verify_installment_info_price_500000
end

And(/^Premium seller info via mweb$/) do
  @m_product_detail_page.verify_premium_seller_info
end

And(/^User should see availability label "([^"]*)" via mweb$/) do |availability_label|
  @m_product_detail_page.verify_product_availability(availability_label)
end
