When(/^User navigate to product detail "([^"]*)"$/) do |product|
  @home_page                 =HomePage.new
  @login_page                =LoginPage.new
  @carts_page                =CartsPage.new
  @bookmarks_page            =BookmarksPage.new
  @product_detail_page       =ProductDetailPage.new
  @m_login_page              =MobileLoginPage.new
  @m_carts_page              =MobileCartsPage.new
  @m_bookmarks_page          =MobileBookmarksPage.new
  @m_product_detail_page     =MobileProductDetailPage.new

  visit_split_true product
  dismiss_one_signal_popup
end

Then(/^User should see 12 products in section list$/) do
  @home_page.verify_product_detail_section
end

Then(/^User should see basic information about product$/) do
  @product_detail_page.wait_for_favorite_button(6)
  @product_detail_page.verify_basic_information_about_product
end

And(/^Pelapak info on the right sidebar$/) do
  @product_detail_page.verify_pelapak_info
end

And(/^Sosial media icon list on the right sidebar$/) do
  @product_detail_page.verify_sosial_media_icon_list
end

And(/^Information product detail tab$/) do
  page.execute_script "window.scrollBy(0,500)"
  @product_detail_page.wait_for_terjual_icon(3)
  @product_detail_page.verify_product_information
end

And(/^Specification product detail tab$/) do
  @product_detail_page.verify_product_specification
end
And(/^Description product detail tab$/) do
  @product_detail_page.verify_product_description
end

And(/^Notes product detail tab$/) do
  @product_detail_page.verify_product_notes
end

And(/^Product detail discount info$/) do
  @product_detail_page.verify_product_detail_discount_info
end

And(/^Product detail average shipping time info$/) do
  @product_detail_page.verify_product_detail_average_shipping_time_info
end

And(/^Product detail free shipping info$/) do
  @product_detail_page.verify_product_detail_free_shipping_info
end

And(/^Product detail rating info$/) do
  @product_detail_page.verify_product_detail_rating_info
end

And(/^Seller reputation info$/) do
  @product_detail_page.verify_pelapak_reputation_info
end

And(/^Product detail installment info with price less than Rp.500.000$/) do
  @product_detail_page.installment_info_price_less_than_500000
end

When(/^User click tab Estimasi Biaya Kirim product$/) do
  @product_detail_page.tab_estimasi_biaya_kirim.click
  @product_detail_page.wait_for_masukkan_tujuan(10)
end

Then(/^User should see shipping estimation info$/) do
  @product_detail_page.verify_shipping_estimation_info
end

When(/^User click tab Feedback product$/) do
  @product_detail_page.tab_feedback.click
  @product_detail_page.wait_for_user_feedback_content(10)
end

Then(/^User should see product feedback list$/) do
  @product_detail_page.verify_feedback_list
end

When(/^User click tab Ulasan Barang$/) do
  @product_detail_page.tab_ulasan_barang.click
  @product_detail_page.wait_for_ulasan_content(10)
end

Then(/^User should see product review list$/) do
  @product_detail_page.verify_review_list
end

Then(/^User should see product detail wholesale info$/) do
  @product_detail_page.verify_product_detail_wholesale_info
end

And(/^Product detail installment info with price Rp.500.000$/) do
  @product_detail_page.verify_installment_info_price_500000
end

And(/^Seller subscriber count info$/) do
  @product_detail_page.verify_seller_subscriber_info
end

And(/^Premium seller info$/) do
  @product_detail_page.verify_premium_seller_info
end

And(/^User should see availability label "([^"]*)"$/) do |availability_label|
  @product_detail_page.verify_product_availability(availability_label)
end
