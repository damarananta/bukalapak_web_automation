When(/^user edit product stock$/) do
  restock('/products/3p2n83-jual-tutup-botol/edit', ENV['CREDENTIAL_SELLER_USERNAME'], ENV['CREDENTIAL_SELLER_PASSWORD'])
end

Then(/^product stock limit will change$/) do
  within('.c-product-buy__stock')do
    expect(page).to have_content('Tersedia > 500 stok barang')
  end
end

Then(/^user edit product stock in staging$/) do
  if ENV['SELLER_USERNAME'] != ""
    restock("#{ENV['SELLER_PRODUCT_EDIT_URL']}", ENV['SELLER_USERNAME'], ENV['SELLER_PASSWORD'])
    logout
  end
  if ENV['SELLER2_USERNAME'] != ""
    restock("#{ENV['SELLER2_PRODUCT_EDIT_URL']}", ENV['SELLER2_USERNAME'], ENV['SELLER2_PASSWORD'])
    logout
  end
  if ENV['SELLER3_USERNAME'] != ""
    restock("#{ENV['SELLER3_PRODUCT_EDIT_URL']}", ENV['SELLER3_USERNAME'], ENV['SELLER3_PASSWORD'])
    logout
  end
end
