When /^Admin create new popular product$/ do
  @popular_product_page.add_popular_product(PopularProductPage::POPULAR_PRODUCT_SAMPLE)
  sleep 1
end

Then /^go to popular product dashboard$/ do
  @popular_product_page = PopularProductPage.new
  @popular_product_page.load
end

Then /^popular product is created$/ do
  current_url=ENV['BASE_URL']+'/popular'
  expect(page).to have_current_path(current_url, url: true)
end

Then(/^admin create campaign sample$/) do
  steps %Q{
    Then Admin go to Manage Discount & Campaign page
    Then access campaign setting
    When delete campaign if exist
    Then click create new campaign button
    Given campaign title "Test_Title_"
    Given campaign upload banner "960x150.jpg"
    Given campaign banner open new window "1"
    Given campaign redirect url "RedirectUrl"
    Given campaign tooltip "Tooltip"
    Given campaign meta description "MetaDesc"
    Given campaign start time "28"
    Given campaign end time "29"
    Given campaign description "Desc"
    Given campaign sub desc "Sub Desc"
    Then click button save campaign
  }
end

Given(/^popular product name section (.*)$/) do |popular_section_name|
  @popular_product_page.load
  sleep 1
  @name_section=popular_section_name
  @popular_product_page.edit_popular_product_section(1,@name_section)
end

Then(/^popular popular section name dashboard should be (.*)$/) do |popular_section_name_dashboard|
  nameSectionEdited = @popular_product_page.popular_section_name_label.text()
  popular_section_name_dashboard.should == nameSectionEdited
end

And(/^popular popular section name homepage should be (.*)$/) do |popular_section_name_homepage|
  visit_url '/'
  if @name_section.include? "&"
    sleep 1
    @popularTab1= @popular_product_page.popular_tab1.text()
    @popularTab2= @popular_product_page.popular_tab2.text()
    splitExpectedNameSection =[]
    splitExpectedNameSection=popular_section_name_homepage.split('&', 2)

    splitExpectedNameSection[0].should==@popularTab1
    splitExpectedNameSection[1].should==@popularTab2
  else
    popularTab= @popular_product_page.popular_tab.text()
    popularTab.should==popular_section_name_homepage
  end
  sleep 1
end

When(/^Admin delete popular product$/) do
  @popular_product_page.delete_popular_product
end

Then(/^popular product is deleted$/) do
  current_url=ENV['BASE_URL']+'/popular'
  expect(page).to have_current_path(current_url, url: true)
end

Then(/^Admin should see popular product list dashboard$/) do
  sleep 1
  @Count = page.all(:xpath,PopularProductPage::POPULAR_PRODUCT_COUNT).count
  while @Count < 7
    @popular_product_page.add_popular_product(PopularProductPage::POPULAR_PRODUCT_SAMPLE)
    @Count += 1
  end
  sleep 2
  @list_popular_product_dashboard = []

  for @a in 1..6
    @popular_product_dashboard=find(:xpath, "//*[@id='manage-popular']/section[1]/div/div[1]/div[2]/div[#{@a}]/div[3]/a").text()
    @list_popular_product_dashboard.push(@popular_product_dashboard)
  end
end

And(/^popular product list homepage should same$/) do
  visit_url '/'
  sleep 3
  @list_popular_product_homepage = []
  page.execute_script "window.scrollBy(0,400)"
  for @i in 1..6
    @popular_product_homepage=find(:xpath, "//*[@id='popular-1-1']/ul/li[#{@i}]/div/article/div[3]/h3/a").text()
    @list_popular_product_homepage.push(@popular_product_homepage)
  end
  puts("list homepage #{@list_popular_product_homepage}")
  puts("list dashboard #{@list_popular_product_dashboard}")
  @list_popular_product_dashboard.should ==  @list_popular_product_homepage
end
