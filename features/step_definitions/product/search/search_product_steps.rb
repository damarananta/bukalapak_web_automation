When(/^User type a product (.*)$/) do |product|
  @search_results_page=SearchResultsPage.new
  @search_results_page.search_product(product)
end

Then(/^User should get the result (.*)$/) do |result|
  @search_results_page.verify_search_result(result)
end
