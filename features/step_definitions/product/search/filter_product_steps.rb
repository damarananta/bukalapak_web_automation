When(/^user visit filter from (.*) with (.*)$/) do |filter_type,filter_param|
  @filter_type=filter_type
  @filter_param=filter_param
  @l=filter_param.split(',')
  sleep 2
  case @filter_type
  when 'Search'
    filter_search_url(@l[0],@l[1],@l[2],@l[3],@l[4],@l[5],@l[6],@l[7],@l[8],@l[9],@l[10],@l[11],@l[12],@l[13],@l[14],@l[15],@l[16])
    sleep 1
  when 'Pelapak1'
    seller='laku6sales'
    filter_pelapak_url(seller,@l[0],@l[1],@l[2],@l[3],@l[4],@l[5],@l[6],@l[7],@l[8],@l[9],@l[10],@l[11],@l[12],@l[13],@l[14],@l[15],@l[16])
    sleep 2
  else
    seller='gemasports'
    filter_pelapak_url(seller,@l[0],@l[1],@l[2],@l[3],@l[4],@l[5],@l[6],@l[7],@l[8],@l[9],@l[10],@l[11],@l[12],@l[13],@l[14],@l[15],@l[16])
    sleep 2
  end
end


Then /^verify product detail should contain search filter param$/ do
  sleep 2
  @price= first(".amount.positive").text
  @range= @price.delete('.').to_i
  @range.should >=@l[7].to_i && @range.should <=@l[8].to_i

  @grosirProductCount = page.all(".product-wholesale-text").count
  @grosirProductCount.should >=1

  @usedProductCount = page.all(".product__condition.product__condition--used").count
  @usedProductCount.should == 0

  @premiumSellerCount = page.all(".premium--sticky-badge__silver.hidden-mobile.autooltip.tooltipstered").count
  @premiumSellerCount.should >=1

  first(:xpath, ".//*[@id]/li/div/article/div[3]/h3/a").click
  sleep 1
  @gojek_element = page.all(".user-shipping-info__courier.user-shipping-info__courier--go-jek").count
  @gojek_element.should >=1

  within(".user-address") do
    expect(page).to have_content('Jakarta Selatan')
  end
  
  within(".product-delivery__free-shipping") do
    expect(page).to have_content('Gratis Biaya Kirim')
  end

  if page.has_css?('.js-product-detail-price.product-detailed-price__installment-percentage')
    within(".js-product-detail-price.product-detailed-price__installment-percentage") do
      expect(page).to have_content('Cicilan  0%')
    end
  else
    within(:xpath,'//*[@id="mod-product-detail-1"]//span/small') do
      expect(page).to have_content('(Bank penyedia cicilan)')
    end
  end

end

Then /^sitebar filter new product should be checked$/ do
  within(:xpath, ".//*[@id='filter_form']/div[5]/div[1]/label") do
    expect(page).to have_xpath %Q{//input[@value="1"]}
  end
end

And /^sitebar filter used product should be checked$/ do
  within(:xpath, ".//*[@id='filter_form']/div[5]/div[2]/label") do
    expect(page).to have_xpath %Q{//input[@value="1"]}
  end
end
And (/^sitebar filter free shipping coverage should contain "([^"]*)"$/) do|fsc|
  #check free shipping coverage
  within(:xpath, ".//*[@id='search_free_shipping_coverage_chzn']/a/span") do
    expect(page).to have_content("#{fsc}")
  end
end

And (/^sitebar filter province should contain "([^"]*)"$/) do|province|
  #check  province
  within(:xpath, ".//*[@id='search_province_chzn']/a/span") do
    expect(page).to have_content("#{province}")
  end
end

And (/^sitebar filter city should contain "([^"]*)"$/) do|city|
  #check city
  within(:xpath, ".//*[@id='search_city_chzn']/a/span") do
    expect(page).to have_content("#{city}")
  end
end

And (/^sitebar filter courier should contain "([^"]*)"$/) do|courier|
  # check courier
  within(:xpath, ".//*[@id='search_courier_chzn']/a/span") do
    expect(page).to have_content("#{courier}")
  end
end

And (/^sitebar filter price min should contain "([^"]*)"$/) do|price_min|
  # check min input
  within(:xpath, ".//*[@id='filter_form']/div[7]/div[1]/div[1]/div") do
    expect(page).to have_xpath %Q{//input[@value="#{price_min}"]}
  end
end

And (/^sitebar filter price max should contain "([^"]*)"$/) do|price_max|
  # check max input
  within(:xpath, ".//*[@id='filter_form']/div[9]/div[1]/div[2]/div") do
    expect(page).to have_xpath %Q{//input[@value="#{price_max}"]}
  end
end

And /^sitebar filter rating should from 0 until 5$/ do
  # check only rating 1-5
  within(:xpath, ".//*[@id='filter_form']/div[10]/div/div[3]/div") do
    expect(page).to have_xpath %Q{//div[@style="top: 100%;"]}
  end
end

And /^sitebar filter rating should 5$/ do
  sleep 1
  within(:xpath, ".//*[@id='filter_form']/div[10]/div/div[3]/div") do
    expect(page).to have_xpath %Q{//div[@style="top: 0%;"]}
  end
end

And /^sitebar filter todays deal should be checked$/ do
  # diskon
  sleep 1
  within(:xpath, ".//*[@id='filter_form']/div[11]/div[1]/label/label") do
    expect(page).to have_xpath %Q{//input[@checked="checked"]}
  end
end

And /^sitebar filter installment should be checked$/ do
  # cicilan
  sleep 1
  within(:xpath, ".//*[@id='filter_form']/div[11]/div[2]/label/label") do
    expect(page).to have_xpath %Q{//input[@checked="checked"]}
  end
end

And /^sitebar filter wholesale should be checked$/ do
  # grosir
  sleep 1
  within(:xpath, ".//*[@id='filter_form']/div[11]/div[3]/label/label") do
    expect(page).to have_xpath %Q{//input[@checked="checked"]}
  end
end

And /^sitebar filter top seller should be checked$/ do
  # top seller
  sleep 1
  within(:xpath, ".//*[@id='filter_form']/div[11]/div[4]/label/label") do
    expect(page).to have_xpath %Q{//input[@checked="checked"]}
  end
end

And /^sitebar filter premium seller should be checked$/ do
  # premium seller
  sleep 1
  within(:xpath, ".//*[@id='filter_form']/div[11]/div[5]/label/label") do
    expect(page).to have_xpath %Q{//input[@checked="checked"]}
  end
end

And /^product discount badge should exist$/ do
  sleep 2
  @discountProductCount = page.all(".product-discount-percentage-amount").count
  @discountProductCount.should >=1
end

And (/^navbar order (.*) should be selected$/) do   |order_option|
  within(:xpath, './/*[@id="product_sort_option"]/option[@selected="selected"]') do
    expect(page).to have_content("#{order_option}")
  end
end
