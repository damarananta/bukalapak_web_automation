Given(/^keyword to search via mweb (.*)$/) do |product|
  @m_search_results_page=MobileSearchResultsPage.new
  @m_search_results_page.search_product(product)
end

Then(/^should get result via mweb (.*)$/) do |result|
  @m_search_results_page.verify_search_result(result)
end

Given(/^keyword to search mweb (.*)$/) do |product|
  search_product_mobile(product)
end

Then(/^should get result mweb (.*)$/) do |result|
  within('#breacrumb_list > ol')do
    expect(page).to have_content(result)
  end
end
