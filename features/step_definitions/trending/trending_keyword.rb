When(/^Admin go to trending keyword dashboard$/) do
  @trending_keyword_page.load
  @trending_keyword_page.wait_for_add_new_trending_button
  @trendingCount=@trending_keyword_page.count_trending_keyword
end

Then(/^Admin click New Trending Keyword button$/) do
  @trending_keyword_page.clear_trending_if_exist
  @trending_keyword_page.add_new_trending_button.click
end

Given(/^trending keyword value "([^"]*)"$/) do |keyword|
  @keyword=keyword
  @trending_keyword_page.trending_keyword_field.set("#{@keyword}")
end

Given(/^random trending keyword value$/) do
  keyword="keyword"+ generate_random_string(20)
  @trending_keyword_page.trending_keyword_field.set(keyword)
end

Given(/^trending keyword url value "([^"]*)"$/) do |url|
  @url=url
  @trending_keyword_page.trending_url_field.set("#{@url}")
end

When(/^Admin click save trending keyword$/) do
  @trending_keyword_page.save_button.click
  sleep 2
end

Then(/^create trending keyword should success$/) do
  @trending_keyword_page.verify_trending_modified_result(@keyword,@url)
  @trendingCount=@trending_keyword_page.count_trending_keyword
  @trendingCount.should == 1
end

Then(/^update trending keyword should success$/) do
  @trending_keyword_page.verify_trending_modified_result(@keyword,@url)
end

Then(/^Admin click edit button trending keyword$/) do
  first(TrendingKeywordPage::EDIT_ELEMENT).click
  sleep 1
end

When(/^Admin click delete button trending keyword$/) do
  @trending_keyword_page.delete_trending
end

Then(/^delete trending keyword should success$/) do
  @trendingCount=@trending_keyword_page.count_trending_keyword
  @trendingCount.should == 0
end

When(/^click go back trending keyword button$/) do
  sleep 1
  @trending_keyword_page.back_button.click
end

Then(/^should go back to trending keyword dashboard$/) do
  expect(@trending_keyword_page.current_url).to include "/trending_keywords"
end
