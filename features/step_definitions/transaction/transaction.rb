Given(/^User want to buy product and ship it with POS$/) do
  visit_url('/')
  @passwords=ENV['BUYER_PASSWORD']
  login(ENV['BUYER_USERNAME'], ENV['BUYER_PASSWORD'])
  buy_product(ENV['SELLER_PRODUCT_URL'])
end

When(/^User fill the transaction details$/) do
  fill_in 'js-iv-buyer-notes-4',:with =>"test"
end

When(/^User choose Pos kilat as a Shipping method$/) do
  find('#js_iv_courier_4_chzn').click
  find('#js_iv_courier_4_chzn_o_2').click
  payment_first_page_click_next
end

Then(/^User Pay the transaction$/) do
  choose_bukadompet "#{ENV['CREDENTIAL_BUYER_PASSWORD']}"
  expect(page).to have_content('BERHASIL!')
  click_on('Lihat Tagihan')
  sleep 3
  within('.memo-column.memo-left.clearfix') do
    expect(page).to have_content('Dibayar')
  end
end
