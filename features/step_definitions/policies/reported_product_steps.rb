Given(/^there is admin want reported product$/) do
  login(ENV['ADMIN2_USERNAME'], ENV['ADMIN2_PASSWORD'])
end

When(/^admin report the product$/) do
  @policies_page = PoliciesPage.new
  @policies_page.load

  @policies_page.search_product("alamat palsu")
  sleep 4
  @policies_page.reported_action
  sleep 3
end

Then(/^admin see text Produk telah dilaporkan$/) do
  expect(page).to have_content('Produk telah dilaporkan')
  sleep 2
end

When(/^admin unreported the product$/) do
  @policies_page = PoliciesPage.new
  @policies_page.load
  @policies_page.search_product("alamat palsu")
  sleep 4
  @policies_page.unreported_action
  sleep 3
end

Then(/^admin see text Produk telah di-unhide$/) do
  sleep 2
  expect(page).to have_content('Produk telah di-unhide')
end
