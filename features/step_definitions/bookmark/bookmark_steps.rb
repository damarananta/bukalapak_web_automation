Then(/^User click favorite button$/) do
  @product_detail_page.favorite
end

Then(/^User click unfavorite button$/) do
  @product_detail_page.unfavorite
end

Then(/^favorite product count list on homepage should correct$/) do
  @bookmarks_page.verify_favorite_count_homepage
end

And(/^favorite product count list on dashboard should correct$/) do
  @bookmarks_page.verify_favorite_count_dashboard
end

When(/^User set all unfavorite$/) do
  @bookmarks_page.set_all_unfavorite
end

Then(/^favorite list on dashboard should be empty$/) do
  @bookmarks_page.favorite_list_should_empty
end

Then(/^page should redirect to login form$/) do
  @login_page.verify_login_form
end

Then(/^favorite product should decrease$/) do
  @bookmarks_page.verify_favorite_count_should_decrease
end
