Then(/^User click favorite button via mweb$/) do
  @m_product_detail_page.favorite
end

Then(/^User click unfavorite button via mweb$/) do
  @m_product_detail_page.unfavorite
end

Then(/^favorite product mweb should decrease$/) do
  @m_bookmarks_page.verify_favorite_count_should_decrease_mweb
end

And(/^favorite product count list mweb dashboard should correct$/) do
  @m_bookmarks_page.verify_favorite_count_dashboard_mweb
end

Then(/^favorite product count list mweb homepage should correct$/) do
  @m_bookmarks_page.verify_favorite_count_homepage_mweb
end
