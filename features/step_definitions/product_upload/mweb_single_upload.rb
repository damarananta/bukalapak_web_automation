When(/^user upload single product from mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_home_page.load
  @m_home_page.login(ENV['SELLER_USERNAME'], ENV['SELLER_PASSWORD'])
  @m_products_new_page = MobileProductsNewPage.new
  @m_products_new_page.load
  @m_products_new_page.upload_new_product(@product_name, @product_first_category, @product_second_category, @product_third_category, @product_weight, @product_stock, @product_price, @product_description)
end

Then(/^user will see their product from product detail on mweb$/) do
  @m_product_detail_page = MobileProductDetailPage.new
  @m_product_detail_page.assert_uploaded_product(@product_name, @product_first_category, @product_second_category, @product_third_category, @product_weight, @product_stock, @product_price, @product_description)
end
