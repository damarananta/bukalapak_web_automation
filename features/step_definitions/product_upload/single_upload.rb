Given(/^user have product to upload$/) do
  @product_name = create_product_name
  @product_first_category = create_product_first_category
  @product_second_category = create_product_second_category
  @product_third_category = create_product_third_category
  @product_weight = create_product_weight
  @product_stock = create_product_stock
  @product_price = create_product_price
  @product_description = create_product_description

  puts "Product Name            : #{@product_name}"
  puts "Product First Category  : #{@product_first_category}"
  puts "Product Second Category : #{@product_second_category}"
  puts "Product Third Category  : #{@product_third_category}"
  puts "Product Weight          : #{@product_weight}"
  puts "Product Stock           : #{@product_stock}"
  puts "Product Price           : #{@product_price}"
  puts "Product Description     : #{@product_description}"
end

When(/^user upload single product$/) do
  @home_page = HomePage.new
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['SELLER_USERNAME'],ENV['SELLER_PASSWORD'])
  @products_new_page = ProductsNewPage.new
  @products_new_page.load
  @products_new_page.upload_new_product(@product_name, @product_first_category, @product_second_category, @product_third_category, @product_weight, @product_stock, @product_price, @product_description)
end

Then(/^user will see their product from product detail$/) do
  @product_detail_page = ProductDetailPage.new
  @product_detail_page.assert_uploaded_product(@product_name, @product_first_category, @product_second_category, @product_third_category, @product_weight, @product_stock, @product_price, @product_description)
end

