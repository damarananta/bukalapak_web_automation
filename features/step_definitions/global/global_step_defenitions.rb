When(/^Admin login to dashboard$/) do
  @login_page=LoginPage.new
  @trending_keyword_page=TrendingKeywordPage.new
  @dashed_filter_page= DashedFilterPage.new
  @dashed_filter_price_page= DashedFilterPricePage.new
  @dashed_filter_location_page= DashedFilterLocationPage.new
  @dashed_filter_merk_page= DashedFilterMerkPage.new
  visit '/login'
  @login_page.login(ENV['ADMIN_USERNAME'], ENV['ADMIN_PASSWORD'])
end

When(/^User login to bukalapak with usename "([^"]*)" password "([^"]*)"$/) do |username,password|
  @login_page=LoginPage.new
  @bookmarks_page = BookmarksPage.new
  @carts_page = CartsPage.new
  visit '/login'
  @login_page.login(username,password)
end

When(/^User login to mweb bukalapak with usename "([^"]*)" password "([^"]*)"$/) do |username,password|
  @m_login_page=MobileLoginPage .new
  @bookmarks_page = BookmarksPage.new
  @carts_page = CartsPage.new
  @m_carts_page = MobileCartsPage.new
  @m_bookmarks_page = MobileBookmarksPage.new
  @m_home_page=MobileHomePage.new
  login_mobile(username,password)
end

Then(/^User logout from bukalapak$/) do
  visit '/logout'
end
