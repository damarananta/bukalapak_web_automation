Given(/^user want buy virtual product using BukaDompet$/) do
   login(ENV['BUYER_USERNAME'], ENV['BUYER_PASSWORD'])
end

When(/^user create "([^"]*)" transaction using BukaDompet with number "([^"]*)"$/) do |vp,nomor|
   @homepage = HomePage.new
   @homepage.buy_vp_home_widget(nomor,vp,nil,nil)
end

When(/^user create "([^"]*)" transaction using BukaDompet with number customer "([^"]*)"$/) do |vp,nomor|
   @homepage = HomePage.new
   @homepage.buy_vp_home_widget(nil,vp,nil,nomor)
end 

Then(/^user get tfa on transaction page$/) do
  @phone_credit = PhoneCreditNew.new
  @phone_credit.wait_for_deposit_payment_button(5)
  @phone_credit.deposit_payment_button.click
  @phone_credit.tfa_alert("12345")
end

Then(/^user can see invoice number at list transaction$/) do
  sleep 3
  expect(page).to have_content('Pembayaran via BukaDompet')
end

When(/^user create "([^"]*)" transaction using BukaDompet with trusted device and same "([^"]*)"$/) do |virtual_product,no|
   @homepage = HomePage.new
   @homepage.buy_vp_home_widget(no,virtual_product,nil,nil)
   @phone_credit = PhoneCreditNew.new
   @phone_credit.wait_for_deposit_payment_button(5)
   @phone_credit.deposit_payment_button.click
   @phone_credit.tfa_alert("12345")
   sleep 2
   @homepage.load
   @homepage.buy_vp_home_widget(no,virtual_product,nil,nil)
   @phone_credit.wait_for_deposit_payment_button(5)
   @phone_credit.deposit_payment_button.click
end

When(/^user create "([^"]*)" transaction using BukaDompet with trusted device and same customer "([^"]*)"$/) do |virtual_product,no|
   @homepage = HomePage.new
   @homepage.buy_vp_home_widget(nil,virtual_product,nil,no)
   @phone_credit = PhoneCreditNew.new
   @phone_credit.wait_for_deposit_payment_button
   @phone_credit.deposit_payment_button.click
   @phone_credit.tfa_alert("12345")
   sleep 2
   @homepage.load
   @homepage.buy_vp_home_widget(nil,virtual_product,nil,no)
   @phone_credit.wait_for_deposit_payment_button
   @phone_credit.deposit_payment_button.click
end

When(/^user create "([^"]*)" transaction using BukaDompet with trusted device and different number "([^"]*)"$/) do |virpro,number|
   @homepage = HomePage.new
   @homepage.buy_vp_home_widget("081265448251",virpro,nil,nil)
   @phone_credit = PhoneCreditNew.new
   @phone_credit.wait_for_deposit_payment_button(5)
   @phone_credit.deposit_payment_button.click
   @phone_credit.tfa_alert("12345")
   sleep 2
   @homepage.load
   @homepage.buy_vp_home_widget(number,virpro,nil,nil)
   @phone_credit.wait_for_deposit_payment_button(5)
   @phone_credit.deposit_payment_button.click
end
