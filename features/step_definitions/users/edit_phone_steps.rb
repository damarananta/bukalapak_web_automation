Given(/^there is user want edit his account$/) do
   login(ENV['USERNAME'], ENV['PASSWORD'])
end

When(/^user edit phone number$/) do
  @account_settings = AccountSettingsPage.new
  @account_settings.load
  page.execute_script "window.scrollBy(0,200)"
  @account_settings.wait_for_phone_edit_button
  @account_settings.phone_edit_button.click
  @account_settings.tfa_alert("12345")
  @edit_phone_page = EditUsersPage.new
  @edit_phone_page.edit_phone_number("081277891021","asdfg")
end

Then(/^user see pop up phone disclaimer$/) do
  @edit_phone_page = EditUsersPage.new
  @edit_phone_page.wait_for_phone_disclaimer(5)
  expect(page).to have_content('Pastikan')
end

When(/^user edit password$/) do
  @account_settings = AccountSettingsPage.new
  @account_settings.load
  page.execute_script "window.scrollBy(0,200)"
  @account_settings.wait_for_password_edit_button
  @account_settings.password_edit_button.click
  @account_settings.tfa_alert("12345")
  @edit_password_page = EditUsersPage.new
  @edit_password_page.edit_password("asdfg","asdfg")
end

Then(/^user back to account settings page and see text Pengaturan$/) do
  expect(page).to have_content('Pengaturan')
end

When(/^user click deactivated button$/) do
  @account_settings = AccountSettingsPage.new
  @account_settings.load
  page.execute_script "window.scrollBy(0,400)"
  @account_settings.wait_for_deactivated_button
  @account_settings.deactivated_button.click
end

Then(/^user get tfa$/) do
  @account_settings = AccountSettingsPage.new
  @account_settings.tfa_alert("12345")
  expect(page).to have_content('Pengaturan')
end

When(/^user click activated button$/) do
  @account_settings = AccountSettingsPage.new
  @account_settings.load
  page.execute_script "window.scrollBy(0,400)"
  @account_settings.wait_for_activated_button
  @account_settings.activated_button.click
end

Then(/^user tfa user is activated$/) do
  @account_settings = AccountSettingsPage.new
  expect(page).to have_content('Pengaturan')
end
