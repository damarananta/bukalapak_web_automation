Given(/^there is transaction and buyer using bukadompet$/) do
  @product_page=ProductDetailPage.new
  @checkout_page=CheckoutPage.new
  @payment_page=PaymentChannel.new
  @invoice_page=InvoicesPage.new
  @admin_discussion_page = AdminDiscussion.new
  @dompet_page= BukaDompet.new
  @transaction_page=TransactionPage.new
  

  login(ENV['BUYER1_USERNAME'], ENV['BUYER1_PASSWORD'])
  @Seller3_Product = ENV['SELLER_PRODUCT_URL']
  @BUYER_PASSWORD=ENV['BUYER1_PASSWORD']
  @product_page.load_product(@Seller3_Product)
  @product_page.click_beli
  @checkout_page.checkout
  sleep 3
  @price=@payment_page.choose_bukadompet_unpassword_ts
  sleep 3
  @transaction_page.tfa_alert_bukadompet_ts(12345)
  sleep 4
end

Given(/^there is transaction with trusted device using bukadompet$/) do
  @product_page=ProductDetailPage.new
  @checkout_page=CheckoutPage.new
  @payment_page=PaymentChannel.new
  @invoice_page=InvoicesPage.new
  @admin_discussion_page = AdminDiscussion.new
  @dompet_page= BukaDompet.new
  @transaction_page=TransactionPage.new

  login(ENV['BUYER3_USERNAME'], ENV['BUYER3_PASSWORD'])
  @Seller3_Product = ENV['SELLER2_PRODUCT_URL']
  @BUYER_PASSWORD=ENV['BUYER3_PASSWORD']
  @product_page.load_product(@Seller3_Product)
  @product_page.click_beli
  @checkout_page.checkout
  sleep 3
  @price=@payment_page.choose_bukadompet_unpassword_ts
  sleep 3
  @transaction_page.tfa_alert_bukadompet_ts(12345)
  sleep 4

  @Seller3_Product = ENV['SELLER2_PRODUCT_URL']
  @BUYER_PASSWORD=ENV['BUYER3_USERNAME']
  @product_page.load_product(@Seller3_Product)
  @product_page.click_beli
  @checkout_page.checkout
  sleep 3
  @payment_page.choose_bukadompet_unpassword_ts
  sleep 4
end

Given(/^there is transaction with address ever used before until remit using bukadompet$/) do
  @product_page=ProductDetailPage.new
  @checkout_page=CheckoutPage.new
  @payment_page=PaymentChannel.new
  @invoice_page=InvoicesPage.new
  @admin_discussion_page = AdminDiscussion.new
  @dompet_page= BukaDompet.new
  @transaction_page=TransactionPage.new
  @payment_purchase_new_page =  PaymentPurchasesNewPage.new

  login(ENV['BUYER4_USERNAME'], ENV['BUYER4_PASSWORD'])
  @Seller3_Product = ENV['SELLER2_PRODUCT_URL']
  @BUYER_PASSWORD=ENV['BUYER4_USERNAME']
  @product_page.load_product(@Seller3_Product)
  @product_page.click_beli
  sleep 7
  @payment_purchase_new_page.button_force_bukadompet_ts.click
  sleep 3
end

Then(/^buyer see invoice page$/) do
  expect(page).to have_content('Pembayaran via BukaDompet')
end
