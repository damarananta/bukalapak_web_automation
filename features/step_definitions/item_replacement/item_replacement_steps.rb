And(/^there is a Secondary seller$/) do
  @home_page                  = HomePage.new
  @dompet_page                = BukaDompet.new
  @product_page               = ProductDetailPage.new
  @checkout_page              = CheckoutPage.new
  @payment_page               = PaymentChannel.new
  @account_settings_page      = AccountSettingsPage.new
  @invoice_page               = InvoicesPage.new
  @transaction_page           = TransactionPage.new
  @replacement_page           = ItemReplacements.new
  @manage_transaction_page    = ManageTransaction.new
  @replacement_rules_page     = ReplacementRules.new
  @secondary_seller_cred      = ENV['SELLER4_USERNAME']
  @secondary_seller_pas       = ENV['SELLER4_PASSWORD']

 
  login(ENV['SELLER4_USERNAME'], ENV['SELLER4_PASSWORD'])
  @dompet_page.load
  @dompet_page.get_current_balance_secondary_seller
  @secondary_seller_balance_before=@dompet_page.get_current_balance_secondary_seller
  
  logout
end
  
And(/^there is a transaction and the state is paid$/) do
  login(ENV['BUYER_USERNAME'], ENV['BUYER_PASSWORD'])
  @Seller3_Product = ENV['SELLER3_PRODUCT_URL']
  @BUYER_PASSWORD=ENV['BUYER_PASSWORD']
  @product_page.load_product(@Seller3_Product)
  @product_page.click_beli
  @checkout_page.checkout
  @price=@payment_page.choose_bukadompet_unpassword
  @transaction_page.tfa_alert_bukadompet(12345)
  @payment_page.choose_bukadompet_after_otp
  @invoice_page.view_invoice
  @invoice_page.get_invoice_id
  @invoice_id=@invoice_page.get_invoice_id
  logout
end

And(/^seller reject the transaction$/) do
  login(ENV['SELLER3_USERNAME'], ENV['SELLER3_PASSWORD'])
  @transaction_page.load
  @transaction_page.seller_reject_orders(@invoice_id)
  logout
  restock("#{ENV['SELLER3_PRODUCT_EDIT_URL']}", ENV['SELLER3_USERNAME'], ENV['SELLER3_PASSWORD'])
  logout
end

And(/^administrator supervisor find another seller to replace buyers desired items$/) do
  login(ENV['CUSTOMER_SUPPORT_USERNAME'], ENV['CUSTOMER_SUPPORT_PASSWORD'])
  @replacement_page.load
  @replacement_page.search_transaction_replacements(@invoice_id)
  @new_price, @new_transaction_id= @replacement_page.get_replacements(@invoice_id)
  logout
end

And(/^buyer get the new seller$/) do
  #new seller accept the new order
  login(ENV['SELLER4_USERNAME'], ENV['SELLER4_PASSWORD'])
  @transaction_page.load
  @transaction_page.seller_accept_orders(@new_transaction_id)
  logout
  #buyer confirm the transaction
  login(ENV['BUYER_USERNAME'], ENV['BUYER_PASSWORD'])
  @transaction_page.search_new_transaction_after_replacements(@new_transaction_id)
  @transaction_page.buyer_confirm_to_remitted
  logout
end

And(/^buyers get charged from first seller items price$/) do
  login(ENV['BUYER_USERNAME'], ENV['BUYER_PASSWORD'])
  @dompet_page.load
  @dompet_page.check_buyer_balance_after_replacements(@buyer_balance_before,@new_price)
  logout
end

Then(/^money will be remitted to Secondary seller$/) do

  @manage_transaction_page.unhold_replacement_transaction(@new_transaction_id)
  #unhold_replacement_transaction
  login(ENV['SELLER4_USERNAME'], ENV['SELLER4_PASSWORD'])
  @dompet_page.load
  @dompet_page.check_secondary_seller_balance_after_replacements(@secondary_seller_balance_before,@new_price)
  logout

  @buyername=ENV['BUYER_USERNAME']
  @IRSellername=ENV['SELLER4_USERNAME']
  @sellername=ENV['SELLER3_USERNAME']
  @final_buyer_balance=@buyer_balance_before-@price
  @final_seller_balance=@seller_balance_before
  @final_replaced_seller_balance=@secondary_seller_balance_before+@new_price
  puts "
      --------------------------------------------------------
      Before IR
      --------------------------------------------------------
      Buyer                 : #{@buyername}
      Dompet Balance        : Rp #{@buyer_balance_before}
      
      Seller                : #{@sellername}
      Balance               : Rp #{@seller_balance_before}

      Replaced Seller       : #{@IRSellername}
      --------------------------------------------------------      
      Transaction Details 
      --------------------------------------------------------
      Transaction ID        : #{@invoice_id}
      Price                 : Rp #{@price}
      --------------------------------------------------------
      Replaced with         
      --------------------------------------------------------
      New Transaction ID    : #{@new_transaction_id}
      New Price             : Rp #{@new_price}
      --------------------------------------------------------
      Final Bukadompet Balance
      --------------------------------------------------------
      Buyer                 : Rp #{@final_buyer_balance}
      Seller                : Rp #{@final_seller_balance}
      Secondary Seller      : Rp #{@final_replaced_seller_balance}
    "

end


And(/^administrator supervisor assigned staff to processed the replacement$/) do
  login(ENV['CUSTOMER_SUPPORT_USERNAME'], ENV['CUSTOMER_SUPPORT_PASSWORD'])
  @replacement_rules_page.load
  @replacement_rules_page.assign_administrator
end

And(/^administrator staff find another seller to replace buyers desired items$/) do
  login(ENV['CUSTOMER_SUPPORT_STAFF_USERNAME'], ENV['CUSTOMER_SUPPORT_STAFF_PASSWORD'])
  @replacement_page.load
  @replacement_page.search_transaction_replacements(@invoice_id)
  @new_price, @new_transaction_id= @replacement_page.get_replacements(@invoice_id)
  logout
end
