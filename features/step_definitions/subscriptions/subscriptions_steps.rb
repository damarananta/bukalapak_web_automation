When(/^User navigate to subscriptions page$/) do
  close_general_cart_popup
  @subscriptions_page=SubscriptionsPage.new
  visit '/subscriptions'
  @subscriptions_page.wait_for_page_title(3)
end

Then(/^User should see subscriptions page$/) do
  pageTitle=@subscriptions_page.page_title.text
  pageTitle.should=='Langganan'
end

When(/^User click Timeline tab$/) do
  @subscriptions_page.timeline_tab.click
  sleep 2
  @subscriptions_page.wait_for_page_title(3)
end

When(/^User click Daftar Pelanggan tab$/) do
  @subscriptions_page.daftar_pelanggan_tab.click
  sleep 2
  @subscriptions_page.wait_for_page_title(3)
end

Then(/^User should see empty info "([^"]*)"$/) do |info|
  eText=@subscriptions_page.empty_text.text
  eText.should==info
end

Then(/^User should see breadcum level3 "([^"]*)"$/) do |breadcum_text|
  breadcumLevel3=@subscriptions_page.breadcum_level3.text
  breadcumLevel3.should==breadcum_text
end

Then(/^User should see subscription list$/) do
  expect(@subscriptions_page).to have_button_lihat_semua_barang
  expect(@subscriptions_page).to have_pelapak_link
  expect(@subscriptions_page).to have_user_activity
  expect(@subscriptions_page).to have_product_box
end

Then(/^User should see timeline list$/) do
  expect(@subscriptions_page).to have_timeline_pelapak_link
  expect(@subscriptions_page).to have_timeline_user_activity
  expect(@subscriptions_page).to have_modify_date
end

Then(/^User should see pelanggan list$/) do
  expect(@subscriptions_page).to have_header_pelanggan
  expect(@subscriptions_page).to have_header_subs_date
  expect(@subscriptions_page).to have_header_transaksi_total
  expect(@subscriptions_page).to have_value_image_pelanggan
  expect(@subscriptions_page).to have_value_link_pelanggan
  expect(@subscriptions_page).to have_value_subs_date
  expect(@subscriptions_page).to have_value_transaksi_total
end

Given(/^"([^"]*)" to the search box subscriptions page$/) do |keyword|
  @subscriptions_page.search_box.send_keys(keyword, :enter)
  sleep 2
  @subscriptions_page.wait_for_page_title(3)
end

Then(/"([^"]*)" should be mentioned in the subscriptions tab results$/) do |result|
  lapak_name_result=@subscriptions_page.pelapak_link.text
  is_true=lapak_name_result.include? "#{result}"
  is_true.should==true
end

Then(/"([^"]*)" should be mentioned in the Daftar Pelanggan tab results$/) do |result|
  subscriber_name_result=@subscriptions_page.value_link_pelanggan.text
  puts "subs #{subscriber_name_result}"
  is_true=subscriber_name_result.include? "#{result}"
  is_true.should==true
end
