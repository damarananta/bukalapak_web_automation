When(/^User navigate to subscriptions page via mweb$/) do
  close_general_cart_popup
  @subscriptions_page=MobileSubscriptionsPage.new
  visit '/subscriptions'
  @subscriptions_page.wait_for_lapak_langganan_tab(3)
end

Then(/^User should see subscriptions page via mweb$/) do
  current_url=ENV['MOBILE_URL']+'/subscriptions'
  expect(page).to have_current_path(current_url, url: true)
end

When(/^User click Timeline tab via mweb$/) do
  @subscriptions_page.timeline_tab.click
  sleep 2
end

When(/^User click Daftar Pelanggan tab via mweb$/) do
  @subscriptions_page.daftar_pelanggan_tab.click
  sleep 2
end

Then(/^User should see empty info "([^"]*)" via mweb$/) do |info|
  eText=@subscriptions_page.empty_text.text
  eText.should==info
end

Then(/^User should see breadcum level2 "([^"]*)" via mweb$/) do |breadcum_text|
  breadcumLevel2=@subscriptions_page.breadcum_level2.text
  breadcumLevel2.should==breadcum_text
end

Then(/^User should see subscription list via mweb$/) do
  expect(@subscriptions_page).to have_pelapak_image
  expect(@subscriptions_page).to have_pelapak_link
  expect(@subscriptions_page).to have_user_activity
  expect(@subscriptions_page).to have_product_box
end

Then(/^User should see timeline list via mweb$/) do
  expect(@subscriptions_page).to have_timeline_pelapak_link
  expect(@subscriptions_page).to have_timeline_user_activity
  expect(@subscriptions_page).to have_timeline_pelapak_image
end

Then(/^User should see pelanggan list via mweb$/) do
  expect(@subscriptions_page).to have_header_pelanggan
  expect(@subscriptions_page).to have_header_subs_date
  expect(@subscriptions_page).to have_value_image_pelanggan
  expect(@subscriptions_page).to have_value_link_pelanggan
  expect(@subscriptions_page).to have_value_subs_date
end

Given(/^"([^"]*)" to the search box subscriptions page via mweb$/) do |keyword|
  @subscriptions_page.search_box.send_keys(keyword, :enter)
  sleep 2
end

Then(/"([^"]*)" should be mentioned in the subscriptions tab results via mweb$/) do |result|
  lapak_name_result=@subscriptions_page.pelapak_link.text
  is_true=lapak_name_result.include? "#{result}"
  is_true.should==true
end

Then(/"([^"]*)" should be mentioned in the Daftar Pelanggan tab results via mweb$/) do |result|
  subscriber_name_result=@subscriptions_page.value_link_pelanggan.text
  puts "subs #{subscriber_name_result}"
  is_true=subscriber_name_result.include? "#{result}"
  is_true.should==true
end
