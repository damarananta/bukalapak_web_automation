When(/^User create new message to "([^"]*)" via mweb$/) do |user_id|
  @m_chat_page = MobileChatPage.new
  accept_onboarding
  @m_chat_page.create_chat(user_id)
  visit '/logout'
end

Then(/^Receiver should get notification via mweb$/) do
  sleep 1
  visit '/messages'
  @m_chat_page.visit_user_panel
  sleep 1
  page.should have_selector(MobileChatPage::NEW_MESSAGE_NOTIF)
end

When(/^Receiver read message from "([^"]*)" via mweb$/) do |user_id|
  sleep 1
  @m_chat_page.set_read_all_chat(user_id)
end

Then(/^notification should reduce via mweb$/) do
  sleep 1
  visit '/'
  accept_onboarding
  @m_chat_page.visit_user_panel
  sleep 1
  page.should have_no_selector(MobileChatPage::NEW_MESSAGE_NOTIF)
end
