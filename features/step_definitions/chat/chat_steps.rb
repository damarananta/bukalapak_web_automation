When(/^User create new message to "([^"]*)"$/) do |user_id|
  @chat_page = ChatPage.new
  @chat_page.create_chat(user_id)
  visit '/logout'
end

Then(/^Receiver should get notification$/) do
  visit '/my_lapak'
  sleep 1
  chat_notif_content=@chat_page.new_notif_chat.text
  chat_notif_content.should=='1'
end

When(/^Receiver read message from "([^"]*)"$/) do |user_id|
  @chat_page.set_read_all_chat(user_id)
end

Then(/^notification should reduce$/) do
  sleep 1
  visit '/my_lapak'
  sleep 1
  page.should have_no_selector(:xpath, ChatPage::NEW_NOTIF_CHAT)
end
