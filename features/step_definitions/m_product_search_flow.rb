When /^user buy item in mweb$/ do
  @m_product_detail = MobileProductDetailPage.new

  @m_product_detail.load_product(ENV['SELLER_PRODUCT_URL'])
  @m_product_detail.click_beli
end

Then(/^user fill buyer name (.*) , email (.*) and phone (.*)$/) do |name, email, phone|
  expect(page).to have_css('#new_payment_invoice')

  within('#new_payment_invoice')do
    fill_in('js-iv-buyer-name', :with => name)
    fill_in('js-iv-email', :with => email)
    fill_in('js-iv-phone', :with => phone)
    sleep 5
  end
end
