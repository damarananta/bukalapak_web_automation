Given(/^user have valid E\-Mail Address$/) do
  @gender       = gender
  @fullname     = create_fullname(@gender)
  @username     = create_username(@fullname)
  @email        = create_email(@username)
  @password     = "pastibisa"
  @phone_number = create_phone_number

  puts "gender      : #{@gender}"
  puts "fullname    : #{@fullname}"
  puts "username    : #{@username}"
  puts "password    : #{@password}"
  puts "email       : #{@email}"
  puts "phone number: #{@phone_number}"
end

When(/^user register on bukalapak$/) do
  @home_page = HomePage.new
  @home_page.load
  dismiss_one_signal_popup
  @home_page.daftar_link.click
  @register_page = RegisterPage.new
  @register_page.register_random_user(@email, @fullname, @password, @username, @gender)
end

When(/^user register on bukalapak via mobile web$/) do
  @m_home_page = MobileHomePage.new
  @m_home_page.load
  dismiss_one_signal_popup
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.visit_register_page
  @m_register_page = MobileRegisterPage.new
  @m_register_page.load
  @m_register_page.register_random_user(@email, @fullname, @password, @username, @gender)
end

Then(/^user can login with new account$/) do
  logout
  @home_page.load
  @home_page.login(@username, @password)
end

Then(/^user can login with new account via mobile web$/) do
  logout
  @m_home_page = MobileHomePage.new
  @m_home_page.load
  @m_home_page.login(@username, @password)
end
