When(/^User navigate to diskon page via mweb$/) do
  @diskon_page=MobileDiskonPage.new
  visit '/diskon'
  @diskon_page.wait_for_page_tilte(3)
end

And(/^click button diskon spesial via mweb$/) do
  @diskon_page.button_diskon_spesial.click
  @diskon_page.wait_for_card_product(3)
end

Then(/^User should see discount product listing via mweb$/) do
  expect(@diskon_page.page_tilte).to have_text('Promo')
  expect(@diskon_page.button_diskon_spesial).to have_text('Special')
  expect(@diskon_page.button_diskon_hari_ini).to have_text('Hari Ini')
  expect(@diskon_page).to have_button_diskon_spesial
  expect(@diskon_page).to have_button_diskon_hari_ini
  expect(@diskon_page).to have_product_name
  expect(@diskon_page).to have_discount_badge
  expect(@diskon_page).to have_reduce_price
  expect(@diskon_page).to have_original_price
  expect(@diskon_page).to have_card_product
end
