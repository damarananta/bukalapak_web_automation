When(/^User navigate to diskon page$/) do
  @diskon_page=DiskonPage.new
  visit '/diskon'
end

Given(/^"([^"]*)" to the search box diskon page$/) do |keyword|
  find(:xpath,'//div/*[@id="search_keywords"]').send_keys(keyword, :enter)
  @diskon_page.wait_for_search_tooltip_result(3)
end

Then(/"([^"]*)" should be mentioned in the diskon page results$/) do |result|
  product_name_result=@diskon_page.product_name.text
  is_true=product_name_result.include? "#{result}"
  is_true.should==true
end

Then(/^click button diskon spesial$/) do
  @diskon_page.button_diskon_spesial.click
  @diskon_page.wait_for_search_box(3)
end
