Given(/^there is user that have been registered$/) do
  @username = ENV['USERNAME']
  @password = ENV['PASSWORD']
  @name     = ENV['NAME']
end

When(/^user login$/) do
  @home_page = HomePage.new
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(@username, @password)
end

Then(/^user will see flash message "([^"]*)"$/) do |flash|
  @home_page = HomePage.new
  @home_page.wait_for_flash_message(10)
  expect(@home_page).to have_flash_message
  expect(@home_page.flash_message).to have_content flash
end

Then(/^user will see their name on the navbar$/) do
  @home_page = HomePage.new
  @home_page.wait_for_username_link
  expect(@home_page.username_link).to have_text @name
end

When(/^user login from login page$/) do
  @login_page = LoginPage.new
  @login_page.load
  @login_page.login(@username, @password)
end

Then(/^user will see user icon on the navbar via mobile web$/) do
  expect(@m_home_page).to have_user_icon
end

When(/^user login from mobile home page$/) do
  @m_home_page = MobileHomePage.new
  @m_home_page.load
  @m_home_page.dismiss_one_signal_popup_homepage
  @m_home_page.login(@username, @password)
end

Then(/^user will see user icon on the navbar via mobile home page$/) do
  expect(@m_home_page).to have_user_icon
end
