Given(/^user push balance on dweb$/) do
  @product_detail_page = ProductDetailPage.new
  @beli_paket_push_page = BeliPaketPushPage.new
  @beli_paket_push_page.load
  @beli_paket_push_page.wait_for_saldo_push_text
  @push_balance = @beli_paket_push_page.get_push_balance
end

When(/^user "([^"]*)" set push on "([^"]*)" on dweb$/) do |user, product|
  @total_product_push = 1
  @product_detail_page.load_product(ENV["#{user}_#{product}_URL"])
  @product_detail_page.wait_for_product_push_button
  scroll_down(200)
  @product_detail_page.product_push_button.click
  @product_detail_page.wait_for_flash_message
end

Then(/^push balance should be subtracted on dweb$/) do
  @beli_paket_push_page.load
  @beli_paket_push_page.wait_for_saldo_push_text
  expect(@beli_paket_push_page.saldo_push_text).to(have_text("#{@push_balance - @total_product_push}"))
end

When(/^user "([^"]*)" set push on "([^"]*)" on mweb$/) do |user, product|
  @total_product_push = 1
  @product_detail_page = ProductDetailPage.new
  @product_detail_page.load_product(ENV["#{user}_#{product}_URL"])
  @product_detail_page.wait_for_product_push_button
  @m_product_detail_page = MobileProductDetailPage.new
  scroll_to(@m_product_detail_page.product_name, true)
  @product_detail_page.product_push_button.click
  @product_detail_page.wait_for_flash_message
end

Then(/^push balance should be subtracted on mweb$/) do
  @m_ringkasan_akun_page.load
  expect(@m_ringkasan_akun_page.push_balance_text).to(have_text("#{@push_balance - @total_product_push}"))
end

When(/^user set push on all products on dweb$/) do
  @barang_dijual_page = BarangDijualPage.new
  @barang_dijual_page.load
  @barang_dijual_page.wait_for_check_all_products_checkbox
  @barang_dijual_page.check_all_products_checkbox.click
  @barang_dijual_page.product_action_combobox.click
  @barang_dijual_page.push_combobox_option.click
  @barang_dijual_page.wait_for_push_button_on_popup_button
  total_product = @barang_dijual_page.product_count_text.text.to_i
  remaining_push = @barang_dijual_page.remaining_push_text.text.to_i
  if (total_product > remaining_push)
    @total_product_push = remaining_push
    @total_push_with_bukadompet = total_product - remaining_push
  else
    @total_product_push = total_product
  end
  @barang_dijual_page.push_button_on_popup_button.click
  @barang_dijual_page.wait_for_flash_message
end

Given(/^user granted with (\d+) push balance active for (\d+) days on dweb$/) do |granted_push, active_days|
  @granted_push = granted_push
  @home_page = HomePage.new
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['ADMIN_USERNAME'], ENV['ADMIN_PASSWORD'])
  @grant_push_page = GrantPushPage.new
  @grant_push_page.load
  @grant_push_page.wait_for_simpan_button
  @grant_push_page.username_field.set(@username)
  @grant_push_page.push_count_field.set(@granted_push)
  @grant_push_page.aktif_hingga_field.set(active_days)
  @grant_push_page.simpan_button.click
  @grant_push_page.wait_for_flash_message
  logout
end

Given(/^user BukaDompet balance on dweb$/) do
  @dompet_page = BukaDompet.new
  @dompet_page.load
  @bukadompet_balance = @dompet_page.get_current_balance
end

Then(/^BukaDompet balance should be subtracted on dweb$/) do
  @dompet_page.load
  expected_bukadompet_balance = "#{add_delimiter(@bukadompet_balance - (1000 * @total_push_with_bukadompet))}"
  expect(@dompet_page.current_balance).to(have_text(expected_bukadompet_balance))
end

When(/^user set push on all products with inactive push on dweb$/) do
  @barang_dijual_page = BarangDijualPage.new
  @barang_dijual_page.load
  @barang_dijual_page.wait_for_check_all_products_checkbox
  @barang_dijual_page.check_all_products_checkbox.click
  @barang_dijual_page.product_action_combobox.click
  @barang_dijual_page.push_combobox_option.click
  @barang_dijual_page.wait_for_push_button_on_popup_button
  if (@barang_dijual_page.has_product_count_text?)
    @total_push_with_bukadompet = @barang_dijual_page.product_count_text.text.to_i
    @barang_dijual_page.push_button_on_popup_button.click
    @barang_dijual_page.wait_for_flash_message
  end
end

Then(/^push balance should not be subtracted on dweb$/) do
  @beli_paket_push_page.load
  @beli_paket_push_page.wait_for_saldo_push_text
  expect(@beli_paket_push_page.saldo_push_text).to(have_text("#{@push_balance}"))
end

Then(/^push failed and pop\-up will appear to suggest top\-up$/) do
  @barang_dijual_page.wait_for_push_popup_section
  expect(@barang_dijual_page).to(have_push_popup_section)
  expect(@barang_dijual_page.push_popup_section).to(have_text("Push Tidak Dapat Dilakukan"))
  expect(@barang_dijual_page).to(have_amount_negative_text)
  expect(@barang_dijual_page.push_popup_section).to have_link("Tambah Saldo BukaDompet", :href => "/deposit/user_topups/new")
end

