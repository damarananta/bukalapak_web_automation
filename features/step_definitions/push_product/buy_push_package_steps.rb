Given(/^user as "([^"]*)"$/) do |user|
  @username = ENV["#{user}_USERNAME"]
  @password = ENV["#{user}_PASSWORD"]
end

When(/^user go to Beli Paket Push page on dweb$/) do
  @beli_paket_push_page = BeliPaketPushPage.new
  @beli_paket_push_page.load
  @beli_paket_push_page.wait_for_saldo_bukadompet_text
  @push_balance = @beli_paket_push_page.get_push_balance
  @bukadompet_balance = @beli_paket_push_page.get_bukadompet_balance
end

When(/^user click Pilih on a package with "([^"]*)" push on dweb$/) do |total_push|
  scroll_to(@beli_paket_push_page.saldo_push_text, true)
  @beli_paket_push_page.get_pilih_push_package_button(total_push).click
  @beli_paket_push_page.wait_for_package_price_text
  @package_price = change_into_integer(@beli_paket_push_page.package_price_text.text)
  @package_active_until = @beli_paket_push_page.package_active_until_date_text.text.strip
  @total_push = total_push.to_i
end

Given(/^user click Beli Paket Push on dweb$/) do
  @beli_paket_push_page.beli_paket_push_button.click
  @beli_paket_push_page.wait_for_saldo_push_text
end

Then(/^BukaDompet balance should be substracted, push balance should be added, and expired date should be updated on dweb$/) do
  expected_bukadompet_balance = @bukadompet_balance - @package_price
  expected_push_balance = @push_balance + @total_push
  expect(@beli_paket_push_page.saldo_bukadompet_text).to(have_text("#{add_delimiter(expected_bukadompet_balance)}"))
  expect(@beli_paket_push_page.saldo_push_text).to(have_text(expected_push_balance.to_s))
  expect(@beli_paket_push_page.push_info_section).to(have_text(@package_active_until))
end

Given(/^user push balance on mweb$/) do
  @m_ringkasan_akun_page = MobileRingkasanAkunPage.new
  @m_ringkasan_akun_page.load
  @m_ringkasan_akun_page.wait_for_push_balance_text
  @push_balance = @m_ringkasan_akun_page.get_push_balance
end

When(/^user go to Beli Paket Push page on mweb$/) do
  @m_beli_paket_push_page = MobileBeliPaketPushPage.new
  @m_beli_paket_push_page.load
  @m_beli_paket_push_page.wait_for_saldo_bukadompet_text
  @bukadompet_balance = change_into_integer(@m_beli_paket_push_page.saldo_bukadompet_text.text)
end

When(/^user click Pilih on a package with "([^"]*)" push on mweb$/) do |total_push|
  @package_price = @m_beli_paket_push_page.get_push_package_price(total_push)
  @package_active_until = @m_beli_paket_push_page.get_push_package_active_until(total_push)
  @total_push = total_push.to_i
  scroll_down(200)
  @m_beli_paket_push_page.get_push_package_radiobutton(total_push).click
end

When(/^user click Beli Paket Push on mweb$/) do
  @m_beli_paket_push_page.beli_button.click
  @m_beli_paket_push_page.wait_for_page_notif_section
end

Then(/^BukaDompet balance should be substracted, push balance should be added, and expired date should be updated on mweb$/) do
  expected_bukadompet_balance = @bukadompet_balance - @package_price
  expected_push_balance = @push_balance + @total_push
  expect(@m_beli_paket_push_page.saldo_bukadompet_text).to(have_text("#{add_delimiter(expected_bukadompet_balance)}"))
  @m_ringkasan_akun_page.load
  expect(@m_ringkasan_akun_page.push_balance_text).to(have_text(expected_push_balance.to_s))
  expect(@m_ringkasan_akun_page.push_active_until_text).to(have_text(@package_active_until))
end
