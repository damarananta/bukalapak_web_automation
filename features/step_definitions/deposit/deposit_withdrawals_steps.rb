Given(/^there is user want to withdrawal with deposit$/) do
  username = ENV['BUYER_USERNAME']
  @password = ENV['BUYER_PASSWORD']
  login(username, @password)
end

When(/^user withdrawal deposit with amount "([^"]*)"$/) do |amount|
  @amount = amount
  @bukadompet_page = BukaDompet.new
  @bukadompet_page.load
  @bukadompet_page.withdrawal_deposit("12345")
  @withdrawal_page = DepositWithdrawalsPage.new
  @withdrawal_page.withdrawal_bukadompet(@amount, @password)
end

Then(/^user see amount withdrawal in that page$/) do
  amount = price_delimiter(@amount)
  expect(page).to have_content(amount)
end
