When /^user choosing JNE YES for shipping$/ do
  visit_url '/'
  login('joneyes', 'apaajadeh')
  buy_product '/p/hobi-koleksi/mainan/lain-lain-345/3kwq7b-jual-mobil-mainan-bekas'

  select_jne_yes
  payment_page_after_choosing_courier
end

Then /^the page should be directed to payment page$/ do
  choose_atm
  within('.memo-column.memo-left.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end

And /^the shipping cost for JNE YES should be matched$/ do
  within('.memo-item.notice.notice--no-border.prices') do
    if(@shipping_price != '0')
      expect(page).to have_content(@shipping_price)
    end
  end
  
  within('.block-payment-code--highlight') do
    expect(page).to have_content(@unique_code)
  end
end
