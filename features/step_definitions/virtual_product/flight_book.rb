#
# DESKTOP WEB
#

When(/^user search oneway flight ticket via landingpage from "([^"]*)" to "([^"]*)"$/) do |departure, arrive|

  @home_page              = HomePage.new
  @flight_book_page       = FlightBookPage.new
  @flight_result          = SearchResultFlightPage.new
  #@confirmation_page      = PurchaseConfirmationPage.new
  #@invoice_detail_page    = InvoiceDetailPage.new

  book_flight(ENV['BUYER6_USERNAME'],ENV['BUYER6_PASSWORD'], "oneway", departure, arrive)
    
  #@flight_new.alfamart_payment_channel
  #@confirmation_page.ignore_phonenumber_confirmation(payment_method)

end

When(/^user search "([^"]*)" flight ticket via landingpage from "([^"]*)" to "([^"]*)"$/) do |type, departure, arrive|
  @home_page              = HomePage.new
  @flight_book_page       = FlightBookPage.new
  @flight_result          = SearchResultFlightPage.new
  #@confirmation_page      = PurchaseConfirmationPage.new
  #@invoice_detail_page    = InvoiceDetailPage.new

  book_flight(ENV['BUYER6_USERNAME'],ENV['BUYER6_PASSWORD'], type, departure, arrive)
  sleep 7
end

When(/^user search return flight at search result page$/) do
  @flight_result.wait_for_research_button
  @flight_result.research_button.click
  
  sleep 5
  @flight_result.radio_return.click
  sleep 20


end


When(/^user research flight at search result page$/) do
  @flight_result.wait_for_research_button
  @flight_result.research_button.click

end

When(/^user filter the flight based maskapai$/) do
  #@flight_result.wait_for_list_flight nil, :count => 3
  #@flight_result.wait_for_list_flight

  #@flight_result.wait_for_progress
  #puts progress

  #if progress == '0%'
  #  puts 'ok'
  #  puts @flight_result.count  
  #end

  sleep 20
  
  @flight_result.wait_for_checkbox_Citilink
  @flight_result.checkbox_Citilink.click
  sleep 10
  #puts @flight_result.list_flight.count
  
  #@flight_result.list_flight.should eql("Citilink")

end

Then(/^user get maskapai that has filtered by maskapai$/) do
  puts "Maskapai list"
  within('.o-layout__item.u-10of12.u-clearfix') do
    page.all(".u-txt--small.u-mrgn-bottom--2").each do |maskapai|
      puts maskapai.text
    end
  end
end


When(/^user filter the flight based range price$/) do
  page.execute_script("window.scrollBy(0,400)")
  @flight_result.wait_for_field_min_price
  #@flight_result.field_min_price.set 1000000
  #@flight_result.field_max_price.set 2000000
  @flight_result.display_button.click
  sleep 15
end

Then(/^user get maskapai that has filtered by price$/) do
  puts "Price list"

  within('.o-layout__item.u-10of12.u-clearfix') do
    page.all(".u-txt--medium.u-align-right.u-mrgn-bottom--1").each do |price|
      puts price.text
    end
  end
end

When(/^user filter the flight based time departured$/) do
  page.execute_script "window.scrollBy(0,600)"
  @flight_result.wait_for_checkbox_0000_0700_dp
  @flight_result.checkbox_0700_1200_dp.click
  sleep 10
end

Then(/^user get maskapai that has filtered by time departured$/) do
  puts "Time departured list"
  
  page.all(".u-txt--medium.u-mrgn-bottom--0.qa-depart").each do |departured|
    puts departured.text
  end
end


When(/^user filter the flight based time arrived$/) do
  #@flight_result.wait_for_checkbox_0000_0700_ar
  #@flight_result.checkbox_0000_0700_ar.click
  sleep 10
end

Then(/^user get maskapai that has filtered by time arrived$/) do
  puts "Time arrived list"
  
  within('.o-layout__item.u-10of12.u-clearfix') do

    page.all(".u-txt--medium").each do |arrived|
      puts arrived.text
    end

  end
end

Then(/^user get flight that has filtered by departure and arrive$/) do
  sleep 30
  puts "-Departure-"
    page.all(".u-txt--small.u-fg--charcoal.u-mrgn-bottom--0.qa-flight-search-origin").each do |da|
      puts da.text
      #@departure = da.first.text
    end


  puts "-Arrive-"
    page.all(".u-txt--small.u-fg--charcoal.u-mrgn-bottom--0.qa-flight-search-dest").each do |de|
      puts de.text
      #@arrive = da.first.text
    end
end

Then(/^user selected departure flight$/) do
  @departure_price = @flight_result.list_departure_price.first.text
  
  @flight_result.select_pergi_button.first.click
  

  sleep 10
  # card is visible
  #find('a', :text => @departure)
  #find('a', :text => @arrive)

  #get IDR departure

end

Then(/^user get return flight$/) do
  sleep 15
  puts "-Departure-"
    page.all(".u-txt--small.u-fg--charcoal.u-mrgn-bottom--0.qa-flight-search-origin").each do |da|
      puts da.text
      #@departure = da.first.text
    end

  puts "-Arrive-"
    page.all(".u-txt--small.u-fg--charcoal.u-mrgn-bottom--0.qa-flight-search-dest").each do |da|
      puts da.text
      #@arrive = da.first.text
    end
end


When(/^user selected return flight and continue the flight booking$/) do
  @return_price = @flight_result.list_return_price.first.text
  
  @flight_result.select_pulang_button.first.click
  
  # return will be match
  #departure will be match
  #arrive will be match
  
  puts "Departure : "+@departure_price
  puts "Return    : "+@return_price

  #total = departure + returnend
end


When(/^user select the flight$/) do
  @flight_result.wait_until_select_pergi_button_visible(30)
  @flight_result.select_pergi_button.first.click

end

When(/^user fill passanger data and continue to payment$/) do
  @flight_checkout = FlightCheckoutPage.new

  @flight_checkout.name_field.set "fadhli"
  @flight_checkout.email_field.set "fadhliutest@gmail.com"
  @flight_checkout.no_hp_field.set "08129329139123"
  
  @flight_checkout.lanjut_pembayaran_button.click

end



#
# MOBILE WEB
#

