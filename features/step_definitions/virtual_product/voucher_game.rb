#
# DESKTOP WEB
#

When(/^user create game voucher \("([^"]*)","([^"]*)"\) transaction using "([^"]*)" as payment method$/) do |username, password, payment_method|

  @home_page           = HomePage.new
  @game_voucher_page   = VPGameVoucherLandingPage.new
  @gamevoucher_new     = GameVoucherNew.new 
  @confirmation_page   = PurchaseConfirmationPage.new
  @invoice_detail_page = InvoiceDetailPage.new

  case payment_method
  when "Alfamart"
    buy_game_voucher(username,password)
    @gamevoucher_new.alfamart_payment_channel
    @confirmation_page.ignore_phonenumber_confirmation(payment_method)
  
  when "Transfer"
    buy_game_voucher(username,password)
    @gamevoucher_new.atm_payment_channel
    @unique_code = @confirmation_page.unique_code_text.text
    @confirmation_page.ignore_phonenumber_confirmation(payment_method)
      
  when "Indomaret"
    buy_game_voucher(username,password)
    @gamevoucher_new.indomaret_payment_channel
    @confirmation_page.ignore_phonenumber_confirmation(payment_method)   
  
  when "BukaDompet"
    buy_game_voucher(username,password)
    @gamevoucher_new.bukadompet_payment_channel
    @confirmation_page.ignore_phonenumber_confirmation(payment_method)
  
  when "BCA KlikPay"
    @ebanking = EbankingRedirectPage.new
    buy_game_voucher(username,password)
    @gamevoucher_new.klikpay_payment_channel
    @ebanking.ebanking_page_button.click
    find_button('bayar').click
    sleep 3
  end
end

When(/^user create game voucher transaction using BukaDompet with insufficient deposit$/) do
  @home_page           = HomePage.new
  @game_voucher_page   = VPGameVoucherLandingPage.new
  @gamevoucher_new     = GameVoucherNew.new 
  
  buy_game_voucher(ENV['BUYER2_USERNAME'], ENV['BUYER2_PASSWORD'])
  @gamevoucher_new.bukadompet_radio.click
      
end

When(/^user create game voucher transaction using bukadompet with wrong password$/) do
  @home_page           = HomePage.new
  @game_voucher_page   = VPGameVoucherLandingPage.new
  @gamevoucher_new     = GameVoucherNew.new

  buy_game_voucher(ENV['BUYER_USERNAME'], ENV['BUYER_PASSWORD'])
  
  @gamevoucher_new.pick_payment_method("bukadompet")
  @gamevoucher_new.deposit_password_field.set("salah password")
  @gamevoucher_new.button_pay.click
end

Then(/^game voucher invoice status should be in wating$/) do
  @invoice_detail_page.wait_for_payment_status_text
  @invoice_detail_page.vp_invoice_should_be_in_waiting
end

Then(/^user cannot create game voucher transaction using BukaDompet$/) do
  @game_voucher_new_page = GameVoucherNew.new
  wording = "Saldo BukaDompet-mu tidak mencukupi untuk membayar transaksi ini. Silakan pilih metode pembayaran lainnya"
  # wording = "Saldo BukaDompet-mu tidak mencukupi untuk transaksi ini." \
  #           " Silakan gunakan metode pembayaran lainnya."

  expect(@game_voucher_new_page).to have_content(wording)
  expect(@game_voucher_new_page).to have_no_content("BERHASIL!")
end

Then(/^user will be displayed game voucher transaction message "([^"]*)"$/) do |msg|
  expect(@gamevoucher_new.deposit_error_notification.text).to eq(msg)
end

#
# MOBILE WEB
#
When(/^user create game voucher transaction using "([^"]*)" as payment method via mweb$/) do |payment_method|
  @m_home_page = MobileHomePage.new
  @m_gamevoucher_page = MobileGameVoucherPage.new
  @m_zynga_page = MobileZyngaPage.new
  @m_gamevoucher_new_page = MobileGameVoucherNew.new
  @m_purchases_page = MobilePurchasesPage.new

  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])
  
  @m_gamevoucher_page.load
  sleep 2
  @m_gamevoucher_page.publishers.first.click
  sleep 2

  page.execute_script("window.scrollBy(0,500)")
  sleep 5
  @m_zynga_page.beli_button.click
  sleep 3

  @m_gamevoucher_new_page.lanjut_button.click
  sleep 3
  
  case payment_method
  when "BukaDompet"
    @m_gamevoucher_new_page.bukadompet_radio.click
    sleep 5
      
  when "Alfamart"
    @m_gamevoucher_new_page.alfamart_radio.click
    @m_gamevoucher_new_page.lanjut_alfamart_button.click
    sleep 5

    @invoice_number = @m_purchases_page.invoice_number.text
    sleep 1
    @m_purchases_page.lihat_tagihan_pembayaran_link.click
  when "Transfer"

    @m_gamevoucher_new_page.transfer_radio.click
    @m_gamevoucher_new_page.lanjut_transfer_button.click
    sleep 5

    @invoice_number = @m_purchases_page.invoice_number.text
    puts "unique code :"+@invoice_number
    sleep 1
    @m_purchases_page.lihat_tagihan_pembayaran_link.click

  when "Indomaret"
    @m_gamevoucher_new_page.indomaret_radio.click
    @m_gamevoucher_new_page.lanjut_alfamart_button.click
    sleep 5

    @invoice_number = @m_purchases_page.invoice_number.text
    sleep 1
    @m_purchases_page.lihat_tagihan_pembayaran_link.click

  end
end

Then(/^game voucher invoice status in mweb should be in wating$/) do
    expect(page).to have_content('Menunggu Pembayaran')
end

When(/^user has deposit in bukadompet and enough to buy the game voucher via mweb$/) do
  @m_gamevoucher_new_page = MobileGameVoucherNew.new
  @m_purchases_page = MobilePurchasesPage.new

  @m_gamevoucher_new_page.bukadompet_password_field.set ENV['BUYER_PASSWORD']
  sleep 3
  @m_gamevoucher_new_page.lanjut_bukadompet_button.click
  sleep 3

  @invoice_number = @m_purchases_page.invoice_number.text
  sleep 1
  @m_purchases_page.lihat_tagihan_pembayaran_link.click
end

When(/^user create game voucher transaction use bukadompet via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_gamevoucher_page = MobileGameVoucherPage.new
  @m_zynga_page = MobileZyngaPage.new
  @m_gamevoucher_new_page = MobileGameVoucherNew.new

  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER2_USERNAME'],ENV['BUYER2_PASSWORD'])

  @m_gamevoucher_page.load
  sleep 2
  @m_gamevoucher_page.publishers.first.click
  sleep 2

  page.execute_script("window.scrollBy(0,500)")
  sleep 5
  @m_zynga_page.beli_button.click
  sleep 3

  @m_gamevoucher_new_page.lanjut_button.click
  @m_gamevoucher_new_page.bukadompet_radio.click
  sleep 2
end

When(/^user create game voucher transaction using bukadompet with wrong password via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_gamevoucher_page = MobileGameVoucherPage.new
  @m_zynga_page = MobileZyngaPage.new
  @m_gamevoucher_new_page = MobileGameVoucherNew.new

  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @m_gamevoucher_page.load
  sleep 2
  @m_gamevoucher_page.publishers.first.click
  sleep 2

  page.execute_script("window.scrollBy(0,500)")
  sleep 5
  @m_zynga_page.beli_button.click
  sleep 2

  @m_gamevoucher_new_page.lanjut_button.click
  sleep 2
end

But /^the game voucher user in mweb fill wrong password$/ do
  @m_gamevoucher_new_page = MobileGameVoucherNew.new

  @m_gamevoucher_new_page.bukadompet_password_field.set "salahpassword"
  sleep 5
  @m_gamevoucher_new_page.lanjut_bukadompet_button.click
  find('.msg', :text => 'Verifikasi Password Gagal')
end
