#
# WIDGET WEB
#

When(/^user create dataplan transaction via home widget via "([^"]*)"$/) do |payment|
  @home_page = HomePage.new
  @phone_credit_new = PhoneCreditNew.new
  @confirmation_page = PurchaseConfirmationPage.new
  @invoice_detail_page = InvoiceDetailPage.new

  
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])
  sleep 3
  @home_page.buy_vp_home_widget(ENV['BUYER_PHONE_NUMBER'],"Paket Data",payment,ENV['BUYER_ELECTRICITY_NUMBER'])

  case payment 

  when "Transfer"
    
    @phone_credit_new.atm_payment_channel
    
    @unique_code = @confirmation_page.unique_code_text.text
    
    @confirmation_page.ignore_phonenumber_confirmation(payment)
    
  end

  sleep 3
end

When(/^user create dataplan transaction via Bukadompet widget via "([^"]*)"$/) do |payment|
  @home_page = HomePage.new
  @dompet_page = BukaDompet.new
  @phone_credit_new = PhoneCreditNew.new
  @invoice_detail_page = InvoiceDetailPage.new
  @confirmation_page = PurchaseConfirmationPage.new

  #@vp_new_page = get_new_vp_page_object(translate_vp_vocabs(vp))
  
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])
  sleep 3

  @dompet_page.load
  @dompet_page.buy_vp_bukadompet_widget(ENV['BUYER_PHONE_NUMBER'],"Paket Data",payment,ENV['BUYER_ELECTRICITY_NUMBER'])
  sleep 3

  case payment 

  when "Transfer"
    
    @phone_credit_new.atm_payment_channel

    @unique_code = @confirmation_page.unique_code_text.text
    
    @confirmation_page.ignore_phonenumber_confirmation(payment)

  end

  sleep 3
end


When(/^user create dataplan transaction via review widget via "([^"]*)"$/) do |payment|
  @home_page = HomePage.new
  @dataplan_page = VPDataPlanLandingPage.new
  @dataplan_datanew = DataNewPage.new
  @confirmation_page = PurchaseConfirmationPage.new
  
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @dataplan_page.load
  page.execute_script("window.scrollBy(0,400)")
  @dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 5
  @dataplan_page.beli_button.click
  
  sleep 5
  
  @dataplan_datanew.bukadompet_payment_channel   
  sleep 3

  @confirmation_page.wait_for_tab_phone_credit
  @confirmation_page.tab_dataplan.click
  @confirmation_page.phone_number_field.set(ENV['BUYER_PHONE_NUMBER'])
  sleep 2
  @confirmation_page.buy_button_dataplan.click
  sleep 3
  
  case payment

  when "Transfer"  
    @dataplan_datanew.atm_payment_channel
    @payment_channel = PaymentChannel.new
    sleep 2
    @unique_code = @confirmation_page.unique_code_text.text
    puts "Expected Unique Code: " + @unique_code
    @confirmation_page.display_invoice_button.click
    sleep 3
  end
end

#
# DESKTOP WEB
#

When(/^user create data plan transaction using "([^"]*)" as payment method$/) do |payment_method|
  @home_page = HomePage.new
  @dataplan_page = VPDataPlanLandingPage.new
  @dataplan_datanew = DataNewPage.new  
  @dataplan_confirmation = PurchaseConfirmationPage.new

  @home_page.load
  
  case payment_method
  
  when "Alfamart"
    @home_page.dismiss_one_signal_popup_homepage
    @home_page.login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

    @dataplan_page.load
    page.execute_script("window.scrollBy(0,400)")
    @dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
    @dataplan_page.wait_for_beli_button  
    @dataplan_page.beli_button.click  
  
    @dataplan_datanew.alfamart_payment_channel
    @dataplan_confirmation.display_invoice_button.click
    sleep 3
  
  when "Indomaret"
    @home_page.dismiss_one_signal_popup_homepage
    @home_page.login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

    @dataplan_page.load
    page.execute_script("window.scrollBy(0,400)")
    @dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
    sleep 5
    @dataplan_page.beli_button.click
  
    sleep 5
  
    @dataplan_datanew.indomaret_payment_channel
    @dataplan_confirmation.display_invoice_button.click
    sleep 3
  
  when "Transfer"
    @home_page.dismiss_one_signal_popup_homepage
    @home_page.login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

    @dataplan_page.load
    page.execute_script("window.scrollBy(0,400)")
    @dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
    sleep 5
    @dataplan_page.beli_button.click
  
    sleep 5
  
    @dataplan_datanew.atm_payment_channel
    @payment_channel = PaymentChannel.new
    sleep 2
    @unique_code = @dataplan_confirmation.unique_code_text.text
    puts "Expected Unique Code: " + @unique_code
    @dataplan_confirmation.display_invoice_button.click
    sleep 3

  when "BukaDompet"
    @home_page.dismiss_one_signal_popup_homepage
    @home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

    @dataplan_page.load
    page.execute_script("window.scrollBy(0,400)")
    @dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
    sleep 5
    @dataplan_page.beli_button.click
  
    sleep 5
  
    @dataplan_datanew.bukadompet_payment_channel   
    @dataplan_confirmation.display_invoice_button.click
    sleep 3

  when "BCA KlikPay (KlikBCA Individu)"
    @home_page.dismiss_one_signal_popup_homepage
    @home_page.login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

    @dataplan_page.load
    page.execute_script("window.scrollBy(0,400)")
    @dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
    sleep 5
    @dataplan_page.beli_button.click
  
    sleep 5
  
    @dataplan_datanew.klikpay_payment_channel
    find_button('bayar').click
    sleep 3        
    
  end  
end

When(/^user create data plan transaction via bukadompet$/) do
  @home_page = HomePage.new
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

  @dataplan_page = VPDataPlanLandingPage.new
  @dataplan_page.load
  page.execute_script("window.scrollBy(0,400)")
  @dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 2
  @dataplan_page.beli_button.click
  sleep 3
  
  @dataplan_datanew = DataNewPage.new
  @dataplan_datanew.bukadompet_radio.click
  sleep 5
end

And /^user has no enough deposit in bukadompet to continue to buy the E-voucher$/ do
  expect(@dataplan_datanew).to have_wording_bukadompet_no_balance
end


When(/^user create data plan transaction using bukadompet with wrong password$/) do
  @home_page = HomePage.new
  @dataplan_page = VPDataPlanLandingPage.new
  @dataplan_datanew = DataNewPage.new

  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @dataplan_page.load
  page.execute_script("window.scrollBy(0,400)")
  @dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 5
  @dataplan_page.beli_button.click
  
  sleep 5

  @dataplan_datanew.bukadompet_radio.click
  sleep 5

  @payment_page_url = URI.parse(current_url)

  @dataplan_datanew.bukadompet_password_field.set "salah password"
  @dataplan_datanew.button_pay.click
  sleep 3

  expect(page).to have_content('Verifikasi Password Gagal')
end


Then(/^invoice status data plan should be paid$/) do
  within('.memo-column.memo-left.clearfix') do
    expect(page).to have_content('Dibayar')
  end
end

#
# Mobile Web
#

When(/^user create dataplan transaction using alfamart as payment method via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_dataplan_page = MobilePaketDataPage.new
  @m_dataplan_new_page = MobileDataNewPage.new
  @m_purchases_page = MobilePurchasesPage.new


  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

  @m_dataplan_page.load
  @m_dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 3
  @m_dataplan_page.beli_button.click
  sleep 2

  @m_dataplan_new_page.lanjut_button.click
  @m_dataplan_new_page.alfamart_radio.click
  sleep 5
  @m_dataplan_new_page.lanjut_alfamart_button.click
  sleep 5
  
  @invoice_number = @m_purchases_page.invoice_number.text
  sleep 1
  @m_purchases_page.lihat_tagihan_pembayaran_link.click
  
end

When(/^user create dataplan transaction using transfer via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

  @m_dataplan_page = MobilePaketDataPage.new
  @m_dataplan_page.load
  @m_dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 3
  @m_dataplan_page.beli_button.click
  sleep 2

  @m_dataplan_new_page = MobileDataNewPage.new
  @m_dataplan_new_page.lanjut_button.click
  @m_dataplan_new_page.transfer_radio.click
  sleep 5
  @m_dataplan_new_page.lanjut_transfer_button.click
  sleep 5
  
  @m_purchases_page = MobilePurchasesPage.new
  @invoice_number = @m_purchases_page.invoice_number.text
  sleep 1
  @m_purchases_page.lihat_tagihan_pembayaran_link.click
end

Then(/^dataplan invoice status in mweb should be in wating$/) do
  expect(page).to have_content('Menunggu Pembayaran')
end

When(/^user create dataplan transaction using bukadompet via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_dataplan_page = MobilePaketDataPage.new
  @m_dataplan_new_page = MobileDataNewPage.new

  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @m_dataplan_page.load
  @m_dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 3
  @m_dataplan_page.beli_button.click
  sleep 2

  @m_dataplan_new_page.lanjut_button.click
  @m_dataplan_new_page.bukadompet_radio.click
  sleep 5
  @m_dataplan_new_page.lanjut_bukadompet_button.click
  sleep 2
end

When(/^user has deposit in bukadompet and enough to buy the dataplan via mweb$/) do
  @m_pulsa_new_page = MobileNewPage.new
  @m_pulsa_new_page.bukadompet_password_field.set ENV['BUYER_PASSWORD']
  sleep 3
  @m_pulsa_new_page.lanjut_bukadompet_button.click
  sleep 3

  @m_purchases_page = MobilePurchasesPage.new
  sleep 2
  @invoice_number = @m_purchases_page.invoice_number.text
  sleep 2
  @m_purchases_page.lihat_tagihan_pembayaran_link.click
end

When(/^user create dataplan transaction use bukadompet via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_dataplan_page = MobilePaketDataPage.new
  @m_dataplan_new_page = MobileDataNewPage.new

  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])

  @m_dataplan_page.load
  @m_dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  @m_dataplan_page.wait_for_beli_button
  @m_dataplan_page.beli_button.click
  
  @m_dataplan_new_page.wait_for_lanjut_button
  sleep 2
  @m_dataplan_new_page.lanjut_button.click
  page.execute_script("window.scrollBy(0,-400)")  
  sleep 2
  page.execute_script("window.scrollBy(0,-400)")  
  @m_dataplan_new_page.bukadompet_radio.click
  sleep 2
end

When(/^user create dataplan transaction using bukadompet with wrong password via mweb$/) do

  @m_home_page = MobileHomePage.new
  @m_dataplan_page = MobilePaketDataPage.new
  @m_dataplan_new_page = MobileDataNewPage.new

  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @m_dataplan_page.load
  @m_dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 3
  @m_dataplan_page.beli_button.click
  sleep 2

  @m_dataplan_new_page.lanjut_button.click  
  sleep 2
end

When(/^user create dataplan transaction using indomaret as payment method via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER2_USERNAME'],ENV['BUYER2_PASSWORD'])

  @m_dataplan_page = MobilePaketDataPage.new
  @m_dataplan_page.load
  @m_dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 3
  @m_dataplan_page.beli_button.click
  sleep 2

  @m_dataplan_new_page = MobileDataNewPage.new
  @m_dataplan_new_page.lanjut_button.click
  @m_dataplan_new_page.indomaret_radio.click
  sleep 5
  @m_dataplan_new_page.lanjut_alfamart_button.click
  sleep 5
  
  @m_purchases_page = MobilePurchasesPage.new
  @invoice_number = @m_purchases_page.invoice_number.text
  sleep 1
  @m_purchases_page.lihat_tagihan_pembayaran_link.click
end

When(/^user has no enough deposit in bukadompet to continue to buy the dataplan via mweb$/) do
  @m_dataplan_new_page.wait_for_class_insufficent_balance
  expect(@m_dataplan_new_page).to have_class_insufficent_balance 
end