#
# WIDGET WEB
#

When(/^user create electricity transaction via home widget via "([^"]*)"$/) do |payment|
  @home_page = HomePage.new
  @phone_credit_new = PhoneCreditNew.new
  @confirmation_page = PurchaseConfirmationPage.new
  @invoice_detail_page = InvoiceDetailPage.new

  
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])
  sleep 3
  @home_page.buy_vp_home_widget(ENV['BUYER_PHONE_NUMBER'],"Token Listrik",payment,ENV['BUYER_ELECTRICITY_NUMBER'])

  case payment 

  when "Transfer"
    
    @phone_credit_new.atm_payment_channel

    @unique_code = @confirmation_page.unique_code_text.text

    @confirmation_page.ignore_phonenumber_confirmation(payment)

  end

  sleep 3
end

When(/^user create electricity transaction via Bukadompet widget via "([^"]*)"$/) do |payment|
  @home_page = HomePage.new
  @dompet_page = BukaDompet.new
  @phone_credit_new = PhoneCreditNew.new
  @invoice_detail_page = InvoiceDetailPage.new
  @confirmation_page = PurchaseConfirmationPage.new

  #@vp_new_page = get_new_vp_page_object(translate_vp_vocabs(vp))
  
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER3_USERNAME'],ENV['BUYER3_PASSWORD'])
  sleep 3

  @dompet_page.load
  @dompet_page.buy_vp_bukadompet_widget(ENV['BUYER_PHONE_NUMBER'],"Token Listrik",payment,ENV['BUYER_ELECTRICITY_NUMBER'])
  sleep 3

  case payment 

  when "Transfer"
    
    @phone_credit_new.atm_payment_channel

    @unique_code = @confirmation_page.unique_code_text.text

    @confirmation_page.ignore_phonenumber_confirmation(payment)

  end

  sleep 3
end


When(/^user create electricity transaction via review widget via "([^"]*)"$/) do |payment|
  @home_page = HomePage.new
  @dataplan_page = VPDataPlanLandingPage.new
  @dataplan_datanew = DataNewPage.new
  @confirmation_page = PurchaseConfirmationPage.new
  
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @dataplan_page.load
  page.execute_script("window.scrollBy(0,400)")
  @dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 5
  @dataplan_page.beli_button.click
  
  sleep 5
  
  @dataplan_datanew.bukadompet_payment_channel   
  sleep 3

  @confirmation_page.wait_for_tab_phone_credit
  @confirmation_page.tab_electricity.click
  @confirmation_page.customer_number_field.set(ENV['BUYER_ELECTRICITY_NUMBER'])
  sleep 2
  @confirmation_page.buy_button_electricity.click
  sleep 3
  
  case payment

  when "Transfer"  
    @dataplan_datanew.atm_payment_channel
    @payment_channel = PaymentChannel.new
    sleep 2
    @unique_code = @confirmation_page.unique_code_text.text
    puts "Expected Unique Code: " + @unique_code
    @confirmation_page.display_invoice_button.click
    sleep 3
  end
end

#
# MOBILE WEB
#

When(/^user create electricity transaction using alfamart as payment method via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_electricity_page = MobileElectricityPage.new
  @m_electricity_new = MobileElectricityNew.new
  @m_purchases_page = MobilePurchasesPage.new


  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @m_electricity_page.load
  @m_electricity_page.meter_number_field.set ENV['BUYER_ELECTRICITY_NUMBER']
  sleep 3
  @m_electricity_page.beli_button.click
  sleep 5

  @m_electricity_new.lanjut_button.click
  @m_electricity_new.alfamart_radio.click
  sleep 2
  @m_electricity_new.lanjut_payment_button.click
  sleep 5

  @invoice_number = @m_purchases_page.invoice_number.text
  sleep 1
  @m_purchases_page.lihat_tagihan_pembayaran_link.click
end

When(/^user create electricity transaction using transfer via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_electricity_page = MobileElectricityPage.new
  @m_electricity_new = MobileElectricityNew.new
  @m_purchases_page = MobilePurchasesPage.new

  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @m_electricity_page.load
  @m_electricity_page.meter_number_field.set ENV['BUYER_ELECTRICITY_NUMBER']
  sleep 3
  @m_electricity_page.beli_button.click
  sleep 2

  @m_electricity_new.lanjut_button.click
  @m_electricity_new.transfer_radio.click
  sleep 2
  @m_electricity_new.lanjut_payment_button.click
  sleep 5

  @invoice_number = @m_purchases_page.invoice_number.text
  sleep 1
  @m_purchases_page.lihat_tagihan_pembayaran_link.click
end

Then(/^electricitys invoice status in mweb should be in wating$/) do
  expect(page).to have_content('Menunggu Pembayaran')
end

When(/^user create electricitys transaction using bukadompet via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_electricity_page = MobileElectricityPage.new
  @m_electricity_new = MobileElectricityNew.new
  @m_purchases_page = MobilePurchasesPage.new

  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @m_electricity_page.load
  @m_electricity_page.meter_number_field.set ENV['BUYER_ELECTRICITY_NUMBER']
  sleep 3
  @m_electricity_page.beli_button.click
  sleep 2

  @m_electricity_new.lanjut_button.click
  @m_electricity_new.bukadompet_radio.click
  sleep 2
end

When(/^user has deposit in bukadompet and enough to buy the electricity via mweb$/) do
  @m_electricity_new_page = MobileElectricityNew.new
  @m_purchases_page = MobilePurchasesPage.new
  
  @m_electricity_new_page.bukadompet_password_field.set ENV['BUYER_PASSWORD']
  sleep 5
  @m_electricity_new_page.lanjut_bukadompet_button.click
  sleep 3

  @invoice_number = @m_purchases_page.invoice_number.text
  sleep 1
  @m_purchases_page.lihat_tagihan_pembayaran_link.click
end

When(/^user create electricity transaction use bukadompet via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_electricity_page = MobileElectricityPage.new
  @m_electricity_new = MobileElectricityNew.new

  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER2_USERNAME'],ENV['BUYER2_PASSWORD'])

  @m_electricity_page.load
  @m_electricity_page.meter_number_field.set ENV['BUYER_ELECTRICITY_NUMBER']
  sleep 3
  @m_electricity_page.beli_button.click
  sleep 2

  @m_electricity_new.lanjut_button.click
  @m_electricity_new.bukadompet_radio.click

  sleep 2
end

When(/^user create electricity transaction using bukadompet with wrong password via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_electricity_page = MobileElectricityPage.new
  @m_electricity_new = MobileElectricityNew.new
  @m_purchases_page = MobilePurchasesPage.new


  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @m_electricity_page.load
  @m_electricity_page.meter_number_field.set ENV['BUYER_ELECTRICITY_NUMBER']
  sleep 3
  @m_electricity_page.beli_button.click
  sleep 5

  @m_electricity_new.lanjut_button.click
  sleep 2
end

When(/^user create electricity transaction using indomaret as payment method via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_electricity_page = MobileElectricityPage.new
  @m_electricity_new = MobileElectricityNew.new
  @m_purchases_page = MobilePurchasesPage.new


  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @m_electricity_page.load
  @m_electricity_page.meter_number_field.set ENV['BUYER_ELECTRICITY_NUMBER']
  sleep 3
  @m_electricity_page.beli_button.click
  sleep 5

  @m_electricity_new.lanjut_button.click
  @m_electricity_new.indomaret_radio.click
  sleep 2
  @m_electricity_new.lanjut_payment_button.click
  sleep 5

  @invoice_number = @m_purchases_page.invoice_number.text
  sleep 1
  @m_purchases_page.lihat_tagihan_pembayaran_link.click

end

But /^the electricity user in mweb fill wrong password$/ do
  @m_pulsa_new_page = MobileElectricityNew.new
  @m_pulsa_new_page.bukadompet_password_field.set "salahpassword"
  sleep 5
  @m_pulsa_new_page.lanjut_bukadompet_button.click
  find('.msg', :text => 'Verifikasi Password Gagal')
end

#
# DESKTOP WEB
#

When(/^user create electricity transaction using "([^"]*)" as payment method$/) do |payment_method|
  @home_page           = HomePage.new
  @electricity_page    = VPElectricityLandingPage.new
  @electricity_new     = ElectricityNew.new 
  @confirmation_page   = PurchaseConfirmationPage.new
  @invoice_detail_page = ElectricityInvoiceDetail.new
  
  case payment_method

  when "Alfamart"
    buy_electricity(username: ENV['BUYER4_USERNAME'],password: ENV['BUYER4_PASSWORD'])
    @electricity_new.alfamart_payment_channel
    @confirmation_page.ignore_phonenumber_confirmation(payment_method)
  
  when "Transfer"
    buy_electricity(username: ENV['BUYER4_USERNAME'],password: ENV['BUYER4_PASSWORD'])
    @electricity_new.atm_payment_channel

    @unique_code = @confirmation_page.unique_code_text.text

    @confirmation_page.ignore_phonenumber_confirmation(payment_method)

  when "Indomaret"

    buy_electricity(username: ENV['BUYER4_USERNAME'],password: ENV['BUYER4_PASSWORD'])
    @electricity_new.indomaret_payment_channel

    @confirmation_page.ignore_phonenumber_confirmation(payment_method)   
        
  when "BukaDompet"
    buy_electricity(username: ENV['BUYER_USERNAME'],password: ENV['BUYER_PASSWORD'])
    @electricity_new.bukadompet_payment_channel

    @confirmation_page.ignore_phonenumber_confirmation(payment_method)

  when "BCA KlikPay"
    @ebanking = EbankingRedirectPage.new
    
    buy_electricity(username: ENV['BUYER4_USERNAME'],password: ENV['BUYER4_PASSWORD'])
    @electricity_new.klikpay_payment_channel
    
    #@ebanking.ebanking_page_button.click
    find_button('bayar').click
    sleep 3

  end
end

When(/^user create electricity transaction using BukaDompet with insufficient deposit$/) do
  @home_page         = HomePage.new
  @electricity_page  = VPElectricityLandingPage.new
  @electricity_new   = ElectricityNew.new 
  
  buy_electricity(username: ENV['BUYER4_USERNAME'], password: ENV['BUYER4_PASSWORD'])
  @electricity_new.deposit_list_item.click
  sleep 2
end

When(/^user create electricity transaction using bukadompet with wrong password$/) do
  @home_page         = HomePage.new
  @electricity_page  = VPElectricityLandingPage.new
  @electricity_new   = ElectricityNew.new 

  buy_electricity(username: ENV['BUYER_USERNAME'], password: ENV['BUYER_PASSWORD'])

  @electricity_new.wait_for_deposit_list_item
  @electricity_new.deposit_list_item.click
  @electricity_new.deposit_password_field.set "salah password"
  @electricity_new.button_pay.click
  
end

Then(/^electricity invoice status should be in wating$/) do
  @invoice_detail_page.vp_invoice_should_be_in_waiting
end

Then(/^user cannot create electricity transaction using BukaDompet$/) do
  wording_has_no_balance(@electricity_new)
end

Then(/^user will be displayed electricity transaction message "([^"]*)"$/) do |msg|
  expect(@electricity_new.deposit_error_notification.text).to eq(msg)
end
