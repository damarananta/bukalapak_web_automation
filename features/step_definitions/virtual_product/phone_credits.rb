#
# WIDGET WEB
#

When(/^user create phonecredit transaction via home widget via "([^"]*)"$/) do |payment|
  @home_page = HomePage.new
  @phone_credit_new = PhoneCreditNew.new
  @confirmation_page = PurchaseConfirmationPage.new
  @invoice_detail_page = InvoiceDetailPage.new

  
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])
  @home_page.buy_vp_home_widget(ENV['BUYER_PHONE_NUMBER'],"Pulsa",payment,ENV['BUYER_ELECTRICITY_NUMBER'])

  case payment 

  when "Transfer"
    
    @phone_credit_new.atm_payment_channel
    
    @unique_code = @confirmation_page.unique_code_text.text

    @confirmation_page.ignore_phonenumber_confirmation(payment)

  end

  sleep 3
end

When(/^user create phonecredit transaction via Bukadompet widget via "([^"]*)"$/) do |payment|
  @home_page = HomePage.new
  @dompet_page = BukaDompet.new
  @phone_credit_new = PhoneCreditNew.new
  @invoice_detail_page = InvoiceDetailPage.new
  @confirmation_page = PurchaseConfirmationPage.new

  #@vp_new_page = get_new_vp_page_object(translate_vp_vocabs(vp))
  
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])
  sleep 3

  @dompet_page.load
  @dompet_page.buy_vp_bukadompet_widget(ENV['BUYER_PHONE_NUMBER'],"Pulsa",payment,ENV['BUYER_ELECTRICITY_NUMBER'])
  sleep 3

  case payment 

  when "Transfer"
    
    @phone_credit_new.atm_payment_channel
    @unique_code = @confirmation_page.unique_code_text.text
    @confirmation_page.ignore_phonenumber_confirmation(payment)

  end

  sleep 3
end


When(/^user create phonecredit transaction via review widget via "([^"]*)"$/) do |payment|
  @home_page = HomePage.new
  @dataplan_page = VPDataPlanLandingPage.new
  @dataplan_datanew = DataNewPage.new
  @confirmation_page = PurchaseConfirmationPage.new
  
  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @dataplan_page.load
  page.execute_script("window.scrollBy(0,400)")
  @dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 5
  @dataplan_page.beli_button.click
  
  sleep 5
  
  @dataplan_datanew.bukadompet_payment_channel   
  sleep 3

  @confirmation_page.wait_for_tab_phone_credit
  @confirmation_page.phone_number_field.set(ENV['BUYER_PHONE_NUMBER'])
  sleep 2
  @confirmation_page.buy_button_phonecredit.click
  sleep 3
  
  case payment

  when "Transfer"  
    @dataplan_datanew.atm_payment_channel
    @payment_channel = PaymentChannel.new
    sleep 2
    @unique_code = @confirmation_page.unique_code_text.text
    puts "Expected Unique Code: " + @unique_code
    @confirmation_page.display_invoice_button.click
    sleep 3
  end
end

#
# DESKTOP WEB
#

When(/^user create phone credit transaction using "([^"]*)" as payment method$/) do |payment_method|
  @home_page           = HomePage.new
  @phone_credit_page   = VPPhoneCreditLandingPage.new
  @phone_credit_new    = PhoneCreditNew.new
  @confirmation_page   = PurchaseConfirmationPage.new
  @invoice_detail_page = InvoiceDetailPage.new

  case payment_method
  
  when "Alfamart"
    buy_phone_credits(username: ENV['BUYER2_USERNAME'],password: ENV['BUYER2_PASSWORD'])
    @phone_credit_new.alfamart_payment_channel

    @confirmation_page.ignore_phonenumber_confirmation(payment_method)

  when "Transfer"
    buy_phone_credits(username: ENV['BUYER2_USERNAME'],password: ENV['BUYER2_PASSWORD'])
    @phone_credit_new.atm_payment_channel

    neutralize_ab_test('ab_test[btp_checkout_reskin]=reskinned')
    @unique_code = @confirmation_page.unique_code_text.text
    
    if page.has_css?('#cboxWrapper')
      @confirmation_page.bukan_button.click
      sleep 2
      @confirmation_page.lewati_proses_link.click
    else
      @confirmation_page.display_invoice_button.click
    end

    #@confirmation_page.ignore_phonenumber_confirmation(payment_method)

  when "Indomaret"
    buy_phone_credits(username: ENV['BUYER2_USERNAME'],password: ENV['BUYER2_PASSWORD'])
    @phone_credit_new.indomaret_payment_channel

    @confirmation_page.ignore_phonenumber_confirmation(payment_method)    

  when "BukaDompet"
    buy_phone_credits(username: ENV['BUYER_USERNAME'],password: ENV['BUYER_PASSWORD'])
    @phone_credit_new.bukadompet_payment_channel(ENV['BUYER_PASSWORD'])

    @confirmation_page.ignore_phonenumber_confirmation(payment_method)

  when "BCA KlikPay (KlikBCA Individu)"
    @ebanking = EbankingRedirectPage.new
    
    buy_phone_credits(username: ENV['BUYER2_USERNAME'],password: ENV['BUYER2_PASSWORD'])
    @phone_credit_new.klikpay_payment_channel
    
    @ebanking.ebanking_page_button.click
    find_button('bayar').click
    sleep 3

  end

end

When(/^user create phone credit transaction using BukaDompet with insufficient deposit$/) do
  @home_page         = HomePage.new
  @phone_credit_page = VPPhoneCreditLandingPage.new
  @phone_credit_new  = PhoneCreditNew.new

  buy_phone_credits(username: ENV['BUYER2_USERNAME'], password: ENV['BUYER2_PASSWORD'])
  @phone_credit_new.deposit_list_item.click
  sleep 2    

end

When(/^user create phone credit transaction using bukadompet with wrong password$/) do
  @home_page         = HomePage.new
  @phone_credit_page = VPPhoneCreditLandingPage.new
  @phone_credit_new  = PhoneCreditNew.new

  buy_phone_credits(username: ENV['BUYER_USERNAME'], password: ENV['BUYER_PASSWORD'])
  @phone_credit_new.bukadompet_payment_channel("salah password")
end

When(/^user create phone credit transaction using Credits card$/) do
  buy_phone_credits(ENV['BUYER_PHONE_NUMBER'],ENV['BUYER2_USERNAME'],ENV['BUYER2_PASSWORD'])
  choose('Kartu Visa/Mastercard')
  payment_choosen_pay

  fill_in 'cc_number_input', :with => '000000000000'
  fill_in 'cc_cvv', :with => '001'
  @payment_page_url = URI.parse(current_url)

  find('.btn.btn--large.btn--red').click
end

When(/^user create phone credit transaction using klikpay BCA$/) do
  buy_phone_credits(ENV['BUYER_PHONE_NUMBER'],ENV['BUYER2_USERNAME'],ENV['BUYER2_PASSWORD'])
  choose('BCA KlikPay (KlikBCA Individu)')
  payment_choosen_pay
end

When(/^user has deposit in Klikpay BCA account and enough to buy the phone credit$/) do
  find_button('bayar').click
  sleep 3
end

Then(/^invoice status phone credit should be paid$/) do
  within('.memo-column.memo-left.clearfix') do
    expect(page).to have_content('Dibayar')
  end
end

Then(/^phone credit invoice status should be in wating$/) do
  @invoice_detail_page = InvoiceDetailPage.new
  @invoice_detail_page.wait_for_payment_status_text
  @invoice_detail_page.vp_invoice_should_be_in_waiting
end

Then(/^the phone credit can not be paid$/) do
  wording_has_no_balance
  expect(page).to have_no_content('BERHASIL!')
end

When(/^user has no enough deposit in bukadompet to continue to buy the phonecredit$/) do
  @phone_credit_new.wait_for_insufficient_deposit_section
  expect(@phone_credit_new).to have_insufficient_deposit_section 
end


Then(/^user will be displayed phone credit transaction message "([^"]*)"$/) do |msg|
  expect(@phone_credit_new.deposit_error_notification.text).to eq(msg)
end

#
# MOBILE WEB
#

When(/^user create phone credit transaction using alfamart as payment method via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER2_USERNAME'],ENV['BUYER2_PASSWORD'])

  @m_pulsa_page = MobilePulsaPage.new
  @m_pulsa_page.load
  @m_pulsa_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 3
  @m_pulsa_page.beli_button.click

  @m_pulsa_new_page = MobileNewPage.new
  @m_pulsa_new_page.lanjut_button.click
  sleep 3
  @m_pulsa_new_page.alfamart_radio.click
  @m_pulsa_new_page.lanjut2_button.click
  sleep 5

  @m_purchases_page = MobilePurchasesPage.new
  @invoice_number = @m_purchases_page.invoice_number.text
  sleep 1
  @m_purchases_page.lihat_tagihan_pembayaran_link.click
end

Then(/^invoice number can be find at mweb list transaction$/) do
  @m_invoices_page = MobileInvoicesPage.new
  @m_invoices_page.load
  sleep 2
  find('#keywords').native.send_keys(@invoice_number,:enter)

  @m_invoices_page.trx_result.click
  sleep 3

  expect(page).to have_content(@invoice_number)
end

When(/^user create phone credit transaction using transfer via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER2_USERNAME'],ENV['BUYER2_PASSWORD'])

  @m_pulsa_page = MobilePulsaPage.new
  @m_pulsa_page.load
  @m_pulsa_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 3
  @m_pulsa_page.beli_button.click

  @m_pulsa_new_page = MobileNewPage.new
  @m_pulsa_new_page.lanjut_button.click
  sleep 3
  @m_pulsa_new_page.transfer_radio.click
  @m_pulsa_new_page.lanjut2_button.click
  sleep 5

  @m_purchases_page = MobilePurchasesPage.new
  @invoice_number = @m_purchases_page.invoice_number.text
  sleep 1
  @m_purchases_page.lihat_tagihan_pembayaran_link.click
end


When(/^user create phone credits transaction using bukadompet via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @m_pulsa_page = MobilePulsaPage.new
  @m_pulsa_page.load
  @m_pulsa_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 3
  @m_pulsa_page.beli_button.click

  @m_pulsa_new_page = MobileNewPage.new
  @m_pulsa_new_page.lanjut_button.click
  sleep 3
  @m_pulsa_new_page.bukadompet_radio.click
  sleep 2
end

When(/^user has deposit in bukadompet and enough to buy the phonecredit via mweb$/) do
  @m_pulsa_new_page = MobileNewPage.new
  @m_pulsa_new_page.bukadompet_password_field.set ENV['BUYER_PASSWORD']
  sleep 3
  @m_pulsa_new_page.lanjut_bukadompet_button.click
  sleep 3

  @m_purchases_page = MobilePurchasesPage.new
  @invoice_number = @m_purchases_page.invoice_number.text
  sleep 1
  @m_purchases_page.lihat_tagihan_pembayaran_link.click
end

When(/^user create phone credit transaction use bukadompet via mweb$/) do
  @m_home_page      = MobileHomePage.new
  @m_pulsa_page     = MobilePulsaPage.new
  @m_pulsa_new_page = MobileNewPage.new

  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER2_USERNAME'],ENV['BUYER2_PASSWORD'])

  @m_pulsa_page.load
  @m_pulsa_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  @m_pulsa_page.wait_for_beli_button
  @m_pulsa_page.beli_button.click

  
  @m_pulsa_new_page.wait_for_lanjut_button
  sleep 2
  @m_pulsa_new_page.lanjut_button.click
  page.execute_script("window.scrollBy(0,-400)")  
  sleep 2
  page.execute_script("window.scrollBy(0,-400)")  
  @m_pulsa_new_page.bukadompet_radio.click
  sleep 2
end

When(/^user create phone credit transaction using bukadompet with wrong password via mweb$/) do
  @m_home_page = MobileHomePage.new
  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  @m_pulsa_page = MobilePulsaPage.new
  @m_pulsa_page.load
  @m_pulsa_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 3
  @m_pulsa_page.beli_button.click
  click_button('Lanjut')
  sleep 2
end

But /^the mweb user fill wrong password$/ do
  @m_pulsa_new_page = MobileNewPage.new
  @m_pulsa_new_page.bukadompet_password_field.set "salahpassword"
  sleep 5
  @m_pulsa_new_page.lanjut_bukadompet_button.click
  find('.msg', :text => 'Verifikasi Password Gagal')
end

When(/^user create phone credit transaction using indomaret as payment method via mweb$/) do
  @m_home_page      = MobileHomePage.new
  @m_pulsa_page     = MobilePulsaPage.new
  @m_pulsa_new_page = MobileNewPage.new
  @m_purchases_page = MobilePurchasesPage.new

  @m_home_page.load
  @m_home_page.dismiss_search_product_onboarding
  @m_home_page.login(ENV['BUYER2_USERNAME'],ENV['BUYER2_PASSWORD'])

  @m_pulsa_page.load
  @m_pulsa_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
  sleep 3
  @m_pulsa_page.beli_button.click
  sleep 2

  @m_pulsa_new_page.lanjut_button.click
  sleep 3
  @m_pulsa_new_page.indomaret_radio.click
  @m_pulsa_new_page.lanjut_indomaret_button.click
  sleep 5

  @invoice_number = @m_purchases_page.invoice_number.text
  sleep 1
  @m_purchases_page.lihat_tagihan_pembayaran_link.click
end

When(/^user create phone credit transaction using klikpay BCA via mweb$/) do
  mweb_buy_phone_credits(ENV['BUYER_PHONE_NUMBER'],ENV['BUYER2_USERNAME'],ENV['BUYER2_PASSWORD'])
  click_button('Lanjut')
  #click_hidden('.logo-payment.logo-payment__bca-klikpay')
  find('.logo-payment.logo-payment__bca-klikpay').click
  sleep 2
  click_button('Lanjut')
  #click_button('Lanjut')
  sleep 5
end

Then(/^trx number can be find at mweb list transaction$/) do

  page.evaluate_script("window.location.reload()")

  sleep 5
  within('.tab.tab-inline')do
    find("li", :text => "Transaksi").click
  end

  within('.flag__head')do
    @no_trx=find('a').text
  end

  sleep 1

  visit'/payment/invoices'
  sleep 2

  close_cart_pop_up
  sleep 1

  find('#keywords').native.send_keys(@no_trx,:enter)

  click_link('Tindakan')

  sleep 2
  click_link('Detail')
  sleep 1

  within('.tab.tab-inline')do
    find("li", :text => "Transaksi").click
  end

  within('.flag__head')do
    find(@no_trx).click
  end

  expect(page).to have_content(@no_trx)

end

When(/^user has no enough deposit in bukadompet to continue to buy the phonecredit via mweb$/) do
  expect(@m_pulsa_new_page).to have_class_insufficent_balance 
end
