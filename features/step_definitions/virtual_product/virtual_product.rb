When(/^user create "([^"]*)" transaction via home widget via "([^"]*)"$/) do |vp, payment|
  @home_page = HomePage.new
  @payment_channel = PaymentChannel.new

  case vp
  when "Paket Data"
    @dataplan_datanew = DataNewPage.new
    @dataplan_confirmation = PaymentPurchasesConfirmationPage.new
  when "Pulsa", "Token Listrik", "Voucher Game"
    @vp_new_page = get_new_vp_page_object(translate_vp_vocabs(vp))
    @confirmation_page = PurchaseConfirmationPage.new
  end

  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])
  sleep 3
  @home_page.buy_vp_home_widget(ENV['BUYER_PHONE_NUMBER'],vp,payment,ENV['BUYER_ELECTRICITY_NUMBER'])

  case vp
  when "Paket Data"
    @dataplan_datanew.atm_payment_channel
  when "Pulsa", "Token Listrik", "Voucher Game"
    @vp_new_page.pick_payment_method(payment.downcase)
    @vp_new_page.normal_payment_button.click
  end
  sleep 2

  case vp
  when "Paket Data"
    @unique_code = @dataplan_confirmation.unique_code.text
    puts "Expected Unique Code: " + @unique_code
    @dataplan_confirmation.lihat_tagihan_button.click
  when "Pulsa", "Token Listrik", "Voucher Game"
    if payment == "transfer"
      @unique_code = @confirmation_page.unique_code_text.text
    end
    unless payment == "bca klikpay"
      @confirmation_page.display_invoice_button.click
    end
  end
  sleep 3
end

When(/^user create "([^"]*)" transaction via Bukadompet widget via "([^"]*)"$/) do |vp, payment|
  @home_page = HomePage.new
  @dompet_page = BukaDompet.new
  @payment_channel = PaymentChannel.new
  @invoice_detail_page = InvoiceDetailPage.new

  case vp
  when "Paket Data"
    @dataplan_datanew = DataNewPage.new
    @dataplan_confirmation = PaymentPurchasesConfirmationPage.new
  when "Pulsa", "Token Listrik", "Voucher Game"
    @vp_new_page = get_new_vp_page_object(translate_vp_vocabs(vp))
    @confirmation_page = PurchaseConfirmationPage.new
  end

  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])
  sleep 3

  @dompet_page.load
  @dompet_page.buy_vp_bukadompet_widget(ENV['BUYER_PHONE_NUMBER'],vp,payment,ENV['BUYER_ELECTRICITY_NUMBER'])
  sleep 3

  case vp
  when "Paket Data"
    @dataplan_datanew.atm_payment_channel
  when "Pulsa", "Token Listrik", "Voucher Game"
    @vp_new_page.pick_payment_method(payment.downcase)
    @vp_new_page.normal_payment_button.click
  end
  sleep 2

  case vp
  when "Paket Data"
    @unique_code = @dataplan_confirmation.unique_code.text
    puts "Expected Unique Code: " + @unique_code
    @dataplan_confirmation.lihat_tagihan_button.click
  when "Pulsa", "Token Listrik", "Voucher Game"
    if payment == "transfer"
      @unique_code = @confirmation_page.unique_code_text.text
    end
    unless payment == "bca klikpay"
      @confirmation_page.display_invoice_button.click
    end
  end
  sleep 3
end

When(/^user create "([^"]*)" transaction via review widget via "([^"]*)"$/) do |vp, payment|
  @home_page = HomePage.new
  @dataplan_page = VPDataPlanLandingPage.new
  @dataplan_datanew = DataNewPage.new
  @dataplan_confirmation = PaymentPurchasesConfirmationPage.new
  @payment_channel = PaymentChannel.new
  @confirmation_page = PurchaseConfirmationPage.new

  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER_USERNAME'],ENV['BUYER_PASSWORD'])

  case vp
  when "Paket Data"
    @dataplan_page.load
    page.execute_script("window.scrollBy(0,400)")
    @dataplan_page.phone_number_field.set ENV['BUYER_PHONE_NUMBER']
    sleep 5
    @dataplan_page.beli_button.click
    sleep 5

    @dataplan_datanew.bukadompet_payment_channel
  when "Pulsa"
    @vp_new_page = get_new_vp_page_object(translate_vp_vocabs(vp))
    buy_phone_credits(with_login: false)
    choose_payment_method("bukadompet", "phone credits")
  when "Token Listrik"
    @vp_new_page = get_new_vp_page_object(translate_vp_vocabs(vp))
    buy_electricity(with_login: false)
    choose_payment_method("bukadompet", "electricity")
  when "Voucher Game"
    @vp_new_page = get_new_vp_page_object(translate_vp_vocabs(vp))
    buy_game_voucher(with_login: false)
    choose_payment_method("bukadompet", "game voucher")
  end

  no_meteran = ENV['BUYER_ELECTRICITY_NUMBER']
  no_hp = ENV['BUYER_PHONE_NUMBER']

  @confirmation_page.wait_for_tab_phone_credit
  case vp
  when "Pulsa"
    @confirmation_page.phone_number_field.set(no_hp)
    sleep 2
    @confirmation_page.buy_button_phonecredit.click
    sleep 3

    @vp_new_page.pick_payment_method(payment.downcase)
    @vp_new_page.normal_payment_button.click

    if payment == "transfer"
      @unique_code = @confirmation_page.unique_code_text.text
    end
    unless payment == "bca klikpay"
      @confirmation_page.display_invoice_button.click
    end
  when "Paket Data"
    @dataplan_confirmation.tab_dataplan.click
    @dataplan_confirmation.phone_number_field.set no_hp
    sleep 2
    @dataplan_confirmation.buy_button_dataplan.click
    sleep 3

    @dataplan_datanew.atm_payment_channel
    sleep 2

    @unique_code = @dataplan_confirmation.unique_code.text
    puts "Expected Unique Code: " + @unique_code
    @dataplan_confirmation.lihat_tagihan_button.click
  when "Token Listrik"
    @confirmation_page.tab_electricity.click
    @confirmation_page.customer_number_field.set(no_meteran)
    sleep 2
    @confirmation_page.buy_button_electricity.click

    @vp_new_page.pick_payment_method(payment.downcase)
    @vp_new_page.normal_payment_button.click

    if payment == "transfer"
      @unique_code = @confirmation_page.unique_code_text.text
    end
    unless payment == "bca klikpay"
      @confirmation_page.display_invoice_button.click
    end
  when "Voucher Game"
    @confirmation_page.tab_gamevoucher.click
    @confirmation_page.buy_button_gamevoucher.click

    @vp_new_page.pick_payment_method(payment.downcase)
    @vp_new_page.normal_payment_button.click

    if payment == "transfer"
      @unique_code = @confirmation_page.unique_code_text.text
    end
    unless payment == "bca klikpay"
      @confirmation_page.display_invoice_button.click
    end
  end
  sleep 3
end

Then(/^invoice number can be find at list transaction$/) do

  @invoice_page = InvoicesPage.new
  @invoice_number = @invoice_page.invoice_number.text
  invoice_number = @invoice_number

  #@list_trx = ListTransactionsPage.new
  sleep 3
  @invoice_page.load
  sleep 2

  @invoice_page.search_field.native.send_keys(@invoice_number, :enter)

  #find('#keywords').native.send_keys(@invoice_number,:enter)

  sleep 5
  #@invoice_page.invoice_list.first.click
  click_link(@invoice_number)
  sleep 5

  @invoice_detail_page = InvoiceDetailPage.new
  expect(@invoice_detail_page.invoice_number_text).to have_content(invoice_number)
end

Then /^E-voucher invoice status should be in wating$/ do
  within('.transaction-memo-box.clearfix') do
    expect(page).to have_content('Menunggu Pembayaran')
  end
end

# LANDING PAGE

Given(/^user is in VP landing page$/) do
  @vp_landing_page = VPPhoneCreditLandingPage.new
  @vp_landing_page.load_page(main_focus: false)
end

When(/^user navigates to VP "([^"]*)" section via landing page$/) do |section|
  @vp_landing_page = if section == "phone credits"
                       VPDataPlanLandingPage.new
                     else
                       VPPhoneCreditLandingPage.new
                     end
  @vp_landing_page.load_page(main_focus: false)

  case section
  when 'phone credits'
    @vp_landing_page.tabs_section.phone_credits_link.click
  when 'data plan'
    @vp_landing_page.tabs_section.data_plan_link.click
  when 'electricity'
    @vp_landing_page.tabs_section.electricity_link.click
  when 'game voucher'
    @vp_landing_page.tabs_section.game_voucher_link.click
  end
end

When(/^user visits old VP URL$/) do
  @vp_landing_page.load_old_url(seo: @seo)
end

When(/^user visits VP "([^"]*)" link$/) do |url|
   
   if url["pulsa"]
      @vp_landing_page = VPPhoneCreditLandingPage.new
   elsif url["data"]
      @vp_landing_page = VPDataPlanLandingPage.new
   elsif url["listrik"]
      @vp_landing_page = VPElectricityLandingPage.new
   elsif url["game"]
      @vp_landing_page = VPGameVoucherLandingPage.new
   end
  visit(url)
end

When(/^user visits VP "([^"]*)" "([^"]*)" SEO page with link "([^"]*)"$/) do |title, section, link|
  @seo = link
  #@vp_landing_page = get_vp_page_object(section)

  case section
    when "phone credits" then VPPhoneCreditLandingPage.new
    when "data plan" then VPDataPlanLandingPage.new
    when "electricity" then VPElectricityLandingPage.new
    when "game voucher" then VPGameVoucherLandingPage.new
  end

  sleep 2
  click_link(title)
end

Then(/^user should be in VP "([^"]*)" landing page$/) do |section|
  expect(get_vp_page_object(section)).to be_displayed
end

Then(/^user should be redirected to VP "([^"]*)" SEO "([^"]*)" link$/) do |section, link|
  expect(get_vp_page_object(section)).to be_displayed(seo: link)
end

Then(/^the E-voucher invoice state should be on "([^"]*)"$/) do |state|
  within(".memo-column.memo-left.clearfix") do  
    expect(page).to have_content(state)
  end
end

Then(/^E-voucher invoice status should be paid$/) do
  @invoice_detail_page.wait_for_payment_status_text
  puts "invoice number : "+@invoice_detail_page.invoice_number_text.text
  @invoice_detail_page.vp_invoice_should_be_paid
end