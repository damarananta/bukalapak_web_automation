When /^the client requests search with (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*) (.*)$/  do |keyword,category,user,campaign,favorite,nego,fp,ts,cond,fsc,prov,city,p_max,p_min,tod_d,tom_d,sb,pg,pp,promoted|
  @response = api_product_search(keyword,category,user,campaign,favorite,nego,fp,ts,cond,fsc,prov,city,p_max,p_min,tod_d,tom_d,sb,pg,pp,promoted)
end

Then(/^the product list condition response should (.*)$/) do |cond|
  json_response = JSON.parse(@response.body)
  json_response['products'].each do |child|
    puts child['condition']
    child['condition'].should== "#{cond}"
  end
end

Then(/^the product list price range response should (\d+) to (\d+)$/) do |p_min,p_max|
  json_response = JSON.parse(@response.body)
  json_response['products'].each do |child|
    puts child['price']
    price=child['price'].to_i
    price.should <= p_max.to_i && price.should >= p_min.to_i
  end
end

Then(/^the product list province and city response should (.*) and (.*)$/) do |prov,city|
  json_response = JSON.parse(@response.body)
  json_response['products'].each do |child|
    puts child['province']
    puts child['city']
    province=child['province']
    kota=child['city']
    province.should == prov.gsub("+", ' ') && kota.should == city.gsub("+", ' ')
  end
end

Then(/^the product list price response should sort by (.*)$/) do |sb|
  json_response = JSON.parse(@response.body)
  json_response['products'].each do |child|
    puts child['price']
    price=child['price'].to_i
    if(sb=='Termurah')
      price_list = []
      price_list.push(price)
      puts price_list
      status=ascending? price_list
      status.should==TRUE
    elsif(sb=='Termahal')
      price_list = []
      price_list.push(price)
      puts price_list
      status=descending? price_list
      status.should==TRUE
    else
      puts "NOT Handle on this step"
    end
  end
end