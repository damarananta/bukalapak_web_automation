Then (/^the response should return product not found$/) do
  json_response = JSON.parse(@response.body)
  json_response['status'].should== 'ERROR'
  json_response['message'].should== 'Produk tidak ditemukan'
  json_response['product'].should== nil
  api_response_debug(@response.code)
end

Then (/^the response should return product detail$/) do
  json_response = JSON.parse(@response.body)
  json_response['message'].should== nil
  json_response['product'].should_not== nil
  assert_status_code(@response.body)
end
