When(/^client inquires "([^"]*)" as unregistered electricity customer number$/) do |cn|
  @response = vp_api_electricity_inquiry(cn, "ERROR")
end

When(/^client requests create "([^"]*)" transaction$/) do |product|
  @product = product
  vp = vp_api_product_list(@product)

  @response =
    case @product
    when "data plan"
      vp_api_data_plan_create({
        data_plan_id: vp[0]["items"][0]["id"].to_s
      })
    when "electricity"
      inq = vp_api_electricity_inquiry

      vp_api_electricity_create({
        name: vp[0]["name"],
        power: inq["customer_info"]["power"],
        segmentation: inq["customer_info"]["segment"].strip,
        package_id: vp[0]["id"].to_s
      })
    when "phone credit"
      vp_api_phone_credit_create({
        phone_credit_id: vp[0]["items"][0]["id"].to_s
      })
    end
end

When(/^client requests registered customer number inquiry$/) do
  @response = vp_api_electricity_inquiry
end

Then(/^electricity API inquiry response should be "([^"]*)"$/) do |status|
  vp_api_status_matcher(@response, status)
end

Then(/^VP API transaction could be found in transactions list$/) do
  desc = ->{ @product == 'electricity' ? 'invoice' : 'transaction' }

  new_trx = JSON.parse(@response.body)["invoice"]["transactions"][0]
  new_inv_id = new_trx["transaction_id"]

  puts "Expected #{desc.call} ID: #{new_inv_id}"

  result = get_request_api("/transactions.json?keywords=#{new_inv_id}")

  expect(result).not_to be_empty
end

Then(/^VP API transaction status should be "([^"]*)"$/) do |status|
  vp_api_status_matcher(@response, status)
end
