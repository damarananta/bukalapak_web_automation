When(/^User request send message to "([^"]*)"$/) do |receiver_id|
  path="/messages.json"
  body = {}
  instant_message={}
  instant_message["receiver_id"]=receiver_id
  instant_message["body_bb"]=ChatPage::CHAT_CONTENT
  instant_message["category"]="1"
  instant_message["client_id"]='1223b8c30a347321299611f873b449ac'
  body["instant_message"]=instant_message
  body["product_id"]="mwza"
  HTTParty.post(@api_url+ path,{body: body,basic_auth: {
                                  username: @auth["user_id"].to_s,
                                  password: @auth["token"]
  }})
end

Then(/^inbox should not empty$/) do
  json_response = JSON.parse(@response.body)
  json_response['inbox'].each do |child|
    child['id'].should_not==nil
    child['updated_at'].should_not==nil
    child['partner_id'].should_not==nil
    child['partner_name'].should_not==nil
    child['partner_avatar'].should_not==nil
    child['partner_verified'].should_not==nil
    child['user_id'].should_not==nil
    child['user_name'].should_not==nil
    child['last_message'].should_not==nil
    child['last_message_read'].should_not==nil
    child['last_message_sent'].should_not==nil
  end
end

Then(/^detail chat should not empty$/) do
  fetch_id_chat
  path="/messages/#{@id_message}.json"
  @response=HTTParty.get(@api_url+path,basic_auth_api)
  json_response = JSON.parse(@response.body)
  json_response['instant_messages'].each do |child|
    child['id'].should_not==nil
    child['created_at'].should_not==nil
    child['read'].should_not==nil
    child['receiver_id'].should_not==nil
    child['receiver_name'].should_not==nil
    child['receiver_avatar'].should_not==nil
    child['sender_id'].should_not==nil
    child['sender_name'].should_not==nil
    child['sender_avatar'].should_not==nil
    child['updated_at'].should_not==nil
  end
end

When(/^the authenticate client requests DELETE conversation$/) do
  fetch_id_chat
  path="/messages/#{@id_message}.json"
  HTTParty.delete(@api_url+path,basic_auth_api)
  sleep 1
end

Then(/^inbox should be empty$/) do
  path="/messages.json"
  @response=HTTParty.get(@api_url+path,basic_auth_api)
  json_response = JSON.parse(@response.body)
  json_response['unread'].should == 0
  json_response['inbox'].should == []
end
