When /^the client requests GET (.*)$/  do |path|
  @response = request_api(path)
end

When /^the authenticate client requests GET (.*)$/  do |path|
  @response = get_request_api(path)
end

When /^the client requests POST (.*)$/  do |path|
  @response = post_get_request_api(path)
  sleep 1
end

Then (/^the response should success$/) do
  assert_status_code(@response.body)
end

Then(/^the response count should (\d+)$/) do |count|
  json_response = JSON.parse(@response.body)
  count_recommend= json_response['recommended_products'].count
  count_recommend.should==count.to_i
  puts count_recommend
end

Then(/^the product list response count should (\d+)$/) do |count|
  json_response = JSON.parse(@response.body)
  count_product_list= json_response['products'].count
  count_product_list.should==count.to_i
  puts count_product_list
end
