Then (/^favorites products count should "([^"]*)"$/) do |qty|
  json_response = JSON.parse(@response.body)
  expect(json_response['products'].count).to eq(qty.to_i)
  assert_status_code(@response.body)
end

Then (/^fill products should not empty$/) do
  json_response = JSON.parse(@response.body)
  fill_products=json_response['fill_products'].count
  fill_products.should_not== 0
  assert_status_code(@response.body)
end

When(/^the client requests DELETE favorites product "([^"]*)"$/) do |id|
  path="/favorites/#{id}/remove.json"
  HTTParty.delete(@api_url + path,basic_auth_api)
end

When(/^the client requests add bulk favorites product "([^"]*)" "([^"]*)"$/) do |id1,id2|
  body = {}
  body["products_id"]=["#{id1}","#{id2}"]
  path="/favorites/bulk_add.json"
  HTTParty.post(@api_url+ path,{body: body,basic_auth: {
                                  username: @auth["user_id"].to_s,
                                  password: @auth["token"]
  }})
end

When(/^the client requests remove bulk favorites product "([^"]*)" "([^"]*)"$/) do |id1,id2|
  body = {}
  body["products_id"]=["#{id1}","#{id2}"]
  path="/favorites/bulk_remove.json"
  HTTParty.patch(@api_url+ path,{body: body,basic_auth: {
                                   username: @auth["user_id"].to_s,
                                   password: @auth["token"]
  }})
end
