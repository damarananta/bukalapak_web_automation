Then (/^create 6 fake trending keywords$/) do
  for i in 0..5
    steps %Q{
      Then Admin click New Trending Keyword button
      Given random trending keyword value
      Given trending keyword url value "Url Test"
      When Admin click save trending keyword
    }
  end
end

Then(/^trending keywords response count should "([^"]*)"$/) do |count|
  json_response = JSON.parse(@response.body)
  count_product_list= json_response['trending_keywords'].count
  count_product_list.should==count.to_i
  puts count_product_list
end
