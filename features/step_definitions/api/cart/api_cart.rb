Then (/^cart respond code should 200 and empty$/) do
  json_response = JSON.parse(@response.body)
  json_response['cart_id'].should_not == nil
  quantity=json_response['cart'].count
  quantity.should <=1  && quantity.should >=0
  json_response['accepted_nego'].should == nil
  json_response['message'].should == nil
  assert_status_code(@response.body)
end

Then (/^cart respond code should 200 and null$/) do
  json_response = JSON.parse(@response.body)
  json_response['cart_id'].should_not == nil
  expect(json_response['cart'].count).to eq(0)
  json_response['accepted_nego'].should == {}
  json_response['message'].should == nil
  assert_status_code(@response.body)
end


Then (/^response should return cart items with quantity "([^"]*)"$/) do |qty|
  json_response = JSON.parse(@response.body)
  json_response['cart_id'].should_not == nil
  json_response['cart'].each do |child|
    child['items'].each do |sub_child|
      quantity=sub_child['quantity'].to_i
      quantity.should==qty.to_i
    end
  end
  assert_status_code(@response.body)
end

Then(/^fetch cart item id$/) do
  json_response = JSON.parse(@response.body)
  json_response['cart'].each do |child|
    child['items'].each do |sub_child|
      @item_id=sub_child['id']
      puts @item_id
    end
  end
end

When(/^the client requests DELETE cart item$/) do
  path="/carts/item/#{@item_id}.json"
  HTTParty.delete(@api_url + path,basic_auth_api)
end

When(/^the client requests update cart items with quantity "([^"]*)"$/) do |qty|
  path="/carts/item/#{@item_id}.json?quantity=#{qty}"
  HTTParty.patch(@api_url + path,basic_auth_api)
end
