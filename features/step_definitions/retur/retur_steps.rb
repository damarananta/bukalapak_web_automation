Given(/^there is seller$/) do
  @home_page                  = HomePage.new
  @product_page               = ProductDetailPage.new
  @checkout_page              = CheckoutPage.new
  @payment_page               = PaymentChannel.new
  @invoice_page               = InvoicesPage.new
  @admin_discussion_page      = AdminDiscussion.new
  @dompet_page                = BukaDompet.new
  @transaction_page           = TransactionPage.new
  @seller_cred                = ENV['SELLER3_USERNAME']
  @seller_pass                = ENV['SELLER3_PASSWORD']
  @buyer_cred                 = ENV['BUYER_USERNAME']
  @buyer_pass                 = ENV['BUYER_PASSWORD']
  @cs_cred                    = ENV['CUSTOMER_SUPPORT_USERNAME']
  @cs_pass                    = ENV['CUSTOMER_SUPPORT_PASSWORD']
  @Seller3_Product            = ENV['SELLER3_PRODUCT_URL']


  @home_page.load if current_path != '/'
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(@seller_cred,@seller_pass)
  #login(ENV['SELLER3_USERNAME'], ENV['SELLER3_PASSWORD'])
  @dompet_page.load
  @dompet_page.wait_for_current_balance
  @seller_balance_before=@dompet_page.get_current_balance
  logout
end

Given(/^there is buyer$/) do
  @home_page.load if current_path != '/'
  @home_page.login(@buyer_cred,@buyer_pass)
  @dompet_page.load
  @buyer_balance_before= @dompet_page.get_current_balance_buyer
  logout
end

Given(/^there is a transaction and the state is delivered$/) do
  @home_page.load if current_path != '/'
  @home_page.login(@buyer_cred,@buyer_pass)
  @product_page.load_product(@Seller3_Product)
  @product_page.click_beli
  @checkout_page.checkout
  @price=@payment_page.choose_bukadompet_unpassword
  @transaction_page.tfa_alert_bukadompet(12345)
  @payment_page.choose_bukadompet_after_otp
  @invoice_page.view_invoice
  @invoice_page.get_invoice_id
  @invoice_id=@invoice_page.get_invoice_id
  logout
  @home_page.load if current_path != '/'
  @home_page.login(@seller_cred,@seller_pass)
  @transaction_page.load(account_type:'sell')
  @transaction_page.seller_accept_orders(@invoice_id)
  @transaction_page.has_no_save_shipping_code?
  logout
end

Given(/^buyer wants to complain$/) do
  @home_page.load if current_path != '/'
  @home_page.login(@buyer_cred,@buyer_pass)
  @transaction_page.buyer_complains(@invoice_id)
end

Given(/^buyer wants to replace the product$/) do
  @transaction_page.replace_product
  logout
end

Given(/^admin approve$/) do
  @home_page.load if current_path != '/'
  @home_page.login(@cs_cred,@cs_pass)
  @admin_discussion_page.admin_approve_retur
  logout
end

When(/^buyer deliver the product$/) do
  @home_page.load if current_path != '/'
  @home_page.login(@buyer_cred,@buyer_pass)
  @transaction_page.buyer_deliver_product_retur(@invoice_id)
  logout
end

When(/^seller accept the product$/) do
  login(ENV['SELLER3_USERNAME'], ENV['SELLER3_PASSWORD'])
  @transaction_page.seller_accept_the_product_retur(@invoice_id)
end

When(/^seller deliver the product$/) do
  @transaction_page.seller_deliver_product_retur
  logout
end

When(/^buyer accept the product$/) do
  login(ENV['BUYER_USERNAME'], ENV['BUYER_PASSWORD'])
  @transaction_page.buyer_accept_product_retur(@invoice_id)
end

When(/^buyer allow the transaction to be remitted$/) do
  within ('#popup_wrapper') do
    find('#payment_return_refund_0').click
    find('.btn.btn--red.btn--large.btn--block.js-payment-return-submit').click
  end
  logout
end

Then(/^the money from transaction will be remmited to seller's bukaDompet$/) do
  login(ENV['CUSTOMER_SUPPORT_USERNAME'], ENV['CUSTOMER_SUPPORT_PASSWORD'])
  visit_url '/payment/discussions/admin?utf8=%E2%9C%93&transaction_id=&state=all&return_type=all&mode=&sorting=desc&commit=Search'

  new_window = window_opened_by do
    first(:link, 'Lihat Diskusi').click
  end

  within_window new_window do
    find('.btn.btn--medium.btn--green.cbox-button-inline.cboxElement').click

    within('#cboxContent') do
      fill_in('remit_notes', :with => 'Buyer telah menerima barang retur dan diskusi telah selesai')
      find('.btn.btn--medium.btn--red.btn--block.js-form-submit').click
      close_window
    end
  end
  logout

  login(ENV['BUYER_USERNAME'], ENV['BUYER_PASSWORD'])
  visit_url('/dompet')
  @final_balance_buyer = @buyer_balance_before-@price
  puts @buyer_balance_before
  puts @price
  within('.u-fg--green-super-dark.u-txt--large') do
    expect(page).to have_content(add_delimiter(@final_balance_buyer))
    sleep 3
  end
  logout

  login(ENV['SELLER3_USERNAME'], ENV['SELLER3_PASSWORD'])
  visit_url('/dompet')
  @final_balance_seller = @seller_balance_before+@price

  within('.u-fg--green-super-dark.u-txt--large') do
    expect(page).to have_content(add_delimiter(@final_balance_seller))
  end
  logout
end

Given(/^there is a transaction that have more than one product from one seller$/) do
  login(ENV['BUYER_USERNAME'], ENV['BUYER_PASSWORD'])
  add_to_cart ENV['SELLER3_PRODUCT_URL']
  close_cart_pop_up

  add_to_cart ENV['SELLER3_PRODUCT_URL_2']
  close_cart_pop_up
  visit_url '/cart/carts'
  payment_multiple_order
  payment_first_page_click_next
  choose_bukadompet(ENV['BUYER_PASSWORD'])
  logout
  login(ENV['SELLER3_USERNAME'], ENV['SELLER3_PASSWORD'])
  confirm_accept_order
  logout
end

Then(/^the money from selected items return transaction will be remmited to seller's bukaDompet$/) do
  login(ENV['CUSTOMER_SUPPORT_USERNAME'], ENV['CUSTOMER_SUPPORT_PASSWORD'])
  visit_url '/payment/discussions/admin?utf8=%E2%9C%93&transaction_id=&state=all&return_type=all&mode=&sorting=desc&commit=Search'

  new_window = window_opened_by do
    first(:link, 'Lihat Diskusi').click

  end

  within_window new_window do
    click_link('Split Remit')
    popup = page.driver.browser.window_handles.last
    page.driver.browser.switch_to.window(popup)
    find(:xpath, '//*[@id="mylapak_content"]/div/section/div/form/div/div[1]/div/fieldset/ul[1]/li[2]/div[1]/input').click
    @return_seller_balance = change_into_integer(find(:xpath, '//*[@id="mylapak_content"]/div/section/div/form/div/div[3]/div/dl[1]/dd[3]').text)
    @total_transaction= change_into_integer(find(:xpath, '//*[@id="mylapak_content"]/div/section/div/form/div/div[3]/div/dl[1]/dd[2]/span[2]').text)
    @buyer_refund_after_partial_remit = @total_transaction-@return_seller_balance
    puts @buyer_refund_after_partial_remit
    puts @return_seller_balance
    within('.content-section__content.content-section--fixed') do
      fill_in('split_fund_seller_fund', :with =>@return_seller_balance)
      fill_in('split_fund_buyer_fund', :with =>@buyer_refund_after_partial_remit)
      fill_in('split_fund_return_conclusion', :with =>'Partial Remit with one seller')
      find('.btn.btn--medium.btn--red.js-split-fund__next').click
    end

    within('.inline-popup-wrapper') do
      find('.btn.btn--red.btn--medium.btn--block.js-split-fund__submit').click
    end
    logout

    login(ENV['BUYER_USERNAME'], ENV['BUYER_PASSWORD'])
    visit_url('/dompet')
    @partial_retur_balance_buyer = @buyer_balance_before-@return_seller_balance
    puts @partial_retur_balance_buyer
    @dompet_page.load

    expect(page).to have_content(add_delimiter(@partial_retur_balance_buyer))
    sleep 3

    logout

    login(ENV['SELLER3_USERNAME'], ENV['SELLER3_PASSWORD'])
    visit_url('/dompet')
    @partial_retur_balance_seller = @seller_balance_before+@return_seller_balance

    @dompet_page.load
    expect(page).to have_content(add_delimiter(@partial_retur_balance_seller))

    logout
  end
end

Given(/^buyer wants to refund the transaction$/) do
  find('.btn.btn--outline-green.btn--large.radio-as-btn__btn.pull-right.js-radio-as-btn__label').click
  choose('Barang tidak sesuai pesanan')
  click_hidden('#payment_return_return_type_refund')
  attach_file('attachment', File.absolute_path('./features/support/image/crushed-boxes.jpg'))
  check('accept_agreement')
  click_button('Kirim Komplain')
  logout
end

Then(/^the money from transaction will be refunded to buyer's BukaDompet$/) do
  logout
  login(ENV['CUSTOMER_SUPPORT_USERNAME'], ENV['CUSTOMER_SUPPORT_PASSWORD'])
  visit_url '/payment/discussions/admin?utf8=%E2%9C%93&transaction_id=&state=all&return_type=all&mode=&sorting=desc&commit=Search'

  new_window = window_opened_by do
    first(:link, 'Lihat Diskusi').click
  end

  within_window new_window do
    find('.btn.btn--medium.btn--red2.cbox-button-inline.cboxElement').click

    within('#cboxContent') do
      fill_in('refund_notes', :with => 'Barang telah dikembalikan ke seller dan segera refund dana buyer')
      find('.btn.btn--medium.btn--red.btn--block.js-form-submit').click
    end
  end
  logout

  login(ENV['BUYER_USERNAME'], ENV['BUYER_PASSWORD'])
  visit_url('/dompet')
  @final_balance_buyer = @buyer_balance_before

  @dompet_page.load
  expect(page).to have_content(add_delimiter(@final_balance_buyer))
  sleep 3

  logout

  login(ENV['SELLER3_USERNAME'], ENV['SELLER3_PASSWORD'])
  visit_url('/dompet')
  @final_balance_seller = @seller_balance_before

  @dompet_page.load
  expect(page).to have_content(add_delimiter(@final_balance_seller))

  logout
end

Then(/^the money from selected items return transaction will be refunded to buyer's bukaDompet$/) do
  logout
  login(ENV['CUSTOMER_SUPPORT_USERNAME'], ENV['CUSTOMER_SUPPORT_PASSWORD'])
  visit_url '/payment/discussions/admin?utf8=%E2%9C%93&transaction_id=&state=all&return_type=all&mode=&sorting=desc&commit=Search'

  new_window = window_opened_by do
    first(:link, 'Lihat Diskusi').click

  end

  within_window new_window do
    click_link('Split Refund')
    popup = page.driver.browser.window_handles.last
    page.driver.browser.switch_to.window(popup)
    find(:xpath, '//*[@id="mylapak_content"]/div/section/div/form/div/div[1]/div/fieldset/ul[1]/li[2]/div[1]/input').click
    @refund_seller_balance = change_into_integer(find(:xpath, '//*[@id="mylapak_content"]/div/section/div/form/div/div[3]/div/dl[1]/dd[3]').text)
    @total_transaction= change_into_integer(find(:xpath, '//*[@id="mylapak_content"]/div/section/div/form/div/div[3]/div/dl[1]/dd[2]/span[2]').text)
    @seller_after_partial_refund = @total_transaction-@refund_seller_balance
    puts @seller_after_partial_refund
    puts @refund_seller_balance
    within('.content-section__content.content-section--fixed') do
      fill_in('split_fund_seller_fund', :with =>@seller_after_partial_refund)
      fill_in('split_fund_buyer_fund', :with =>@refund_seller_balance)
      fill_in('split_fund_return_conclusion', :with =>'Partial Refund with one seller')
      find('.btn.btn--medium.btn--red.js-split-fund__next').click
    end

    within('.inline-popup-wrapper') do
      find('.btn.btn--red.btn--medium.btn--block.js-split-fund__submit').click
    end
    logout

    login(ENV['BUYER_USERNAME'], ENV['BUYER_PASSWORD'])
    visit_url('/dompet')
    @partial_retur_balance_buyer = @buyer_balance_before-@total_transaction+@refund_seller_balance

    @dompet_page.load
    expect(page).to have_content(add_delimiter(@partial_retur_balance_buyer))
    sleep 3

    logout

    login(ENV['SELLER3_USERNAME'], ENV['SELLER3_PASSWORD'])
    visit_url('/dompet')
    @partial_retur_balance_seller = @seller_balance_before+@seller_after_partial_refund

    @dompet_page.load
    expect(page).to have_content(add_delimiter(@partial_retur_balance_seller))

    logout
    close_window
  end
end

Given(/^admin change the return type to Return to Bukalapak because seller did not response return request$/) do
  login(ENV['CUSTOMER_SUPPORT_USERNAME'], ENV['CUSTOMER_SUPPORT_PASSWORD'])
  visit_url '/payment/discussions/admin?utf8=%E2%9C%93&transaction_id=&state=all&return_type=all&mode=&sorting=desc&commit=Search'

  new_window = window_opened_by do
    first(:link, 'Lihat Diskusi').click
  end

  within_window new_window do
    click_link 'Ubah Tipe Retur'
    choose 'Pengembalian Barang ke Bukalapak'
    click_button("Ubah")
    click_link 'Setujui Tipe Retur'
    expect(page).to have_content('Persetujuan Tipe Retur: Pengembalian Barang ke Bukalapak')
    click_link 'Setujui Retur'
    close_window
  end
  logout
end

Then(/^buyer accept the product to be remitted$/) do
  @home_page.load if current_path != '/'
  @home_page.login(@buyer_cred,@buyer_pass)
  @transaction_page.load(account_type:'buy')
  @transaction_page.filter_transactions(@invoice_id)
  @transaction_page.buyer_confirm_to_remitted
  logout
end
