When /^user buy a product$/ do
  
  @home_page = HomePage.new
  @product_detail = ProductDetailPage.new
  @payment_purchases = PaymentPurchasesNewPage.new
  @confirmation_page = PaymentPurchasesConfirmationPage.new
  @payment_channel = PaymentChannel.new
  @checkout_page = CheckoutPage.new

  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER4_USERNAME'],ENV['BUYER4_PASSWORD'])

  @product_detail.load_product(ENV['SELLER_PRODUCT_URL'])
  @product_detail.click_beli
end

And(/^user adjust the quantity$/) do
  @payment_purchases.adjust_quantity
end

And (/^user choose "([^"]*)"$/) do |payment_method|
  if payment_method == 'bukadompet'
    @payment_purchases.paid_with_bukadompet(ENV['BUYER4_PASSWORD'])
    @confirmation_page.view_invoice
  elsif payment_method == 'atm'
    @payment_purchases.choose_atm
    @confirmation_page.view_invoice
  elsif payment_method == 'alfamart'
    @payment_purchases.choose_alfamart
    @confirmation_page.view_invoice
  elsif payment_method == 'indomaret'
    @payment_purchases.choose_indomaret
    @confirmation_page.view_invoice
  elsif payment_method == 'mandiri'
    @payment_purchases.choose_mandiri_clickpay('1234567890', '9810')
    @confirmation_page.view_invoice
  elsif payment_method == 'bca'
    @payment_purchases.choose_bca_klikpay
    go_back
    @confirmation_page.invoices_page_search
  else
    return false
  end
end

Then(/^the satus at invoice will be "([^"]*)"$/) do |state|
  expect(page).to have_content(state, wait: 10)
end

Then(/^the invoice that used atm should has unique code$/) do
  puts "Actual Unique Code: #{find(".block-payment-code--highlight").text}"
  expect(@payment_purchases).to have_unique_code
end
