When /^user buy item$/ do
  
  @home_page = HomePage.new
  @product_detail = ProductDetailPage.new
  @payment_purchases = PaymentPurchasesNewPage.new
  @confirmation_page = PaymentPurchasesConfirmationPage.new

  @home_page.load
  @home_page.dismiss_one_signal_popup_homepage
  @home_page.login(ENV['BUYER4_USERNAME'],ENV['BUYER4_PASSWORD'])

  @product_detail.load_product(ENV['SELLER_PRODUCT_URL'])
  @product_detail.click_beli
end

And(/^user first chooses "([^"]*)" as the payment method$/) do |payment_method|
  @payment_purchases.choose_payment_but_dicontinue(payment_method)
end

And (/^back to step 1$/) do
  sleep 2
  go_back
end

And (/^user changes their address or courier$/) do
  @payment_purchases.change_address
end

And (/^user pay their transaction using "([^"]*)"$/) do |payment_method|
  if payment_method == 'bukadompet'
    @payment_purchases.paid_with_bukadompet(ENV['BUYER4_PASSWORD'])
  
  elsif payment_method == 'atm'
    @payment_purchases.choose_atm
  
  elsif payment_method == 'indomaret'
    @payment_purchases.choose_indomaret
  
  elsif payment_method == 'alfamart'
    @payment_purchases.choose_alfamart

  elsif payment_method == 'mandiri_clickpay'
    @payment_purchases.choose_mandiri_clickpay('1234567890', '9810')
    sleep 3
  else
    return false
  end
end

Then(/^the satus should be "([^"]*)"$/) do |state|
  @confirmation_page.view_invoice
  expect(page).to have_content(state, wait: 10)
end

Then(/^the invoice should has unique code$/) do
  puts "Actual Unique Code: #{find(".block-payment-code--highlight").text}"
  expect(@payment_purchases).to have_unique_code
end
