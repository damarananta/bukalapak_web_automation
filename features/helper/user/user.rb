def gender
  gender = ["user_gender_perempuan","user_gender_laki-laki"].sample
end

def create_fullname(gender)
  if gender == "user_gender_perempuan"
    firstname = ["Yulinar", "Irwin" , "Petrisia", "Anis" ].sample
    lastname  = ["Elfrida", "Harnia", "Meiga"   , "Latif"].sample
  else
    firstname = ["Raga"   , "Fadhli"  , "Fauridho", "Suprapto", "Rizki" , "Febryan", "Dedi"   , "Ananda" , "Aranda"  , "Aditya", "Wira"   ].sample
    lastname  = ["Pinilih", "Maulidri", "Mahran"  , ""        , "Andika", "Yeremi" , "Setiadi", "Pratama", "Soedjono", "Mirza" , "Pramudy"].sample
  end
  fullname = firstname + " " + lastname
end

def create_username(fullname)
  username = fullname.downcase.delete(" ") + Random.new.rand(10000).to_s
end

def create_email(username)
  provider = ["hotmail","rocketmail","ymail","gmail"].sample
  email = username + "@" + provider + ".com"
end

def create_phone_number
  "0856" + rand.to_s[3..9]
end

def create_address
  "Plaza City View lantai 1 dan 2"
end
