def select_poskilat
  neutralize_ab_test("&ab_bukadompet=normal&ab_payment_view_v2=normal&ab_checkout_button=normal")

  within('.js-purchase-courier.purchase-courier-input') do 
    find('.chzn-container.chzn-container-single').click
    sleep 2
    find('.active-result.chzn-courier.chzn-courier--pos', :text => 'Pos Kilat').click        
  end
end
