def payment_page_after_choosing_courier  
  within('.order-review--content__fees.clearfix') do
    @shipping_price = change_into_integer(find('.js-list-item--item.js-iv-review__shipping__fee').text)
  end
  
  scroll_down(100)

  if(page.has_selector?('.js-encourage-deposit'))
    find('#js-purchase-encourage-deposit-next').click
  else
    find('#js-purchase-next').click
  end
  
  sleep 3
end
