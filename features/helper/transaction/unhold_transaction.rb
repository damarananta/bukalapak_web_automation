def unhold_replacement_transaction
  login(ENV['CUSTOMER_SUPPORT_USERNAME'], ENV['CUSTOMER_SUPPORT_PASSWORD'])
  visit_url('/payment/transactions/manage')
  fill_in('payment_transaction_searchable_search_keywords', :with => @new_transaction_id)
  find(:id, 'payment_transaction_searchable_search_keywords').native.send_keys(:enter)

  within ('.actions') do
    find('.button.red.cancel.admin-action').click
  end

  page.driver.browser.switch_to.alert.accept
  sleep 2
  #Override to force remit after unhold replacement
  fill_in('payment_transaction_searchable_search_keywords', :with => @new_transaction_id)
  find(:id, 'payment_transaction_searchable_search_keywords').native.send_keys(:enter)

  within ('.actions') do
    find('.button.submit.green.next-state.remit-admin').click
  end

  page.driver.browser.switch_to.alert.accept
  sleep 2
  logout
end
