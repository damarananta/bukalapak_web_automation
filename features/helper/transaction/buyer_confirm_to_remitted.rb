def buyer_confirm_to_remitted
  click_link('Konfirmasi Terima Barang')
  find('.c-icon.c-icon--thumb-up').click
  first('.c-btn.c-btn--small.u-mrgn-bottom--1.js-recomendation-comment-btn').click
  find(:xpath, '//*[@id="js-recomendation-form"]/div/div[6]/div[1]/label').click
  click_button('Simpan')
end

def buyer_complain
  url = URI.parse(current_url)
  puts("URL = " + url.to_s)
  @newurl = url.to_s+"/arrival_confirmation?complain=true"
  visit_url(@newurl)
  #wait_for_ajax
  sleep 3
  find('.c-icon.c-icon--thumb-down').click
  first('.c-btn.c-btn--small.u-mrgn-bottom--1.js-recomendation-comment-btn').click
  click_button('Simpan dan Lanjutkan Komplain')
end

def wait_for_ajax
  Timeout.timeout(Capybara.default_max_wait_time) do
    loop until finished_all_ajax_requests?
  end
end

def finished_all_ajax_requests?
  page.evaluate_script('jQuery.active').zero?
end
