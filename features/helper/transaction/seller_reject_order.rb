def seller_reject_order
  visit_url("/payment/transactions?active_tab=sell&payment_transaction_searchable_search%5Bbuyer_id%5D=&payment_transaction_searchable_search%5Bcourier%5D=&payment_transaction_searchable_search%5Bexclude_tele_sales_status%5D=&payment_transaction_searchable_search%5Bfilter_by%5D=sell&payment_transaction_searchable_search%5Bfilter_date_range_by%5D=&payment_transaction_searchable_search%5Bfilter_need_action_trx%5D=&payment_transaction_searchable_search%5Bfrom_date%5D=&payment_transaction_searchable_search%5Bfrom_payment_hour%5D=&payment_transaction_searchable_search%5Bhide_invoice_unpaid%5D=&payment_transaction_searchable_search%5Bhide_virtual%5D=&payment_transaction_searchable_search%5Bincludes%5D%5B%5D=buyer&payment_transaction_searchable_search%5Bincludes%5D%5B%5D=seller&payment_transaction_searchable_search%5Bincludes%5D%5B%5D=shipping&payment_transaction_searchable_search%5Bincludes%5D%5B%5D=items&payment_transaction_searchable_search%5Binside_coverage_city%5D=&payment_transaction_searchable_search%5Binvoice_or_transaction_ids%5D=&payment_transaction_searchable_search%5Bis_auto_fraud%5D=&payment_transaction_searchable_search%5Bkeywords%5D=&payment_transaction_searchable_search%5Bmax_amount%5D=&payment_transaction_searchable_search%5Bmin_amount%5D=&payment_transaction_searchable_search%5Bmode%5D=&payment_transaction_searchable_search%5Bpage%5D=&payment_transaction_searchable_search%5Bpaid_to%5D=&payment_transaction_searchable_search%5Bpayment_method%5D=&payment_transaction_searchable_search%5Bper_page%5D=10&payment_transaction_searchable_search%5Bproduct_only%5D=true&payment_transaction_searchable_search%5Bsort_by%5D=&payment_transaction_searchable_search%5Bsorting%5D=&payment_transaction_searchable_search%5Bstate%5D=&payment_transaction_searchable_search%5Bto_date%5D=&payment_transaction_searchable_search%5Bto_payment_hour%5D=&payment_transaction_searchable_search%5Buser_id%5D=18&payment_transaction_searchable_search%5Bwithout_invoice%5D=")
  within('.entry-action') do
    search_transaction
    sleep 2
  end
  find(:xpath, '//*[@id="nego_list"]/ul/li[1]/div[6]/a[2]').click
  sleep 3
  within ('.seller-rejection') do
    choose('Stok Habis')
  end
  click_button('Tolak Pesanan')
end
