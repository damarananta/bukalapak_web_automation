def search_transaction
  fill_in('keywords', :with => @invoice_id)
  find(:id, 'keywords').native.send_keys(:enter)
  sleep 2
end

def search_transaction_replacement
  visit_url('/replacements?utf8=%E2%9C%93&mode=&transaction_id=&product_name=&amount=&seller_name=&per_page=25&state=all&reason=&replacement_type=all&start_date[day]=2&start_date[month]=12&start_date[year]=2016&finish_date[day]=2&finish_date[month]=12&finish_date[year]=2020&commit=Search')
  fill_in('transaction_id', :with => @invoice_id)
  find(:id, 'transaction_id').native.send_keys(:enter)
  sleep 2
end

def search_new_transaction_after_replacement
  fill_in('keywords', :with => @new_transaction_id)
  find(:id, 'keywords').native.send_keys(:enter)
  sleep 2
end
