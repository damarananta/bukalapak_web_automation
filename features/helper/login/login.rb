def login(username, password)
  visit_url '/' if current_path != '/'
  click_link('Login')
  fill_in 'user_session_username', :with => username
  fill_in 'user_session_password', :with => password
  click_button('Login')
  dismiss_one_signal_popup
  dismiss_abandoned_popup
end


def login_mobile(username, password)
  visit_split_true '/' if current_path != '/'
  accept_onboarding
  dismiss_one_signal_popup
  sleep 1
  find('.icon-login').click
  sleep 2
  fill_in 'user_session_username', :with => username
  fill_in 'user_session_password', :with => password
  find(".js-btn-menu-login").click
  dismiss_abandoned_popup
  sleep 3
end

def logout
  visit_url '/logout'
  sleep 1
end
