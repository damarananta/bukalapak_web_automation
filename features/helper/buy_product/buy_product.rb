def buy_product(product)
  visit_url product
  neutralize_ab_test("&one_click_payment=normal")

  within('.c-product-buy__actions') do
    click_on('Beli')
  end
end

def book_flight(username, password, type, departure, arrive)
  #type fill with oneway or return

  @home_page.load{ |page| page.dismiss_one_signal_popup_homepage }
  @flight_book_page.load
  sleep 4
  @flight_book_page.load
  @home_page.login(username, password)

  
  @flight_book_page.wait_until_login_button_invisible(30)

  #neutralize_ab_test("&ab_test[notif_secondary_navbar]=old")
  
  #@flight_book_page.load
  page.execute_script("window.scrollBy(0,200)")

  case type
  
  when "oneway"

    #@flight_book_page.radio_oneway.click
    @flight_book_page.departure(departure)
    @flight_book_page.arrived(arrive)
    #@flight_book_page.dep_date
    @flight_book_page.add_passenger(1, 1, 1)
    sleep 3
    @flight_book_page.search_button.click

  when "return"
  
    @flight_book_page.wait_for_radio_return
    @flight_book_page.radio_return.click
    
    @flight_book_page.departure(departure)
    @flight_book_page.arrived(arrive)
    @flight_book_page.dep_date
    @flight_book_page.back_date
    @flight_book_page.add_passenger(2, 2, 2)
    @flight_book_page.search_button.click

  end

end

def buy_phone_credits(no_hp: ENV['BUYER_PHONE_NUMBER'],
                      username: ENV['BUYER2_USERNAME'],
                      password: ENV['BUYER2_PASSWORD'],
                      with_login: true)

  @home_page.load { |page| page.dismiss_one_signal_popup_homepage }
  @home_page.login(username, password) if with_login
  
  neutralize_ab_test("&ab_test[notif_secondary_navbar]=old")
  
  @phone_credit_page.load_page
  @phone_credit_page.phone_number_field.set(no_hp)
  sleep 2
  @phone_credit_page.buy_button.click
  sleep 3
end

def buy_vp_home_widget(no_hp, vp, payment, no_meteran)
  dismiss_abandoned_popup

  within('#mod-tab-1')do
    find("span", :text => vp).click
    sleep 3
  end

  within('.vc-tab.vc-tab--small.vc-widget.vc-widget--wide')do

    if vp =="Pulsa"
      fill_in 'phone_number', :with => no_hp
      sleep 3
    elsif vp =="Paket Data"
      fill_in 'phone_number', :with => no_hp
      sleep 3
    elsif vp =="Token Listrik"
      fill_in 'customer_number', :with => no_meteran
      sleep 3
    else

    end

    click_button('Beli')
  end
  sleep 3
  choose(payment)
  payment_choosen_pay
  sleep 2
  click_on('Lihat Tagihan')
  sleep 3
end

def buy_vp_bukadompet_widget(no_hp, vp, payment, no_meteran)
  dismiss_abandoned_popup

  within('.vc-tab.vc-tab--small.vc-widget.vc-widget--wide')do

    find("span", :text => vp).click
    sleep 2

    if vp =="Pulsa"
      fill_in 'phone_number', :with => no_hp
      sleep 2
    elsif vp =="Paket Data"
      fill_in 'phone_number', :with => no_hp
      sleep 2
    elsif vp =="Token Listrik"
      fill_in 'customer_number', :with => no_meteran
      sleep 2
    else

    end

    click_button('Beli')
  end
  sleep 3
  choose(payment)
  payment_choosen_pay
  sleep 2
  click_on('Lihat Tagihan')
  sleep 3
end

def buy_vp_confirmation_widget(no_hp, vp, payment, no_meteran)

  if vp =="Pulsa"
      fill_in 'phone_number', :with => no_hp
      sleep 2
    elsif vp =="Paket Data"
      fill_in 'phone_number', :with => no_hp
      sleep 2
    elsif vp =="Token Listrik"
      fill_in 'customer_number', :with => no_meteran
      sleep 2
    else

  end

end

def mweb_buy_phone_credits(no_hp, username, password)
  visit_url '/'
  login_mobile(username, password)
  mweb_visit_virtual_product('/vp/voucher/pulsa')
  sleep 3
  dismiss_abandoned_popup
  fill_in 'phone_number', :with => "081287507270"
  sleep 3
  click_button('Beli')
end

def mweb_buy_dataplan(no_hp, username, password)
  visit_url '/'
  login_mobile(username, password)
  mweb_visit_virtual_product('/vp/voucher/data')
  sleep 3
  dismiss_abandoned_popup
  fill_in 'phone_number', :with => "081287507270"
  sleep 3
  click_button('Beli')
end

def buy_data_plan(no_hp, username, password)
  visit_url '/'
  login(username, password)

  neutralize_ab_test("&ab_test[notif_secondary_navbar]=old")

  visit_virtual_product('Paket Data')
  sleep 3
  dismiss_abandoned_popup
  fill_in 'phone_number', :with => no_hp
  sleep 2
  page.execute_script "window.scrollBy(0,500)"
  sleep 3
  click_hidden('.c-btn.c-btn--green.c-btn--large.c-btn--block.js-submit-button')
  sleep 3
end

def buy_electricity(username: ENV['BUYER_USERNAME'],
                    password: ENV['BUYER_PASSWORD'],
                    meter_no: ENV['BUYER_ELECTRICITY_NUMBER'],
                    with_login: true)
  
  @home_page.load { |page| page.dismiss_one_signal_popup_homepage }
  @home_page.login(username, password) if with_login

  neutralize_ab_test("&ab_test[notif_secondary_navbar]=old")

  @electricity_page.load_page
  page.execute_script("window.scrollBy(0,100)")
  @electricity_page.customer_number_field.set(meter_no)
  
  @electricity_page.wait_for_buy_button
  sleep 2
  @electricity_page.buy_button.click
  sleep 3
end

def mweb_buy_token_listrik(no_meteran, username, password)
  visit_url '/'
  login_mobile(username, password)
  mweb_visit_virtual_product('/vp/voucher/listrik')
  sleep 3
  dismiss_abandoned_popup
  fill_in 'customer_number', :with => no_meteran
  sleep 3
  click_button('Beli')
end

def buy_game_voucher(username,password,with_login: true)
  
  @home_page.load { |page| page.dismiss_one_signal_popup_homepage }
  @home_page.login(username, password) if with_login

  @game_voucher_page.load_page
  sleep 1
  @game_voucher_page.publishers.first.click
  sleep 2
  page.execute_script("window.scrollBy(0,700)")
  @game_voucher_page.buy_button.click
end

def mweb_buy_product(username, password, product)
  visit_url '/'
  login_mobile(username, password)
  visit_url product
  # dismiss_abandoned_popup
  sleep 2
  within('.product-buy-actions') do
    click_button('Beli')
  end
  if(page.has_selector?('#js-purchase-encourage-deposit-next'))
    find('#js-purchase-encourage-deposit-next').click
  elsif(page.has_selector?('js-progress-page--next'))
    find('#js-progress-page--next').click
  else
    click_on('Lanjut')
  end
end
