def choose_changed_bukadompet(password)
  within('.section-page.section-purchase.section-purchase__payment.active') do
    scroll_down(100)
  end

  within('.payment-method__option.payment-method__option--deposit') do
    find('.radio').click

    within('.text-green') do
      @deposit = change_into_integer(find('.amount').text)
    end

    @price = change_into_integer(find('.js-iv-review__deposit').text)
  end

  if(page.has_selector?('#js-iv-deposit'))
    if(page.has_selector?('.form-field-item.payment-info-content__deposit__password'))
      within('.form-field-item.payment-info-content__deposit__password') do
        fill_in 'deposit_password', :with => password
      end
      
      if(page.has_selector?('.form-field-item.payment-info-content__deposit__password'))
        find('.btn.btn--red.btn--large.btn--block.js-iv-submit-deposit.js-tfa-required-button').click
        sleep 3
      else
        find('.btn.btn--red.btn--large.btn--block.js-tfa-required-button').click
        sleep 3
      end
      
    else
      find('.btn.btn--red.btn--large.btn--block.js-iv-submit-deposit.js-iv-submit-unpassword.js-tfa-required-button').click
      sleep 2
    end

  else
    if(page.has_selector?('.form-field-item.payment-info-content__deposit__password'))
      within('.form-field-item.payment-info-content__deposit__password') do
        find('.btn.btn--red.btn--large.pull-right.js-iv-submit-ab-bukadompet.js-tfa-required-button').click
        sleep 3
      end
    else
      find('.btn.btn--red.btn--large.btn--block.js-iv-submit-deposit.js-iv-submit-unpassword.js-tfa-required-button').click
      sleep 2
    end
  end

  expect(page).to have_content('BERHASIL!')
  click_on('Lihat Tagihan')
  sleep 3
end
