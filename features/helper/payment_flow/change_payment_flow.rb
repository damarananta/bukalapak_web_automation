def change_flow_bukadompet
  within('.section-page.section-purchase.section-purchase__payment.active') do
    scroll_down(100)
  end

  within('.payment-method__option.payment-method__option--deposit.active') do
    find('.radio').click
  end

  within('.payment-method__option.payment-method__option--atm') do
    find('.radio').click
  end
end

def change_flow_atm
  within('.section-page.section-purchase.section-purchase__payment.active') do
    scroll_down(100)
  end

  within('.payment-method__option.payment-method__option--atm') do
    find('.radio').click
  end

  within('.payment-method__option.payment-method__option--alfamart') do
    find('.radio').click
  end
end

def change_flow_alfamart
  within('.section-page.section-purchase.section-purchase__payment.active') do
    scroll_down(100)
  end

  within('.payment-method__option.payment-method__option--alfamart') do
    find('.radio').click
  end

  within('.payment-method__option.payment-method__option--atm') do
    find('.radio').click
  end
end

def change_flow_indomaret
  within('.section-page.section-purchase.section-purchase__payment.active') do
    scroll_down(100)
  end

  within('.payment-method__option.payment-method__option--indomaret') do
    find('.radio').click
  end

  within('.payment-method__option.payment-method__option--cimb_clicks') do
    find('.radio').click
  end
end

def change_flow_bca
  within('.section-page.section-purchase.section-purchase__payment.active') do
    scroll_down(100)
  end

  within('.payment-method__option.payment-method__option--bca_klikpay') do
    find('.radio').click
  end

  within('.payment-method__option.payment-method__option--cimb_clicks') do
    find('.radio').click
  end
end

def change_flow_cimb
  within('.section-page.section-purchase.section-purchase__payment.active') do
    scroll_down(100)
  end

  within('.payment-method__option.payment-method__option--cimb_clicks') do
    find('.radio').click
  end

  within('.payment-method__option.payment-method__option--atm') do
    find('.radio').click
  end
end

def change_flow_mandiri
  within('.section-page.section-purchase.section-purchase__payment.active') do
    scroll_down(100)
  end

  within('.payment-method__option.payment-method__option--mandiri_clickpay') do
    find('.radio').click
  end

  within('.payment-method__option.payment-method__option--atm') do
    find('.radio').click
  end
end

def change_payment_next
  if(page.has_selector?('.js-encourage-deposit'))
    find('#js-purchase-encourage-deposit-next').click
  else
    find('#js-purchase-next').click
  end
end
