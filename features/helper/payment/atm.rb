def choose_atm
  choose 'Transfer'
  click_button 'Bayar'
  within('.block-payment-code__code.clearfix') do
    @unique_code = find('.block-payment-code--highlight').text
  end
  click_link 'Lihat Tagihan'
  sleep 5
end

def mweb_choose_atm
  within('#choose_payment_methods') do 
   find('.js-collapsible-list.clearfix' ,:text => 'Transfer').click
  end
  click_on('Lanjut')

  @unique_code = find('.block-payment-code__amount').text

  click_on('Lihat Tagihan')
  sleep 3
end
