def choose_mandiri_ecash
  within('.section-page.section-purchase.section-purchase__payment.active') do
    scroll_down(100)
  end

  within('.payment-method__option.payment-method__option--mandiri_ecash') do
    find('.radio').click
  end
  find('.btn.btn--block.btn--large.btn--red.js-iv-submit.js-purchase-submit').click
  sleep 5
end
