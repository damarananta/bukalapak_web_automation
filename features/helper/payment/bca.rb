def choose_bca
  within('.section-page.section-purchase.section-purchase__payment.active') do
    scroll_down(100)
  end

  within('.payment-method__option.payment-method__option--bca_klikpay') do
    find('.radio').click
  end
  find('.btn.btn--block.btn--large.btn--red.js-iv-submit.js-purchase-submit').click
  sleep 10
end


