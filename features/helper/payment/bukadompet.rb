def choose_bukadompet(password)
  choose 'BukaDompet'
  within('.payment-method__option.payment-method__option--deposit.active') do
    within('.text-green') do
      @deposit = change_into_integer(find('.amount').text)
    end
    @price = change_into_integer(find('.js-iv-review__deposit').text)
    within('.form-field-item.payment-info-content__deposit__password') do
      fill_in 'deposit_password', :with => password
    end
  end
  click_button 'Bayar'
  sleep 3
  expect(page).to have_content('BERHASIL!')
  sleep 3
  click_on('Lihat Tagihan')
  @invoice_id_regex = find(:xpath, '//*[@id="normal_page"]/section/div/div/div[3]/section[3]/div[3]/div/div/a').text
  @invoice_id = @invoice_id_regex.scan(/\d/).join
  sleep 2
end

def choose_force_bukadompet
  find('.btn.btn--red.btn--large.js-tfa-required-button').click
  expect(page).to have_content('BERHASIL!')
  click_on('Lihat Tagihan')
  sleep 2
end

def choose_bukadompet_mweb(password)  
  within('#choose_payment_methods') do
    @deposit = change_into_integer(first(:css, '.amount').text)
    @price = change_into_integer(find('.js-list-item--item.js-iv-review__total').text)
    within('.payment-terms.js-payment__deposit.js-payment__deposit--valid.active') do
      fill_in 'js-iv-deposit', :with => password
    end
  end
  payment_choosen_pay
  find_button('Lanjut').click
  sleep 3  
end
