def payment_first_page_click_next
  neutralize_ab_test("&ab_bukadompet=normal&ab_payment_view_v2=normal&ab_checkout_button=normal&checkout_button=normal")
  sleep 3
  scroll_down(100)

  if(page.has_selector?('.js-encourage-deposit'))
    find('#js-purchase-encourage-deposit-next').click
  else
    find('#js-purchase-next').click
  end
  sleep 3
end

def payment_multiple_order
  visit_url '/cart/carts'
  neutralize_ab_test("ab_select_cart=normal")

  within('.cart-utilities.clearfix') do
    find('#js-select-all').click
    sleep 3
  end

  click_button('Bayar Transaksi yang Dipilih')
  sleep 3
end
