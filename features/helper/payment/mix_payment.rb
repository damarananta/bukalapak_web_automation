def mix_payment_process
  @deposit_amount = find('.text-green.js-deposit-deduction-amount').text
    find('.purchase-page__toggle', :text => 'Bayar sebagian dengan BukaDompet?').click
    
  within ('#js-payment-field-mix__panel') do
    fill_in 'js-iv-mix-input', :with => '1000'
    find('#js-iv-mix-action').click
    expect(page).to have_content('Nominal akan digunakan ketika klik bayar', wait: 2)
    # Nominal disesuaikan metode pembayaran ini dan akan digunakan ketika klik bayar.
  end
end

def choose_mix_payment(payment)
  if payment == "atm"
    choose 'Transfer'
    
    mix_payment_process

    click_button 'Bayar'
    within('.block-payment-code__code.clearfix') do
      @unique_code = find('.block-payment-code--highlight').text
    end
    click_link 'Lihat Tagihan'
    sleep 3
  
  elsif payment == "mandiri"
    choose 'Mandiri Clickpay'

    mix_payment_process

    click_button 'Bayar'
    sleep 3

    fill_in 'payment_payment_card_number', :with => '1234567890'
    fill_in 'token_response', :with => '9810'

    click_button 'Bayar'
    click_link 'Lihat Tagihan'
    sleep 2

  elsif payment == "bca"
    within('.section-page.section-purchase.section-purchase__payment.active') do
      scroll_down(100)
    end

    within('.payment-method__option.payment-method__option--bca_klikpay') do
      find('.radio').click
    end
    
    mix_payment_process

    find('.btn.btn--block.btn--large.btn--red.js-iv-submit.js-purchase-submit').click
    sleep 8
    go_back

  elsif payment == "cimb"
    within('.section-page.section-purchase.section-purchase__payment.active') do
      scroll_down(100)
    end

    within('.payment-method__option.payment-method__option--cimb_clicks') do
      find('.radio').click
    end

    mix_payment_process

    find('.btn.btn--block.btn--large.btn--red.js-iv-submit.js-purchase-submit').click
    sleep 8

    find('.btn_cancel_cimbclicks').click
    modal_accept_confirm
    sleep 5

    visit_url '/payment/invoices?utf8=%E2%9C%93&payment_invoice_search_search%5Bkeywords%5D=&payment_invoice_search_search%5Bmode%5D=menunggu&payment_invoice_search_search%5Bhas_voucher%5D=0'

    find(:xpath, '//*[@id="nego_list"]/ul/li[1]/div[1]/h3/a').click
    sleep 5
    expect(page).to have_content('Menunggu Pembayaran')

  elsif payment == "alfamart"
    choose 'Alfamart'
    
    @deposit_amount = find('.text-green.js-deposit-deduction-amount').text
    find('.purchase-page__toggle', :text => 'Bayar sebagian dengan BukaDompet?').click
    
    mix_payment_process

    click_button 'Bayar'
    sleep 3
    click_on('Lihat Tagihan')
    sleep 5

  elsif payment == "indomaret"
    choose 'Indomaret'

    @deposit_amount = find('.text-green.js-deposit-deduction-amount').text
    find('.purchase-page__toggle', :text => 'Bayar sebagian dengan BukaDompet?').click
    
    mix_payment_process

    click_button 'Bayar'
    sleep 3
    click_on('Lihat Tagihan')
    sleep 5
  else
    return false
  end  
end
