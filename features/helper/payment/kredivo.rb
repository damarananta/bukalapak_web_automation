def choose_kredivo_30_day_payment
  within('.section-page.section-purchase.section-purchase__payment.active') do
    scroll_down(100)
  end

  within('.payment-method__option.payment-method__option--kredivo') do
      find('.radio').click
  end
  
  sleep 5
  
  page.execute_script('window.scrollBy(0, $(window).height()*100)')

  within('.payment-method-info.payment-info--kredivo.u-overflow-visible.active') do
    find('#kredivo_payment_type_chzn').click

    within('.chzn-drop') do
      find('li#kredivo_payment_type_chzn_o_0', :text => 'Bayar dalam 30 hari').click
    end

  end

  find('.btn.btn--block.btn--large.btn--red.js-iv-submit.js-purchase-submit').click
end
