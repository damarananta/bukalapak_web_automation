def use_voucher(voucher)
  neutralize_ab_test("&ab_bukadompet=normal&ab_payment_view_v2=normal&ab_checkout_button=normal&checkout_button=normal")
  sleep 3
  scroll_down(100)

  within('.js-iv-review__total-wrapper.active') do
    @total_before = change_into_integer(find('.order-review__total-fee--amount.js-list-item--item.js-iv-review__total').text)
  end

  within('.form-field-item.payment-field-voucher') do
    find('.purchase-page__toggle').click
  end

  within('.voucher-form-wrapper') do
    fill_in 'js-iv-voucher-input', :with => voucher
    sleep 2
    find('#js-iv-voucher-toggle').click
    find('.msg.text-green').text
  end

  within('.js-iv-review__total-wrapper.active') do
    @voucher = change_into_integer(find('.order-review__total-fee--amount.js-list-item--item.js-iv-review__total').text)
  end
  if(page.has_selector?('.js-encourage-deposit'))
    find('#js-purchase-encourage-deposit-next').click
  else
    find('#js-purchase-next').click
  end
  sleep 2  
end
