include RSpec::Matchers

def dismiss_abandoned_popup
  if(page.has_button?('close')) || page.has_css?('.mfp-close')
    if page.has_css?('.mfp-close')
      find('.mfp-close').click
    else
      click_button('close')
    end
  end
end

def close_cart_pop_up
  within('#cboxWrapper') do
    click_hidden('#cboxClose')
  end
end

def dismiss_ask_phonenumber
  if(page.has_button?('close')) || page.has_css?('.mfp-close')
    if current_path.include? '/p'
      find('.mfp-close').click
    else
      click_button('close')
    end
  end
end

def close_general_cart_popup
  if page.has_css?('#cboxClose')
    find('#cboxClose').click
  end
end

def visit_mobile_pelapak
  visit_split_true '/'
  accept_onboarding
  sleep 3
  page.execute_script "window.scrollBy(0,700)"
  find(:xpath, ".//*[@id='page']/section[2]/ul/li[1]/article/div[3]/div[1]/div/h5/a").click
  sleep 1
end

def detail_product
  sleep 1
  first('.product__name.line-clamp--2').click
  sleep 1
end

def assert_count_qty_cart
  if page.has_xpath?('.//*[@id]//input[@value="1"]')
    page.should have_xpath('.//*[@id]//input[@value="1"]')
  else
    page.should have_xpath('.//*[@id]//input[@value="2"]')
  end
end

def check_warning_stock
  if page.has_css?('.u-fg--red')
    @stock=find('.u-fg--red').text
    @stock=@stock.to_i
    puts "left stock #{@stock}"
  elsif page.has_css?('.text-orange')
    @stock=find('.text-orange').text
    @stock=@stock.to_i
    puts "left stock #{@stock}"
  else
    @stock=2
  end
end
