def get_vp_page_object(section)
  case section
  when "phone credits" then VPPhoneCreditLandingPage.new
  when "data plan" then VPDataPlanLandingPage.new
  when "electricity" then VPElectricityLandingPage.new
  when "game voucher" then VPGameVoucherLandingPage.new
  end
end

def get_new_vp_page_object(section)
  case section
  when "phone credits" then PhoneCreditNew.new
  when "electricity" then ElectricityNew.new
  when "game voucher" then GameVoucherNew.new
  end
end

def translate_vp_vocabs(vocab)
  case vocab.downcase
  when "pulsa" then "phone credits"
  when "token listrik" then "electricity"
  when "voucher game" then "game voucher"
  end
end
