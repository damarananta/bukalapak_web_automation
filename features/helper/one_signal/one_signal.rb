def dismiss_one_signal_popup
  click_button("No Thanks") if page.has_css?(".onesignal-popover-dialog")
end

def generate_random_string(number)
  charset = Array('A'..'Z') + Array('a'..'z')
  Array.new(number) { charset.sample }.join
end

def visit_split_true(url)
  visit "#{url}"+"?SPLIT_DISABLE=true"
end

def accept_onboarding
  click_button("Oke") if page.has_css?('.btn.btn--red.search-bar-overlay__button.js-search-overlay-close')
end

def accept_nego_popup
  find('#colorblox-close-button').click if page.has_css?('#colorblox-close-button')
end

def ingnore_notif_mobile
  if page.has_css?("#onesignal-popover-cancel-button")
    find('#onesignal-popover-cancel-button').click
  end
end
