def neutralize_ab_test(abtest_params)
  url = URI.parse(current_url)
  
  if url.query == nil
    url.query = abtest_params
  else
    url.query += abtest_params
  end

  visit_url "#{url.path}?#{url.query}"
end
