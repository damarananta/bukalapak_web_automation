def capture_dashed_filter_desc_wording
  @category=find(:xpath,'//tr[1]/td[1]').text

  @condition=find(:xpath,'//tr[1]/td[2]').text
  @condition=@condition.split.map(&:capitalize).join(' ')

  @price=find(:xpath,'//tr[1]/td[3]').text
  @price_number=@price.gsub(/[^\d]/, '')
  @price_text= @price.gsub("#{@price_number}",'')
  @price_text=@price_text.split.map(&:capitalize).join(' ')

  @location=find(:xpath,'//tr[1]/td[4]').text
  @location= @location.gsub("-",' ')
  @location= @location.split.map(&:capitalize).join(' ')

  @merk=find(:xpath,'//tr[1]/td[5]').text
  @merk=@merk.split.map(&:capitalize).join(' ')
end


def verify_dashed_filter_detail
  if(@merk== ''&& @price_text== '')
    @expect_combination_meta_desc="Jual beli #{@category}. Tersedia Produk #{@condition} aman dan mudah di #{@location}, jaminan uang kembali 100% di Bukalapak."
  elsif (@price_text== '' && @location== '')
    @expect_combination_meta_desc="Jual beli #{@category} #{@merk}. Tersedia #{@merk} #{@condition} aman dan mudah, jaminan uang kembali 100% di Bukalapak."
  elsif (@price_text== '')
    @expect_combination_meta_desc="Jual beli #{@category} #{@merk}. Tersedia #{@merk} #{@condition} aman dan mudah di #{@location}, jaminan uang kembali 100% di Bukalapak."
  elsif (@merk== '')
    @expect_combination_meta_desc="Jual beli #{@category}. Tersedia Produk #{@condition} #{@price_number} #{@price_text} aman dan mudah di #{@location}, jaminan uang kembali 100% di Bukalapak."
  else
    @expect_combination_meta_desc="Jual beli #{@category} #{@merk}. Tersedia #{@merk} #{@condition} #{@price_number} #{@price_text} aman dan mudah di #{@location}, jaminan uang kembali 100% di Bukalapak."
  end
  @expect_combination_meta_desc=@expect_combination_meta_desc.gsub(/  +/,' ')
  # metadesc content
  if(@meta_desc=='')
    page.should have_xpath("//meta[2][@content='#{@expect_combination_meta_desc}']",visible: false)
  else
    page.should have_xpath("//meta[2][@content='#{@meta_desc}']",visible: false)
  end

  #description
  if(@desc=='')
    description_detail=find('.bhlm-description').text
    description_detail.should==@expect_combination_meta_desc
  else
    description_detail=find('.bhlm-description').text
    description_detail.should==@desc
  end
end
