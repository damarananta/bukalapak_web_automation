def add_delimiter(amount)
  amount = amount.to_s
  amount.gsub(/(\d)(?=(\d{3})+(?!\d))/, "\\1.")
end

def change_into_integer(string)
	string.delete("Rp.").to_i
end