def restock(product_edit_url, seller_username, seller_password)
  visit_url '/'
  login(seller_username, seller_password)
  visit_url product_edit_url
  sleep 3
  fill_in 'product_stock', :with => 99999
  click_on 'Simpan'
  sleep 1
end
