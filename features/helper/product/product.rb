def add_product
  visit_url '/' if current_path != '/'
  find('#search_keywords').set(keyword)
  find(:xpath, '//*[@id="new_search"]/div[2]/button/span').click
  sleep 1
end

def create_product_name
  product_name = "tutup botol #{rand(100000)}"
end

def create_product_first_category
  first_category = "Hobi & Hiburan"
end

def create_product_second_category
  second_category = "Mainan"
end

def create_product_third_category
  third_category = "Others"
end

def create_product_weight
  weight = "1000"
end

def create_product_stock
  stock = "100"
end

def create_product_price
  price = "500000"
end

def create_product_description
  description = "Phasellus blandit leo ut odio. Praesent ac massa at ligula laoreet iaculis. Nulla facilisi. Vivamus consectetuer hendrerit lacus. Praesent nonummy mi in odio. Aenean ut eros et nisl sagittis vestibulum. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Duis vel nibh at velit scelerisque suscipit. Pellentesque ut neque. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo."
end
