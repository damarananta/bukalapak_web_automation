def mweb_single_product
  visit_url '/products/new?'
  fill_in 'product_name', :with => product_name
  find('.btn.btn--small.btn--gray.js-set-category-manually').click
  find('#first_category').click
  find('.option-icon.option-icon-perawatan-kecantikan').click
  page.driver.browser.all(:xpath, '//input[@name="product_images_upload[data]"]')[0].send_keys(File.absolute_path ('./features/support/image/tutup botol.jpg'))
  within ('.condition-input-product') do
    page.execute_script('window.scrollTo(0,600)')
    page.find(:xpath, '/html/body/div[3]/div[7]/div/div/div[1]/section/form/div[1]/div[5]/div/div/label[1]').click
  end
  fill_in 'product_price', :with => '12000000'
  fill_in 'product_weight', :with => '1200'
  fill_in 'product_description_bb', :with => 'test description product test bukalapak'
  find('.btn.btn--red.btn--medium.btn--hook-fbpixel.btn--commit.js-btn--submit').click
  page.driver.browser.switch_to.alert.dismiss
  expect(page).to have_selector(:link_or_button, 'Set Tidak Dijual')
end
