def visit_virtual_product(virtual_product)
  visit_url '/vp/voucher/pulsa'

  within('.c-list-ui.c-list-ui--link.c-list-ui--grouped.c-list-ui--vert.u-pad-top--0.u-pad-bottom--0')do
    click_link(virtual_product)
  end

  sleep 3
end


def mweb_visit_virtual_product(url)
  visit_url(url)
end

def payment_choosen_pay
  if page.has_button?('Lanjut')
    find_button('Lanjut').click
  else
    find_button('Bayar').click
  end
end

def wording_has_no_balance(page)
  wording = "Saldo BukaDompet-mu tidak mencukupi untuk membayar transaksi ini. Silakan pilih metode pembayaran lainnya"
  # wording = "Saldo BukaDompet-mu tidak mencukupi untuk transaksi ini." \
  #           " Silakan gunakan metode pembayaran lainnya."

  expect(page).to have_content(wording)
  expect(page).to have_no_content("BERHASIL!")
end

def mweb_wording_has_no_balance
  #expect(page).to have_content('Saldo BukaDompet-mu tidak mencukupi untuk membayar transaksi ini. Silakan pilih metode pembayaran lainnya')
  expect(page).to have_content('Saldo BukaDompet-mu tidak mencukupi untuk transaksi ini. Silakan gunakan metode pembayaran lainnya.')

  expect(page).to have_no_content('BERHASIL!')
end

# choose payment method and click pay button
def choose_payment_method(type, section)
  @vp_new_page ||= get_new_vp_page_object(section)
  @ebanking_redirect_page = EbankingRedirectPage.new

  @vp_new_page.pick_payment_method(type)

  if type == "bukadompet"
    if @vp_new_page.has_deposit_password_field?
      @vp_new_page.deposit_password_field.set(ENV['BUYER_PASSWORD'])
    end
    @vp_new_page.deposit_payment_button.click
  else
    @vp_new_page.normal_payment_button.click
  end

  if type == "bca klikpay"
    if @ebanking_redirect_page.has_ebanking_page_button?
      @ebanking_redirect_page.ebanking_page_button.click
    end

    click_button("bayar")
  end
end
