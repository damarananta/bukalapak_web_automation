def search_product(keyword)
  visit_split_true '/'
  dismiss_one_signal_popup
  sleep 1
  find('#search_keywords').send_keys(keyword, :enter)
  sleep 3
end

def search_product_mobile(keyword)
  visit '/'
  dismiss_one_signal_popup
  sleep 1
  find('#page_header > div.search-bar.active.js-search-bar.search-bar--auto-top > div > input').click
  find('#search-form-new-input').send_keys(keyword, :enter)
end
