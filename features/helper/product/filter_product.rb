def access_filter_page
  visit '/'
  dismiss_one_signal_popup
  dismiss_abandoned_popup
  find('.omnisearch__submit.c-omnisearch__button').click
  sleep 3
end

def filter_search_url(key,n,u,fsc,prov,city,courier,pmin,pmax,lte,gte,td,ins,ws,ts,ps,sb)
visit "/products?SPLIT_DISABLE=true?utf8=&search[keywords]=#{key}&search[new]=#{n}&search[used]=#{u}&search[free_shipping_coverage]=#{fsc}&search[province]=#{prov}&search[city]=#{city}&search[courier]=#{courier}&search[price_min]=#{pmin}&search[price_max]=#{pmax}&search[rating_array]=5+0+0+0+0&search[rating_gte]=#{gte}&search[rating_lte]=#{lte}&search[todays_deal]=#{td}&search[installment]=#{ins}&search[wholesale]=#{ws}&search[top_seller]=#{ts}&search[premium_seller]=#{ps}&search[sort_by]=#{sb}"
end

def filter_pelapak_url(pelapak,key,n,u,fsc,prov,city,courier,pmin,pmax,lte,gte,td,ins,ws,ts,ps,sb)
visit "/#{pelapak}/products?SPLIT_DISABLE=true?utf8=&search[keywords]=#{key}&search[new]=#{n}&search[used]=#{u}&search[free_shipping_coverage]=#{fsc}&search[province]=#{prov}&search[city]=#{city}&search[courier]=#{courier}&search[price_min]=#{pmin}&search[price_max]=#{pmax}&search[rating_array]=5+0+0+0+0&search[rating_gte]=#{gte}&search[rating_lte]=#{lte}&search[todays_deal]=#{td}&search[installment]=#{ins}&search[wholesale]=#{ws}&search[top_seller]=#{ts}&search[premium_seller]=#{ps}&search[sort_by]=#{sb}"
end
