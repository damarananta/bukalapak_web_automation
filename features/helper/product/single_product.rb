def single_product
  visit_url '/products/new?'
  find("label", text: "Baru", :match => :first).click
  fill_in 'product_name', :with => product_name
  find('.btn.btn--small.btn--gray.js-set-category-manually').click
  within('.form-field-item.no-category') do
    find('.chzn-single.chzn-default').click
    find('.active-result.option-icon.option-icon-perawatan-kecantikan').click
  end
  within('#second_category') do
    find('.chzn-single.chzn-default').click
    find('.active-result', :text => 'Perawatan Wajah').click
  end
  within('#third_category') do
    find('.chzn-single.chzn-default').click
    find('.active-result', :text => 'Masker Wajah Wanita').click
  end
  page.driver.browser.all(:xpath, '//input[@name="product_images_upload[data]"]')[0].send_keys(File.absolute_path ('./features/support/image/tutup botol.jpg'))
  fill_in 'product_weight', :with => '1200'
  fill_in 'product_price', :with => '12000000'
  fill_in 'product_description_bb', :with => 'test description product test bukalapak'
  find('.btn.btn--red.btn--large.btn--hook-fbpixel').click
end

def product_name
  firstword = ['tutup','buka','atas','depan','belakang','tinggi'].sample
  lastword = ['botol','pintu','langit','senja','jalan','sampah'].sample
  thirdword = ['merah','biru','ungu'].sample
  productname = firstword+" "+lastword+" "+thirdword
end
