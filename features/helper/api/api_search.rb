def api_product_search(keyword,category,user,campaign,favorite,negotiation,fit_price,ts,cond,fsc,prov,city,p_max,p_min,tod_d,tom_d,sb,pg,per_page,promoted)
  user_agent='Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:51.0) Gecko/20100101 Firefox/51.0'
  if(category!= '')
    path="category_id=#{category}?"
  elsif (user!= '')
    path="user_id=#{user}?"
  elsif (campaign!='')
    path="campaign_ids=#{campaign}?"
  elsif (favorite!='')
    path="favorited=#{favorite}?"
  end
  path="/products.json?#{path}&keywords=#{keyword}&nego=#{negotiation}&harga_pas=#{fit_price}&top_seller=#{ts}&conditions=#{cond}&free_shipping_coverage=#{fsc}&province=#{prov}&city=#{city}&price_max=#{p_max}&price_min=#{p_min}&todays_deal=#{tod_d}&tomorrows_deal=#{tom_d}&sort_by=#{sb}&page=#{pg}&per_page=#{per_page}&promoted=#{promoted}"
  puts "#{@api_url}#{path}"
  HTTParty.get(@api_url + path,basic_auth_api)
end
