def api_authentication(username, password)
  HTTParty.post("#{@api_url}/authenticate.json", {
                  basic_auth: {
                    username: username,
                    password: password
                  }
  })
end
