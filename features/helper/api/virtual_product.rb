def vp_api_data_plan_create(body = {})
  body["phone_number"] ||= ENV["BUYER_PHONE_NUMBER"]
  body["payment_method"] ||= "atm"
  body["deposit_password"] ||= ""

  HTTParty.post("#{@api_url}/data_plan_transactions.json", {
    body: body,
    basic_auth: vp_api_get_basic_auth_hash
  })
end

def vp_api_electricity_create(body = {})
  body["customer_number"] ||= ENV["BUYER_ELECTRICITY_NUMBER"]
  body["payment_payment[payment_method]"] ||= "atm"
  body["deposit_password"] ||= ""

  HTTParty.post("#{@api_url}/electricity_transactions.json", {
    body: body,
    basic_auth: vp_api_get_basic_auth_hash
  })
end

def vp_api_electricity_inquiry(number = ENV["BUYER_ELECTRICITY_NUMBER"], status = "OK")
  retval = nil

  3.times do |i|
    break if vp_api_get_electricity_inquiry_status(
      retval = request_api(
        "/electricity_transactions/inquiry.json?customer_number=#{number}"
      )
    ) == status
    puts "Attempt ##{i}: FAILED"
    sleep 3
  end

  retval
end

def vp_api_get_basic_auth_hash
  @auth ||= api_authentication(ENV["BUYER_USERNAME"], ENV["BUYER_PASSWORD"])
  { username: @auth["user_id"].to_s, password: @auth["token"] }
end

def vp_api_get_electricity_inquiry_status(request)
  JSON.parse(request.body)["status"]
end

def vp_api_phone_credit_create(body = {})
  body["phone_number"] ||= ENV["BUYER_PHONE_NUMBER"]
  body["payment_method"] ||= "atm"
  body["deposit_password"] ||= ""

  HTTParty.post("#{@api_url}/phone_credit_transactions.json", {
    body: body,
    basic_auth: vp_api_get_basic_auth_hash
  })
end

def vp_api_product_list(type)
  JSON.parse(vp_api_products(type).body)[
    case type
    when "phone credit" then "phone_credits"
    when "data plan" then "data_plans"
    when "electricity" then "electricities"
    end
  ]
end

def vp_api_products(type = nil)
  url = "/virtual_credits.json"
  url += "?type=#{type}" unless type.nil?
  request_api(url)
end

def vp_api_status_matcher(request, status)
  expect(vp_api_get_electricity_inquiry_status(request)).to eq(status)
end
