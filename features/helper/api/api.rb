def request_api(path)
  HTTParty.get(@api_url + path)
end

def get_request_api(path)
  HTTParty.get(@api_url + path,basic_auth_api)
end

def post_get_request_api(path)
  HTTParty.post(@api_url + path,basic_auth_api)
end

def basic_auth_api
  {
    basic_auth: {
      username: @auth["user_id"].to_s,
      password: @auth["token"]
    }
  }
end

def api_response_debug(status_code)
  if status_code ==200
    puts "success response"
  else
    puts @response.body
  end
end

def assert_status_code(response)
  json_response = JSON.parse(response)
  json_response['status'].should== 'OK'
  status_code=@response.code
  status_code.should==200
  api_response_debug(status_code)
end

def ascending? arr
  arr.each_cons(2).all?{|i,j| i <= j}
end

def descending? arr
  arr.each_cons(2).all?{|i,j| i >= j}
end
