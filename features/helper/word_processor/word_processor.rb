def capitalize_each_word(letter)
  letter.split.map(&:capitalize).join(' ')
end

def weight_normalizer(weight)
  weight = weight.to_i
  if weight < 1000
    weight = weight.to_s + " gram"
  elsif weight >= 1000
    weight = weight.to_s[0..-4] + ".0 kilogram"
  end
  return weight
end

def stock_normalizer(stock)
  stock = stock.to_i
  if stock > 50
    stock = "Tersedia > 50 stok barang"
  end
  return stock
end

def price_delimiter(price)
  price.to_s.reverse.gsub(/...(?=.)/,'\&.').reverse
end
