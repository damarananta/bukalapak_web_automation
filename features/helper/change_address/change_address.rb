def change_address
  neutralize_ab_test("&ab_bukadompet=normal&ab_payment_view_v2=normal&ab_checkout_button=normal&checkout_button=normal")
  sleep 2
  within ('.payment-address__wrapper') do
    find('#js_iv_address_id_chzn').click
    sleep 1
      if (page.has_selector?('#js_iv_address_id_chzn_o_1'))
        find('#js_iv_address_id_chzn_o_1').click
      else
        within('#cboxWrapper') do
          find('.btn.btn--gray.btn--large.tooltip.payment-new-address-link.js-update-address.tooltipstered').click
          fill_in('js-iv-new-name', :with => ENV['NAME_RECIPIENT'])
          fill_in('js-iv-new-phone', :with => ENV['PHONE_RECIPIENT'])
          find('#js_iv_new_province_chzn').click
          find('#js_iv_new_province_chzn_o_21').click
          sleep 1

          within('.select.optional.user_address_address_city') do
            find('#js_iv_new_city_chzn').click
            find('#js_iv_new_city_chzn_o_17').click
          end
      
          within('.form-field-item.js-new-address-area-field') do
            find('#js_iv_new_area_chzn').click
            find('#js_iv_new_area_chzn_o_1').click
          end

          within('.form-field-item.payment-popup-address__address') do
            fill_in('js-iv-new-address', :with => ENV['ADDRESS_RECIPIENT'])
          end
          
          find('#js_iv_new_post_code_chzn').click
          find('#js_iv_new_post_code_chzn_o_1').click

          find('js-update-address-submit btn btn--green btn--large float-right').click
        end
      end
  end
end
