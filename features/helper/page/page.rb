def scroll_down(range)
  page.execute_script "window.scrollBy(0,#{range})"
end

def go_back
  page.evaluate_script('window.history.back()')
end

def modal_accept_confirm
  page.driver.browser.switch_to.alert.accept rescue Selenium::WebDriver::Error::NoAlertOpenError
end

def click_hidden(css)
  sleep 3
  page.execute_script("$('#{css}').click()")
  sleep 3
end

def close_window
  page.execute_script "window.close();"
end

def expect_flash_message_to_have(flash)
  within(:css, ".c-notification.c-notification--flash.c-notification--success.flash.flash--success") do
    expect(page).to have_content flash
  end
end

def expect_navbar_have_to_have(content)
  within('.c-nav-list__item.js-nav-helper__item--username') do
    expect(page).to have_content content
  end
end

def scroll_to(element, is_top)
  # if is_top true >> scroll top of element to top of screen
  # if is_top false >> scroll bottom of element to bottom of screen
  script = <<-JS
  arguments[0].scrollIntoView(arguments[1]);
  JS
  page.driver.browser.execute_script(script, element.native, is_top)
end
