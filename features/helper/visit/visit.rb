def visit_url(url)
  visit url
  dismiss_one_signal_popup
  dismiss_abandoned_popup
  sleep 1
end
