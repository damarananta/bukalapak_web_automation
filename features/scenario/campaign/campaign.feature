@dtb3 @desktop
Feature: DTB-Dashboard Campaign
  Admin should able to create, update, delete and filter active campaign
 
  @campaign @add-new-campaign
  Scenario: Admin create new campaign normal case
    When Admin login to dashboard
    Then Admin go to Manage Discount & Campaign page
    Then access campaign setting
    When delete campaign if exist
    Then click create new campaign button
    Given campaign title "Test_Title_"
    Given campaign upload banner "960x150.jpg"
    Given campaign banner open new window "1"
    Given campaign redirect url "RedirectUrl"
    Given campaign tooltip "Tooltip"
    Given campaign meta description "MetaDesc"
    Given campaign start time "28"
    Given campaign end time "29"
    Given campaign description "Desc"
    Given campaign sub desc "Sub Desc"
    Then click button save campaign
    Then create new campaign should success

  @campaign @edit-campaign
  Scenario: Admin edit campaign normal case
    When Admin login to dashboard
    Then Admin go to Manage Discount & Campaign page
    Then access campaign setting
    Then click edit campaign button
    Given campaign title "Test_Title_update"
    Given campaign upload banner "960x150.jpg"
    Given campaign banner open new window "1"
    Given campaign redirect url "RedirectUrl_update"
    Given campaign tooltip "Tooltip_update"
    Given campaign meta description "MetaDesc_update"
    Given campaign start time "28"
    Given campaign end time "29"
    Given campaign description "Desc_update"
    Given campaign sub desc "Sub Desc_update"
    Then click button save campaign
    Then edit campaign should success

  @campaign @filter-campaign
  Scenario: Admin filter campaign list and should get active campaign result
    When Admin login to dashboard
    Then Admin go to Manage Discount & Campaign page
    Then access campaign setting
    Then click checkbox active campaign
    Then click filter campaign button
    Then filter active campaign should success


  @campaign @delete-campaign
  Scenario: Admin delete campaign
    When Admin login to dashboard
    Then Admin go to Manage Discount & Campaign page
    Then access campaign setting
    When delete campaign if exist
    Then delete campaign should success
