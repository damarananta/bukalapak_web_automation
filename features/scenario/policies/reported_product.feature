@ts @product_action @desktop @ts-8
Feature: TS - Reported and Unreported Product by Admin

  Admin can report and unreport product

  @reported_product
  Scenario: Admin report product

    Given there is admin want reported product 
    When admin report the product
    Then admin see text Produk telah dilaporkan
    
  @unreported_product
  Scenario: Admin unreported product

    Given there is admin want reported product 
    When admin unreported the product
    Then admin see text Produk telah di-unhide
