@dtb4 @pelapak @mobile
Feature: DTB-Mobile Pelapak Page

  @detail-pelapak
  Scenario: Visit pelapak page 

    When User navigate to "ragapinilih" page via mweb 
    Then User should see detail information about pelapak via mweb

  @search-pelapak-product
  Scenario: Search pelapak product

    When User navigate to "ragapinilih" page via mweb 
    Given "Testing 30" to the search box seller page via mweb
    Then "Testing 30" should be mentioned in the seller page results via mweb

  @pelapak-label
  Scenario: Visit pelapak page & filter by label

    When User navigate to "ragapinilih" page via mweb 
    Then User select pelapak label via mweb
      And User should get product count "5" via mweb
