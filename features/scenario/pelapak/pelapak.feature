@dtb5 @pelapak @desktop
Feature: DTB-Desktop Pelapak Page

  @pelapak-label
  Scenario: User go to pelapak page. User should able to click product label 

    When User navigate to "niken" page
    When User click label "4" 
    Then User should get product count "3"
    When User click label "5"
    Then User should get product count "1"
   
   @pelapak-search
   Scenario: Search Pelapak product

    When User navigate to "niken" page
    Given "Testing" to the search box seller page
    Then "Testing" should be mentioned in the seller page results
