@vp @api @dataplan
Feature: VP-Data Plan API 

  Scenario: Buy data plan
    When client requests create "data plan" transaction
    Then VP API transaction status should be "OK"
    And VP API transaction could be found in transactions list
