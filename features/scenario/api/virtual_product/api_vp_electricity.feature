@vp @api @electricity
Feature: VP-Electricity API

  @inquiry
  Scenario: Customer number inquiry
    When client requests registered customer number inquiry
    Then electricity API inquiry response should be "OK"

  @inquiry
  Scenario: Wrong customer number inquiry
    When client inquires "01428800709" as unregistered electricity customer number
    Then electricity API inquiry response should be "ERROR"

  Scenario: Buy electricity
    When client requests create "electricity" transaction
    Then VP API transaction status should be "OK"
    And VP API transaction could be found in transactions list
