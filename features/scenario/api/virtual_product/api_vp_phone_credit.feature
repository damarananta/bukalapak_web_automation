@vp @api @phonecredit
Feature: VP-Phone Credit API

  Scenario: Buy phone credit
    When client requests create "phone credit" transaction
    Then VP API transaction status should be "OK"
    And VP API transaction could be found in transactions list
