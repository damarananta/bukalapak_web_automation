Feature: DTB-API Cart
  
  @dtb @smoke-test-api @api-add-to-cart
  Scenario: Verify add to cart should success(authenticate)
    When User authenticate to API with username "lamhotjmsqatest5" password "123456"
    When the client requests POST /carts/add_product/53k8mq?&quantity=1.json
    When the authenticate client requests GET /carts.json
    Then the response should success

  @dtb @smoke-test-api @api-update-cart
  Scenario:  Verify update cart should success and cart items qty
    When User authenticate to API with username "lamhotjmsqatest5" password "123456"
    When the authenticate client requests GET /carts.json
    Then fetch cart item id
    When the client requests update cart items with quantity "3"
    When the authenticate client requests GET /carts.json
    Then the response should success

  @dtb @smoke-test-api @api-delete-cart
  Scenario: Verify fetch cart should success and cart items data (authenticate)
    When User authenticate to API with username "lamhotjmsqatest5" password "123456"
    When the authenticate client requests GET /carts.json
    Then fetch cart item id
    When the client requests DELETE cart item
    When the authenticate client requests GET /carts.json
    Then the response should success
