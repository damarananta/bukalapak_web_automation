@dtb4
Feature: DTB-API Cart
  
  @api @api-view-cart
  Scenario: Verify fetch cart should success and empty(not authenticate)
    When the client requests GET /carts.json
    Then cart respond code should 200 and empty
   
  @api @api-add-to-cart
  Scenario: Verify add to cart should success(authenticate)
    When User authenticate to API with username "lamhot" password "lamhot"
    When the client requests POST /carts/add_product/13?&quantity=1.json
    When the authenticate client requests GET /carts.json
    Then response should return cart items with quantity "1"

  @api @api-view-cart
  Scenario: Verify fetch cart should success and cart items qty
    When User authenticate to API with username "lamhot" password "lamhot"
    When the authenticate client requests GET /carts.json
    Then response should return cart items with quantity "1"

  @api @api-update-cart
  Scenario:  Verify update cart should success and cart items qty
    When User authenticate to API with username "lamhot" password "lamhot"
    When the authenticate client requests GET /carts.json
    Then fetch cart item id
    When the client requests update cart items with quantity "3"
    When the authenticate client requests GET /carts.json
    Then response should return cart items with quantity "3"

  @api @api-delete-cart
  Scenario: Verify fetch cart should success and cart items data (authenticate)
    When User authenticate to API with username "lamhot" password "lamhot"
    When the authenticate client requests GET /carts.json
    Then fetch cart item id
    When the client requests DELETE cart item
    When the authenticate client requests GET /carts.json
    Then cart respond code should 200 and null