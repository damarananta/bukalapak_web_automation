@api-chat
Feature: DTB-API Chat

  @dtb @smoke-test-api @api-create-chat
  Scenario: Client requests creating new messages
    When User authenticate to API with username "lamhotjmsqatest1" password "123456"
    When User request send message to "26095163"

  @dtb @smoke-test-api @api-fetch-chat
  Scenario: Client requests conversation list
    When User authenticate to API with username "lamhotjmsqatest2" password "123456"
    When the authenticate client requests GET /messages.json
    Then inbox should not empty
    Then the response should success

  @dtb @smoke-test-api @api-detail-chat
  Scenario: Client requests detail consversation
    When User authenticate to API with username "lamhotjmsqatest2" password "123456"
    When the authenticate client requests GET /messages.json
    Then detail chat should not empty

  @dtb @smoke-test-api @api-delete-chat
  Scenario: Client requests delete spesific conversation
    When User authenticate to API with username "lamhotjmsqatest2" password "123456"
    When the authenticate client requests GET /messages.json
    When the authenticate client requests DELETE conversation
    Then inbox should be empty