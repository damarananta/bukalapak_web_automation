@dtb4 @api-trending-keywords
Feature: DTB-API Trending Keywords
  Get trending keywords

  @desktop @fake-trending-keywords
  Scenario: create 6 fake trending keywords
    When Admin login to dashboard
    When Admin go to trending keyword dashboard
    Then create 6 fake trending keywords

  @api @api-get-trending-keywords
  Scenario: Get trending keywords (use token)
    When User authenticate to API with username "lamhot" password "lamhot"
    When the authenticate client requests GET /trending_keywords.json
    Then the response should success
    Then trending keywords response count should "5"
    
  @api @api-get-trending-keywords
  Scenario: Get trending keywords
    When the client requests GET /trending_keywords.json?limit=6
    Then the response should success
    Then trending keywords response count should "6"
