Feature: DTB-API Get Product Detail
  
  @dtb @smoke-test-api @product-detail
  Scenario: Get product detail
    When the client requests GET /products/53k8mq.json
    Then the response should return product detail

  @dtb4 @api @product-detail
  Scenario: Get product detail
    When the client requests GET /products/h.json
    Then the response should return product detail

  @dtb4 @api @product-detail
  Scenario: Get product detail (not found)
    When the client requests GET /products/notfound.json
    Then the response should return product not found
