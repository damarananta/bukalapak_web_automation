Feature: DTB-API Get Product List
  
  @dtb @smoke-test-api @product-list
  Scenario Outline: Get product list
     When the client requests GET <Product>
     Then the response should success

    Examples:
    |Product                    |                      
    |/products.json             |
    |/products.json?keywords=mtb|


  @dtb @smoke-test-api @product-list
  Scenario Outline: Get product list
     When the client requests GET <Product>
     Then the response should success
     Then the product list response count should <Count>

    Examples:
    |Product                                           |Count|
    |/products.json?keywords=mtb&page=1&per_page=15    |15   |
    |/products.json?page=1&per_page=10                 |10   |
