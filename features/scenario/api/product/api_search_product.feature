Feature: DTB-API Search & Filter Product

  @dtb @smoke-test-api @api-promoted
  Scenario Outline: Search Test Pagination
    When User authenticate to API with username "lamhotjmsqatest5" password "123456"
    When the client requests search with <Keyword> <Category> <User> <Campaign> <Favorite> <Nego> <FP> <TS> <Cond> <Fsc> <Prov> <City> <P_Max> <P_Min> <Tod_D> <Tom_D> <SB> <PG> <PP> <Promoted>
    Then the response should success
    Then the product list response count should <Count>

   Examples:
    |Keyword|Category|User|Campaign|Favorite|Nego|FP|TS|Cond|Fsc|Prov|City|P_Max|P_Min|Tod_D|Tom_D|SB|PG|PP|Count|Promoted|
    |baju   |		     |    |        |        |    |  |  |    |   |    |    |     |     |     |     |  |  |  |30   |0       |
    |baju   |        |    |        |        |    |  |  |    |   |    |    |     |     |     |     |  |2 |10|10   |1       |
  
   @dtb @smoke-test-api @api-product-search
   Scenario Outline: Test Price Range, Test Used/New Product, Sort by Terbaru,Acak
    When User authenticate to API with username "lamhotjmsqatest5" password "123456"
    When the client requests search with <Keyword> <Category> <User> <Campaign> <Favorite> <Nego> <FP> <TS> <Cond> <Fsc> <Prov> <City> <P_Max> <P_Min> <Tod_D> <Tom_D> <SB> <PG> <PP> <Promoted>
    Then the response should success
    Then the product list response count should <Count>
    Then the product list condition response should <Cond>
    Then the product list price range response should <P_Min> to <P_Max>
    Then the product list province and city response should <Prov> and <City>
    Then the product list price response should sort by <SB>

   Examples:
    |Keyword|Category|User|Campaign|Favorite|Nego|FP|TS|Cond|Fsc|Prov       |City         |P_Max |P_Min |Tod_D|Tom_D|SB      |PG|PP|Count|Promoted|
    |       |		     |    |        |        |    |  |  |used|   |DKI+Jakarta|Jakarta+Barat|500000|100000|     |     |Terbaru |1 |5 |5    |0       |
    |       |		     |    |        |        |    |  |  |new |   |DKI+Jakarta|Jakarta+Barat|100000|50000 |     |     |Acak    |2 |10|10   |0       |
    |       |		     |    |        |        |    |  |  |used|   |DKI+Jakarta|Jakarta+Barat|453440|100000|     |     |Termahal|1 |5 |5    |0       |
    |       |		     |    |        |        |    |  |  |new |   |DKI+Jakarta|Jakarta+Barat|878888|50000 |     |     |Termurah|2 |10|10   |0       |

  @dtb4 @api @api-search-staging
  Scenario Outline: Search Test Pagination
    When User authenticate to API with username "lamhot" password "lamhot"
    When the client requests search with <Keyword> <Category> <User> <Campaign> <Favorite> <Nego> <FP> <TS> <Cond> <Fsc> <Prov> <City> <P_Max> <P_Min> <Tod_D> <Tom_D> <SB> <PG> <PP> <Promoted>
    Then the response should success
    Then the product list response count should <Count>

   Examples:
    |Keyword|Category|User|Campaign|Favorite|Nego|FP|TS|Cond|Fsc|Prov|City|P_Max|P_Min|Tod_D|Tom_D|SB|PG|PP|Count|Promoted|
    |Testing|        |    |        |        |    |  |  |    |   |    |    |     |     |     |     |  |  |10|0    |1       |
    |Testing|        |    |        |        |    |  |  |    |   |    |    |     |     |     |     |  |  |10|10   |0       |

