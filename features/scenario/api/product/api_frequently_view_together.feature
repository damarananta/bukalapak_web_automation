Feature: DTB-API Get Frequently View Together (FVT) API
  
  @dtb @smoke-test-api @fvt
  Scenario Outline: Get FVT
     When the client requests GET <Product>
     Then the response should success
     Then the response count should <Count>

    Examples:
    |Product                                   |Count|
    |/products/1vuxlv/fvt.json                 |6    |
    |/products/1vuxlv/fvt.json?offset=0&limit=1|1    |
