Feature: DTB-API Get Frequently Bought Together (FBT) API
  
  @dtb @smoke-test-api @fbt
  Scenario Outline: Get FBT
     When the client requests GET <Product>
     Then the response should success
     Then the response count should <Count>

    Examples:
    |Product                                   |Count|
    |/products/1vuxlv/fbt.json                 |6    |
    |/products/1vuxlv/fbt.json?offset=0&limit=7|7    |
    |/products/1vuxlv/fbt.json?offset=0&limit=1|1    |
