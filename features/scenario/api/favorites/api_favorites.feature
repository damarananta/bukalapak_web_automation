@dtb4 @api-favorites
Feature: DTB-API Favorites
     
 @api @api-view-favorites
  Scenario: View favorites with empty favorite product should return fill products
    When User authenticate to API with username "lamhot" password "lamhot"
    When the authenticate client requests GET /favorites.json
    Then favorites products count should "0"
    And fill products should not empty

  @api @api-add-favorites
  Scenario: Add product to favorite list
    When User authenticate to API with username "lamhot" password "lamhot"
    When the client requests POST /favorites/13/add.json

  @api @api-username-favorites
  Scenario: View username favorites
    When the client requests GET /favorites/lamhot.json
    Then favorites products count should "1"

  @api @api-remove-favorites
  Scenario: remove favorites
    When User authenticate to API with username "lamhot" password "lamhot"
    When the client requests DELETE favorites product "13"
    When the authenticate client requests GET /favorites.json
    Then favorites products count should "0"

  @api @api-bulk-favorites
  Scenario: add bulk favorites
    When User authenticate to API with username "lamhot" password "lamhot"
    When the client requests add bulk favorites product "13" "w"
    When the authenticate client requests GET /favorites.json
    Then favorites products count should "2"

  @api @api-bulk-favorites
  Scenario: remove bulk favorites
    When User authenticate to API with username "lamhot" password "lamhot"
    When the client requests remove bulk favorites product "13" "w"
    When the authenticate client requests GET /favorites.json
    Then favorites products count should "0"
