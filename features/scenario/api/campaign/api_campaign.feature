Feature: DTB-API Get Campaign info
  Gets list of available campaigns.
 
  @dtb @smoke-test-api @api-campaign
  Scenario: Get campaign info
    When the client requests GET /deals/campaigns.json
    Then the response should success
