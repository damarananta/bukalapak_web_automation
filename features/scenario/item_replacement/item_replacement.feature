@desktop
Feature: TRXS-Item Replacement 

  Administrator should find Item replacement for transaction that rejected by seller 

@trxs @replacements @adminreplacements
Scenario: IR rejected and replace manually 
  There is a transaction that rejected by seller and user agreed to replace the items with the same product but from different seller 

  Given there is seller
  And there is a Secondary seller
  And there is buyer
  And there is a transaction and the state is paid
  And seller reject the transaction 
  And administrator supervisor find another seller to replace buyers desired items 
  And buyer get the new seller 
  And buyers get charged from first seller items price
  Then money will be remitted to Secondary seller 

@trxs @replacements @assignedreplacement
Scenario: IR rejected and replace by assigned administrator 

  There is a transaction that rejected by seller and user agreed to replace the items with the same product but from different seller. Then customer support supervisor assigned staff to processed the replacement 

  Given there is seller
  And there is a Secondary seller 
  And there is buyer
  And there is a transaction and the state is paid
  And seller reject the transaction 
  And administrator supervisor assigned staff to processed the replacement 
  And administrator staff find another seller to replace buyers desired items
  And buyer get the new seller
  And buyers get charged from first seller items price
  Then money will be remitted to Secondary seller 
  