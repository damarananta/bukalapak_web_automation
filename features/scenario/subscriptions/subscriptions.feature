@dtb5 @subsriptions @desktop
Feature: DTB-Desktop Subscriptions & Timeline

  @visit-subscriptions
  Scenario: Visit subscriptions (empty) 
    
    Given User login to bukalapak with usename "nadiaseptia42641" password "123456"
    When User navigate to subscriptions page
    Then User should see subscriptions page
       And User should see breadcum level3 "Lapak langganan"
    When User click Timeline tab
    Then User should see empty info "Belum ada aktivitas"
       And User should see breadcum level3 "Timeline langganan"
    When User click Daftar Pelanggan tab
    Then User should see empty info "Belum memiliki pelanggan."
       And User should see breadcum level3 "Daftar pelanggan"

  @list-subscriptions
  Scenario: Visit subscriptions (fetch subscriptions list,timeline list, & subscriber list)
    
    Given User login to bukalapak with usename "lamhot" password "lamhot"
    When User navigate to subscriptions page
    Then User should see subscription list
    Given "niken" to the search box subscriptions page
    Then "niken" should be mentioned in the subscriptions tab results
    When User click Timeline tab
    Then User should see timeline list
    When User click Daftar Pelanggan tab
    Then User should see pelanggan list
    Given "seller2" to the search box subscriptions page
    Then "seller2" should be mentioned in the Daftar Pelanggan tab results
    