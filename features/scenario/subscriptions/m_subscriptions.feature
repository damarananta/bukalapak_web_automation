@dtb4 @subsriptions @mobile
Feature: DTB-Mobile Subscriptions & Timeline

  @visit-subscriptions
  Scenario: Visit subscriptions (empty) 
    
    Given User login to mweb bukalapak with usename "nadiaseptia42641" password "123456"
    When User navigate to subscriptions page via mweb
    Then User should see subscriptions page via mweb
       And User should see breadcum level2 "Lapak langganan" via mweb
    When User click Timeline tab via mweb
    Then User should see empty info "Belum memiliki lapak langganan." via mweb
       And User should see breadcum level2 "Timeline langganan" via mweb
    When User click Daftar Pelanggan tab via mweb
    Then User should see empty info "Belum memiliki pelanggan." via mweb
       And User should see breadcum level2 "Daftar pelanggan" via mweb

  @list-subscriptions
  Scenario: Visit subscriptions (fetch subscriptions list,timeline list, & subscriber list)
    
    Given User login to mweb bukalapak with usename "lamhot" password "lamhot" 
    When User navigate to subscriptions page via mweb
    Then User should see subscription list via mweb
    Given "niken" to the search box subscriptions page via mweb
    Then "niken" should be mentioned in the subscriptions tab results via mweb
    When User click Timeline tab via mweb
    Then User should see timeline list via mweb
    When User click Daftar Pelanggan tab via mweb
    Then User should see pelanggan list via mweb
    Given "seller2" to the search box subscriptions page via mweb
    Then "seller2" should be mentioned in the Daftar Pelanggan tab results via mweb
