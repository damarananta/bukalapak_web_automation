@dtb2 @desktop @dashed-filter-merk @dashed-filter
Feature: DTB-Dashboard Dashed Filter Merk

  Admin should able to do crud action on merk dashed filter page

  Scenario Outline:Admin create new dashed filter merk
    
    When Admin login to dashboard
    And Admin go to dashed filter page
    Then click dashed filter merk link
    Then Admin click add button
    Given merk url value <Url>
    Given merk name value <Name>
    When Admin click simpan dashed filter
    Then create dashed filter merk should success
  
  Examples:
    |Url           |Name   |
    |samsung-kamera|Samsung|

  Scenario Outline: Admin update dashed filter merk
    
    When Admin login to dashboard
    And Admin go to dashed filter page
    Then click dashed filter merk link
    When Admin click edit button merk dashed filter 
    Given merk url value <Url>
    Given merk name value <Name>
    When Admin click simpan dashed filter
    Then update dashed filter merk should success
  
  Examples:
    |Url   |Name  |
    |lenovo|Lenovo|
  
   @dtb @dashed-filter-merk @dashed-filter
   Scenario: Filter/search dashed filter merk
   
    When Admin login to dashboard
    And Admin go to dashed filter page
    Then click dashed filter merk link
    When Admin search dashed filter general with url "lenovo"
    And click button filter 
    Then Admin should get result dashed filter general with url "lenovo"
  
   @dtb @dashed-filter-merk @dashed-filter
   Scenario: Admin delete dashed filter merk
    
    When Admin login to dashboard
    And Admin go to dashed filter page
    Then click dashed filter merk link
    Then Admin click delete button dashed filter
    Then delete dashed filter merk should success
