@dtb2 @desktop @dashed-filter-location @dashed-filter
Feature: DTB-Dashboard Dashed Filter Location

  Admin should able to do crud action on location dashed filter page

  Scenario Outline:Admin create new dashed filter location

    When Admin login to dashboard
    And Admin go to dashed filter page
    Then click dashed filter location link
    Then Admin click add button
    Given url location value <Url>
    Given province location <Province>
    Given city location value <City>
    When Admin click simpan dashed filter
    Then create dashed filter location should success
  
  Examples:
    |Url    |Province   |City           |
    |jakarta|DKI Jakarta|Jakarta Selatan|

  Scenario Outline: Admin update dashed filter location

    When Admin login to dashboard
    And Admin go to dashed filter page
    Then click dashed filter location link
    When Admin click edit button location dashed filter 
    Given url location value <Url>
    Given province location <Province>
    Given city location value <City>
    When Admin click simpan dashed filter
    Then update dashed filter location should success
  
  Examples:
    |Url  |Province      |City  |
    |medan|Sumatera Utara|Medan |

  Scenario: Filter/search dashed filter location
   
    When Admin login to dashboard
    And Admin go to dashed filter page
    Then click dashed filter location link
    When Admin search dashed filter general with url "medan"
    And click button filter 
    Then Admin should get result dashed filter general with url "medan"

  Scenario: Admin delete dashed filter location
    When Admin login to dashboard
    And Admin go to dashed filter page
    Then click dashed filter location link
    Then Admin click delete button dashed filter
    Then delete dashed filter location should success
