@dtb2 @desktop @dashed-filter-price @dashed-filter
Feature: DTB-Dashboard Dashed Filter Price

  Admin should able to do crud action on price dashed filter page

  Scenario Outline:Admin create new dashed filter price

    When Admin login to dashboard
    And Admin go to dashed filter page
    Then click dashed filter price link
    Then Admin click add button
    Given url price value <Url>
    Given min price value <Min_Price>
    Given max price value <Max_Price>
    When Admin click simpan dashed filter
    Then create dashed filter price should success
  
  Examples:
    |Url      |Min_Price  |Max_Price|
    |loro-juta|1000000    |2000000  |

  Scenario Outline: Admin update dashed filter price

    When Admin login to dashboard
    And Admin go to dashed filter page
    Then click dashed filter price link
    When Admin click edit button price dashed filter 
    Given url price value <Url>
    Given min price value <Min_Price>
    Given max price value <Max_Price>
    When Admin click simpan dashed filter
    Then update dashed filter price should success
  
  Examples:
    |Url      |Min_Price  |Max_Price|
    |tolu-juta|3000000    |4000000  |

  Scenario: Filter/search dashed filter location
   
    When Admin login to dashboard
    And Admin go to dashed filter page
    Then click dashed filter price link
    When Admin search dashed filter general with url "tolu-juta"
    And click button filter 
    Then Admin should get result dashed filter general with url "tolu-juta"

  Scenario: Admin delete dashed filter price

    When Admin login to dashboard
    And Admin go to dashed filter page
    Then click dashed filter price link
    Then Admin click delete button dashed filter
    Then delete dashed filter price should success
