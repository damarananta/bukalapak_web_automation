@dtb6 @desktop @dashed-filter-page @dashed-filter
Feature: DTB-Dashboard Dashed Filter Page

  Admin should able to do crud action on dashed filter page

  @dashed-filter-page @dashed-filter
  Scenario Outline: Verify meta description and description

    When Admin login to dashboard
    And Admin go to dashed filter page
    Then Admin click delete button dashed filter
    Then Admin click add button
    Given category page value <Category>
    Given condition page option <Condition>
    Given price page value <Price>
    Given location page value <Location>
    Given merk page value <Merk>
    #Given attach banner page file <Banner_File>
    Given meta desc dashed filter text <Meta_Desc>
    Given page dashed filter text <Desc>
    When Admin click simpan dashed filter page
    Then create dashed filter page should success
    Then capture one of dashed filter page wording
    When Admin click detail dashed filter page
    Then dashed filter page detail should contain page wording

  Examples:
    |Category|Condition|Price    |Location       |Merk|Banner_File |Meta_Desc       |Desc            |      
    |2       |3        |500ribuan|jakarta-selatan|acer|1400x300.jpg|Lorem Ipsum meta|                |
    |2       |3        |500ribuan|jakarta-selatan|acer|1400x300.jpg|                |Lorem Ipsum desc|

  Scenario Outline: When Admin create new dashed filter page success condition
    
    When Admin login to dashboard
    And Admin go to dashed filter page
    Then Admin click delete button dashed filter
    Then Admin click add button
    Given category page value <Category>
    Given condition page option <Condition>
    Given price page value <Price>
    Given location page value <Location>
    Given merk page value <Merk>
    #Given attach banner page file <Banner_File>
    Given meta desc dashed filter text <Meta_Desc>
    Given page dashed filter text <Desc>
    When Admin click simpan dashed filter page
    Then create dashed filter page should success
    Then capture one of dashed filter page wording
    When Admin click detail dashed filter page
    Then dashed filter page detail should contain page wording

  Examples:
    |Category|Condition|Price    |Location|Merk  |Banner_File |Meta_Desc            |Desc            |      
    |183     |1        |         |        |iphone|1400x300.jpg|Lorem Ipsum meta desc|Lorem Ipsum desc|
    |183     |1        |500ribuan|        |      |1400x300.jpg|Lorem Ipsum meta desc|Lorem Ipsum desc|
    |183     |2        |         |        |acer  |1400x300.jpg|                     |                |               

   Scenario Outline: When Admin create new dashed filter page fail condition

    When Admin login to dashboard
    And Admin go to dashed filter page
    Then Admin click add button
    Given category page value <Category>
    Given condition page option <Condition>
    Given price page value <Price>
    Given location page value <Location>
    Given merk page value <Merk>
    #Given attach banner page file <Banner_File>
    Given meta desc dashed filter text <Meta_Desc>
    Given page dashed filter text <Desc>
    When Admin click simpan dashed filter page
    Then create dashed filter page should fail
  
  Examples:
    |Category|Condition|Price  |Location       |Merk  |Banner_File|Meta_Desc            |Desc            |      
    |183     |2        |2jutaan|jakarta selatan|iphone|960x150.jpg|Lorem Ipsum meta desc|Lorem Ipsum desc|

   
   Scenario Outline: When Admin update dashed filter page success condition
   
    When Admin login to dashboard
    And Admin go to dashed filter page
    Then Admin click edit button page dashed filter
    Given condition page option <Condition>
    Given price page value <Price>
    Given location page value <Location>
    Given merk page value <Merk>
    #Given attach banner page file <Banner_File>
    Given meta desc dashed filter text <Meta_Desc>
    Given page dashed filter text <Desc>
    When Admin click simpan dashed filter page
    Then update dashed filter page should success
    Then capture one of dashed filter page wording
    When Admin click detail dashed filter page
    Then dashed filter page detail should contain page wording
  
  Examples:
    |Category|Condition|Price    |Location     |Merk  |Banner_File |Meta_Desc                   |Desc                   |      
    |183     |2        |500ribuan|jakarta-barat|iphone|1400x300.jpg|Lorem Ipsum meta desc update|Lorem Ipsum desc update|


    Scenario: Filter/search dashed filter page
   
     When Admin login to dashboard
     And Admin go to dashed filter page
     When Admin search dashed filter page with param "Buku" "Baru" "500ribuan" "jakarta-barat" "iphone"
     And click button filter 
     Then Admin should get result dashed filter page with description "Buku" "baru" "500ribuan" "jakarta-barat" "iphone"

    @dtb @delete-dashed-filter @dashed-filter
    Scenario: Admin delete one of page dashed filter
     
     When Admin login to dashboard
     And Admin go to dashed filter page
     Then Admin click delete button dashed filter
     Then delete page dashed filter should success

  @dashed-filter-combine @dashed-filter
  Scenario Outline: When Admin create new page dashed filter use custom
  location,merk and price

    When Admin login to dashboard
    And Admin go to dashed filter page
    Then Admin click delete button dashed filter

    Then click dashed filter merk link
    Then Admin click add button
    Given merk url value <Merk_Url>
    Given merk name value <Merk_Name>
    When Admin click simpan dashed filter
    
    And Admin go to dashed filter page
    Then click dashed filter location link
    Then Admin click add button
    Given url location value <Loc_Url>
    Given province location <Province>
    Given city location value <City>
    When Admin click simpan dashed filter

    And Admin go to dashed filter page
    Then click dashed filter price link
    Then Admin click add button
    Given url price value <Price_Url>
    Given min price value <Min_P>
    Given max price value <Max_P>
    When Admin click simpan dashed filter
    
    And Admin go to dashed filter page
    Then Admin click add button
    Given category page value <Category>
    Given condition page option <Condition>
    Given price page value <Price_Url>
    Given location page value <Loc_Url>
    Given merk page value <Merk_Url>
    When Admin click simpan dashed filter page
    Then capture one of dashed filter page wording
    When Admin click detail dashed filter page
    Then bhlm combine should contain page wording

  Examples:
    |Category|Condition|Price_Url|Min_P |Max_P |Loc_Url|Province   |City           |Merk_Url|Merk_Name|
    |2       |2        |murah    |400000|500000|loc-url|DKI Jakarta|Jakarta Selatan|merk-url|Acer     |
    