Feature: Product

  User can upload product, edit product and delete product.

  @restock
  Scenario: User restock product on production
    In production we need to re-stock product in order to keep payment @smoke-test-desktop running without no stock product issue.

    When user edit product stock
    Then product stock limit will change

  @restock-staging
  Scenario: User restock product on staging
    In staging we need to re-stock product in order to keep our automation running without no stock product issue.

    Then user edit product stock in staging
