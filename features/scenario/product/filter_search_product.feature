Feature: DTB-Desktop Filter Search & Pelapak product

  User able to filter product after get search result
 
  Scenario Outline: User filter with params from search page (all params)

    When user visit filter from <Filter_Type> with <Filter_Param>
    Then sitebar filter new product should be checked
    And sitebar filter used product should be checked
    And sitebar filter free shipping coverage should contain "Bali"
    And sitebar filter province should contain "Banten"
    And sitebar filter city should contain "Lebak"
    And sitebar filter courier should contain "GO-JEK GO-SEND"
    And sitebar filter price max should contain "10000"
    And sitebar filter price min should contain "100000"
    And sitebar filter rating should from 0 until 5
    And sitebar filter todays deal should be checked
    And sitebar filter installment should be checked
    And sitebar filter wholesale should be checked
    And sitebar filter top seller should be checked
    And sitebar filter premium seller should be checked

  @smoke-test-desktop @dtb @filter-product
  Examples:
    |Filter_Type|Filter_Param                                                  |
    |Search     |,1,1,3,Banten,Lebak,GO-JEK+GO-SEND,10000,100000,5,0,1,1,1,1,1,| 
    |Pelapak1   |,1,1,3,Banten,Lebak,GO-JEK+GO-SEND,10000,100000,5,0,1,1,1,1,1,|

  Scenario Outline: User filter with params from search page (discount filter product)

    When user visit filter from <Filter_Type> with <Filter_Param>
    And sitebar filter todays deal should be checked
    And product discount badge should exist

  @smoke-test-desktop @dtb @filter-product
  Examples:
    |Filter_Type|Filter_Param                                                  |
    |Search     |,1,1,,,,,,,5,0,1,0,0,0,0, |



  Scenario Outline: User filter with params from search page (rate 5 and verify filter result)

    When user visit filter from <Filter_Type> with <Filter_Param>
    And sitebar filter todays deal should be checked
    And sitebar filter rating should 5
    And sitebar filter installment should be checked
    And sitebar filter wholesale should be checked
    And sitebar filter premium seller should be checked
    And verify product detail should contain search filter param


  @smoke-test-desktop @dtb @filter-product
  Examples:
    |Filter_Type|Filter_Param                                                                       |
    |Search     |Pingpong,1,0,7,DKI+Jakarta,Jakarta+Selatan,GO-JEK+GO-SEND,10,5000000,5,5,0,1,1,0,1,|
    |Pelapak2   |Pingpong,1,0,7,DKI+Jakarta,Jakarta+Selatan,GO-JEK+GO-SEND,10,5000000,5,5,0,1,1,0,1,|

  Scenario Outline: User filter with params from search page (verify order)

    When user visit filter from <Filter_Type> with <Filter_Param>
    And navbar order <Order_Option> should be selected
   
  @smoke-test-desktop @dtb @filter-product
  Examples:
    |Filter_Type|Filter_Param                                               |Order_Option   |
    |Search     |baju muslim,1,1,,,,,,,5,0,0,0,0,0,0,last_relist_at:desc    |Terbaru        |
    |Search     |baju muslim,1,1,,,,,,,5,0,0,0,0,0,0,price:asc              |Termurah       |
    |Search     |baju muslim,1,1,,,,,,,5,0,0,0,0,0,0,price:desc             |Termahal       |
    |Search     |baju muslim,1,1,,,,,,,5,0,0,0,0,0,0,_score:desc            |Relevansi      |
    |Search     |baju muslim,1,1,,,,,,,5,0,0,0,0,0,0,weekly_sales_ratio:desc|Terlaris       |
    |Search     |,1,1,,,,,,,5,0,1,0,0,0,0,percentage:desc                   |Diskon terbesar|
    |Pelapak1   |samsung,1,1,,,,,,,5,0,0,0,0,0,0,last_relist_at:desc        |Terbaru        |
    |Pelapak1   |samsung,1,1,,,,,,,5,0,0,0,0,0,0,price:asc                  |Termurah       |
    |Pelapak1   |samsung,1,1,,,,,,,5,0,0,0,0,0,0,price:desc                 |Termahal       |
    |Pelapak1   |samsung,1,1,,,,,,,5,0,0,0,0,0,0,_score:desc                |Relevansi      |
    |Pelapak1   |samsung,1,1,,,,,,,5,0,0,0,0,0,0,weekly_sales_ratio:desc    |Terlaris       |
