@dtb3 @desktop
Feature: DTB-Desktop Popular Product

  Admin should able to create, delete, edit or view popular product

  @popular-product
  Scenario: Admin create new popular product
    When Admin login to dashboard
    Then go to popular product dashboard
    When Admin create new popular product
    Then popular product is created

  @popular-product
  Scenario Outline: Admin edit popular product section
    When Admin login to dashboard
    Then go to popular product dashboard
    Then admin create campaign sample
    Given popular product name section <Popular_Section_Name>
    Then popular popular section name dashboard should be <Popular_Section_Name_Dashboard>
    And popular popular section name homepage should be <Popular_Section_Name_Homepage>

  Examples:
    |Popular_Section_Name|Popular_Section_Name_Dashboard|Popular_Section_Name_Homepage|
    |SectionName1        |SectionName1                  |SectionName1                 |

  @popular-product
  Scenario Outline: Admin login to see popular product list dashboard and comparing with the popular product list home page
    When Admin login to dashboard
    Then go to popular product dashboard
    Then Admin should see popular product list dashboard
    Given popular product name section <Popular_Section_Name>
    Then popular popular section name dashboard should be <Popular_Section_Name_Dashboard>
    And popular popular section name homepage should be <Popular_Section_Name_Homepage>
    And popular product list homepage should same

  Examples:
    |Popular_Section_Name     |Popular_Section_Name_Dashboard|Popular_Section_Name_Homepage|
    |SectionName1&SectionName2|SectionName1&SectionName2     |SectionName1&SectionName2    |

  @popular-product
  Scenario: Admin delete popular product
    When Admin login to dashboard
    Then go to popular product dashboard
    When Admin delete popular product
    Then popular product is deleted
