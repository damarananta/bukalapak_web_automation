Feature: DTB-Mobile Product Detail

Condition:Product discount, installment, premium seller, wholesale,
          free shipping, having average estimation time & subscriber

  1. General product information
  2. Detail product (information, spesification, & description info)
  3. Shipping estimation info
  4. Product feedback list
  5. Product Review list
  6. Feature Share product

    @login-product-detail @dtb4 @mobile 
    Scenario: View Product detail verify user should see,general product info, pelapak info,
            sosial media icon list, detail product info, shipping estimation info, feedback list, 
            & product review list

    Given User login to mweb bukalapak with usename "lamhot" password "lamhot"
    When User navigate to product detail "/p/fashion/women/watch/2-jual-jam-tangan-aigner"
    Then User should see basic information about product via mweb
      And Pelapak info via mweb
      And Product detail sosial media icon list via mweb
      And Product detail free shipping info via mweb
      And Information product detail via mweb
      And Specification product detail via mweb
      And Description product detail via mweb
      And Notes product detail via mweb
      And Product detail discount info via mweb
      And Seller reputation info via mweb
      And Product detail feedback list via mweb
      And Product detail rating & review list via mweb
    When User click Cek Ongkos Kirim link via mweb
      Then User should see shipping estimation info via mweb
    When User navigate to product detail "/p/hobi-hiburan/buku/1d-jual-buku-buku-tentang-sendok-aja-deh"
    Then User should see product detail wholesale info via mweb
      And Product detail installment info with price Rp.500.000 via mweb
      And Premium seller info via mweb
    When User navigate to product detail "/p/hp-elektronik/handphone-hp/1l-jual-jam-tangan-aigner-draft-2"
    Then User should see availability label "STOK HABIS" via mweb
    When User navigate to product detail "/p/hobi-hiburan/sport/basket/1-jual-bola-basket"
    Then User should see availability label "PELAPAK TIDAK AKTIF" via mweb

