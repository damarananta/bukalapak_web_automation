Feature: DTB-Desktop Product Detail

Condition:Product discount, installment, premium seller, wholesale,
          free shipping, having average estimation time & subscriber

  1. Product discount / related product list
  2. Frequently View Together (FVT) list
  3. General product information
  3. Detail product (information, spesification, & description info)
  4. Shipping estimation info
  5. Product feedback list
  6. Product Review list
  7. Feature Share product

 @product-detail @dtb @smoke-test-desktop
  Scenario:  View Product detail (login)
    Given User login to bukalapak with usename "lamhotjmsqatest5" password "123456"
    When User navigate to product detail "/p/fashion-anak/anak-laki-laki/celana-1432/53k8mh-jual-bat-minton-test-2"
    Then User should see 12 products in section list

  @product-detail @dtb  @smoke-test-desktop 
  Scenario: View Product detail (non-login)
    When User navigate to product detail "/p/fashion-anak/anak-laki-laki/celana-1432/53k8mh-jual-bat-minton-test-2"
    Then User should see 12 products in section list


  @login-product-detail @dtb5  @desktop 
  Scenario: View Product detail verify user should see,general product info, pelapak info,
            sosial media icon list, detail product info, shipping estimation info, feedback list, 
            & product review list

    Given User login to bukalapak with usename "lamhot" password "lamhot"
    When User navigate to product detail "/p/fashion/women/watch/2-jual-jam-tangan-aigner"
    Then User should see basic information about product
      And Pelapak info on the right sidebar
      And Product detail average shipping time info
      And Product detail free shipping info
      And Sosial media icon list on the right sidebar
      And Information product detail tab
      And Specification product detail tab
      And Description product detail tab
      And Notes product detail tab
      And Product detail discount info
      And Product detail rating info
      And Seller reputation info
      And Product detail installment info with price less than Rp.500.000
    When User click tab Estimasi Biaya Kirim product
    Then User should see shipping estimation info
    When User click tab Feedback product
    Then User should see product feedback list
    When User click tab Ulasan Barang
    Then User should see product review list
    When User navigate to product detail "/p/hobi-hiburan/buku/1d-jual-buku-buku-tentang-sendok-aja-deh"
    Then User should see product detail wholesale info
      And Product detail installment info with price Rp.500.000
      And Seller subscriber count info
      And Premium seller info
    When User navigate to product detail "/p/hp-elektronik/handphone-hp/1l-jual-jam-tangan-aigner-draft-2"
    Then User should see availability label "STOK HABIS"
    When User navigate to product detail "/p/hobi-hiburan/sport/basket/1-jual-bola-basket"
    Then User should see availability label "PELAPAK TIDAK AKTIF"
