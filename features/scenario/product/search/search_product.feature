@desktop
Feature: DTB-Desktop Search Product

  User able to search the product and get the right result.

  @dtb5 @product-search
  Scenario Outline: Search the Product
    When User type a product <Product>
    Then User should get the result <Result>
  
  Examples:
    | Product      | Result      |
    | Kemeja       | Kemeja      |
    | Kaos Oblong  | Kaos Oblong |
