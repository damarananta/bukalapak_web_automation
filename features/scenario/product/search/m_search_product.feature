@mobile
Feature: DTB-Mobile Search Product

  User able to search the product via mweb and get the right result.

  @dtb4 @product-search
  Scenario Outline: Search the Product via mweb
    Given keyword to search via mweb <Product>
    Then should get result via mweb <Result>
  
  Examples:
    |Product      |Result      |
    |Testing      |testing     |
