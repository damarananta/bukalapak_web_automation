@desktop @btp @force-bukadompet @flow-normal
Feature: BTP-Force-Bukadompet
  
  @first-checkout
  Scenario: User buy goods using bukadompet with activated tfa
    User buy goods by using bukadompet where the tfa is activated and he/she choosing force bukadompet to pay the transaction in the first checkout

      When user buy goods from seller
      And use "force_bukadompet" button
      Then the invoice's status should be "Dibayar"

  @second-checkout
  Scenario: User buy goods using bukadompet with activated tfa in payment method page
    User buy goods by using bukadompet where the tfa is activated and he/she choosing bukadompet in payment method page by clicking 'Pilih Metode Pembayaran lain'

      When user buy goods from seller
      And use "encourage_bukadompet" button
      Then the invoice's status should be "Dibayar"
