Feature: BTP-CIMB Click

  @smoke-test-desktop @btp
  Scenario: User create transaction using CIMB and canceled it
    Instead continue the payment process, user does cancellation and it should be back to Product Detail page without the transaction getting paid

    When user create transaction using CIMB click
    And user cancelled the transaction
    Then transaction will not get paid

  @smoke-test-mobile @btp
  Scenario: User create transaction in mobile web using CIMB and canceled it
    Instead continue the payment process, user does cancellation and it should be back to Product Detail page without the transaction getting paid

    When user create transaction in mobile web using CIMB click
    And user cancels the transaction
    Then transaction in mobile web will not get paid
