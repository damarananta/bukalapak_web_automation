Feature: BTP-Mandiri E-Cash

    @smoke-test-desktop @btp
    Scenario: User create transaction using Mandiri e-cash and canceled it
      Instead continue the payment process, user does cancellation and it should be back to Product Detail page without the transaction getting paid

      When user create transaction using Mandiri e-cash
      And user cancel the transaction
      Then transaction could not be paid

    @smoke-test-mobile @btp
    Scenario: User create transaction using Mandiri e-cash and canceled it (mobile web)
      Instead continue the payment process, user does cancellation and it should be back to Product Detail page without the transaction getting paid

      When user create transaction using Mandiri e-cash in mobile web
      And user cancel the transaction in mobile web
      Then transaction could not be paid in mobile web
