Feature: BTP-Payment via Indomaret

  @smoke-test-desktop @btp
  Scenario: User pay their order via Indomaret
    Invoice status should be in waiting until the order has paid via indomaret

    When user create transaction using indomaret as payment method
    Then invoice status should be in wating until the order has paid via indomaret

  @smoke-test-mobile @btp
  Scenario: User pay their order via Indomaret using web browser in mobile platform
    Invoice status should be in waiting until the order has paid via indomaret

    When user create transaction using indomaret as payment method in mobile browser
    Then invoice status should be in wating until there is a confirmation from indomaret
