Feature: BTP-BCA Klikpay

    @smoke-test-desktop @btp
    Scenario: User create transaction using BCA and canceled it
      Instead continue the payment process, user does cancellation and it should be back to Product Detail page without the transaction getting paid

      When user create transaction using Klik BCA
      And user go back
      Then transaction should not be paid

    @smoke-test-mobile @btp
    Scenario: User create transaction using BCA and canceled it
      Instead continue the payment process, user does cancellation and it should be back to Product Detail page without the transaction getting paid

      When user create transaction using BCA clickpay
      And user return back
      Then transaction should in waiting and not to be paid
