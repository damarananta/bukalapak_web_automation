Feature: BTP-Transfer Payment Method

  @smoke-test-desktop @btp
  Scenario: User create transaction via ATM transfer as payment method
    This payment must has matched unique code in confirmation page and invoice page
    The unique code obtained from the last 3 digits of the price

    When user create transaction via transfer
    Then invoice status should be in wating
    And unique code should match

  @smoke-test-mobile @btp
  Scenario: User create transaction via ATM transfer as payment method (mweb)
    This payment must has matched unique code in confirmation page and invoice page
    The unique code obtained from the last 3 digits of the price

    When user create mweb transaction via transfer
    Then invoice status should be waiting
    And unique code should be matching
