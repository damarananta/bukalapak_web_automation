Feature: BTP-Credit Card

  @smoke-test-desktop @btp
  Scenario: User using credit card for the transaction and cancel it
    Instead continue the payment process, user does cancellation and it should be back to Product Detail page without the transaction getting paid

      When user using credit card for the transaction
      But the data filled inappropriately
      Then transaction could not continue

  @smoke-test-desktop @btp
  Scenario: User using credit card but the data has not filled
    Instead continue the payment process, user do not fill the data and click the back button on the browser, it should be back to Product Detail page without the transaction getting paid

      When user using credit card as payment
      But the data has not filled
      Then the status in waiting

  @smoke-test-mobile @btp
  Scenario: User using credit card in mobile web for the transaction and cancel it
    Instead continue the payment process, user does cancellation and it should be back to Product Detail page without the transaction getting paid

      When user using credit card in mobile web for the transaction
      But the data filled in mobile web inappropriately
      Then transaction in mobile web could not continue
