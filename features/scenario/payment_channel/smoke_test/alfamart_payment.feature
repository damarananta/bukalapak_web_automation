Feature: BTP-Payment via Alfamart

  @smoke-test-desktop @btp
  Scenario: User pay their order via Alfamart
    Invoice status should be in waiting until the order has paid via alfamart

    When user create transaction using alfamart as payment method
    Then invoice status should be in wating until any confirmation from alfamart that has been paid

  @smoke-test-mobile @btp
  Scenario: User pay their order via Alfamart
    Invoice status should be in waiting until the order has paid via alfamart

    When user create transaction using alfamart
    Then invoice status should be in wating until any confirmation from alfamart
