Feature: BTP-Bukadompet Payment Method

  @smoke-test-desktop @btp
  Scenario: User create transaction using Bukadompet as payment method
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge). In this scenario user has enough deposit in bukadompet to pay the transaction.

    When user create transaction using bukadompet
    And user has deposit in bukadompet and enough to buy the goods
    Then invoice status should be paid
    And deposit should reduced by the goods price amount

  @smoke-test-desktop @btp
  Scenario: Not enough balance in Bukadompet
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the balance is not enough to pay so the transaction can not be continued

    When user create transaction via bukadompet
    And user has no enough deposit in bukadompet to continue to buy the goods
    Then the goods can not be paid

  @smoke-test-desktop @btp
  Scenario: User create transaction using Bukadompet with wrong password
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the password is wrong so the transaction can not be continued

    When user create transaction using bukadompet with wrong password
    And user has enough deposit in bukadompet to continue to buy the goods
    But the filled password is wrong
    Then the transaction should not be continued

  @smoke-test-mobile @btp
  Scenario: User create transaction using Bukadompet in mobile web as payment method
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge). In this scenario user has enough deposit in bukadompet to pay the transaction.

    When user create transaction in mobile web using bukadompet
    And user has deposit in bukadompet and enough to buy the goods in mobile web
    Then invoice status in mobile web should be paid
    And deposit would reduced by the goods price amount

  @smoke-test-mobile @btp
  Scenario: Not enough balance in mweb Bukadompet
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the balance is not enough to pay so the transaction can not be continued

    When user create transaction in mweb via bukadompet
    And user has no enough deposit in mweb bukadompet to continue to buy the goods
    Then the goods in mweb can not be paid

  @smoke-test-mobile @btp
  Scenario: User create transaction in mweb using Bukadompet with wrong password
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the password is wrong so the transaction can not be continued

    When user create transaction in mweb using bukadompet with wrong password
    And user has enough deposit in bukadompet to continue to buy the goods in mweb
    But the filled password is wrong in mweb
    Then the transaction in mweb should not be continued
