Feature: PNL-Kredivo

  @smoke-test-desktop @pnl
  Scenario: User using Kredivo for payment with invalid login credential
    Instead of completing related payment process with a valid login credential (phone number and PIN), user will entering invalid login credential on Kredivo payment page

    Given user logged-in to BukaLapak desktop web
    Then user make a transaction
    When user chose Kredivo for payment method with "Within 30-days payment" option
    Then related payment option must be selected on Kredivo payment page
    When user entered invalid login credential (desktop web)
    Then error message will be appeared (desktop web)
    When user clicked "Cancel and Go Back to BukaLapak" link
    Then related invoice will be appeared with "Waiting for Payment" status
    And with "Kredivo" payment method

  @smoke-test-mobile @pnl
  Scenario: User using Kredivo for payment with invalid login credential
    Instead of completing related payment process with a valid login credential (phone number and PIN), user will entering invalid login credential on Kredivo payment page

    When user make a transaction on BukaLapak mobile web
    And chose Kredivo for payment method
    When user entered invalid login credential
    Then error message will be appeared
    When user tapped "Cancel and Go Back to BukaLapak" link
    Then related invoice will be appeared with "Waiting for Payment" on "Status" tab
    And with Kredivo payment guide
