Feature: BTP-Mandiri Klikpay

    @smoke-test-desktop @btp
    Scenario: User create transaction using Mandiri clickpay and canceled it
      Instead continue the payment process, user does cancellation and it should be back to Product Detail page without the transaction getting paid

      When user create transaction using Mandiri clickpay
      But user do not continue the transaction
      Then transaction should not getting paid