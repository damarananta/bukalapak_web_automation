@desktop
Feature: BTP-Mix-Payment

  @btp @mix-payment @atm
    Scenario: User buys goods using mixed payment Transfer+Bukadompet
      To use mixed payment method, User should has enough deposit in bukadompet to pay the goods in partially or whole of them. The Mixed-payment only can use bukadompet plus to the other payment exclude Kredivo and Credit Card. In this scenario, the mixed payment will be via transfer and bukadompet.

        When user buys the goods
        And user using mixed payment "atm" and bukadompet
        Then the invoice status should be in "Menunggu Pembayaran"
        And via transfer should has unique code

  @btp @mix-payment @alfamart
    Scenario: User buys goods using mixed payment Alfamart+Bukadompet
      To use mixed payment method, User should has enough deposit in bukadompet to pay the goods in partially or whole of them. The Mixed-payment only can use bukadompet plus to the other payment exclude Kredivo and Credit Card. In this scenario, the mixed payment will be via alfamart and bukadompet.

        When user buys the goods
        And user using mixed payment "alfamart" and bukadompet
        Then the invoice status should be in "Menunggu Pembayaran"

  @btp @mix-payment @indomaret
    Scenario: User buys goods using mixed payment Indomaret+Bukadompet
      To use mixed payment method, User should has enough deposit in bukadompet to pay the goods in partially or whole of them. The Mixed-payment only can use bukadompet plus to the other payment exclude Kredivo and Credit Card. In this scenario, the mixed payment will be via indomaret and bukadompet.

        When user buys the goods
        And user using mixed payment "indomaret" and bukadompet
        Then the invoice status should be in "Menunggu Pembayaran"

  @btp @mix-payment @bca
    Scenario: User buys goods using mixed payment BCA+Bukadompet
      To use mixed payment method, User should has enough deposit in bukadompet to pay the goods in partially or whole of them. The Mixed-payment only can use bukadompet plus to the other payment exclude Kredivo and Credit Card. In this scenario, the payment will be mixed between BCA and bukadompet.

        When user buys the goods
        And user using mixed payment "bca" and bukadompet
        Then the invoice status should be in "Menunggu Pembayaran"

  @btp @mix-payment @mandiri
    Scenario: User buys goods using mixed payment Mandiri+Bukadompet
      To use mixed payment method, User should has enough deposit in bukadompet to pay the goods in partially or whole of them. The Mixed-payment only can use bukadompet plus to the other payment exclude Kredivo and Credit Card. In this scenario, the payment will be mixed between Mandiri and bukadompet.

        When user buys the goods
        And user using mixed payment "mandiri" and bukadompet
        Then the invoice status should be in "Menunggu Pembayaran"
