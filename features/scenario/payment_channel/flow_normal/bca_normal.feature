@desktop @exclude
Feature: BTP-BCA Klikpay

    @flow-normal @btp @staging
    Scenario: User create transaction using BCA and canceled it
      Instead continue the payment process, user does cancellation and it should be back to Product Detail page without the transaction getting paid

      When user create transaction using Klik BCA at staging
      And user go back at staging
      Then transaction should not be paid at staging
