@desktop
Feature: BTP-Payment via Indomaret

  @flow-normal @btp @staging
  Scenario: User pay their order via Indomaret
    Invoice status should be in waiting until the order has paid via indomaret

    When user create transaction using indomaret as payment method at staging
    Then invoice status should be in wating until the order has paid via indomaret at staging
