@desktop @exclude
Feature: BTP-Credit Card
 
  @flow-normal @btp @staging
  Scenario: User using credit card for the transaction and cancel it
    Instead continue the payment process, user does cancellation and it should be back to Product Detail page without the transaction getting paid

      When user using credit card for the transaction at staging
      But the data filled inappropriately at staging
      Then transaction could not continue at staging
