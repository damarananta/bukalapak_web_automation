@desktop
Feature: BTP-Payment via Alfamart

  @flow-normal @btp @staging
  Scenario: User pay their order via Alfamart
    Invoice status should be in waiting until the order has paid via alfamart

    When user create transaction using alfamart as payment method at staging
    Then invoice status should be in wating until any confirmation from alfamart that has been paid at staging
