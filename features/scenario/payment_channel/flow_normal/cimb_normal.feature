@desktop @exclude
Feature: BTP-CIMB Click

  @flow-normal @btp @staging
  Scenario: User create transaction using CIMB and canceled it
    Instead continue the payment process, user does cancellation and it should be back to Product Detail page without the transaction getting paid

    When user create transaction using CIMB click at staging
    And user cancelled the transaction at staging
    Then transaction will not get paid at staging
