@desktop
Feature: BTP-Bukadompet Payment Method

  @flow-normal @btp @staging
  Scenario: User create transaction using Bukadompet as payment method
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge). In this scenario user has enough deposit in bukadompet to pay the transaction.

    When user create transaction using bukadompet at staging
    And user has deposit in bukadompet and enough to buy the goods at staging
    Then invoice status should be paid at staging
    And deposit should reduced by the goods price amount at staging
