@desktop
Feature: BTP-Transfer Payment Method

  @flow-normal @btp @staging
  Scenario: User create transaction via ATM transfer as payment method
    This payment must has matched unique code in confirmation page and invoice page
    The unique code obtained from the last 3 digits of the price

    When user create transaction via transfer at staging
    Then invoice status should be in wating at staging
    And unique code should match at staging
