@desktop
Feature: BTP-Mandiri Klikpay

    @flow-normal @btp @staging
    Scenario: User create transaction using Mandiri clickpay and canceled it
      Instead continue the payment process, user does cancellation and it should be back to Product Detail page without the transaction getting paid

      When user create transaction using Mandiri clickpay at staging
      But user do not continue the transaction at staging
      Then transaction should not getting paid at staging