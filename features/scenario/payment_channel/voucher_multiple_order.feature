@desktop @btp @multiple-order @invoice
Feature: BTP-Multiple Order
  
  @atm
  Scenario: User buy several goods using voucher with payment method ATM
    User buy several goods by using available voucher that provided. Total must be reduced after voucher is used and in this scenario user using ATM as the payment

      When user buy goods from different seller
      And use voucher
      And choose "atm" as the payment method
      Then the invoice state should be on "Menunggu Pembayaran"
      And the invoice should have unique code
      And the voucher should be applied on the invoice

  @bukadompet
  Scenario: User buy several goods using voucher with payment method BukaDompet
    User buy several goods by using available voucher that provided. Total must be reduced after voucher is used and in this scenario user using Bukadompet as the payment

      When user buy goods from different seller
      And use voucher
      And choose "bukadompet" as the payment method
      Then the invoice state should be on "Dibayar"
      And the voucher should be applied on the invoice

  @indomaret
  Scenario: User buy several goods using voucher with payment method Indomaret
    User buy several goods by using available voucher that provided. Total must be reduced after voucher is used and in this scenario user pay the orders via indomaret

      When user buy goods from different seller
      And use voucher
      And choose "indomaret" as the payment method
      Then the invoice state should be on "Menunggu Pembayaran"
      And the voucher should be applied on the invoice

  @alfamart
  Scenario: User buy several goods using voucher with payment method Alfamart
    User buy several goods by using available voucher that provided. Total must be reduced after voucher is used and in this scenario user pay the orders via alfamart

      When user buy goods from different seller
      And use voucher
      And choose "alfamart" as the payment method
      Then the invoice state should be on "Menunggu Pembayaran"
      And the voucher should be applied on the invoice
  @bca
  Scenario: User buy several goods using voucher with payment method BCA Klikpay
    User buy several goods by using available voucher that provided. Total must be reduced after voucher is used and in this scenario user pay the orders via bca, but the user cancel the transaction and should be back to Product Detail page without the transaction getting paid with discounted total

      When user buy goods from different seller
      And use voucher
      And choose "bca klikpay" as the payment method
      Then the invoice state should be on "Menunggu Pembayaran"
      And the voucher should be applied on the invoice

  @mandiri-clickpay
  Scenario: User buy several goods using voucher with payment method Mandiri Clickpay
    User buy several goods by using available voucher that provided. Total must be reduced after voucher is used and in this scenario user pay the orders via mandiri, but the user cancel the transaction and should be back to Product Detail page without the transaction getting paid with discounted total

      When user buy goods from different seller
      And use voucher
      And choose "mandiri clickpay" as the payment method
      Then the invoice state should be on "Menunggu Pembayaran"
      And the voucher should be applied on the invoice
