Feature: DTB-Mobile Cart

  @dtb @smoke-test-mobile @add-to-cart
  Scenario: Add to cart non login (smoke test)

    When User navigate to product detail "/p/fashion-anak/anak-laki-laki/celana-1432/53k8mh-jual-bat-minton-test-2"
    Then User click button buy via mweb
    #Then product should exist on basket via mweb

  @dtb @smoke-test-mobile @add-to-cart
  Scenario: Add to cart via mweb login (smoke test)

    Given User login to mweb bukalapak with usename "qatesting8" password "123456"
       And delete product from cart via mweb
    When User navigate to product detail "/p/fashion-anak/anak-laki-laki/celana-1432/53k8mh-jual-bat-minton-test-2"
    Then User click button buy via mweb
    Then product should exist on basket via mweb
       And delete product from cart via mweb
    Then cart should be empty via mweb

  @dtb4 @mobile @add-to-cart
  Scenario: Add to cart non login

    When User navigate to product detail "/p/hobi-hiburan/buku/14-jual-buku-testing-30"
    Then User click button buy via mweb
    Then product should exist on basket via mweb

  @dtb4 @mobile @add-to-cart
  Scenario: Add to cart via mweb login

    Given User login to mweb bukalapak with usename "lamhot" password "lamhot"
      And delete product from cart via mweb
    When User navigate to product detail "/p/hobi-hiburan/buku/14-jual-buku-testing-30"  
    Then User click button buy via mweb
    Then product should exist on basket via mweb
    And delete product from cart via mweb
    Then cart should be empty via mweb

  @dtb4 @mobile @pop-up-cart
  Scenario: Add to cart and verify pop up cart mweb
    Given User login to mweb bukalapak with usename "seller2" password "seller2"
      And delete product from cart via mweb
    When User navigate to product detail "/p/hobi-hiburan/buku/14-jual-buku-testing-30"
    Then User add product to cart via mweb
    When User click mini cart icon
    Then verify product should exist popup cart mweb
    When User click delete icon from popup cart mweb
    Then popup cart mweb should be empty
    Then cart should be empty via mweb
   
  @dtb4 @mobile @bulk-favorite
  Scenario: Bulk favorite & delete all carts

    Given User login to mweb bukalapak with usename "lamhot" password "lamhot"
    When User navigate to product detail "/p/hobi-hiburan/buku/14-jual-buku-testing-30"
    When User add product to cart via mweb
    Then User click Hapus dan Simpan ke Daftar Barang Favorit via mweb
    And product should exist on bookmarks via mweb
    Then cart should be empty via mweb

  @dtb4 @desktop @bulk-favorite
  Scenario: clear bulk favorite

    Given User login to bukalapak with usename "lamhot" password "lamhot"
    When User set all unfavorite
 