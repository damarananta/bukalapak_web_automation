Feature: DTB-Desktop Cart

  @dtb @smoke-test-desktop @add-to-cart
  Scenario: Add to cart non login (smoke test)
    When User navigate to product detail "/p/fashion-anak/anak-laki-laki/celana-1432/53k8mh-jual-bat-minton-test-2"
    When User click button buy

  @dtb @smoke-test-desktop @add-to-cart
  Scenario: Add to cart login (smoke test)
    Given User login to bukalapak with usename "qatesting8" password "123456"
      And User delete carts if exist
    When User navigate to product detail "/p/fashion-anak/anak-laki-laki/celana-1432/53k8mh-jual-bat-minton-test-2"
    When User click button buy
    Then product should exist on basket
    And delete product from cart
    Then User logout from bukalapak

  @dtb5 @desktop @add-to-cart
  Scenario: Add to cart non login
    When User navigate to product detail "/p/hobi-hiburan/buku/14-jual-buku-testing-30"
    When User add product to cart
    Then product should exist on basket

  @dtb5 @desktop @add-to-cart
  Scenario: Add to cart login
    Given User login to bukalapak with usename "lamhot" password "lamhot"
      And User delete carts if exist
    When User navigate to product detail "/p/hobi-hiburan/buku/14-jual-buku-testing-30"
    When User add product to cart
    Then product should exist on basket
    And delete product from cart
    Then User logout from bukalapak
    
  @dtb5 @desktop @pop-up-cart
  Scenario: Add to cart & delete from popup cart
    Given User login to bukalapak with usename "nadiaseptia42641" password "123456"
       And User delete carts if exist
    When User navigate to product detail "/p/hobi-hiburan/buku/14-jual-buku-testing-30"
    When User add product to cart
    Then verify pop-up cart shoud correct
    When User clicked on delete icon popup card
    Then User should see product was successfully deleted
    Then cart should be empty
    Then User logout from bukalapak

  @dtb5 @desktop @bulk-favorite
  Scenario: Bulk favorite & delete all carts

    Given User login to bukalapak with usename "lamhot" password "lamhot"
    When User navigate to product detail "/p/hobi-hiburan/buku/14-jual-buku-testing-30"
    When User add product to cart
    When User click Hapus dan Simpan ke Daftar Barang Favorit
    And product should exist on bookmarks
    When User set all unfavorite
    Then cart should be empty
    Then User logout from bukalapak
