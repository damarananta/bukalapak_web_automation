Feature: Courier JNE YES

  @courier-agent @exclude
  Scenario: User create transaction via ATM and using JNE YES for shipping
    User pay the transaction via ATM and choosing JNE YES as courier services. After choosing the courier, user would be directed to payment page

    When user choosing JNE YES for shipping
    Then the page should be directed to payment page
    And the shipping cost for JNE YES should be matched
