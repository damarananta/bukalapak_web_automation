Feature: Courier POS Kilat

  @courier-agent @exclude
  Scenario: User create transaction via ATM and using POS Kilat for shipping
    User pay the transaction via ATM and choosing POS Kilat as courier services. After choosing the courier, user would be directed to payment page

    When user choosing POS Kilat for shipping
    Then the page will be directed to payment page
    And the shipping cost for Pos Kilat should be matched
