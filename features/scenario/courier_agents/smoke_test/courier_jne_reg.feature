Feature: Courier JNE REG

  @courier-agent @exclude
  Scenario: User create transaction via ATM and using JNE REG for shipping
    User pay the transaction via ATM and choosing JNE REG as courier services. After choosing the courier, user would be directed to payment page

    When user choosing JNE REG for shipping
    Then the page would be directed to payment page
    And the shipment cost should be matched
