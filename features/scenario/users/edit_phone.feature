@ts @editusers @ts-4
Feature: TS-EditAccount

  User can edit account settings in untrusted device and get tfa
  
  @desktop @editphone
  Scenario: user can edit phone number
  Given there is user want edit his account 
  When user edit phone number
  Then user see pop up phone disclaimer

  @desktop @editpassword
  Scenario: user can edit password
  Given there is user want edit his account
  When user edit password
  Then user back to account settings page and see text Pengaturan

  @desktop @deactivated-tfa
  Scenario: User can deacticated tfa
  Given there is user want edit his account
  When user click deactivated button
  Then user get tfa
  
  @desktop @activated-tfa
  Scenario: User can acticated tfa
  Given there is user want edit his account
  When user click activated button
  Then user tfa user is activated
