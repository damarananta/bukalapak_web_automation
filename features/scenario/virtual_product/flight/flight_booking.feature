@exclude @vp5
Feature: VP-Flight booking
  User booking flight ticket from bukalapak.com

  @vp @flight @desktop
  Scenario: User booking oneway flight ticket 
    
    When user search "oneway" flight ticket via landingpage from "Jakarta" to "Denpasar"
    And user select the flight
    And user fill passanger data and continue to payment

