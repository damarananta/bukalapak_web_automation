@exclude @vp5
Feature: VP-Search flight landing page
  User search flight ticket from bukalapak.com

  @vp @flight @desktop
  Scenario Outline: User search oneway flight ticket via landing page
    
    When user search oneway flight ticket via landingpage from "<departure>" to "<arrived>"

    Examples:
      |departure |arrived  |
      |Padang    |Denpasar |

  @vp @flight @desktop
  Scenario: User search return flight ticket via landing page
    
    When user search "return" flight ticket via landingpage from "Jakarta" to "Denpasar"
    Then user get flight that has filtered by departure and arrive
    When user selected departure flight
    Then user get return flight
    When user selected return flight and continue the flight booking
