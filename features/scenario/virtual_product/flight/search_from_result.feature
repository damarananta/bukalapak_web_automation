@exclude @vp10
Feature: VP-Filter flight from search result page
  User can buy flight ticket from bukalapak.com

  Background:
    When user search oneway flight ticket via landingpage from "Padang" to "Denpasar"

  @vp @flight @desktop
  Scenario: search return flight from result page 
    And user search return flight at search result page

  @vp @flight @desktop
  Scenario: research from result page 
    And user research flight at search result page
    
  @vp @flight @desktop
  Scenario: filter by maskapai
    And user filter the flight based maskapai
    Then user get maskapai that has filtered by maskapai
    
  @vp @flight @desktop
  Scenario: filter by range price
    And user filter the flight based range price
    Then user get maskapai that has filtered by price
    
  @vp @flight @desktop
  Scenario: filter by time departured
    And user filter the flight based time departured
    Then user get maskapai that has filtered by time departured
    
  @vp @flight @desktop
  Scenario: filter by time arrived
    And user filter the flight based time arrived
    Then user get maskapai that has filtered by time arrived

