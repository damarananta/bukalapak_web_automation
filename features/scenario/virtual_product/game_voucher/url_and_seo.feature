@exclude @vp10 @vp @landingpage @desktop @gamevoucher @seo
Feature: VP-Landing Page (URL, Redirection, and SEO)

  Scenario Outline: Navigate through VP gamevoucher landing page
    When user navigates to VP "<section>" section via landing page
    Then user should be in VP "<section>" landing page

    Examples:
      | section       |
      | game voucher  |

  Scenario Outline: Visit new VP gamevoucher landing page URL directly
    When user visits VP "<link>" link
    Then user should be in VP "<section>" landing page

    Examples:
      | link                      | section       |
      | voucher-game              | game voucher  |

  Scenario Outline: Visit new VP gamevoucher landing page URL via old URL redirect
    When user visits VP "<link>" link
    Then user should be in VP "<section>" landing page

    Examples:
      | link                     | section       |
      | vp/voucher/game          | game voucher  |

  Scenario Outline: Visit SEO part of VP gamevoucher landing page
    Given user is in VP landing page
    When user visits VP "<title>" "<section>" SEO page with link "<link>"
    And user visits old VP URL
    Then user should be redirected to VP "<section>" SEO "<link>" link

    Examples:
      | title                  | link                     | section       |
      | Battlenet              | battlenet                | game voucher  |
      | Facebook Game Card     | facebook-game-card       | game voucher  |
      | Garena                 | garena                   | game voucher  |
      | Gemscool               | gemscool                 | game voucher  |
      | Itunes Gift Card       | itunes                   | game voucher  |
      | LYTO                   | lyto                     | game voucher  |
      | Megaxus                | megaxus                  | game voucher  |
      | MOGPlay                | mogplay                  | game voucher  |
      | Playstation Network    | playstation              | game voucher  |
      | Steam                  | steam                    | game voucher  |
      | Travian                | travian                  | game voucher  |
      | WaveGame               | wavegame                 | game voucher  |
      | XBOX                   | xbox                     | game voucher  |
      | Zynga                  | zynga                    | game voucher  |
