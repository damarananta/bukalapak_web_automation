@vp9 @mobile @vp @gamevoucher
Feature: VP-Voucher game via mweb
  User can buy game voucher from bukalapak.com

  Scenario: User pay their game voucher order via Alfamart
    Invoice status should be in waiting until the order has paid via alfamart

    When user create game voucher transaction using "Alfamart" as payment method via mweb
    Then invoice status in mweb should be in wating until any confirmation from alfamart that has been paid
    And invoice number can be find at mweb list transaction

  Scenario: User pay their game voucher order via ATM
    Invoice status should be in waiting until the order has paid via transfer

    When user create game voucher transaction using "Transfer" as payment method via mweb
    Then game voucher invoice status in mweb should be in wating
    And invoice number can be find at mweb list transaction  

  Scenario: User create transaction game voucher using Bukadompet as payment method
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction

    When user create game voucher transaction using "BukaDompet" as payment method via mweb
    And user has deposit in bukadompet and enough to buy the game voucher via mweb
    Then mweb invoice status should be paid
    And invoice number can be find at mweb list transaction

  Scenario: Not enough balance in Bukadompet
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the balance is not enough to pay so the transaction can not be continued

    When user create game voucher transaction use bukadompet via mweb
    And user has no enough deposit in bukadompet to continue to buy the goods via mweb
    
  Scenario: User create transaction using Bukadompet with wrong password
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the password is wrong so the transaction can not be continued

    When user create game voucher transaction using bukadompet with wrong password via mweb
    And user has enough deposit in bukadompet to continue to buy the goods via mweb
    But the game voucher user in mweb fill wrong password
    

  Scenario: User pay their game voucher order via Indomaret
    Invoice status should be in waiting until the order has paid via Indomaret

    When user create game voucher transaction using "Indomaret" as payment method via mweb
    Then invoice status in mweb should be in wating until the order has paid via indomaret
    And invoice number can be find at mweb list transaction
    