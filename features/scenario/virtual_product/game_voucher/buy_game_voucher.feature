@vp @desktop @gamevoucher
Feature: VP-Voucher game
  User can voucher game from bukalapak.com

  @vp6 @landingpage @bukadompet
  Scenario Outline: Buy voucher game via BukaDompet
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction

    When user create game voucher ("<username>","<password>") transaction using "BukaDompet" as payment method
    Then E-voucher invoice status should be paid
    And invoice number can be find at list transaction

    Examples:
    |username    |password |ket                     |
    |fadhlitest1 |password |sufficient BukaDompet balance |

  @vp6 @landingpage @atm
  Scenario Outline: Buy voucher game via ATM

    When user create game voucher ("<username>","<password>") transaction using "Transfer" as payment method
    Then game voucher invoice status should be in wating
    And E-voucher unique code should match

    Examples:
    |username    |password |ket                     |
    |fadhlitest2 |password |                        |
    |fadhlitest5 |password |user without phonenumber|

  @vp5 @landingpage @bca-klikpay
  Scenario Outline: Buy voucher game via Klikpay BCA
    To use bukadompet payment, the user should has deposit in Klikpay BCA account
    and also should be enough to pay the total of transaction

    When user create game voucher ("<username>","<password>") transaction using "BCA KlikPay" as payment method
    Then invoice number can be find at list transaction

    Examples:
    |username    |password |ket                     |
    |fadhlitest2 |password |                        |

  @vp5 @landingpage @alfamart
  Scenario Outline: Buy voucher game via Alfamart
    Invoice status should be in waiting until the order has paid via alfamart

    When user create game voucher ("<username>","<password>") transaction using "Alfamart" as payment method
    Then E-voucher invoice status should be in wating until any confirmation from Alfamart that has been paid
    And invoice number can be find at list transaction

    Examples:
    |username    |password |ket                     |
    |fadhlitest2 |password |                        |
    |fadhlitest5 |password |user without phonenumber|

  @vp4 @landingpage @indomaret
  Scenario Outline: Buy voucher game via Indomaret
    Invoice status should be in waiting until the order has paid via Indomaret

    When user create game voucher ("<username>","<password>") transaction using "Indomaret" as payment method
    Then E-voucher invoice status should be in wating until the order has paid via Indomaret
    And invoice number can be find at list transaction

    Examples:
    |username    |password |ket                     |
    |fadhlitest2 |password |                        |
    |fadhlitest5 |password |user without phonenumber|


  @vp4 @landingpage @bukadompet @insufficient
  Scenario: Buy voucher game via BukaDompet but Not enough balance in Bukadompet
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the balance is not enough to pay so the transaction can not be continued

    When user create game voucher transaction using BukaDompet with insufficient deposit
    Then user cannot create game voucher transaction using BukaDompet

  @vp4 @landingpage @bukadompet
  Scenario: Buy voucher game via BukaDompet with wrong password
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the password is wrong so the transaction can not be continued

    When user create game voucher transaction using bukadompet with wrong password
    Then user will be displayed game voucher transaction message "Verifikasi Password Gagal"

