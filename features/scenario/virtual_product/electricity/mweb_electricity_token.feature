@vp8
Feature: VP-Electricity via mweb
  User can buy electricity from bukalapak.com

  @mobile @vp @electricity
  Scenario: User pay their electricity order via Alfamart
    Invoice status should be in waiting until the order has paid via alfamart

    When user create electricity transaction using alfamart as payment method via mweb
    Then invoice status in mweb should be in wating until any confirmation from alfamart that has been paid
    And invoice number can be find at mweb list transaction

  @mobile @vp @electricity
  Scenario: User pay their electricity order via ATM
    Invoice status should be in waiting until the order has paid via transfer

    When user create electricity transaction using transfer via mweb
    Then electricitys invoice status in mweb should be in wating
    And invoice number can be find at mweb list transaction  

  @mobile @vp @electricity
  Scenario: User create transaction electricity using Bukadompet as payment method
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction

    When user create electricitys transaction using bukadompet via mweb
    And user has deposit in bukadompet and enough to buy the electricity via mweb
    Then mweb invoice status should be paid
    And invoice number can be find at mweb list transaction

  @mobile @vp @electricity
  Scenario: Not enough balance in Bukadompet
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the balance is not enough to pay so the transaction can not be continued

    When user create electricity transaction use bukadompet via mweb
    And user has no enough deposit in bukadompet to continue to buy the goods via mweb
    
  @mobile @vp @electricity
  Scenario: User create transaction using Bukadompet with wrong password
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the password is wrong so the transaction can not be continued

    When user create electricity transaction using bukadompet with wrong password via mweb
    And user has enough deposit in bukadompet to continue to buy the goods via mweb
    But the electricity user in mweb fill wrong password
    
  @mobile @vp @electricity
  Scenario: User pay their electricity order via Indomaret
    Invoice status should be in waiting until the order has paid via Indomaret

    When user create electricity transaction using indomaret as payment method via mweb
    Then invoice status in mweb should be in wating until the order has paid via indomaret
    And invoice number can be find at mweb list transaction
