@exclude @vp10 @vp @landingpage @desktop @electricity @seo
Feature: VP-Electricity landing Page (URL, Redirection, and SEO)

  Scenario Outline: Navigate through VP electricity landing page
    When user navigates to VP "<section>" section via landing page
    Then user should be in VP "<section>" landing page

    Examples:
      | section       |
      | electricity   |

  Scenario Outline: Visit new VP landing page URL directly
    When user visits VP "<link>" link
    Then user should be in VP "<section>" landing page

    Examples:
      | link                      | section       |
      | listrik-pln/token-listrik | electricity   |
      
  Scenario Outline: Visit new VP electricity landing page URL via old URL redirect
    When user visits VP "<link>" link
    Then user should be in VP "<section>" landing page

    Examples:
      | link                     | section       |
      | vp/voucher/token-listrik | electricity   |
      | vp/voucher/listrik       | electricity   |
      
  Scenario Outline: Visit SEO part of VP electricity landing page
    Given user is in VP landing page
    When user visits VP "<title>" "<section>" SEO page with link "<link>"
    And user visits old VP URL
    Then user should be redirected to VP "<section>" SEO "<link>" link

    Examples:
      | title                  | link                     | section       |
      | Token Listrik Prabayar | token-listrik            | electricity   |
