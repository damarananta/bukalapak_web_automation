@vp @desktop @electricity
Feature: VP-Electricity Token
  User can buy electricity token from bukalapak.com

  @landingpage @alfamart @vp9
  Scenario: User pay their electricity order via Alfamart
    Invoice status should be in waiting until the order has paid via alfamart

    When user create electricity transaction using "Alfamart" as payment method
    Then E-voucher invoice status should be in wating until any confirmation from Alfamart that has been paid
    And invoice number can be find at list transaction

  @landingpage @atm @vp9
  Scenario: User create elecrticity transaction via ATM transfer

    When user create electricity transaction using "Transfer" as payment method
    Then electricity invoice status should be in wating
    And E-voucher unique code should match

  @landingpage @bukadompet @vp10
  Scenario: User create transaction electricity using Bukadompet as payment method
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction

    When user create electricity transaction using "BukaDompet" as payment method
    Then E-voucher invoice status should be paid
    And invoice number can be find at list transaction

  @landingpage @bukadompet @vp10 @insufficient
  Scenario: Not enough balance in Bukadompet
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the balance is not enough to pay so the transaction can not be continued

    When user create electricity transaction using BukaDompet with insufficient deposit
    Then user cannot create electricity transaction using BukaDompet

  @landingpage @bukadompet @vp3
  Scenario: User create transaction using Bukadompet with wrong password
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the password is wrong so the transaction can not be continued

    When user create electricity transaction using bukadompet with wrong password
    Then user will be displayed electricity transaction message "Verifikasi Password Gagal"

  @landingpage @indomaret @vp3
  Scenario: User pay their electricity order via Indomaret
    Invoice status should be in waiting until the order has paid via Indomaret

    When user create electricity transaction using "Indomaret" as payment method
    Then E-voucher invoice status should be in wating until the order has paid via Indomaret
    And invoice number can be find at list transaction

  @landingpage @bca-klikpay @vp3
  Scenario: User create transaction phone credit using Klikpay BCA as payment method
    To use bukadompet payment, the user should has deposit in Klikpay BCA account
    and also should be enough to pay the total of transaction

    When user create electricity transaction using "BCA KlikPay" as payment method
    Then invoice number can be find at list transaction
