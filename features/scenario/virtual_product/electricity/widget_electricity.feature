@vp3 @vp @desktop @electricity
Feature: VP-Electricity from widget
  User can buy electricity token from bukalapak.com

  @widget
  Scenario: User create electricity transaction via home widget use ATM

    When user create electricity transaction via home widget via "Transfer"
    Then the E-voucher invoice state should be on "Menunggu Pembayaran"
    And E-voucher unique code should match
    And invoice number can be find at list transaction

  @exclude @widget
  Scenario: User create electricity transaction via BukaDompet widget use ATM

    When user create electricity transaction via Bukadompet widget via "Transfer"
    Then the E-voucher invoice state should be on "Menunggu Pembayaran"
    And E-voucher unique code should match
    And invoice number can be find at list transaction

  @exclude @widget
  Scenario: User create electricity transaction via review widget use ATM

    When user create electricity transaction via review widget via "Transfer"
    Then the E-voucher invoice state should be on "Menunggu Pembayaran"
    And E-voucher unique code should match
    And invoice number can be find at list transaction
