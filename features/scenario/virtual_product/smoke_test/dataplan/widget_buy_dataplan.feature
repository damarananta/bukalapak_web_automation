@vp @dataplan
Feature: VP-Buy Dataplan from widget
  User can buy dataplan from bukalapak.com

  @exclude @smoke-test-desktop @widget
  Scenario: User create dataplan transaction via home widget use ATM 

    When user create dataplan transaction via home widget via "Transfer"
    Then the E-voucher invoice state should be on "Menunggu Pembayaran"
    And E-voucher unique code should match
    And invoice number can be find at list transaction

  
  @exclude @smoke-test-desktop @widget
  Scenario: User create dataplan transaction via BukaDompet widget use ATM 

    When user create dataplan transaction via Bukadompet widget via "Transfer"
    Then the E-voucher invoice state should be on "Menunggu Pembayaran"
    And E-voucher unique code should match
    And invoice number can be find at list transaction
  