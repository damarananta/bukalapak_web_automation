@vp @dataplan
Feature: VP-Buy Dataplan
  User can buy dataplan from bukalapak.com
  
  @smoke-test-desktop
  Scenario: User pay their data plan order via Alfamart
    Invoice status should be in waiting until the order has paid via alfamart

    When user create data plan transaction using "Alfamart" as payment method
    Then the E-voucher invoice state should be on "Menunggu Pembayaran"
    And invoice number can be find at list transaction

  @smoke-test-desktop
  Scenario: User pay their data plan order via Indomaret
    Invoice status should be in waiting until the order has paid via Indomaret

    When user create data plan transaction using "Indomaret" as payment method
    Then the E-voucher invoice state should be on "Menunggu Pembayaran"
    And invoice number can be find at list transaction

  @exclude @smoke-test-desktop
  Scenario: User create data plan transaction via ATM/Transfer 
    
    When user create data plan transaction using "Transfer" as payment method
    Then the E-voucher invoice state should be on "Menunggu Pembayaran"
    And unique code should match
    And invoice number can be find at list transaction

  @smoke-test-desktop
  Scenario: Not enough balance in Bukadompet
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction
    but in this case the balance is not enough to pay so the transaction can not be continued

    When user create data plan transaction via bukadompet
    And user has no enough deposit in bukadompet to continue to buy the E-voucher

  @exclude @smoke-test-desktop
  Scenario: User create transaction using Bukadompet with wrong password
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction
    but in this case the password is wrong so the transaction can not be continued

    When user create data plan transaction using bukadompet with wrong password
    And user has enough deposit in bukadompet to continue to buy the goods
    Then the transaction should not be continued 
  