@vp @electricity 
Feature: VP-Buy Electricity Token
  User can buy electricity token from bukalapak.com

  @smoke-test-desktop @landingpage @alfamart
  Scenario: User pay their electricity order via Alfamart
    Invoice status should be in waiting until the order has paid via alfamart

    When user create electricity transaction using "Alfamart" as payment method
    Then E-voucher invoice status should be in wating until any confirmation from Alfamart that has been paid
    And invoice number can be find at list transaction

  @exclude @smoke-test-desktop @landingpage @atm
  Scenario: User create elecrticity transaction via ATM transfer

    When user create electricity transaction using "Transfer" as payment method
    Then electricity invoice status should be in wating
    And E-voucher unique code should match

  @smoke-test-desktop  @landingpage @bukadompet @insufficient
  Scenario: Not enough balance in Bukadompet
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the balance is not enough to pay so the transaction can not be continued

    When user create electricity transaction using BukaDompet with insufficient deposit
    Then user cannot create electricity transaction using BukaDompet

  @exclude @smoke-test-desktop @landingpage @bukadompet
  Scenario: User create transaction using Bukadompet with wrong password
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the password is wrong so the transaction can not be continued

    When user create electricity transaction using bukadompet with wrong password
    Then user will be displayed electricity transaction message "Verifikasi Password Gagal"

  @smoke-test-desktop @landingpage @indomaret
  Scenario: User pay their electricity order via Indomaret
    Invoice status should be in waiting until the order has paid via Indomaret

    When user create electricity transaction using "Indomaret" as payment method
    Then E-voucher invoice status should be in wating until the order has paid via Indomaret
    And invoice number can be find at list transaction
