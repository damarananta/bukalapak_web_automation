@vp @phonecredit
Feature: VP-Buy Phone Credit
  User can buy phone credit from bukalapak.com

  @smoke-test-desktop
  Scenario: User pay their phone credit order via Alfamart
    Invoice status should be in waiting until the order has paid via alfamart

    When user create phone credit transaction using "Alfamart" as payment method
    Then E-voucher invoice status should be in wating until any confirmation from Alfamart that has been paid
    And invoice number can be find at list transaction

  @exclude @smoke-test-desktop
  Scenario: User create phone credit transaction via ATM

    When user create phone credit transaction using "Transfer" as payment method
    Then phone credit invoice status should be in wating
    And E-voucher unique code should match

  @smoke-test-desktop
  Scenario: Not enough balance in Bukadompet
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the balance is not enough to pay so the transaction can not be continued

    When user create phone credit transaction using BukaDompet with insufficient deposit
    And user has no enough deposit in bukadompet to continue to buy the phonecredit


  @exclude @smoke-test-desktop
  Scenario: User create transaction using Bukadompet with wrong password
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the password is wrong so the transaction can not be continued

    When user create phone credit transaction using bukadompet with wrong password
    Then user will be displayed phone credit transaction message "Verifikasi Password Gagal"

  @smoke-test-desktop
  Scenario: User pay their phone credit order via Indomaret
    Invoice status should be in waiting until the order has paid via Indomaret

    When user create phone credit transaction using "Indomaret" as payment method
    Then E-voucher invoice status should be in wating until the order has paid via Indomaret
    And invoice number can be find at list transaction

  @exclude @smoke-test-desktop @widget @atm
  Scenario: User create phone credit transaction via home widget use ATM

    When user create "Pulsa" transaction via home widget via "Transfer"
    Then phone credit invoice status should be in wating
    And unique code should match
    And invoice number can be find at list transaction

  @exclude @smoke-test-desktop @widget @atm
  Scenario: User create phone credit transaction via BukaDompet widget use ATM

    When user create "Pulsa" transaction via Bukadompet widget via "Transfer"
    Then phone credit invoice status should be in wating
    And unique code should match
    And invoice number can be find at list transaction
