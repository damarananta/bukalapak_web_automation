@vp @desktop @gamevoucher
Feature: VP-Buy Voucher game
  User can voucher game from bukalapak.com

  @smoke-test-desktop @landingpage @alfamart
  Scenario Outline: Buy voucher game via Alfamart
    Invoice status should be in waiting until the order has paid via alfamart

    When user create game voucher ("<username>","<password>") transaction using "Alfamart" as payment method
    Then E-voucher invoice status should be in wating until any confirmation from Alfamart that has been paid
    And invoice number can be find at list transaction

    Examples:
    |username    |password                 |ket                     |
    |fadhlitest2 |passwordpasswordpassword |                        |
    |fadhlitest7 |password                 |user without phonenumber|

  @exclude @smoke-test-desktop @landingpage @atm
  Scenario: Buy voucher game via ATM

    When user create game voucher transaction using "Transfer" as payment method
    Then game voucher invoice status should be in wating
    And E-voucher unique code should match

  @smoke-test-desktop @landingpage @bukadompet @insufficient
  Scenario: Buy voucher game via BukaDompet but Not enough balance in Bukadompet
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the balance is not enough to pay so the transaction can not be continued

    When user create game voucher transaction using BukaDompet with insufficient deposit
    Then user cannot create game voucher transaction using BukaDompet

  @exclude @smoke-test-desktop @landingpage @bukadompet
  Scenario: Buy voucher game via BukaDompet with wrong password
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the password is wrong so the transaction can not be continued

    When user create game voucher transaction using bukadompet with wrong password
    Then user will be displayed game voucher transaction message "Verifikasi Password Gagal"

  @smoke-test-desktop @landingpage @indomaret
  Scenario Outline: Buy voucher game via Indomaret
    Invoice status should be in waiting until the order has paid via Indomaret

    When user create game voucher ("<username>","<password>") transaction using "Indomaret" as payment method
    Then E-voucher invoice status should be in wating until the order has paid via Indomaret
    And invoice number can be find at list transaction

    Examples:
    |username    |password                 |ket                     |
    |fadhlitest2 |passwordpasswordpassword |                        |
    |fadhlitest7 |password                 |user without phonenumber|

