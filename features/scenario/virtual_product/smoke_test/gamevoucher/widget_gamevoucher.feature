@vp @gamevoucher
Feature: VP-Buy Voucher game from widget
  User can voucher game from bukalapak.com

  @exclude @smoke-test-desktop @widget @atm
  Scenario: User create game voucher transaction via home widget use ATM

    When user create "Voucher Game" transaction via home widget via "Transfer"
    Then E-voucher invoice status should be in wating
    And unique code should match
    And invoice number can be find at list transaction

  @exclude @smoke-test-desktop @widget @atm
  Scenario: User create game voucher transaction via BukaDompet widget use ATM

    When user create "Voucher Game" transaction via Bukadompet widget via "Transfer"
    Then E-voucher invoice status should be in wating
    And unique code should match
    And invoice number can be find at list transaction
