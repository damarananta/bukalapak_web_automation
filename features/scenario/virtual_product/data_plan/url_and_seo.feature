@exclude @vp10 @vp @landingpage @desktop @dataplan @seo
Feature: VP-Dataplan landing Page (URL, Redirection, and SEO)
  
  Scenario Outline: Navigate through VP dataplan landing page
    When user navigates to VP "<section>" section via landing page
    Then user should be in VP "<section>" landing page

    Examples:
      | section       |
      | data plan     |
  
  Scenario Outline: Visit new VP landing page URL directly
    When user visits VP "<link>" link
    Then user should be in VP "<section>" landing page

    Examples:
      | link                      | section       |
      | paket-data                | data plan     |
  
  Scenario Outline: Visit new VP landing page URL via old URL redirect
    When user visits VP "<link>" link
    Then user should be in VP "<section>" landing page

    Examples:
      | link                     | section       |
      | vp/voucher/data          | data plan     |
  
  Scenario Outline: Visit SEO part of VP landing page
    Given user is in VP landing page
    When user visits VP "<title>" "<section>" SEO page with link "<link>"
    And user visits old VP URL
    Then user should be redirected to VP "<section>" SEO "<link>" link

    Examples:
      | title                  | link                     | section       |
      | Paket Data Telkomsel   | paket-internet-telkomsel | data plan     |
      | Paket Data Indosat     | paket-internet-indosat   | data plan     |
      | Kuota Data XL          | paket-internet-xl        | data plan     |
      | Internet 3 (Tri)       | paket-internet-tri       | data plan     |
