@vp @dataplan @desktop 
Feature: VP-Dataplan from widget
  User can buy dataplan from bukalapak.com

  @vp2 @widget
  Scenario: User create dataplan transaction via home widget use ATM 

    When user create dataplan transaction via home widget via "Transfer"
    Then the E-voucher invoice state should be on "Menunggu Pembayaran"
    And E-voucher unique code should match
    And invoice number can be find at list transaction

  @vp2 @exclude @widget
  Scenario: User create dataplan transaction via BukaDompet widget use ATM 

    When user create dataplan transaction via Bukadompet widget via "Transfer"
    Then the E-voucher invoice state should be on "Menunggu Pembayaran"
    And E-voucher unique code should match
    And invoice number can be find at list transaction
  
  @vp2 @exclude  @widget
  Scenario: User create dataplan transaction via review widget use ATM 

    When user create dataplan transaction via review widget via "Transfer"
    Then the E-voucher invoice state should be on "Menunggu Pembayaran"
    And E-voucher unique code should match
    And invoice number can be find at list transaction    
