@mobile @vp @dataplan
Feature: VP-Dataplan via mweb
  User can buy dataplan from bukalapak.com

  @vp7
  Scenario: User pay their dataplan order via Alfamart
    Invoice status should be in waiting until the order has paid via alfamart

    When user create dataplan transaction using alfamart as payment method via mweb
    Then invoice status in mweb should be in wating until any confirmation from alfamart that has been paid
    And invoice number can be find at mweb list transaction
  
  @vp7
  Scenario: User pay their dataplan order via ATM
    Invoice status should be in waiting until the order has paid via transfer

    When user create dataplan transaction using transfer via mweb
    Then dataplan invoice status in mweb should be in wating
    And invoice number can be find at mweb list transaction  
  
  @vp7
  Scenario: User create transaction dataplan using Bukadompet as payment method
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction

    When user create dataplan transaction using bukadompet via mweb
    And user has deposit in bukadompet and enough to buy the dataplan via mweb
    Then mweb invoice status should be paid
    And invoice number can be find at mweb list transaction
  
  @vp7
  Scenario: Not enough balance in Bukadompet
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the balance is not enough to pay so the transaction can not be continued

    When user create dataplan transaction use bukadompet via mweb
    And user has no enough deposit in bukadompet to continue to buy the dataplan via mweb
    
  @vp7
  Scenario: User create transaction using Bukadompet with wrong password
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the password is wrong so the transaction can not be continued

    When user create dataplan transaction using bukadompet with wrong password via mweb
    And user has enough deposit in bukadompet to continue to buy the goods via mweb
    But the mweb user fill wrong password
    
  @vp7
  Scenario: User pay their dataplan order via Indomaret
    Invoice status should be in waiting until the order has paid via Indomaret

    When user create dataplan transaction using indomaret as payment method via mweb
    Then invoice status in mweb should be in wating until the order has paid via indomaret
    And invoice number can be find at mweb list transaction