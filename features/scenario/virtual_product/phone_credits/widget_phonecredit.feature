@vp1 @vp @phonecredit @desktop
Feature: VP-Phone Credit widget
  User can buy phone credit from bukalapak.com

  @widget @atm
  Scenario: User create phone credit transaction via home widget use ATM

    When user create phonecredit transaction via home widget via "Transfer"
    Then phone credit invoice status should be in wating
    And E-voucher unique code should match
    And invoice number can be find at list transaction

  @exclude @widget @atm
  Scenario: User create phone credit transaction via BukaDompet widget use ATM

    When user create phonecredit transaction via Bukadompet widget via "Transfer"
    Then phone credit invoice status should be in wating
    And E-voucher unique code should match
    And invoice number can be find at list transaction

  @exclude @widget @atm
  Scenario: User create phone credit transaction via review widget use ATM

    When user create phonecredit transaction via review widget via "Transfer"
    Then phone credit invoice status should be in wating
    And E-voucher unique code should match
    And invoice number can be find at list transaction
