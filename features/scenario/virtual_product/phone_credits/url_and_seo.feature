@exclude @vp10 @vp @landingpage @desktop @phonecredit @seo
Feature: VP-Phonecredit landing Page (URL, Redirection, and SEO)

  Scenario Outline: Navigate through VP phonecredit landing page
    When user navigates to VP "<section>" section via landing page
    Then user should be in VP "<section>" landing page

    Examples:
      | section       |
      | phone credits |

  Scenario Outline: Visit new VP phonecredit landing page URL directly
    When user visits VP "<link>" link
    Then user should be in VP "<section>" landing page

    Examples:
      | link                      | section       |
      | pulsa                     | phone credits |

  Scenario Outline: Visit new phonecredit VP landing page URL via old URL redirect
    When user visits VP "<link>" link
    Then user should be in VP "<section>" landing page

    Examples:
      | link                     | section       |
      | vp/voucher/pulsa         | phone credits |
      
  Scenario Outline: Visit SEO part of VP phonecredit landing page
    Given user is in VP landing page
    When user visits VP "<title>" "<section>" SEO page with link "<link>"
    And user visits old VP URL
    Then user should be redirected to VP "<section>" SEO "<link>" link

    Examples:
      | title                  | link                     | section       |
      | simPATI                | telkomsel-simpati        | phone credits |
      | Kartu AS               | telkomsel-kartu-as       | phone credits |
      | Pulsa XL               | xl                       | phone credits |
      | IM3 Ooredoo            | indosat-im3-ooredoo      | phone credits |
      | Mentari Ooredoo        | indosat-mentari-ooredoo  | phone credits |
      | Pulsa Axis             | axis                     | phone credits |
      | Pulsa Smartfren        | smartfren                | phone credits |
      | Pulsa Tri              | tri                      | phone credits |
      | Pulsa Bolt             | bolt                     | phone credits |
