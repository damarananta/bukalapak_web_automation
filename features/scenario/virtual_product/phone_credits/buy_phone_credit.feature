@vp1 @vp @phonecredit @desktop
Feature: VP-Phone Credit
  User can buy phone credit from bukalapak.com

  @landingpage @alfamart
  Scenario: User pay their phone credit order via Alfamart
    Invoice status should be in waiting until the order has paid via alfamart

    When user create phone credit transaction using "Alfamart" as payment method
    Then E-voucher invoice status should be in wating until any confirmation from Alfamart that has been paid
    And invoice number can be find at list transaction

  @landingpage @atm
  Scenario: User create phone credit transaction via ATM

    When user create phone credit transaction using "Transfer" as payment method
    Then phone credit invoice status should be in wating
    And E-voucher unique code should match

  @landingpage @bukadompet
  Scenario: User create transaction phone credit using Bukadompet as payment method
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction

    When user create phone credit transaction using "BukaDompet" as payment method
    Then E-voucher invoice status should be paid
    And invoice number can be find at list transaction

  @landingpage @bukadompet @insufficient
  Scenario: Not enough balance in Bukadompet
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the balance is not enough to pay so the transaction can not be continued

    When user create phone credit transaction using BukaDompet with insufficient deposit
    And user has no enough deposit in bukadompet to continue to buy the phonecredit

  @landingpage @bukadompet
  Scenario: User create transaction using Bukadompet with wrong password
    To use bukadompet payment, the user should has deposit in bukadompet
    and also should be enough to pay the total of transaction (included delivery charge)
    but in this case the password is wrong so the transaction can not be continued

    When user create phone credit transaction using bukadompet with wrong password
    Then user will be displayed phone credit transaction message "Verifikasi Password Gagal"

  @landingpage @indomaret
  Scenario: User pay their phone credit order via Indomaret
    Invoice status should be in waiting until the order has paid via Indomaret

    When user create phone credit transaction using Indomaret as payment method
    Then E-voucher invoice status should be in wating until the order has paid via Indomaret
    And invoice number can be find at list transaction

  @landingpage @bca-klikpay
  Scenario: User create transaction phone credit using Klikpay BCA as payment method
    To use Klikpay BCA payment, the user should has deposit in Klikpay BCA account
    and also should be enough to pay the total of transaction

    When user create phone credit transaction using BCA KlikPay (KlikBCA Individu) as payment method
    Then E-voucher invoice status should be paid
    And invoice number can be find at list transaction
