@rev @push-product @buy-push-package
Feature: REV-Buy Push Package
  User should be able to buy push package

  @desktop @rev-03
  Scenario Outline: User buy a push package with BukaDompet on dweb
    Given user as "SELLER2"
    When user login on dweb
    And user go to Beli Paket Push page on dweb
    And user click Pilih on a package with "<total_push>" push on dweb
    And user click Beli Paket Push on dweb
    Then BukaDompet balance should be substracted, push balance should be added, and expired date should be updated on dweb

    Examples:
    | total_push |
    | 50         |
    | 100        |
    | 250        |
    | 500        |

  @mobile @rev-03
  Scenario Outline: User buy a push package with BukaDompet on dweb
    Given user as "SELLER2"
    When user login on mweb
    Given user push balance on mweb
    And user go to Beli Paket Push page on mweb
    And user click Pilih on a package with "<total_push>" push on mweb
    And user click Beli Paket Push on mweb
    Then BukaDompet balance should be substracted, push balance should be added, and expired date should be updated on mweb

    Examples:
    | total_push |
    | 50         |
    | 100        |
    | 250        |
    | 500        |
