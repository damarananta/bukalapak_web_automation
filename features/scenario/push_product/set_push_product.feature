@rev @push-product
Feature: REV-Set Push Product
  User should be able to set push on product

  @desktop @set-push-product @rev-03
  Scenario: User set push on a product dweb
    Given user as "SELLER2"
    And user login
    And user push balance on dweb
    When user "SELLER2" set push on "PRODUCT1" on dweb
    Then push balance should be subtracted on dweb

  @mobile @set-push-product @rev-03
  Scenario: User set push on a product mweb
    Given user as "SELLER2"
    When user login from mobile home page
    Given user push balance on mweb
    When user "SELLER2" set push on "PRODUCT1" on mweb
    Then push balance should be subtracted on mweb

  @desktop @set-push-products @rev-03
  Scenario: User set push on more than one product
    Given user as "SELLER2"
    When user login
    Given user push balance on dweb
    When user set push on all products on dweb
    Then push balance should be subtracted on dweb

  @desktop @set-push-products @rev-03
  Scenario: User set push on more than one product
  Case    : Push with insufficient active non-zero push balance
    Given user as "SELLER3"
    And user granted with 3 push balance active for 10 days on dweb
    When user login
    Given user push balance on dweb
    And user BukaDompet balance on dweb
    When user set push on all products on dweb
    Then push balance should be subtracted on dweb
    And BukaDompet balance should be subtracted on dweb

  @desktop @set-push-products @rev-03
  Scenario Outline: User set push on more than one product
  Case            : Push with push balance on grace period
                    Push with inactive push balance
    Given user as "<user>"
    When user login
    Given user push balance on dweb
    And user BukaDompet balance on dweb
    When user set push on all products with inactive push on dweb
    Then push balance should not be subtracted on dweb
    And BukaDompet balance should be subtracted on dweb

    Examples:
    | user    |
    | SELLER4 | #user with push balance on grace period
    | SELLER5 | #user with expired push balance

  @desktop @set-push-products @rev-03
  Scenario Outline: User set push on more than one product with overlimit loan
  Case            : Push with push balance on grace period
                    Push with inactive push balance
    Given user as "<user>"
    When user login
    Given user push balance on dweb
    When user set push on all products with inactive push on dweb
    Then push failed and pop-up will appear to suggest top-up
    And push balance should not be subtracted on dweb

    Examples:
    | user    |
    | SELLER7 | #user with push balance on grace period
    | SELLER8 | #user with expired push balance
