@ts @product_keyword @ts-9
Feature: TS-Product Keywords Suspected

  Admin manage product keyword suspected on admin page
  
  @desktop @activate_product_keyword
  Scenario: Admin can add product keyword
  Given there is admin user 
  When user add product keyword
  Then product keyword will appear on product keyword page
  
  @desktop @deactivate_product_keyword
  Scenario: Admin can add product keyword
  Given there is admin user 
  When user deactivate product keyword
  Then product keyword will appear on product keyword page
  