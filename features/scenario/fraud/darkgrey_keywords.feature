@ts @darkgreykeyword @ts-2
Feature: TS-Darkgreykeywords

  Admin manage darkgrey keyword for products on admin page
  
  @desktop 
  Scenario: Admin can add darkgrey keyword
  Given there is admin user 
  When user add darkgrey keyword
  Then darkgrey keyword will appear on darkgrey keyword page
  
  @desktop
  Scenario: Admin can delete darkgrey keyword for products on admin page
  Given there is admin user
  When user delete darkgrey keyword
  Then darkgrey keyword will not appear on darkgrey keyword page
