@ts @blacklistkeyword @ts-1
Feature: TS-Blacklistkeywords

  Admin manage blacklist keyword for products on admin page
  
  @desktop
  Scenario: Admin can add blacklist keyword
  Given there is admin user 
  When user add blacklist keyword
  Then blacklist keyword will appear on blacklist keyword page
  
  @desktop
  Scenario: Admin can delete blacklist keyword
  Given there is admin user 
  When user delete blacklist keyword
  Then blacklist keyword will not appear on blacklist keyword page
  