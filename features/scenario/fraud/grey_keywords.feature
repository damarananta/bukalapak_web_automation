@ts @greykeywords @ts-6
Feature: TS-Greykeywords

  Admin manage grey keyword for products on admin page
  
  @desktop
  Scenario: Admin can add grey keyword
  Given there is admin user 
  When user add grey keyword
  Then grey keyword will appear on grey keyword page
  
  @desktop
  Scenario: Admin can delete grey keyword for products on admin page
  Given there is admin user
  When user delete grey keyword
  Then grey keyword will not appear on grey keyword page