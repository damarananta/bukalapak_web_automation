@dtb3 @desktop @trending
Feature: DTB-Dashboard Trending

  Admin should able to do crud action on trending keyword dashboard
  
  @back-to-trending
  Scenario: Go to create trending keyword page then click go back button.
    
    When Admin login to dashboard
    When Admin go to trending keyword dashboard
    Then Admin click New Trending Keyword button
    When click go back trending keyword button
    Then should go back to trending keyword dashboard

  @add-trending
  Scenario:Admin create new trending
    
    When Admin login to dashboard
    When Admin go to trending keyword dashboard
    Then Admin click New Trending Keyword button
    Given trending keyword value "Keyword Test"
    Given trending keyword url value "Url Test"
    When Admin click save trending keyword
    Then create trending keyword should success

  @back-to-trending
  Scenario: Go to update trending keyword page then click go back button.

    When Admin login to dashboard
    When Admin go to trending keyword dashboard
    Then Admin click edit button trending keyword
    When click go back trending keyword button
    Then should go back to trending keyword dashboard

  @edit-trending
  Scenario: Admin update trending

    When Admin login to dashboard
    When Admin go to trending keyword dashboard
    Then Admin click edit button trending keyword
    Given trending keyword value "Keyword Test Update"
    Given trending keyword url value "Url Test Update"
    When Admin click save trending keyword
    Then update trending keyword should success
  
   @delete-trending
   Scenario: Admin delete trending keyword

    When Admin login to dashboard
    When Admin go to trending keyword dashboard
    Then Admin click delete button trending keyword 
    Then delete trending keyword should success
