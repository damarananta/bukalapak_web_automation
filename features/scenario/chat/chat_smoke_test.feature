Feature: DTB-Desktop Chat
  Seller/buyer should able to create new message, get notification, and read message
  1. Sender create new message to other user
  2. Receiver should get notification
  3. Receiver should able to read message
  
  @chat @smoke-test-desktop @dtb
  Scenario: Users use desktop
    
    Given User login to bukalapak with usename "lamhotjmsqatest3" password "123456"
    When User create new message to "26095556"
    Given User login to bukalapak with usename "lamhotjmsqatest4" password "123456"
    Then Receiver should get notification
    When Receiver read message from "26095247"
    Then notification should reduce
