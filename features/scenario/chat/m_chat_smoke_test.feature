Feature: DTB-Mobile Web Chat
  Seller/buyer should able to create new message, get notification, and read message
    
  @chat @smoke-test-mobile @dtb
  Scenario: Users use mobile web
    
    Given User login to mweb bukalapak with usename "lamhotjmsqatest6" password "123456"
    When User create new message to "26095721" via mweb
    Given User login to mweb bukalapak with usename "lamhotjmsqatest7" password "123456"
    Then Receiver should get notification via mweb
    When Receiver read message from "26095655" via mweb
    Then notification should reduce via mweb
    