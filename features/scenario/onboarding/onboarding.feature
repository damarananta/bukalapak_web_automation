@rfr @onb
Feature: RFR-User Onboard
  User will get some onboarding feature to help user complete their information on bukalapak

  @desktop @onb-1
  Scenario: Phone Confirmation Popup and apps Download Reminder
    User will get phone confirmation popup and apps download reminder after do registration

    Given user have valid E-Mail Address
    When user register on bukalapak
    Then user will get phone number confirmation popup
    And welcome popup

  @desktop @confirm-email @onb-1
  Scenario: User confirm email
    Given user have valid E-Mail Address
    When user register on bukalapak
    And user confirmed email
    Then the onboarding checklist on "email" will be checked

  @desktop @save-address @onb-1
  Scenario: User save address
    Given user have valid E-Mail Address
    When user register on bukalapak
    And user save address
    Then the onboarding checklist on "address" will be checked

  @desktop @save-phone-number @onb-1
  Scenario: User save phone-number
    Given user have valid E-Mail Address
    When user register on bukalapak
    And user save phone number
    Then the onboarding checklist on "phone" will be checked

  @desktop @topup @onb-1
  Scenario: User topup deposit Rp. 1.000.000
    Given user have valid E-Mail Address
    When user register on bukalapak
    And user confirmed email
    And user topup deposit "1000000"
    Then the onboarding checklist on "deposit" will be checked

  @desktop @shop @onb-1
  Scenario: User paid transaction
    Given user have valid E-Mail Address
    When user register on bukalapak
    And user save phone number
    And user save address
    And user confirmed email
    And user topup deposit "1000000"
    And user paid transaction
    Then the onboarding checklist on "shop" will be checked

  @mobile
  Scenario: Phone Confirmation Popup and apps Download Reminder
    User will get phone confirmation popup and apps download reminder after do registration

    #Given user have valid E-Mail Address
    #When user register on bukalapak via mobile web
    #Then user will get phone number confirmation popup via mobile web
    #And android reminder popup via mobile web
