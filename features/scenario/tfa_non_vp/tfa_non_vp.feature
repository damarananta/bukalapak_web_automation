@ts @tfa_non_vp @desktop @ts-7
Feature: TS- Transaction non VP and get TFA

  User get TFA in many condition when buy a product in BL using BukaDompet
  
  @tfa_buy_product
  Scenario: User buy product on untrusted device, address that never used before and get tfa

    Given there is transaction and buyer using bukadompet
    Then buyer see invoice page
  
  @nontfa_buy_product
  Scenario: User buy product on trusted device, address ever used before and not get tfa

    Given there is transaction with trusted device using bukadompet
    Then buyer see invoice page
  
  @nontfa_buy_product_ever-remit
  Scenario: User buy product on untrusted device, address ever used before until remit and not get tfa

    Given there is transaction with address ever used before until remit using bukadompet
    Then buyer see invoice page
