@rev @premium-account
Feature: REV-Subscribe Premium Account
  User should be able to subscribe to premium account

  @desktop @rev-02
  Scenario Outline: User subscribe to premium account
    Given user have valid E-Mail Address
    When user register on bukalapak
    And user confirmed email
    And user topup deposit "<topup_amount>"
    And user subscribe premium "<package_name>" for <period> with bukadompet on dweb
    Then user will go to premium dashboard

    Examples:
      | package_name | period | topup_amount |
      | Basic        | 30     | 1000000      |
      | Professional | 30     | 1000000      |
      | Professional | 365    | 3000000      |
