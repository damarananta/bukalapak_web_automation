@dgt @register
Feature: DGT-Register New User 
  As a new user of Bukalapak, user want to create a new account to be used for login into Bukalapak

@desktop
Scenario: User register a new account 
  Given user have valid E-Mail Address 
  When user register on bukalapak
  Then user can login with new account
  And user will see their name on the navbar

@mobile
Scenario: User register a new account
  Given user have valid E-Mail Address 
  When user register on bukalapak via mobile web
  Then user can login with new account via mobile web
  And user will see user icon on the navbar via mobile web
  