@dtb4 
Feature: DTB-Mobile Web Bookmark/Favorite
  User should able to favorite/unfavorite product, see list favorites(homepage & dashboard),
  set all unfavorite
  
  @favorite @mobile
  Scenario: User favorites, unfavorites,check homepage & dashboard bookmark from mobile web

    Given User login to mweb bukalapak with usename "lamhot2" password "lamhot2"
    When User navigate to product detail "/p/hobi-hiburan/buku/14-jual-buku-testing-30"
    Then User click favorite button via mweb
    When User navigate to product detail "/p/hobi-hiburan/buku/z-jual-buku-testing-25"
    Then User click favorite button via mweb
    Then favorite product count list mweb homepage should correct
    And favorite product count list mweb dashboard should correct
    When User navigate to product detail "/p/hobi-hiburan/buku/14-jual-buku-testing-30"
    Then User click unfavorite button via mweb
    Then favorite product mweb should decrease

  @favorite  @desktop
  Scenario: Clear mobile web smoke test favorite if exist

    Given User login to bukalapak with usename "lamhot2" password "lamhot2"
    When User set all unfavorite
