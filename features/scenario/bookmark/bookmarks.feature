@dtb5 @desktop
Feature: DTB-Desktop Bookmark/Favorite
  User should able to favorite/unfavorite product, see list favorites(homepage & dashboard),
  set all unfavorite
  
  @favorite
  Scenario: User favorites, unfavorites,check homepage & dashboard bookmark from desktop

    Given User login to bukalapak with usename "niken" password "niken"
    When User navigate to product detail "/p/hobi-hiburan/buku/14-jual-buku-testing-30"
    Then User click favorite button
    When User navigate to product detail "/p/hobi-hiburan/buku/z-jual-buku-testing-25"
    Then User click favorite button
    Then favorite product count list on homepage should correct
    And favorite product count list on dashboard should correct
    When User navigate to product detail "/p/hobi-hiburan/buku/14-jual-buku-testing-30"
    Then User click unfavorite button
    Then favorite product should decrease
    Then User logout from bukalapak

  @favorite
  Scenario: Set all unfavorite

    Given User login to bukalapak with usename "niken" password "niken"
    When User set all unfavorite
    Then favorite list on dashboard should be empty
