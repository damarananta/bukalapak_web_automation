@desktop @btp @agent-o2o
Feature: BTP-Agent-o2o
  
  @bukadompet
  Scenario: Agent buy goods using bukadompet
    Agent buy goods by using bukadompet where the price of the product should has administration cost

      When agent buy goods from seller
      And the product should has administration cost
      Then agent pay using "bukadompet"

  @atm
  Scenario: Agent buy goods using atm
    Agent buy goods by using atm where the price of the product should has administration cost

      When agent buy goods from seller
      And the product should has administration cost
      Then agent pay using "atm"

  @alfamart
  Scenario: Agent buy goods using alfamart
    Agent buy goods by using alfamart where the price of the product should has administration cost

      When agent buy goods from seller
      And the product should has administration cost
      Then agent pay using "alfamart"

  @indomaret
  Scenario: Agent buy goods using indomaret
    Agent buy goods by using indomaret where the price of the product should has administration cost

      When agent buy goods from seller
      And the product should has administration cost
      Then agent pay using "indomaret"
