@sg @mobile @sg-1
Feature: Single Upload mweb

  @upload
  Scenario: Single Upload from mobile web
    Given user have product to upload
    When user upload single product from mweb
    Then user will see their product from product detail on mweb
