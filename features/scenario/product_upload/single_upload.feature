@sg @desktop @sg-1
Feature: Single Upload from desktop

  @upload
  Scenario: Single Upload from desktop
    Given user have product to upload
    When user upload single product
    Then user will see their product from product detail
