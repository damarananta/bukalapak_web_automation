@desktop
Feature: TRXS-Remit

  Check whether remittance feature working proper as it is 

@trxs @remit
Scenario: Transaction successfully remitted 

Given there is seller 
And there is buyer 
And there is a transaction and the state is delivered 
Then buyer accept the product to be remitted 

