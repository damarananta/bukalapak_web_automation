@desktop
Feature: TRXS-Retur

  User who not satisfied with product that have been delivered can use retur feature to replace the product or get a refund.
  
  @trxs @retur @returfull 
  Scenario: User replace defect product with a new one
    If User get defect product buyer can ask seller for replacing the product with a new one.

    Given there is seller
    And there is buyer
    And there is a transaction and the state is delivered
    And buyer wants to complain
    And buyer wants to replace the product
    And admin approve
    When buyer deliver the product
    And seller accept the product
    And seller deliver the product
    And buyer accept the product
    And buyer allow the transaction to be remitted
    Then the money from transaction will be remmited to seller's bukaDompet
    
  @exclude @trxs @retur @returpartial
  Scenario: User replace defect product partially 

    User can request to replace or return a defects product partially within a transaction that have more than one product in single transaction

    Given there is seller
    And there is buyer
    And there is a transaction that have more than one product from one seller
    And buyer wants to complain
    And buyer wants to replace the product
    And admin approve
    When buyer deliver the product
    And seller accept the product
    And seller deliver the product
    And buyer accept the product
    And buyer allow the transaction to be remitted
    Then the money from selected items return transaction will be remmited to seller's bukaDompet
    