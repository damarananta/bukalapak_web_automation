@desktop
Feature: TRXS-Refund

  User who not satisfied with product that have been delivered can use retur feature to replace the product or get a refund.
  
  @trxs @refund @fullrefund
  Scenario: User can refund the transaction 

    If user want to refund the money so the money will be refunded to user instead or remitted to seller 

    Given there is seller 
    And there is buyer 
    And there is a transaction and the state is delivered
    And buyer wants to complain 
    And buyer wants to refund the transaction 
    And admin approve 
    When buyer deliver the product 
    And seller accept the product
    Then the money from transaction will be refunded to buyer's BukaDompet
    
  @exclude @trxs @refund @partialrefund 
  Scenario: User can refund the transaction partially 

    User can request to refund a transaction partially within a transaction that have more than one product in single transaction
    Given there is seller
    And there is buyer
    And there is a transaction that have more than one product from one seller
    And buyer wants to complain
    And buyer wants to refund the transaction 
    And admin approve
    When buyer deliver the product
    And seller accept the product
    Then the money from selected items return transaction will be refunded to buyer's bukaDompet
    
  @trxs @returntobl
  Scenario: Items return to Bukalapak

    If Seller didn't response buyer return request for maximum 5 days, the items should be sent to Bukalapak
    Given there is seller
    And there is buyer
    And there is a transaction and the state is delivered
    And buyer wants to complain
    And buyer wants to replace the product
    And admin change the return type to Return to Bukalapak because seller did not response return request 
    When buyer deliver the product
    Then the money from transaction will be refunded to buyer's BukaDompet