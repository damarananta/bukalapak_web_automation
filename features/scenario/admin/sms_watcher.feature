@desktop @notif @push-notification
Feature: NOTIF-SMS Watcher

  Admin can manage SMS Watcher

  @exclude
  Scenario: Admin can access the SMS Watcher page

    Given there is admin user
    When user login
    When user visit "/admin"
    And user click on "SMS Watcher"
    Then user should be in SMS Watcher admin page
