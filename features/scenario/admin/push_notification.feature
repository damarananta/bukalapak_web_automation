@desktop @notif @push-notification
Feature: NOTIF-Push Notification

  Marketing User can access push notification admin page

  @exclude
  Scenario: Marketing access the push notification admin page

    Given there is marketing user
    When user login
    When user visit "/admin"
    And user click on "Push Notifications"
    Then user should be in push notification admin page
