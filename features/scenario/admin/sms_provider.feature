@desktop @notif @push-notification
Feature: NOTIF-SMS Provider

  Admin can change SMS Provider

  @exclude
  Scenario: Admin can access the SMS Provider page

    Given there is admin user
    When user login
    When user visit "/admin"
    And user click on "SMS Provider"
    Then user should be in SMS Provider admin page

  @exclude
  Scenario: Admin can change the high priority

    Given there is admin user
    When user login
    When user visit "/admin"
    And user click on "SMS Provider"
    And user fill the high priority choice with "Route SMS"
    And user click on "Simpan"
    Then user should be in SMS Provider admin page
    Then user should see the high priority with "Route SMS"
  
  @exclude
  Scenario: Admin can change the medium priority

    Given there is admin user
    When user login
    When user visit "/admin"
    And user click on "SMS Provider"
    And user fill the medium priority choice with "Nexmo"
    And user click on "Simpan"
    Then user should be in SMS Provider admin page
    Then user should see the medium priority with "Nexmo"
  
  @exclude
  Scenario: Admin can change the low priority

    Given there is admin user
    When user login
    When user visit "/admin"
    And user click on "SMS Provider"
    And user fill the low priority choice with "Nexmo"
    And user click on "Simpan"
    Then user should be in SMS Provider admin page
    Then user should see the low priority with "Nexmo"
