@desktop @notif @newsletter
Feature: NOTIF-Newsletter

  Marketing User can access newsletter admin page

  @exclude
  Scenario: Marketing access the newsletter product admin page

    Given there is marketing user
    When user login
    When user visit "/admin"
    And user click on "Newsletter Management"
    Then user should be in Newsletter Product admin page
  @exclude
  Scenario: Marketing access the newsletter announcement admin page

    Given there is marketing user
    When user login
    When user visit "/admin"
    And user click on "Newsletter Management"
    And user click on "Announcement Newsletter"
    Then user should be in Newsletter Announcement admin page
