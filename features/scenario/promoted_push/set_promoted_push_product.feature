@rev @promoted-push @set-promoted-push
Feature: REV-Set Promoted Push
  User should be able to set promoted push of a product

  @desktop @rev-01
  Scenario Outline: User set promoted push of a product on dweb
    Given user as "SELLER"
    When user login
      And user go to list of Barang Dijual on dweb
      And click Promoted Push on a product with name "<product_name>" on dweb
      And fill cost per click with 210, active date with today, set status to active, click Simpan on dweb
    Then product will have promoted push active date info
    And click Promoted Push on a product with name "<product_name>" on dweb
      And click Nonaktif, click Simpan on dweb
    Then product will not have promoted push active date info

    Examples:
    | product_name                             |
    | Skiphop Duo Signature Diaper Bag Chevron |

  @mobile @rev-01
  Scenario Outline: User set promoted push of a product on mweb
    Given user as "SELLER"
    When user login from mobile home page
      And user go to list of Barang Dijual on mweb
      And user search for "<product_name>" on Barang Dijual on mweb
      And click Promoted Push on a product with name "<product_name>" on mweb
      And fill cost per click with 210, active date with today, set status to active, click Simpan on mweb
    Then success message active promoted push appear on mweb
    When user go to list of Barang Dijual on mweb
      And user search for "<product_name>" on Barang Dijual on mweb
      And click Promoted Push on a product with name "<product_name>" on mweb
      And click Nonaktif, click Simpan on mweb
    Then success message nonactive promoted push appear on mweb

    Examples:
      | product_name |
      | skiphop      |

  @desktop @rev-01 @exclude
  Scenario Outline: User set promoted push of a product via Promoted Push page on dweb
    Given user as "SELLER"
    When user login
      And user go to Promoted Push page on dweb
      And click Atur Promoted Push on a product with name "<product_name>" on dweb
      And fill cost per click with <cpc>, active date with today, click Aktif, click Simpan on dweb
    Then the product's promoted push status will be Aktif on dweb
    And click Atur Promoted Push on a product with name "<product_name>" on dweb
      And click Nonaktif, and click Simpan on dweb
    Then the product's promoted push status will be Tidak Aktif on dweb

    Examples:
    | product_name                             | cpc |
    | SkipHop Duo Signature Diaper Bag Chevron | 210 |

  @mobile @rev-01 @exclude
  Scenario Outline: User set promoted push of a product via Promoted Push page on mweb
    Given user as "SELLER"
    When user login from mobile home page
      And user go to Promoted Push page on mweb
      And click Atur Promoted Push on a product with name "<product_name>" on mweb
      And fill cost per click with <cpc>, active date with today, click Aktif, click Simpan on mweb
    Then the product's promoted push status will be Aktif on mweb
    And click Atur Promoted Push on a product with name "<product_name>" on mweb
      And click Nonaktif, and click Simpan on mweb
    Then the product's promoted push status will be Tidak Aktif on mweb

    Examples:
    | product_name          | cpc |
    | SkipHop Duo Signature | 210 |
