@rev @promoted-push @click-promoted-product
Feature: REV-Click Promoted Push Product
  User should be able to click on promoted products and cut the seller's budget 

  @desktop @rev-01
  Scenario: Unregistered user click on a promoted product on dweb
    Given user as "SELLER"
      And user login
      And user with a promoted product "Cooler Bag Salur" with cpc 200 
      And with promoted push budget on dweb
    When unregistered user search and click on the promoted product on dweb
    Then promoted push budget of the seller should be subtracted on dweb

  @mobile @rev-01
  Scenario: Unregistered user click on a promoted product on mweb
    Given user as "SELLER"
      And user login from mobile home page
      And user with a promoted product "Cooler Bag Salur" with cpc 200
      And with promoted push budget on mweb
    When unregistered user search and click on the promoted product on mweb
    Then promoted push budget of the seller should be subtracted on mweb
