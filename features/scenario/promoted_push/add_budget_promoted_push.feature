@rev @promoted-push @add-budget-promoted-push
Feature: REV-Add Budget Promoted Push
  User should be able to use add budget promoted push

  @desktop @rev-01
  Scenario: User add budget for promoted push on dweb
    Given a seller as user
    When user login on dweb
      And user go to Promoted Push on dweb
      And click "Tambah Budget" on dweb
      And fill budget 200 and click "Tambah" on dweb
    Then budget should add up with the previous budget on dweb

  @mobile @rev-01
  Scenario: User add budget for promoted push on mweb
    Given a seller as user
    When user login on mweb
      And user go to Promoted Push on mweb
      And click "Tambah Budget" on mweb
      And fill budget 200 and click "Tambah" on mweb
    Then budget should add up with the previous budget on mweb
