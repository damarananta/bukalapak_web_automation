@ts @tfa_on_vp @desktop @ts-5
Feature: TS-Get TFA When Buy Phone Credit With BukaDompet
  User can buy phone credit from bukalapak.com using BukaDompet and get TFA

  @tfa-bukadompet-vp
  Scenario Outline: User create transaction phone credit and paket data using Bukadompet as payment method
    
    Given user want buy virtual product using BukaDompet
    When user create "<virtual_product>" transaction using BukaDompet with number "<number>" 
    Then user get tfa on transaction page
    And user can see invoice number at list transaction

    Examples:
      | virtual_product | number       | 
      | Pulsa           | 081276128764 |
      | Paket Data      | 081276128764 |

  @tfa-bukadompet-token_listrik
  Scenario Outline: User create transaction token listrik using Bukadompet as payment method
    
    Given user want buy virtual product using BukaDompet
    When user create "<virtual_product>" transaction using BukaDompet with number customer "<number>" 
    Then user get tfa on transaction page
    And user can see invoice number at list transaction

    Examples:
      | virtual_product | number       | 
      | Token Listrik   | 01428800700  |
  
  
  @non-tfa-bukadompet-vp
  Scenario Outline: User create transaction paket data and pulsa using Bukadompet as payment method but has'nt received tfa because the device has been get tfa before
    
    Given user want buy virtual product using BukaDompet
    When user create "<virtual_product>" transaction using BukaDompet with trusted device and same "<number>"
    And user can see invoice number at list transaction

    Examples:
      | virtual_product | number       | 
      | Pulsa           | 081276128921 |
      | Paket Data      | 081276128921 |

  @non-tfa-bukadompet-token_listrik
  Scenario Outline: User create transaction token listrik using Bukadompet as payment method but has'nt received tfa because the device has been get tfa before
    
    Given user want buy virtual product using BukaDompet
    When user create "<virtual_product>" transaction using BukaDompet with trusted device and same customer "<number>"
    And user can see invoice number at list transaction

    Examples:
      | virtual_product | number       | 
      | Token Listrik   | 01428800700  |

  @non-tfa-bukadompet-vp-different
  Scenario Outline: User create transaction paket data and pulsa with different number using Bukadompet as payment method but has'nt received tfa because the device has been get tfa before
    
    Given user want buy virtual product using BukaDompet
    When user create "<virtual_product>" transaction using BukaDompet with trusted device and different number "<phone_number>"
    And user can see invoice number at list transaction

    Examples:
      | virtual_product | phone_number |
      | Pulsa           | 081288890012 |
      | Paket Data      | 081244445212 |
