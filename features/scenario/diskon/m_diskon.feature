@dtb4 @mobile @deal
Feature: DTB-Mobile Diskon

  @today-deal
  Scenario: Visit deal page

    When User navigate to diskon page via mweb
    Then User should see discount product listing via mweb
   
  @special-deal
  Scenario: Visit special deal page

    When User navigate to diskon page via mweb
      And click button diskon spesial via mweb
    Then User should see discount product listing via mweb
