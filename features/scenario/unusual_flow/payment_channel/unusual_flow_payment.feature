@desktop @unusual-flow
Feature: BTP-Unusual Flow

  @btp @unusual-flow @bukadompet
  Scenario: User buy goods using BukaDompet
    User buys item by using Bukadompet. In step 2 (choose payment method), the user does not pay directly, but he/she change their payment method to another, and back to step 1, changes their address/courier and afterwards he/she made payments 

      When user buy item
      And user first chooses "bukadompet" as the payment method
      And back to step 1
      And user changes their address or courier
      And user pay their transaction using "atm"
      Then the satus should be "Menunggu Pembayaran"
      Then the invoice should has unique code

  @btp @unusual-flow @atm
  Scenario: User buy goods using ATM
     User buys item via ATM. In step 2 (choose payment method), the user does not pay directly, but he/she change their payment method to another, and back to step 1, changes their address/courier and afterwards he/she made payments 

      When user buy item
      And user first chooses "atm" as the payment method
      And back to step 1
      And user changes their address or courier
      And user pay their transaction using "alfamart"
      Then the satus should be "Menunggu Pembayaran"

  @btp @unusual-flow @alfamart
  Scenario: User buy goods using Alfamart
     User buys item via Alfamart. In step 2 (choose payment method), the user does not pay directly, but he/she change their payment method to another, and back to step 1, changes their address/courier and afterwards he/she made payments 

      When user buy item
      And user first chooses "alfamart" as the payment method
      And back to step 1
      And user changes their address or courier
      And user pay their transaction using "indomaret"
      Then the satus should be "Menunggu Pembayaran"

  @btp @unusual-flow @indomaret
  Scenario: User buy goods using Indomaret
     User buys item via Indomaret. In step 2 (choose payment method), the user does not pay directly, but he/she change their payment method to another, and back to step 1, changes their address/courier and afterwards he/she made payments 

      When user buy item
      And user first chooses "indomaret" as the payment method
      And back to step 1
      And user changes their address or courier
      And user pay their transaction using "alfamart"
      Then the satus should be "Menunggu Pembayaran"

  @btp @unusual-flow @bca
  Scenario: User buy goods using BCA Clickpay
     User buys item via BCA Clickpay. In step 2 (choose payment method), the user does not pay directly, but he/she change their payment method to another, and back to step 1, changes their address/courier and afterwards he/she made payments 

      When user buy item
      And user first chooses "bca" as the payment method
      And back to step 1
      And user changes their address or courier
      And user pay their transaction using "bukadompet"
      Then the satus should be "Dibayar"

  @btp @unusual-flow @mandiri_clickpay
  Scenario: User buy goods using Mandiri Clickpay
     User buys item via Mandiri Clickpay. In step 2 (choose payment method), the user does not pay directly, but he/she change their payment method to another, and back to step 1, changes their address/courier and afterwards he/she made payments 

      When user buy item
      And user first chooses "mandiri_clickpay" as the payment method
      And back to step 1
      And user changes their address or courier
      And user pay their transaction using "bukadompet"
      Then the satus should be "Dibayar"

  @btp @unusual-flow @cimb
  Scenario: User buy goods using CIMB Clicks
     User buys item via CIMB Clicks. In step 2 (choose payment method), the user does not pay directly, but he/she change their payment method to another, and back to step 1, changes their address/courier and afterwards he/she made payments 

      When user buy item
      And user first chooses "cimb" as the payment method
      And back to step 1
      And user changes their address or courier
      And user pay their transaction using "mandiri_clickpay"
      Then the satus should be "Menunggu Pembayaran"

  @btp @unusual-flow @credit-card
  Scenario: User buy goods using Credit Card
     User buys item via Credit Card. In step 2 (choose payment method), the user does not pay directly, but he/she change their payment method to another, and back to step 1, changes their address/courier and afterwards he/she made payments 

      When user buy item
      And user first chooses "credit_card" as the payment method
      And back to step 1
      And user changes their address or courier
      And user pay their transaction using "bukadompet"
      Then the satus should be "Dibayar"
