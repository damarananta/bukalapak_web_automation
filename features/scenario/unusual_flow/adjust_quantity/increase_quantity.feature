@desktop @unusual-flow @increase-quantity
Feature: BTP-Increase-Quantity-Products

  @btp @bukadompet
  Scenario: User increase quantity of products in product detail and buy using BukaDompet
    User buys item by using Bukadompet. Before paying, buyer adjust the number of products in checkout page. The total price should be changed as the number of quantities.

      When user buy a product
      And user adjust the quantity
      And user choose "bukadompet"
      Then the satus at invoice will be "Dibayar"

  @btp @atm
  Scenario: User increase quantity of products in product detail and buy using ATM
    User buys item by using atm. Before paying, buyer adjust the number of products in checkout page. The total price should be changed as the number of quantities.

      When user buy a product
      And user adjust the quantity
      And user choose "atm"
      Then the satus at invoice will be "Menunggu Pembayaran"
      Then the invoice that used atm should has unique code

  @btp @alfamart
  Scenario: User increase quantity of products in product detail and buy using alfamart
    User buys item by using Alfamart. Before paying, buyer adjust the number of products in checkout page. The total price should be changed as the number of quantities.

      When user buy a product
      And user adjust the quantity
      And user choose "alfamart"
      Then the satus at invoice will be "Menunggu Pembayaran"

  @btp @indomaret
  Scenario: User increase quantity of products in product detail and buy using indomaret
    User buys item by using indomaret. Before paying, buyer adjust the number of products in checkout page. The total price should be changed as the number of quantities.

      When user buy a product
      And user adjust the quantity
      And user choose "indomaret"
      Then the satus at invoice will be "Menunggu Pembayaran"

  @btp @bca
  Scenario: User increase quantity of products in product detail and buy using bca
    User buys item by using bca. Before paying, buyer adjust the number of products in checkout page. The total price should be changed as the number of quantities.

      When user buy a product
      And user adjust the quantity
      And user choose "bca"
      Then the satus at invoice will be "Menunggu Pembayaran"

  @btp @mandiri
  Scenario: User increase quantity of products in product detail and buy using mandiri
    User buys item by using mandiri. Before paying, buyer adjust the number of products in checkout page. The total price should be changed as the number of quantities.

      When user buy a product
      And user adjust the quantity
      And user choose "mandiri"
      Then the satus at invoice will be "Menunggu Pembayaran"
