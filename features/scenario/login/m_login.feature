@mwc @login @mwc-1
Feature: MWC-Login

  @mobile @m-home-page
  Scenario: User login using username from mobile homepage
    Given there is user that have been registered
    When user login from mobile home page
    Then user will see user icon on the navbar via mobile home page
