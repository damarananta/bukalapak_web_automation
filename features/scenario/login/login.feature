@dgt @login
Feature: DGT-Login
  
  User who has registered with bukalapak account before can logged in with these methods:
  1. username
  2. email
  3. facebook
  4. google+
   
  @desktop @home-page
  Scenario: User login using username from homepage
    Given there is user that have been registered
    When user login
    Then user will see their name on the navbar

  @desktop @login-page
  Scenario: User login using username from login page
    Given there is user that have been registered
    When user login from login page
    Then user will see their name on the navbar
