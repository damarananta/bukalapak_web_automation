# bukalapak_web_automation
Software Automation Testing for bukalapak using cucumber

## How to Setup (Ubuntu 16.04)
   
1. Install Ruby 2.3.0 (pick one!)
  * using rbenv click [here](https://github.com/rbenv/rbenv#installation)
  * using rvm click [here](https://rvm.io/rvm/install)
  
2. Install Git
  ```
    sudo apt-get install git
  ```

3. Configure Git
  ```
    git config --global user.name "Your Name Here"
    git config --global user.email "your-github-email@example.com"
  ```

4. Generate Public Key
  ```
    ssh-keygen # then, just press enter
  ```

5. Add key to Github
  ```
    cat ~/.ssh/id_rsa.pub # copy the content to github
  ```

6. Clone the project
  ```
    git clone git@github.com:bukalapak/bukalapak_web_automation.git 
    cd bukalapak_web_automation
  ```
  
7. Copy .env file
  ```
    cp env.sample .env
  ```

8. Install [bundler](http://bundler.io/)
  ```
    gem install bundler
  ```
9. Run bundle install
  ```
    bundle install
  ```
  
  or if you like to keep it simple
  ```
    bundle
  ```
10. Installing Firefox ESR

  * Download firefox esr [here](https://www.mozilla.org/en-US/firefox/organizations/all/)
   
  * Extract downloaded file into /opt
     ```
       sudo tar -xvjf firefox-4.....tar.bz2 -C /opt
     ```
   
  * Back up your existing symbolic link to the default Firefox executable
     ```
       sudo mv /usr/bin/firefox /usr/bin/firefox-old
     ```
  
  * Create a symbolic link to the Firefox ESR executable:
     ```
       sudo ln -s /opt/firefox/firefox /usr/bin/firefox
     ```
  
11. Go back to bukalapak_web_automation folder and run cucumber to make sure everything is installed
   ```
      cucumber --tag @initiate
   ```
  
```  
When the browser open bukalapak homepage
Then You are ready to create automation script
```

## How to Run all test

```
cucumber --tag ~@smoke-test-desktop --tag ~@smoke-test-mobile
```
## Read This Before you start!
[Contribution Guideline] (https://github.com/bukalapak/bukalapak_web_automation/blob/master/Contribution-Guidline.md)
